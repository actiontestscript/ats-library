# ActionTestScript [![pipeline status](https://gitlab.com/actiontestscript/ats-core/badges/master/pipeline.svg)](https://gitlab.com/actiontestscript/ats-core/commits/master)
ActionTestScript is a structured and readable testing language used to create reliable and performant GUI automated tests campaigns.
Tests scripts are defined by a sequence of 'actions' executed on web or desktop application.
Scripts written in ATS are converted into java classes and then executed using Selenium and TestNG frameworks.

Only following browsers are available for the moment with ATS : *Chrome, Edge, Firefox and Opera*.

With ATS, tests designers are only focused on the functional actions to do and don't have to worry about technical considerations.

## Getting Started

Here is a simple example of ATS script :

```
channel-start -> myFirstChannel -> chrome
goto-url -> google.com
keyboard -> automated testing$key(ENTER) -> INPUT [id = lst-ib]
click -> A [text = Test automation - Wikipedia]
scroll -> 300
scroll -> 0 -> A [font-style = italic, text = Graphical user interface testing]
over -> IMG [src =~ .*Test_Automation_Interface.png]
channel-close -> myFirstChannel
```

This script start Chrome browser, navigate to url 'google.com', enter 'automated test' string into input text field and hit 'enter' key.
The script click on the wikipedia link to open the page and do some mouse over and scroll actions.
At the end of the script the started channel called 'myFirstChannel' is closed and terminate the browser process.

Pretty simple no ?

### Prerequisites

* You have to install a standard Java 21 JDK into the folder of your choice (OpenJDK will do the job but you can use Oracle standard distribution or GraalVM 21).
* Apache Maven or Ant to generate and compile ATS project.
* Install TestNG plugin on your favorite java IDE (for Eclipse : http://testng.org/doc/eclipse.html).
* Read TestNG documentation about suite management.
* For mobile testing you need to install 'MobileStation', this tool will set the mobile in 'test mode'
	- [Mobile Station for Windows](https://www.actiontestscript.org/releases/ats-mobile/windows/), you can set Android devices in test mode
	- [Mobile Station for MacOs](https://www.actiontestscript.org/releases/ats-mobile/macos/), you can set Android and IOS devices in test mode

### Automatic install

* You have to use or install a standard Java 21 (or more recent) JDK runtime to launch following command line :

```
cd [path-to-your-ats-project]
java AtsLauncher.java -ats-report=1 -suiteXmlFiles=[comma-separated-suites-to-launch-in-exec-folder]
```

This script will download and install mandatory tools from [actiontestscript.org](http://www.actiontestscript.org) website in order to launch ATS test suites

### Install

Download ATS components : https://actiontestscript.org/releases/

You can unzip archive into the folder of your choice, but if you do not install ATS on *[User-Home-Directory]/.actiontestscript* folder, you have to create an environment variable named **ATS_HOME** and set it's value to your ATS installation folder.

After installation of ATS components you can create ATS projects with the following folder structure.

```
ATS Project folder
	/libs/
	/src/
		/assets/
			/data/
			/lang/
			/resources/
		/exec/
		/main/
			/ats/
				/subscripts/
			/java/
	/target/
	/.atsProjectProperties
	/pom.xml
```

In the **src/main/ats** folder you can create ATS scripts using notepad with - *.ats* - extension.

## Sample ATS project

You can download an ATS test project : https://gitlab.com/actiontestscript/ats-test/-/archive/master/ats-test-master.zip

Unzip this folder and now you can edit *.atsProjectProperties* and *pom.xml* files according to your needs. 

You can launch test of the project using java command line, Ant target or Maven goal.


## Execute project

Generate and compile ATS project using Maven and execute - *compile* - goal.
* Java files generated from ats scripts will be created into the *target/generated* folder of the project.
* Compiled classes will be created classes into the *target/classes* folder of the project.

```
cd [path-to-your-ats-project]
mvn compile
```

You can now use TestNG suite files to define your testing campaigns, please visit [TestNG](http://testng.org/doc/) to see how use TestNG with your favorite Java IDE or to create test suite executions.

**Or**

You can generate and compile project using command line with java (see 'Installing' chapter for ATS distribution path)

* Generate java files

```
cd [path-to-your-ats-project]
java -cp [ats-distribution-path]\libs/*;libs/* com.ats.generator.Generator
```
Java files will be generated into project folder *target/generated* from scripts found in *src/main/ats* folder. 
Java files in *src/main/java* folder will be copied into the *target/generated* folder.

* Generate and compile project (for this command using JRE is not enough, you need to use a JDK to compile classes)

```
cd [path-to-your-ats-project]
java -cp [ats-distribution-path]\libs/*;libs/* com.ats.generator.Generator -comp
```
Generated and copied classes from *target/generated* folder will be compiled into *target/classes* folder and all files.
Contents of folder *src/assets/* will be copied into the *target/classes* folder.

Tests are ready to be launched with TestNG but you have to first create and define a TestNG suite xml file, in this file you can define groups, package or scripts you want to include or exclude from execution.
Here the command line to launch tests defined in 'suite.xml' file :

```
cd [path-to-your-ats-project]
java -cp [ats-distribution-path]\libs/*;libs/*;target/classes org.testng.TestNG src/exec/suite.xml
```

**Or**

You can generate, compile and execute project using command line with Java and Maven (see 'Installing' chapter for ATS distribution path)

* Execute Maven

```
cd [path-to-your-ats-project]
mvn clean test
```

The default TestNG suite in the pom.xml file will be executed, this suite is define with *'maven-surefire-plugin'* and *'suiteXmlFile'* property.

## ATS Features

With the ATS project, various additional features can be used:

* ### Report Concatenation:

  This feature allows you to concatenate multiple runtime metadata files generating single **xml** and **html** reports with information from all selected runtime files.

  #### Prerequisites
  The runs used to do the concatenation must each be running with a viable ATSV output file.

  #### command line
    ```
    java -cp [JavaClassPath] com.ats.tools.report.CampaignReportConcatenatorGenerator --output [output folder] --ats-report [0-3] --validation-report [0-1] --concat-report-folders [comma-separated-execution-folder-ouput-to-concatenate]
    ```

  #### options
  * JavaClassPath (**required**): Paths to Agilitest libraries and required libraries.
  * com.ats.tools.report.CampaignReportConcatenatorGenerator (**required**) : name of the class
    --output (**required**) : output folder, it must exist.
    --ats-report (**optional**) : specify the level of reporting of the developer
    (0 : disabled, 1 : standard, 2 : detailed report, 3 : detailed report with screenshots)
    --validation-report (**optional**) : activation parameter for the validation report
    (0 : disabled, 1 : enabled)
  * --concat-report-folders (**required**): comma-separated list of **absolute** execution folder paths.

  #### additional information:
  * This feature generates the **xml** and **html** reports in case they do not exist in the input folder.

  #### usage metrics:
    Usage metrics can be collected and exported to an OpenTelemetry endpoint.

    Metrics sent are `tests_count`, `tests_passed` and `tests_failed` from all executed tests in the suite.

    When the System environment variable `OTEL_ENDPOINT` is set, the metrics are automatically exported to this url.
    
    With the env var `OTEL_RESOURCES_ATTRIBUTES=<attribute1>=<value1>,<attribute2>=<value2>`, the metrics are tagged with specific attributes.

## Customize ATS on host machine

Each machine running ATS scripts has it's own performance or available resources to execute automated tests.
You can change configuration on the installed ATS distribution like wait action by browsers or tested application installation path.

Here is an example of global ATS configuration file (*.atsProperties* file in ATS root install folder)

```
<?xml version="1.0" encoding="UTF-8"?>
<execute>
	<proxy>
		<type>system</type>
	</proxy>
	<appBounding>
		<x>20</x>
		<y>20</y>
		<width>1300</width>
		<height>960</height>
	</appBounding>
	<maxTry>
		<searchElement>15</searchElement>
		<getProperty>10</getProperty>
		<webService>1</webService>	
		<interactable>20</interactable>		
	</maxTry>
	<timeOut>
		<script>60</script>
		<pageLoad>120</pageLoad>
		<watchDog>180</watchDog>
		<webService>20</webService>
	</timeOut>
	<wait>
		<enterText>0</enterText>
		<gotoUrl>0</gotoUrl>
		<mouseMove>0</mouseMove>
		<search>0</search>
		<switchWindow>0</switchWindow>
	</wait>
	<browsers>
		<browser>
			<name>chrome</name>
			<options>
				<option>--disable-web-security</option>
				<option>--incognito</option>
			</options>
		</browser>
		<browser>
			<name>msedge</name>
			<debugPort>998877</debugPort>
		</browser>
		<browser>
			<name>opera</name>
			<path>C:\Program Files\Opera\52.0.2871.40\opera.exe</path>
			<waitAction>150</waitAction>
		</browser>
		<browser>
			<name>firefox</name>
			<waitAction>200</waitAction>
			<waitProperty>70</waitProperty>
		</browser>
	</browsers>
	<applications>
		<application>
			<name>notepad++</name>
			<path>C:\Program Files (x86)\Notepad++\notepad++.exe</path>
		</application>
		<application>
			<name>filezilla</name>
			<path>C:\Program Files\FileZilla FTP Client\filezilla.exe</path>
		</application>
	</applications>
	<mobiles>
		<mobile>
			<name>settings</name>
			<endpoint>192.168.0.6:8080</endpoint>
			<package>settings</package>
			<waitAction>100</waitAction>
		</mobile>
	</mobiles>
	<apis>
		<api>
			<name>fakerest</name>
			<url>https://fakerestapi.azurewebsites.net</url>
		</api>
	</apis>
</execute>
```
* You can start browsers with screen max size (for the full screen mode use F11 key)
```
<appBounding>max</appBounding>
```
* You can define the proxy used by browsers during execution of the test.
Proxy types available are *'system'*, *'auto'*, *'direct'* and *'manual'*, if *'manual'* type is selected you have to define host and port of the proxy :
```
<proxy>
	<type>manual</type>
	<host>proxyhost</host>
	<port>8080</port>
</proxy>
```
* You can launch ATS executions on a remote machine with ATS components installed.
First start (manually or automatically) the windowsdriver or the linuxdriver on the remote machine, and then define the endpoint to connect to this driver :
```
<SystemDriver>
	<driver>http://192.168.1.1:9700</driver>
</SystemDriver>
```
* **[appBounding]** : initial window size of a tested application and it's initial position
* **[maxTry -> searchElement]** : action's maxTry to wait element exists or wait element is interactable before execute
* **[maxTry -> getProperty]** : action's maxTry get property to wait element's property exists and property is not null
* **[maxTry -> webService]** : api REST or SOAP webservice max try if request fail with timeout
* **[maxTry -> interactable]** : max try to interact with element (move, disabled, hidden etc..)
* **[timeOut -> script]** : javascript execution timeout (in sec.) for web application only
* **[timeOut -> pageLoad]** : page load timeout (in sec.) for web application only
* **[timeOut -> watchDog]** : action's execution timeout (in sec.) to prevent browser or application infinite hangup
* **[timeOut -> webService]** : api REST or SOAP webservice requests execution timeout (in sec.)
* **[wait -> search]** : wait (in millisec.) before start to search an element
* **[wait -> enterText]** : wait (in millisec.) before enter text character
* **[wait -> gotoUrl]** : wait (in millisec.) before execute gotoUrl action
* **[wait -> mouseMove]** : wait (in millisec.) before execute mouse move
* **[wait -> switchWindow]** : wait (in millisec.) before switch window action
* For each browser used you can define following parameters :
  - **[path]** : the application binary file path
  - **[waitAction]** : wait after each actions (in millisec.)
  - **[waitProperty]** : wait for double check attributes value (in millisec.) 
  - **[debugPort]** : Chromium only ! define a debug port, can be used to fix 'DevToolsActivePort file doesn't exist' error
* You can add options to the browsers driver
* You can define application name and path of installed applications on the host machine
* You can define a chromium like browser in the available browsers list. In order to use a chromium like browser you have to define *'name'*, *'driver'* and *'path'* attributes of the browser element. The *'driver'* attribute is the driver file *(without extension)* in the ATS drivers folder and the *'path'* attribute is the executable file of the chromium browser :
```
<browser>
	<name>chromium</name>
	<driver>chromiumdriver</driver>
	<path>C:\Program\chromium\chrome.exe</path>
</browser>
```
* If you want to use JxBrowser based application, you have to define *'name'*, *'driver'* and *'path'* attributes of the browser element. The *'driver'* attribute is the driver file *(without extension)* in the ATS drivers folder and the *'path'* attribute is the executable file of the JxBrowser start script, the remote debug port used by default is 9222 (you have to enable debug mode to the JxBrowser based application).
You can find all chromium driver versions here : https://sites.google.com/chromium.org/driver, you have to choose the version corresponding to your version of chromium engine in your JxBrowser based application:
```
<browser>
	<name>jx</name>
	<driver>jxbrowser</driver>
	<path>C:\Program\path_to_jxbrowser_based_application\app.exe</path>
</browser>
```
* Chromium binaries files and Chromium driver for Windows can be found here : http://commondatastorage.googleapis.com/chromium-browser-snapshots/index.html?prefix=Win_x64/1000027/*


* ATS will automatically be updated with the last version of the supported browsers, in order to use another browser driver version you can define an executable driver name in the *'driver'* property of a named browser. If you want to use the version 73 of the chromedriver you have to download the right version of the driver, rename it, and copy it into the *'drivers'* folder :
```
<browser>
	<name>chrome</name>
	<driver>chromedriver_73</driver>
</browser>
```
* Change user profile default path with Chromium based browsers (Chrome, MsEdge, Chromium), Firefox and Opera, if the folder does not exists it will be created before to launch the browser :
```
<browser>
	<name>chrome</name>
	<profileDir>C:/atsProfiles/chrome/automatic_joe</profileDir>
</browser>
```
NB: Relative path can be used
```
<browser>
	<name>chrome</name>
	<profileDir>reusableUserProfile</profileDir>
</browser>
```
the folder containing the user profile data will be created here : 
*%userprofile%/ats/profiles/[BROWSER_NAME]/reusableUserProfile*

* Add options to use with the web driver (example for Chromium browsers) :
```
<browser>
	<name>chrome</name>
	<options>
		<option>--disable-web-security</option>
		<option>--incognito</option>
		<option>--start-maximized</option>
	</options>
</browser>
```
* Add options to allow plugins with Chrome browser :
```
<browser>
	<name>chrome</name>
	<options>
		<option>--always-authorize-plugins=true</option>
	</options>
	<excludedOptions>
		<option>disable-background-networking</option>
	</excludedOptions>
</browser>
```
* With Chromium browsers, you can force to remove options to use with the web driver :
```
<browser>
	<name>chrome</name>
	<excludedOptions>
		<option>allow-cross-origin-auth-prompt</option>
		<option>allow-file-access</option>
		<option>allow-file-access-from-files</option>
		<option>allow-pre-commit-input</option>
		<option>allow-running-insecure-content</option>
		<option>disable-background-networking</option>
		<option>disable-backgrounding-occluded-windows</option>
		<option>disable-client-side-phishing-detection</option>
		<option>disable-default-apps</option>
		<option>disable-hang-monitor</option>
		<option>disable-popup-blocking</option>
		<option>disable-prompt-on-repost</option>
		<option>enable-blink-features</option>
		<option>enable-logging</option>
		<option>ignore-certificate-errors</option>
		<option>log-level</option>
		<option>password-store</option>
		<option>use-mock-keychain</option>
    <otpion>--no-sandbox</option> 
    <otpion>--disable-gpu</option> 
    <otpion>--disable-dev-shm-usage</option> 
	</excludedOptions>
</browser>
```
In docker environment, --no-sandbox, --disable-gpu, --disable-dev-shm-usage **are mandatory**
* In order to use old versions of Firefox, you need to define custom path and driver name, sometimes options need to be excluded too :
```
<browser>
	<name>firefox</name>
	<path>D:\programs\firefox52.4\bin\firefox.exe</path>
	<driver>geckodriver-0.17</driver>
	<excludedOptions>
		<option>moz:debuggerAddress</option>
	</excludedOptions>
</browser>
```
* Add specifics options to use with Internet Explorer :
```
<browser>
	<name>ie</name>
	<options>
		<option>useShellWindowsApiToAttachToIe</option>
		<option>enablePersistentHovering</option>
		<option>destructivelyEnsureCleanSession</option>
		<option>disableNativeEvents</option>
		<option>ignoreZoomSettings</option>
		<option>useCreateProcessApiToLaunchIe</option>
		<option>requireWindowFocus</option>
		<option>usePerProcessProxy</option>
	<options>
</browser>
```
* Add launch arguments to Windows applications :
```
<application>
	<name>notepad++</name>
	<path>C:\Program Files (x86)\Notepad++\notepad++.exe</path>
	<options>
		<option>C:\myFolder\myFileToOpenWhenNotepadStart.txt</option>
	<options>
</application>
```
* Define a Neoload configuration to record and design load testing project with Neoload. ATS Neoload actions will enable Neoload UserPath recording and send commands to execute some design actions in a Neoload project. In order to enable Neoload recording you have to define Neoload proxy and service parameters :
```
<neoload>
	<recorder>
		<host>192.168.0.100</host>
		<port>8090</port>
	</recorder>
	<design>
		<api>Design/v1/Service.svc</api>
		<port>7400</port>
	</design>
</neoload>
```
## List of available ATS actions

```
channel-start : Start a new application channel with specified name
channel-switch : Switch to specified channel name (send application window to foreground)
channel-close : Close specified channel name
goto-url : Navigate to specified url
callscript : Call Ats or Java subscript
keyboard : Enter keyboard action
click : Simple click on found element
over : Make mouse over action on found element
drag : Mouse start drag action on found element
drop : Mouse drop action on found element
swipe : Execute mouse swipe gesture on found element
scroll : Execute mouse wheel action
check-property : Check property value of found element
check-count : Check number of element found with defined criterias
check-value : Make comparison between two values
property : Catch property value of found element into variable
window-resize : Resize or relocate current channel application window
window-switch : Switch between windows (or tabs) of current channel
window-close : Close indexed window
select : Select item by value, text or index of found select element
javascript : Execute javascript code on found element
comment : Add comment to script
```
## List of channel types that can be started with ATS
The ``channel-start`` action is defined with the name of the channel and one of the following types
* Web browser channel : using the following names only, ``chrome``, ``msedge``, ``firefox``, ``opera``, ``brave``
```
channel-start -> mychannel -> chrome
```
* System channel : using the following names only, ``desktop``, ``explorer``
```
channel-start -> mychannel -> desktop
channel-start -> mychannel -> explorer
```
* System native application : using ``file:///`` prefix, you have to define the url path to an executable file on the system
```
channel-start -> mychannel -> file:///url_path_to_executable_file
```
* Universal Windows Apps : using ``uwp://`` prefix, you have to define the application name and the name of the main window separated with a slash (/)
```
channel-start -> mychannel -> uwp://Microsoft.WindowsNotepad/Notepad
```
* Web services : using ```http://``` or ```https://``` prefix, you have to define the url to a SOAP or a REST webservice
```
channel-start -> mychannel -> http://url_to_a_webservice
```
* Mobile applications : using ```mobile://``` prefix, tou have to define the endpoint to a mobile application (you need to use [ATS Mobile Station](https://gitlab.com/actiontestscript/mobile/mobile-station)) and the package name of the app
```
channel-start -> mychannel -> mobile://192.168.1.100:8080/com.myandroid.app
```
* Attach to existing process : using ```proc://``` prefix, you can define one string that must exists in the name of the running process
```
channel-start -> mychannel -> proc://notepad
```
* Attach to existing window by name : using ```window://``` prefix, you can define one string that must be exists in the name of an existing window
```
channel-start -> mychannel -> window://notepad
```
* Attach to existing window by handle : using ```handle://``` prefix, you can define the exact handle of an existing window (handle can be found using ```desktop``` channel)
```
channel-start -> mychannel -> handle://12345
```
* Sap Gui session : using ```sap://``` prefix, you can define an existing SAP session with the client id used by the session
```
channel-start -> mychannel -> sap://SAP_SESSION_NAME/ClientID
```
```
You can run a bat script using channel-start -> channel-bat -> $asset(resources/scripts/echo_script.bat).
However, on Windows, in your System > Developers Area > Terminal settings, the value must be "Windows console host" otherwise the channel will not open.
```


## Using passwords and crypted data

In order to use encrypted data during tests execution you can use '$pass(PASSWORD_KEY)' code.
Data for passwords values are saved in a self crypted file in the ATS project folder.
You can add security and encrypt this data file using AES 128 CBC key.
This key has to be 16 characters length and can be managed using the 3 following ways (in order of pre-emption)
* Environment variable named 'ATS_KEY'
* Tag 'atsKey' in the '.atsProperties' file (not shared with git collaboration)
```
<execute>
     <atsKeyx>16LengthPasswKey</atsKeyx>
     <appBounding>
        <width>1200</width>
        <height>980</height>
     </appBounding>
     ...
<execute>
```
* Tag 'atsKey' in the '.atsProjectProperties' file in the ATS project folder (shared with git collaboration)
```
<atsProject>
     <atsKey>16LengthPasswKey</atsKey>
     <name>ats-project</name>
     <version>0.0.1</version>
  ...
<atsProject>
```
## Video Report

a feature allows to generate visual report to opensource format (ATSV). This one contains all the relative data of the script execution

## Video player
### Automatic ATSV html player file ``(since 2023)``

At the end of ATSV file generation, a html video report "***ATSV html Player***" is automatically created beside this first.

Generic Path :
* ```<project_folder>/target/ats-output/<suite_name>/<script_name>.atsv```
* ```<project_folder>/target/ats-output/<suite_name>/<script_name>_atsv.html```

A custom watermark can be integrated by copying your own in the following folder bellow the following name :
* Folder path : ```<project_folder>/src\assets\resources\reports\images```
* Watermark name : ```watermark.png```

It is possible to disable this generation with the following parameter :
* ``-Dhtmlplayer`` : ```["off", "false", "0", "no", "n"]```

# Thirdparty components

* [Neoload](https://www.neotys.com/) - Load testing platform
* [Selenium](https://www.seleniumhq.org/) - The testing framework used
* [TestNG](http://testng.org/doc/) - Test runner

>## HTML Player component ``(Shorly depreciated)``
>### How to install
>
>* Download the latest release of the app available at [this link](https://github.com/pierrehub2b/ats_visual_player/releases/latest/download/release.zip).
>
>After, you have two options:
>* you can deploy the application directly on your own WebApp or server
>* for local use, you can run the "player.bar" file. Your default browser will launch automatically
>	
>### How to use
>Simply open a ATSV file using the browse file component in the application page 
>
>### Features
>* You can put some of your own ATSV in a directory (located in the src directory) and reference the relative path to that in the "library.json" file. The referenced files will be available when you open the player.
>    * example: if you create a directory named "My ATSV" and put a "test.atsv" inside, you juste have to insert "My ATSV/test.atsv" in the "library.json" (according to the JSON nomenclature)
>* You can reference a ATSV file directly in the player URL. The called file will be opened and read when you access to the page
>    * example: if you put [http://localhost?url=/My ATSV/test.atsv](http://localhost?url=/My%20ATSV/test.atsv), the file "test.atsv" located in the "My ATSV" folder will be launched at start. **Note that only relative path to the server or full Web public URL will work**
>* You can customize the player interface by adding the logo, or another link to your company's JSON library. To do this, all you have to do is define new values ​​in the "settings.txt" file contained in the SRC folder

# Docker integration
# actiontestscript/linux

## Docker file :
- [linux base](https://github.com/ats-docker/linux-base.git) : ` docker pull actiontestscript/linux-base `
- [linux browsers](https://github.com/ats-docker/linux-browsers.git) : ` docker pull actiontestscript/linux-browsers `
- [linux](https://github.com/ats-docker/linux.git) : ` docker pull actiontestscript/linux `



## Getting started
### Linux Container on Windows host *(PowerShell)*
#### Quick Start with ATS Git project
##### Using AtsLauncher

``` powershell
docker run --rm -v ${PWD}\target:/home/ats-user/ats-test actiontestscript/linux sh -c "git clone https://gitlab.com/actiontestscript/ats-test.git . && java AtsLauncher.java output=/home/ats-user/ats-test/ats-output outbound=false atsreport=3 suiteXmlFiles=demo"
```
> Reports and result files will be created in **_target/ats-output_** folder

##### Using Maven

``` powershell
docker run --rm -v ${PWD}:/home/ats-user/ats-test actiontestscript/linux:latest sh -c "git clone https://gitlab.com/actiontestscript/ats-test.git /home/ats-user/temp/ && cp -r /home/ats-user/temp/* /home/ats-user/ats-test && cd /home/ats-user/ats-test && mvn clean test -Doutbound=false -Dats-report=3 -Dsurefire.suiteXmlFiles=src/exec/demo.xml"
```
> Reports and result files will be created in ***target/surefire-reports*** folder

#### Quick Start with local ATS project
##### Using AtsLauncher
 ``` powershell
docker run --rm -v ${PWD}:/home/ats-user/ats-project actiontestscript/linux:latest sh -c "cd /home/ats-user/ats-project && java AtsLauncher.java outbound=false atsreport=3 suiteXmlFiles=demo"
```
> Reports and result files will be created in ***target/ats-output*** folder

##### Using Maven

``` powershell
docker run --rm -v ${PWD}:/home/ats-user/ats-project actiontestscript/linux:latest sh -c "cd /home/ats-user/ats-project && mvn clean test -Doutbound=false -Dats-report=3 -Dsurefire.suiteXmlFiles=src/exec/demo.xml"
```
> Reports and result files will be created in ***target/surefire-reports*** folder


### Linux Container on Linux host *(Shell)*
#### Quick Start with ATS Git project
##### Using AtsLauncher

``` sh
mkdir target
docker run --rm -v ./target:/home/ats-user/ats-test actiontestscript/linux sh -c "git clone https://gitlab.com/actiontestscript/ats-test.git . && java AtsLauncher.java output=/home/ats-user/ats-test/ats-output outbound=false atsreport=3 suiteXmlFiles=demo"
```
> Reports and result files will be created in **_target/ats-output_** folder

##### Using Maven

``` sh
mkdir target
docker run --rm -v ./target:/home/ats-user/ats-test actiontestscript/linux:latest sh -c "git clone https://gitlab.com/actiontestscript/ats-test.git /home/ats-user/temp/ && cp -r /home/ats-user/temp/* /home/ats-user/ats-test && cd /home/ats-user/ats-test && mvn clean test -Doutbound=false -Dats-report=3 -Dsurefire.suiteXmlFiles=src/exec/demo.xml"
```
> Reports and result files will be created in ***target/surefire-reports*** folder


#### Quick Start with local ATS project
##### Using AtsLauncher
 ``` sh
docker run --rm -v ./:/home/ats-user/ats-project actiontestscript/linux:latest sh -c "cd /home/ats-user/ats-project && java AtsLauncher.java outbound=false atsreport=3 suiteXmlFiles=demo"
```
> Reports and result files will be created in ***target/ats-output*** folder

##### Using Maven

``` sh
docker run --rm -v ./:/home/ats-user/ats-project actiontestscript/linux:latest sh -c "cd /home/ats-user/ats-project && mvn clean test -Doutbound=false -Dats-report=3 -Dsurefire.suiteXmlFiles=src/exec/demo.xml"
```
> Reports and result files will be created in ***target/surefire-reports*** folder


---

## Description
Docker images for ATS testing. It contains the following images:
- actiontestscript/linux-base<br>
It is based on ubuntu:24.10 and it contains the following packages:
  - curl
  - sudo
  - wget
  - git
  - unzip
  - zip
  - bzip2
  - nvi
  - java openjdk 21.0.2

<br>

- actiontestscript/linux-browsers<br>
It is based on actiontestscript/linux-base. It contains the following browsers:
  - firefox
  - google-chrome
  - opera
  - brave
  - microsoft-edge

<br>

- actiontestscript/linux<br>
It is based on actiontestscript/linux-browsers. It contains the following packages:
  - ATS java libs
  - ATS linuxdriver
  - geckodriver
  - chromedriver
  - edgedriver
  - operadriver
  - bravedriver

---

## Usage
## Hosts Windows

<br>

### Settings
To execute .ps1 scripts in PowerShell under Windows, you must modify the ExecutionPolicy settings.
Launch PowerShell in administrator mode and run the following commands:

This command will display the currently set execution policy on your system.

``` powershell
Get-ExecutionPolicy
```

<br>

To set the ExecutionPolicy to `RemoteSigned`, use the following command:
``` powershell
Set-ExecutionPolicy RemoteSigned
```
<br>



After executing all the scripts, it is recommended to revert back to the basic configuration.

``` powershell
Set-ExecutionPolicy Restricted
```

<br>

## Script :

<br>

### Using Docker image in current projects directories:

- Open the PowerShell console and navigate to the desired directory.
- Map the current directory and grant appropriate permissions.

  <br>

``` powershell
cd <desired directory>
docker pull actiontestscript/linux
docker run --rm -u 0 -v ${PWD}:/home/ats-user/projects actiontestscript/linux sh -c "chown -R 1000:1000 /home/ats-user/projects"
```

- Execute the test with `atsLauncher`.
    - `atsreport`:
        - 1: Simple execution report
        - 2: Detailed execution report
        - 3: Detailed execution report with screenshots
    - `suiteXmlFiles`:
        - List of sequences to launch, separated by commas and without spaces.

<br>

``` powershell
cd <desired directory>
docker pull actiontestscript/linux
docker run --rm -it -v ${PWD}:/home/ats-user/projects actiontestscript/linux sh -c "java AtsLauncher.java outbound=false atsreport=3 suiteXmlFiles=suite"
```

<br>

### Using a simple git clone and target directorie
  - Create a directory. <br>
  Example: `output`
  - Assign appropriate permissions to the directory.<br>


  ``` powershell
  cd <desired directory>
  mkdir output
  docker pull actiontestscript/linux
  docker run --rm -it -u 0 -v ${PWD}\output:/home/ats-user/outputs actiontestscript/linux sh -c "chown -R 1000:1000 /home/ats-user/outputs"
  ```

<br>

- Clone the test project using standard Git authentication and launch the test suite. <br>
  Example: `ats-test`<br>


    ``` powershell
    docker pull actiontestscript/linux
    docker run --rm -it -v ${PWD}\output:/home/ats-user/outputs actiontestscript/linux sh -c "git clone https://gitlab.com/actiontestscript/ats-test.git . \
    && java AtsLauncher.java output=/home/ats-user/outputs/ats-test outbound=false atsreport=3 suiteXmlFiles=suite "
    ```

<br>

- Or clone the test project using Git token access and launch test suites `suite` and `suite1`. <br>
  Example: `ats-test`<br>

    ``` powershell
    docker pull actiontestscript/linux
    docker run --rm -it -v ${PWD}\output:/home/ats-user/outputs actiontestscript/linux sh -c "git clone https://oauth2:<YOUR_PERSONAL_TOKEN_ACCESS>@gitlab.com/actiontestscript/ats-test.git . \
    && java AtsLauncher.java output=/home/ats-user/outputs/ats-test outbound=false atsreport=3 suiteXmlFiles=suite,suite1 "
    ```

<br>

### Create an image and add git credentials
  - Connect to Git, GitLab, or other platforms using your credentials. Create a read-type access token for the desired repository and obtain your `YOUR_PERSONAL_ACCESS_TOKEN`.

  - Create a credentials file:<br>
  **Save the file without an extension**.<br>
  Example for GitLab:<br>
        
    ``` powershell
    New-Item -Path "${PWD}\gitlab-credential" -ItemType File | Out-Null
    notepad "${PWD}\gitlab-credential"
    ```

    ```
    https://oauth2:<YOUR_PERSONAL_ACCESS_TOKEN>@gitlab.com
    ```
    Save the file with `CTRL+S`

    <br>

  - Create a Docker file<br>
    **Save the file without an extension**.<br>

    ``` powershell
    New-Item -Path "${PWD}\dockerfile_with_credential_git" -ItemType File | Out-Null
    notepad "${PWD}\dockerfile_with_credential_git"
    ```
    ``` docker
    # Dockerfile
    # syntax=docker/dockerfile:1
    FROM actiontestscript/linux
    ARG CREDENTIALS
    RUN git config --global credential.helper store \
      && echo "$CREDENTIALS" > ~/.git-credentials
    ```
    Save the file with `CTRL+S`
    
    <br>


  - Build image
    ``` powershell
    docker pull actiontestscript/linux
    docker build --build-arg CREDENTIALS="$(cat ${PWD}\gitlab-credential)" -f dockerfile_with_credential_git -t linux:credentialGit .
    ```

    <br>

  - Clone the ATS projects repository into the current directory.<br>

    ``` powershell
    cd <desired directory>
    docker run --rm -it -u 0 -v ${PWD}:/home/ats-user/projects linux:credentialGit sh -c "chown -R 1000:1000 /home/ats-user/projects"
    docker run --rm -it -v ${PWD}:/home/ats-user/projects linux:credentialGitsh -c "git clone https://gitlab.com/<url suite> ."
    ```

<br/>

***

## Host Linux

<br>

## Script :

<br>

### Using Docker image in current directorie:
  - Open the Linux console and navigate to the desired directory.
  - Mount the current directory and ensure appropriate permissions are set.<br>

  ``` sh
  cd <desired directory>
  docker pull actiontestscript/linux
  docker run --rm -it -u 0 -v ./:/home/ats-user/projects actiontestscript/linux sh -c "chown -R 1000:1000 /home/ats-user/projects"
  ```

  - Execute the test with `atsLauncher`.
    - `atsreport`:
        - 1: Simple execution report
        - 2: Detailed execution report
        - 3: Detailed execution report with screenshots
    - `suiteXmlFiles`:
        - List of sequences to launch, separated by commas and without spaces.

  ``` sh
  docker pull actiontestscript/linux
  docker run --rm -it -v ./:/home/ats-user/projects actiontestscript/linux sh -c "java AtsLauncher.java outbound=false atsreport=3 suiteXmlFiles=suite"
  ```

<br>

### Using a simple Git clone and target directory

  - Create a directory. 
  Example: `output`
  - Assign appropriate permissions to the directory.<br>

  ``` sh
  cd <desired directory>
  mkdir output
  docker pull actiontestscript/linux
  docker run --rm -it -u 0 -v ./output:/home/ats-user/outputs actiontestscript/linux sh -c "chown -R 1000:1000 /home/ats-user/outputs"
  ```

<br>

  - Clone the ATS project using standard Git authentication and launch the test suite.
  Example: `ats-test`

<br>

  ``` sh
  docker pull actiontestscript/linux
  docker run --rm -it -v ./output:/home/ats-user/outputs actiontestscript/linux sh -c "git clone https://gitlab.com/actiontestscript/ats-test.git . \
  && java AtsLauncher.java output=/home/ats-user/outputs/ats-test outbound=false atsreport=3 suiteXmlFiles=suite"
  ```

  - Or clone the ATS project using Git token access and launch the `suite` and `suite1` test suites.
  Example: `ats-test`

<br>

  ``` sh
  docker pull actiontestscript/linux
  docker run --rm -it -v ./output:/home/ats-user/outputs actiontestscript/linux sh -c "git clone https://oauth2:<YOUR_PERSONAL_TOKEN_ACCESS>@gitlab.com/actiontestscript/ats-test.git . \
  && java AtsLauncher.java output=/home/ats-user/outputs/ats-test outbound=false atsreport=3 suiteXmlFiles=suite,suite1 "
  ```

<br>

### Create an Image and Add Git Credentials
  - Connect to Git, GitLab, or other platforms using your credentials. Create a read-type access token for the desired repository and obtain your `YOUR_PERSONAL_ACCESS_TOKEN`.

  - Create a credentials file:<br>
  Example for GitLab:

    ``` sh
    cd <desired directory>
    vi gitlab-credential
    ```

    To insert the following line, press `i` and copy and paste it :  <br/>
  
    ``` sh
    https://oauth2:<YOUR_PERSONAL_ACCESS_TOKEN>@gitlab.com
    ```

    To save the file, press `ESC` and then type `:wq` and press `Enter`.
This will save the file and exit the editor.


  - Create a Dockerfile<br>

    ```sh
    vi dockerfile_with_credential_git
    ```
    
    <br>
    To insert the following line, press `i` and copy and paste it :<br>

    ``` docker
    # Dockerfile
    # syntax=docker/dockerfile:1
    FROM actiontestscript/linux
    ARG CREDENTIALS
    RUN git config --global credential.helper store \
      && echo "$CREDENTIALS" > ~/.git-credentials
    ```
    
    <br>
    To save the file, press `ESC` and then type `:wq` and press `Enter`.
    This will save the file and exit the editor.
    
    <br>

  - Build image <br>
  
    ``` sh
    docker pull actiontestscript/linux
    docker build --build-arg CREDENTIALS="$(cat ./gitlab-credential)" -f dockerfile_with_credential_git -t linux:credentialGit .
    ```

    <br>

  - Clone the ATS projects repository into the current directory.<br>

    ``` sh
    cd <desired directory>
    docker run --rm -it -u 0 -v ./:/home/ats-user/projects linux:credentialGit sh -c "chown -R 1000:1000 /home/ats-user/projects"
    docker run --rm -it -v ./:/home/ats-user/projects linux:credentialGitsh -c "git clone https://gitlab.com/<url suite> ."
    ```

<br/>

***

## Docker CI Example 
### Gitlab
The file name used for GitLab CI is `.gitlab-ci.yml`.<br>


``` yaml
docker-run:
  image: actiontestscript/linux
  script:
      - java AtsLauncher.java outbound=false atsreport=3 suiteXmlFiles=demo,tech
      - if [ -f "target/ats-output/testng-failed.xml" ]; then exit 1; fi
  artifacts:
    when: always
    name: 'ats-tests-output'
    expire_in: 10 day
    paths: 
      - target/ats-output/*
```

## Authors

* **Pierre Huber** - *Initial work* - [Pierrehub2b](https://github.com/pierrehub2b)

## License

This project is licensed under the Apache 2.0 License - see the [LICENSE.md](LICENSE.md) file for details
