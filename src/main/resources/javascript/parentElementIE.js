var elem=arguments[0].parentElement,result=[];

while (elem != null){
	addElement(elem);
	if(elem.parentElement == null || elem.parentElement.nodeName == 'BODY' || elem.parentElement.nodeName == 'HTML'){
		break;
	}
	elem = elem.parentElement;
};

function addElement(e){
	
	var r = e.getBoundingClientRect();
	var numeric = e.getAttribute('inputmode')=='numeric';
	var password = e.getAttribute('type')=='password';

	result[result.length] = [e, e.tagName, numeric, password, r.top+0.0001, r.left+0.0001, r.width+0.0001, r.height+0.0001, r.left+0.0001, r.top+0.0001, 0.0001, 0.0001, {}, false];
}