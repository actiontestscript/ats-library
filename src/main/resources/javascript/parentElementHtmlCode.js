var elem = arguments[0];
var result = arguments[0].outerHTML.replace(/[\n|\t|\r]/g, '').replace(/> +</g, '');

elem = elem.parentElement;

while (elem != null){
	result = addElement(elem, result);
	elem = elem.parentElement;
};

function addElement(e, code){
	
	let tag = e.tagName.toLowerCase();
	let startCode = '<' + tag ;
	let endCode = '>' + code + '</' + tag + '>';
	
	for (var att, i = 0, atts = e.attributes, n = atts.length; i < n; i++){
	    att = atts[i];
		startCode += ' ' + att.nodeName + '="' + att.nodeValue + '"';
	}

	return startCode + endCode;
}