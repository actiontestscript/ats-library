const element = arguments[0];
var textValue, result = {};

textValue = element.innerText;
if(textValue){
	result['text'] = textValue.trim().replace(/\xA0/g," ").replace(/\n+/g," ");
}else{
	result['text'] = "";
};