var elem=arguments[0].parentElement,result=[];

while (elem != null){
	addElement(elem);
	if(elem.parentElement == null || elem.parentElement.nodeName == 'BODY' || elem.parentElement.nodeName == 'HTML'){
		break;
	}
	elem = elem.parentElement;
};

function addElement(e){

	let r = new DOMRect(0.00001, 0.00001, 0.00001, 0.00001);
	let numeric = false;
	let password = false;
	
	try{
		r = e.getBoundingClientRect();
		numeric = e.getAttribute('inputmode')=='numeric';
		password = e.getAttribute('type')=='password';
	}catch(error){}

	result[result.length] = [e, e.tagName, numeric, password, r.x+0.0001, r.y+0.0001, r.width+0.0001, r.height+0.0001, r.left+0.0001, r.top+0.0001, 0.0001, 0.0001, {}, false];
}