/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.recorder;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ats.executor.ActionStatus;
import com.ats.executor.TestBound;
import com.ats.generator.variables.CalculatedValue;
import com.ats.script.actions.Action;
import com.ats.script.actions.ActionAssertValue;
import com.ats.script.actions.ActionCallscript;
import com.ats.tools.Utils;
import com.ats.tools.logger.ExecutionLogger;
import com.ats.tools.report.utils.ReportImageFormat;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class VisualAction implements IVisualData {

	private static final Pattern pattern = Pattern.compile("[\\p{C}]");

	public static final String START_SCRIPT = "StartScriptAction";
	public static final String API_TYPE = "api";
	public static final String VIDEO_TYPE = "video";

	private TestBound channelBound;
	private String channelName;
	private String data = "";
	private int duration;
	private VisualElement element;
	private int error = ActionStatus.NO_ERROR;
	private ArrayList<byte[]> images;
	private String imageType;
	private int imageRef;
	private int index;
	private int line;
	private byte[] record;
	private String script;
	private boolean stop = true;
	private Long timeLine;
	private String type;
	private String value = "";

	public VisualAction() {
	}

	public VisualAction(ActionAssertValue action, long timeLine) {
		initData(action, timeLine);
		setError(ActionStatus.NO_ERROR);
	}

	public VisualAction(ActionCallscript action, long timeLine, String calledScript, CalculatedValue param) {
		initData(action, timeLine);
		setValue(calledScript);
		setError(ActionStatus.NO_ERROR);

		if (param != null) {
			final JsonObject json = new JsonObject();
			json.addProperty("asset", param.getCalculated());
			setData(json.toString());
		}
	}

	public VisualAction(Action action, long timeLine) {
		initData(action, timeLine);
		setError(ActionStatus.CHANNEL_NOT_STARTED);
	}

	private void initData(Action action, long timeLine) {
		this.images = new ArrayList<>(Arrays.asList(Base64.getDecoder().decode(new String(Utils.EMPTY_SCREEN).getBytes())));

		this.type = action.getClass().getName();
		this.line = action.getLine();
		this.timeLine = timeLine;

		this.script = action.getScript().getTestName();
		this.channelBound = new TestBound(0D, 0D, 150D, 100D);
		this.channelName = "not_started_channel";
	}

	@Override
	public Element getAction(
			ExecutionLogger logger,
			String summary,
			Document document,
			Path xmlFolderPath,
			boolean analytics,
			ReportImageFormat imgFormat,
			boolean savePic) {

		final Element action = document.createElement("action");
		action.setAttribute("type", getType());

		action.appendChild(document.createElement("line")).setTextContent(String.valueOf(getLine()));
		action.appendChild(document.createElement("script")).setTextContent(getScript().replaceAll("\\-iter\\d*$", ""));
		action.appendChild(document.createElement("timeLine")).setTextContent(String.valueOf(getTimeLine()));
		action.appendChild(document.createElement("error")).setTextContent(String.valueOf(getError()));
		action.appendChild(document.createElement("stop")).setTextContent(String.valueOf(isStop()));
		action.appendChild(document.createElement("duration")).setTextContent(String.valueOf(getDuration()));
		action.appendChild(document.createElement("passed")).setTextContent((String.valueOf(getError() == 0)));
		action.appendChild(document.createElement("value")).setTextContent(getValue());

		final String data = getData();

		try {
			JsonElement jsonElement = JsonParser.parseString(data);
			if (jsonElement instanceof JsonNull) {
				action.appendChild(document.createElement("data")).setTextContent(data);
			} else {
				final JsonObject jsonData = jsonElement.getAsJsonObject();
				action.appendChild(extractJsonData(document, jsonData));
			}
		} catch (Exception e) {
			action.appendChild(document.createElement("data")).setTextContent(data);
		}

		if(savePic) {
			Element img = document.createElement("img");
			img.setAttribute("type", getImageType());

			if (!API_TYPE.equals(getImageType()) && imgFormat != null) {

				img.setAttribute("width", String.valueOf(getChannelBound().getWidth().intValue()));
				img.setAttribute("height", String.valueOf(getChannelBound().getHeight().intValue()));
				img.setAttribute("id", "pic_" + String.valueOf(index) + "_" + System.currentTimeMillis());

				if (record != null && record.length > 1) {
					img.setAttribute("type", VIDEO_TYPE);
					img.setAttribute("src", Base64.getEncoder().encodeToString(record));
				} else {
					imgFormat.format(img, getError() != 0, getImageType(), images, element, getImageRef());
				}
			}

			action.appendChild(img);
		}


		Element channel = document.createElement("channel");
		channel.setAttribute("name", getChannelName());

		Element channelBound = document.createElement("bound");
		Element channelX = document.createElement("x");
		channelX.setTextContent(String.valueOf(getChannelBound().getX().intValue()));
		channelBound.appendChild(channelX);

		Element channelY = document.createElement("y");
		channelY.setTextContent(String.valueOf(getChannelBound().getY().intValue()));
		channelBound.appendChild(channelY);

		Element channelWidth = document.createElement("width");
		channelWidth.setTextContent(String.valueOf(getChannelBound().getWidth().intValue()));
		channelBound.appendChild(channelWidth);

		Element channelHeight = document.createElement("height");
		channelHeight.setTextContent(String.valueOf(getChannelBound().getHeight().intValue()));
		channelBound.appendChild(channelHeight);

		channel.appendChild(channelBound);
		action.appendChild(channel);

		if (getElement() != null) {

			Element element = document.createElement("element");
			element.setAttribute("tag", getElement().getTag());

			Element criterias = document.createElement("criterias");
			criterias.setTextContent(getElement().getCriterias());
			element.appendChild(criterias);

			Element foundElements = document.createElement("foundElements");
			foundElements.setTextContent(String.valueOf(getElement().getFoundElements()));
			element.appendChild(foundElements);

			Element searchDuration = document.createElement("searchDuration");
			searchDuration.setTextContent(String.valueOf(getElement().getSearchDuration()));
			element.appendChild(searchDuration);

			Element elementBound = document.createElement("bound");
			Element elementX = document.createElement("x");
			elementX.setTextContent(String.valueOf(getElement().getRectangle().getX().intValue()));
			elementBound.appendChild(elementX);

			Element elementY = document.createElement("y");
			elementY.setTextContent(String.valueOf(getElement().getRectangle().getY().intValue()));
			elementBound.appendChild(elementY);

			Element elementWidth = document.createElement("width");
			elementWidth.setTextContent(String.valueOf(getElement().getRectangle().getWidth().intValue()));
			elementBound.appendChild(elementWidth);

			Element elementHeight = document.createElement("height");
			elementHeight.setTextContent(String.valueOf(getElement().getRectangle().getHeight().intValue()));
			elementBound.appendChild(elementHeight);

			Element elementHpos = document.createElement("hpos");
			elementHpos.setTextContent(getElement().getHpos());
			elementBound.appendChild(elementHpos);
			Element elementVpos = document.createElement("vpos");
			elementVpos.setTextContent(getElement().getVpos());
			elementBound.appendChild(elementVpos);

			element.appendChild(elementBound);
			action.appendChild(element);
		}

		return action;
	}



	public String getImageFileName() {
		return index + "." + imageType;
	}

	public boolean saveRecord(Path folder) {
		if (record != null && record.length > 1) {
			try {
				FileOutputStream fos = new FileOutputStream(folder.resolve(getImageFileName()).toFile());
				fos.write(record);
				fos.close();
				return true;
			} catch (IOException e) {
			}
		}
		return false;
	}

	private static Element extractJsonData(Document document, JsonObject jsonData) {

		final Element data = document.createElement(Action.DATA_JSON);

		for (String key : jsonData.keySet()) {
			Element element;
			if (Utils.isNumeric(key)) {
				element = document.createElement("parameter");
				element.setAttribute("type", "num");
				element.setAttribute("name", key);
			} else {
				element = document.createElement("parameter");
				element.setAttribute("type", key);
			}

			element.setAttribute("value", jsonData.get(key).getAsString());
			data.appendChild(element);
		}

		return data;
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public void setChannelBound(TestBound channelBound) {
		this.channelBound = channelBound;
	}

	public TestBound getChannelBound() {
		return channelBound;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setData(String value) {
		if (value != null) {
			this.data = value.replaceFirst("data::", "");
		}
	}

	public String getData() {
		final Matcher matcher = pattern.matcher(data);
		return matcher.replaceAll("");
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getDuration() {
		return duration;
	}

	public void setElement(VisualElement element) {
		this.element = element;
	}

	public VisualElement getElement() {
		return element;
	}

	public void setError(int error) {
		this.error = error;
	}

	public int getError() {
		return error;
	}

	public void setImages(ArrayList<byte[]> list) {
		this.images = list;
	}

	public ArrayList<byte[]> getImages() {
		return images;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public String getImageType() {
		return imageType;
	}

	public void setImageRef(int imageRef) {
		this.imageRef = imageRef;
	}

	public int getImageRef() {
		return imageRef;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}

	public void setLine(int line) {
		this.line = line;
	}

	public int getLine() {
		return line;
	}

	public void setRecord(byte[] value) {
		this.record = value;
	}

	public byte[] getRecord() {
		return record;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public String getScript() {
		return script;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}

	public boolean isStop() {
		return stop;
	}

	public void setTimeLine(Long timeLine) {
		this.timeLine = timeLine;
	}

	@Override
	public Long getTimeLine() {
		return timeLine;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setValue(String value) {
		if (value != null) {
			this.value = value.replaceFirst("value::", "");
		}
	}

	public String getValue() {
		final Matcher matcher = pattern.matcher(value);
		return matcher.replaceAll("");
	}
}