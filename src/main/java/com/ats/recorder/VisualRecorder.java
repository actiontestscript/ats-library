/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

package com.ats.recorder;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.ats.AtsSingleton;
import com.ats.element.test.TestElement;
import com.ats.executor.ActionStatus;
import com.ats.executor.ActionTestScript;
import com.ats.executor.channels.Channel;
import com.ats.executor.channels.EmptyChannel;
import com.ats.executor.drivers.desktop.DesktopResponse;
import com.ats.generator.objects.MouseDirection;
import com.ats.script.Project;
import com.ats.script.Script;
import com.ats.script.ScriptHeader;
import com.ats.script.actions.Action;
import com.ats.script.actions.ActionChannelStart;
import com.ats.script.actions.ActionChannelSwitch;
import com.ats.script.actions.ActionGotoUrl;
import com.ats.script.actions.ActionMouse;
import com.ats.script.actions.ActionMouseDragDrop;
import com.ats.script.actions.ActionMouseKey;
import com.ats.script.actions.ActionMouseScroll;
import com.ats.script.actions.ActionMouseSwipe;
import com.ats.script.actions.ActionScripting;
import com.ats.script.actions.ActionText;
import com.ats.script.actions.ActionWindowResize;
import com.ats.script.actions.ActionWindowState;
import com.ats.script.actions.ActionWindowSwitch;
import com.ats.script.actions.IActionStoppable;
import com.ats.tools.logger.ExecutionLogger;
import com.ats.tools.report.HtmlPlayerReport;
import com.ats.tools.report.XmlReport;
import com.ats.tools.report.utils.ReportImageFormat;

public class VisualRecorder implements IVisualRecorder {

	private static final String ATSV_HTML = "atsv-html";
	private static final String CLEAN_ATSV = "clean-atsv";
	public static final String ENV_ATSV_HTML = "ATSV_HTML";
	
	private Channel channel;
	private String outputPath;
	private ScriptHeader scriptHeader;

	private int visualQuality = 3;
	private boolean xml = false;
	private boolean atsvHml = false;
	private ReportImageFormat format = null;

	private boolean recording = false;

	private TestSummary summary = new TestSummary();
	private ActionTestScript topScript;

	private ExecutionLogger logger;
	private boolean saveXmlPic = true;

	public VisualRecorder(ActionTestScript topScript, ScriptHeader header, Project project, boolean xml, boolean atsvHml, int quality, ReportImageFormat format, boolean saveXmlPic) {

		this.channel = AtsSingleton.getInstance().getCurrentChannel();
		this.topScript = topScript;
		this.logger = new ExecutionLogger();
		this.atsvHml = atsvHml;
		this.format = format;
		this.saveXmlPic = saveXmlPic;

		final Path output = project.getReportFolder().resolve(header.getPackagePath());
		output.toFile().mkdirs();

		initAndStart(output, header, xml, quality);
	}

	public VisualRecorder(ActionTestScript topScript, File outputFolder, ScriptHeader header, boolean xml, boolean atsvHml, int quality, ExecutionLogger logger, ReportImageFormat format, boolean saveXmlPic) {

		this.channel = AtsSingleton.getInstance().getCurrentChannel();
		this.topScript = topScript;
		this.logger = logger;
		this.atsvHml = atsvHml;
		this.format = format;
		this.saveXmlPic = saveXmlPic;

		final Path output = outputFolder.toPath();
		initAndStart(output, header, xml, quality);
	}

	//--------------------------------------------------------------------------------------------
	//--------------------------------------------------------------------------------------------

	private void initAndStart(Path output, ScriptHeader header, boolean xml, int quality) {
		this.outputPath = output.toFile().getAbsolutePath();
		this.scriptHeader = header;
		this.xml = xml;

		if(quality > 0) {
			this.visualQuality = quality;
		}
	}

	@Override
	public void updateSummary(String testName, int testLine, String data) {
		summary.appendData(data);
	}

	@Override
	public void updateSummaryFail(String testName, int testLine, String app, String errorMessage, TestError.TestErrorStatus testErrorStatus) {
		summary.setFailData(testName, testLine, errorMessage, testErrorStatus);
	}

	@Override
	public void updateSummaryFailPass(String testName, int testLine, String errorMessage, TestError.TestErrorStatus testErrorStatus) {
		summary.addFailPassData(testName, testLine, errorMessage);
	}

	//--------------------------------------------------------------------------------------------
	//--------------------------------------------------------------------------------------------

	@Override
	public void terminate() {
		if(channel != null) {
			final Path path = Paths.get(outputPath);
			boolean isCommandLineExecution = path.toString().contains("Command line suite");

			logger.sendInfo("stop visual recording", scriptHeader.getQualifiedName());
			channel.stopVisualRecord(topScript.getStatus(), summary);
			channel.saveVisualReportFile(path, scriptHeader.getQualifiedName() + Script.ATS_VISUAL_FILE_EXTENSION, logger);
			
			if(Boolean.parseBoolean(System.getProperty(ATSV_HTML)) || atsvHml || Boolean.parseBoolean(System.getenv(ENV_ATSV_HTML)) || "1".equals(System.getenv(ENV_ATSV_HTML))) {
				HtmlPlayerReport.createHTMLPlayerReport(logger, path, scriptHeader.getQualifiedName());
			}

			boolean isAtsvExists = Path.of(outputPath).resolve(scriptHeader.getName() + ".atsv").toFile().exists();
			
			if(xml || isCommandLineExecution || isAtsvExists) {
				XmlReport.createReport(topScript, path, scriptHeader, logger, format, Boolean.parseBoolean(System.getProperty(CLEAN_ATSV)), saveXmlPic);
			}
		}
	}

	private void setChannel(Channel channel) {
		if(!recording && channel != null && !(channel instanceof EmptyChannel)) {
			recording = true;
			final DesktopResponse resp = channel.startVisualRecord(scriptHeader, visualQuality, topScript.getStarted());
			if(resp != null && resp.getErrorCode() < 0) {
				channel.sendLog(resp.getErrorCode(), "unable to start visual recording", resp.getErrorMessage());
			}
		}
		this.channel = channel;
	}

	private boolean isSyncAction(String actionName) {
		if( actionName == ActionMouse.class.getName() || actionName == ActionGotoUrl.class.getName() ||
			actionName == ActionMouseKey.class.getName() ||  actionName == ActionMouseScroll.class.getName() ||
			actionName == ActionText.class.getName() || actionName == ActionScripting.class.getName() ||
			actionName == ActionWindowState.class.getName() || actionName == ActionWindowSwitch.class.getName() ||
			actionName == ActionMouseDragDrop.class.getName() || actionName == ActionMouseSwipe.class.getName() ||
			actionName == ActionChannelSwitch.class.getName() || actionName == ActionWindowResize.class.getName()
		) {
			return true;
		}
		return false;
	}

	@Override
	public void createVisualStartChannelAction(ActionChannelStart action, long duration, String scriptName, int scriptLine) {
		setChannel(action.getStatus().getChannel());

		channel.createVisualAction(
				true,
				action,
				scriptLine,
				scriptName,
				topScript.getTimeLine() - duration,
				isSyncAction(action.getClass().getName()));

		channel.sleep(100);
		update(action.getStatus().getCode(), duration, action.getName(), action.getActionData().toString());
	}

	@Override
	public void createVisualAction(Action action, String scriptName, int scriptLine) {
		setChannel(action.getStatus().getChannel());

		boolean stop = false;
		if (action instanceof IActionStoppable) {
			stop = ((IActionStoppable)action).isStop();
		}

		channel.createVisualAction(
				stop,
				action,
				scriptLine,
				scriptName,
				topScript.getTimeLine(),
				isSyncAction(action.getClass().getName()));
	}

	@Override
	public void update(int error, long duration, String value, String data) {
		channel.updateVisualAction(error, duration, value, data);
	}

	@Override
	public void update(int error, long duration, String value) {
		channel.updateVisualAction(error, duration, value);
	}

	@Override
	public void updateScreen(boolean ref) {
		channel.sleep(100);
		channel.updateVisualAction(ref);
	}

	@Override
	public void update(String value) {
		channel.updateVisualAction(value);
	}

	@Override
	public void update(String value, String data) {
		channel.updateVisualAction(value, data);
	}

	@Override
	public void update(String type, MouseDirection position) {
		channel.updateVisualAction(type, position.getHorizontalPos(), position.getVerticalPos());
	}

	@Override
	public void update(int error, long duration) {
		channel.updateVisualAction(error, duration);
	}

	@Override
	public void update(TestElement element) {
		channel.updateVisualAction(element);
	}

	@Override
	public void updateScreen(TestElement element) {
		channel.updateVisualAction(element);
	}

	//-----------------------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void updateScreen(ActionStatus st) {
		update(st.getCode(), st.getDuration());
	}

	@Override
	public void updateScreen(ActionStatus st, String value) {
		update(st.getCode(), st.getDuration(), value);
	}

	@Override
	public void updateTextScreen(ActionStatus st, String value) {
		update(st.getCode(), st.getDuration(), value, st.getMessage());
	}

	@Override
	public void updateScreen(ActionStatus st, String type, MouseDirection position) {
		update(st.getCode(), st.getDuration());
		update(type, position);
	}

	//-----------------------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void update(int error, long duration, TestElement element) {
		update(error, duration);
		update(element);
	}

	@Override
	public void update(int error, long duration, String value, String data, TestElement element) {
		update(error, duration, value, data);
		update(element);
	}
}