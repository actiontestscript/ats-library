package com.ats.recorder;

import java.nio.file.Path;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ats.tools.logger.ExecutionLogger;
import com.ats.tools.report.utils.ReportImageFormat;

public interface IVisualData {
	Element getAction(
			ExecutionLogger logger,
			String summary,
			Document document,
			Path xmlFolderPath,
			boolean analytics,
			ReportImageFormat format,
			boolean savePic);

	public Long getTimeLine();
}