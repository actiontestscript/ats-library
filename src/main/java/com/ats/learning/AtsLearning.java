package com.ats.learning;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;

import com.ats.element.SearchedElement;
import com.ats.element.test.TestElement;
import com.ats.executor.drivers.engines.IDriverEngine;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class AtsLearning implements IAtsLearning {

	private static final String ATS_SUITE_LEARNING = "ats-learning.jsonl";

	private FileWriter fw;

	public AtsLearning(Path suiteOutputPath) {
		try {
			fw = new FileWriter(suiteOutputPath.getParent().resolve(ATS_SUITE_LEARNING).toFile(), true);
		} catch (IOException e) {
			fw = null;
			e.printStackTrace();
		}
	}

	@Override
	public void terminate() {
		if(fw != null) {
			try {
				fw.close();
				fw = null;
			} catch (IOException e) {}
		}
	}

	@Override
	public void saveAtsLearning(TestElement testElement, IDriverEngine engine, SearchedElement searchedElement) {

		final String userContent = "this html element with it's parents tree and their attributes:"
				+ engine.getParentsDomCode(testElement.getFoundElement().getValue())
				+ "\nin order to find this element, I use following ATS code:"
				+ "\nELEMENT1 [one or more comma separated attributes using format: \"key=value\"] etc...";

		final String assistantContent = searchedElement.getAtsLearning();

		try {

			final JsonObject user = new JsonObject();
			user.addProperty("role", "user");
			user.addProperty("content", userContent);

			final JsonObject assistant = new JsonObject();
			assistant.addProperty("role", "assistant");
			assistant.addProperty("content", assistantContent);

			final JsonArray messages = new JsonArray();
			messages.add(user);
			messages.add(assistant);

			final JsonObject data = new JsonObject();
			data.add("messages", messages);

			fw.write(data.toString() + "\n");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}