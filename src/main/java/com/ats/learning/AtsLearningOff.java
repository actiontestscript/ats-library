package com.ats.learning;

import com.ats.element.SearchedElement;
import com.ats.element.test.TestElement;
import com.ats.executor.drivers.engines.IDriverEngine;

public class AtsLearningOff implements IAtsLearning {
	
	public AtsLearningOff() {}
	
	@Override
	public void saveAtsLearning(TestElement testElement, IDriverEngine engine, SearchedElement searchedElement) {}
	
	@Override
	public void terminate() {}
}