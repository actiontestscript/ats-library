/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

package com.ats.tools;

import java.util.Base64;

public class OperatingSystem {
	
    private static String OS = System.getProperty("os.name").toUpperCase();
    
	private static final String WINDOWS = "WINDOWS";
	private static final String LINUX = "LINUX";
	private static final String MACOS = "MACOS";
	
	private static final String MICROSOFT = "MICROSOFT";
	
	private static final String WINDOWS_ICON =  "iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABmJLR0QArgDOAOl0s3seAAAACXBIWXMAAA3XAAAN1wFCKJt4AAAAB3RJTUUH5wUdCB4FBz4bQAAAAzJJREFUSMetlcuLXEUUxn9Vt+7tx8y03T3PTEZHiQMiuhECbhVXEVf+Df4D7iWLbAVBEFy6ceE+OyUEXIkQER1Ek7iI42RemaSn0337UXWOi3u7b/dMJiahD9w+XYe69dX3fafqmk++DixsHF0eern6eKDvSdBqEMUrBFG8jLIShKlc/Bd8yGto21l+gHDtj5NLv5hPr+9fPumG69uHg5WuF4JAUM1ztrjkeXLsx/VivmhWiy2sVc1ubORDO/RydftwsNILyqxiKHC/o+t94TP7eKDvd72M0RXQPI9/x2MmqsXIAvUIXnYGk1e9Qur1A4doJUjxoqgWIPkzGWslw+tzEVu1iMVaxGuLCZuLCfPzjqEIH311h39sBYAg1FymbbFTZ2DOGl4qwau1iHdWS7zSjPltt8OVtxusL5Yox5ZqKSKyZgp85zjF9FsQKSRVgijO50YBVCx8eWWRNy5UaM7FWAPGGIwB83OP9UbMci15ugEKpK2MQTnB+bw7ACywtVpm5UmLKCjP2ggKvRZCFRfyPgcQY5hZL6kS0lbGIF8fUX0q8xcByUyeYDDLUMD5/MQC9EX59e9H7M7ZMzNv/9smsdCcTwpGp2gdnAzoiU69l5k8ZgAudsSJPaNN5CJc7HCJO2V8EVESTu1LM5NHoE4Nb27Mc7FROkP33mGXtzZrrNQr50qyc5xStqaQRDVj4HMENc8g6ot4IAUgM24jXAg6PsliZ4ugY5PzrfcCbN9rMwxCc6FExZnxfTPaxPOGG32VAA4EPv5un3rvmHqsbK7McWmpwnKjwp8HKRebZRRDkjhKSYSzhshCZM7nmJ/k4vbvENGJ6+x0HvL73RbcbY0nf3trj9WKY6sWs1lLuLBcZbVRYalepl6v0vVKeurD4YaqbVEWpmBtDNUGdB+C+CkX9lLPXur5cT+F2xm4za/4kjE88JNahq51lu9j+wRuUQ5io//VWRTaQTnyMiGTQr9zy0K4tlY1u86cB9IE657/Fhp0H3Dw1zfR0btf7C0lRzfKjuXY6kZstOSM4IxgNWAM2Mhhhl2sDDHqMRJAA0Z9JuHoCR6kn5I++on7259z5+b2f4av5EUIWEuLAAAAAElFTkSuQmCC";
	private static final String LINUX_ICON = "iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAMAAADXqc3KAAAC91BMVEVHcEzQ0NDm5uY4ODg7ODD39/f5+fnQfgP4+PjU1NT+ugB6eno4ODg5OTm8vLw7Ozt0dHRCQkI1NTV3d3c9PT3/mg//nwX+wQD+nQBDQ0P+mQ3+oQBRUVH+pgX+rAD+nw3ZiABLS0v+oQA8PDznjwD+qghCQkL+rAD+tAH+qAD+uADnjQCAWRjskgAxMTFOTk7+owBFRUX9ngD+uACmpqZAOC3wkAPnjQD5oQEyMjL/sQBjVkD+nQDnjwAxMTH+swAzMzP+sgD/pgD/tADc3Nz+uQD+yQD+qQA7Ozv+tADYiQn29vb+vwD+sQD+tgD+vADo6Oj+ywD+pQD+twD+vQD+sAD+2gCjnpdLS0vyvANvYU2WcyL/rAD5qgHv7+/+0gCIZB7+pwBOTk7MzMw7OzstLS02NjZzWCo8PDxRUVFBQUHNzc3g4OA2NjYxMTHb29v09PSpoJXW1tY7Ojj/vwD+3QDf39/+zgCAgIDg397+zQjgoAdVVVU0NDRBQUHFxcX+wABIPi3NzMy0tLQzMzPp6elEREReVzjh4eIxMTGWjXs/Pz+7u7tBQUE5OTlqampCQkJ6enpJSUlqamo6OTjGxsY6Ojo3NzeJiYlYWFg3Nzd7e3ttbW3Q0dHZ2dn2oQI8Ny7+0Qf1pgG7jiI8OztaU0eciUn64THZpVvYsRj/wAD4+Pjt7e2lpaX/rwDw8PDy8vLj4+PCwsLo6Oi3t7fV1dXY2Ng4ODjBwcFoaGhgYGD19fX39/f09PRlZWX/owe9vb3/oQD/wQD/ygD29vbx8fFERETz8/O5ubmDeGfmrSnn5+fu7u7/uwDvwQj03Kn/twD/vAD/0AD/vwDs7Oz/xQD91AX5+fn/zgBhYWHd3d00NDTOzs7+xwDHx8f/ugD/xwCtnxrT09NLS0tAQEDR0dG1iBZSUlJwcHD/zgTKyspFRT+BbUY8PDz7wD6tqqeIiIinp6eDg4Oenp5jY2OgoKD3sSzTfyX0xFX9xRdzYjv+2hemi0O3wNkOAAAAqnRSTlMA/v7+Cf7+Af7+/v4CEP7+/v5l/v44pv5JK9oV/R91QgSDTgYu2Zre2P7+GgYY1HNP5BOS/vQbJKnHVtyFMQH+0f6++P7+/v4O/jT+/v7+/v7+/v7+/v7+zP7+/sPu/vzy/tv+MQJj/rBBVP7+2zT+/v7+J83+/v7+/v6LyFbq/v5L/v7+/v7+/mj+/v4YPOvW3v7+/v5FfP7+OP7+/v7+Iv7+/v7+/v7+/mEIANMAAAFwSURBVCjPY2CAA55eXgZsIHPiDOGaFExxlv4PnwR21WFq6ps8W3jOTMFaDIlpn7/unPdRRAhDYtauX3+WfsEisWDZ8oWLFi+Zjmn5yt8/53+fMoEHXaK468e3Fe8nPc5C1xDB2TF3ak8Fc5IHqoSayfUNm8se8d3enmGHLC4pw8jmHce0LVughTkNWUJx35rVR9Yzbd2xYzt/bipCXNnwAMee3Wc23tny9Dn/2zyEhP6xoxyr917JedW87UX3k06EhHUUGwfrhoD8er5WbsG2Rri4Wey186zH12zcuqnp3Zudr6uhwkZWMUGXzoVvPn2L8V7ly2ftD0ugEjZ+gcEhYdHcLpzJRQVVDeWl8cbSYAmldRd8Lt+IPLllE+f6wruruFYdFhcDS8gdOut/8arbqbUn1t5Mf3A/8eB+KVGwhIS6lquvo6e7qZ6lQwJXqKaqPDvUEnZdA6d19hY6QKaX8yoNWZRANLeFKNNWUYAIAAAnK4nxI6VrLgAAAABJRU5ErkJggg==";
	private static final String MACOS_ICON = "iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABmJLR0QArgDOAOl0s3seAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH5wUdCC0Z51QiPwAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAOySURBVEjHhZbNa1xlFMZ/5713OpP5SCfWlDaF2oWVuijit2hduBEU7U5QFyoKrkTBhYILRf+P7IVCQYhiLBRBpbiwbnQjpotCNJrMpGmbTGbu+76Pi3vnexIvnMW8897z8ZznOefaJ2v/ftrxyedB4v8e+cir6W0eODaPsMNuYhhpWvrM3v697b/uzSf5dQOBRt415ccAZ7Y2WHm8QaWUjjgzQDN+C+9DSENPiWWFF4uDK5OPmfFaeY9yaYEowwZONZV9P1CMMUkJIK+RF/oRDDS8HHzGw01HVF7V4RAVoQQpAZy32WnLBhVbu8V9Z5uowDfPVfl/dnCwFA94TcABSgDfx9N4tHuXo/UFEATiAA6HITMUhUxT+TkCkBXO+hZg//tLaK+NeQMvnqlmmOUQaWBGq93iyuq3yEbPh5YHCAIfhxZE59ef4e4dFCIui5yuOUKMfYLkbBPs7Oxw/fovOWiaNJFaAPNFU21IhLTawJRiGcQQuPceR4wjpLTcQSkt0WjMDyrrQ9Nvsr21uqnvducHWA+J1CNaCZMRu3tcenCLc2eWpmhp5Ak4l4zpByB4T0rQsIKxl0u4opy4t0+zWkXSmJDMjCjApUUPNJKoIYlCB3FaWKPhOh2OlGsjFfYdjHO+fz5GU3mwbDaH+/R2+13mKguFE83Q7CFCUzQsTJxakWGhV5cFNrduMTdXwWTUa2XS0hEgFoBYIZ5ieBWlCUgVNCI0y106YVFDmI6e5qWVLQh7oMAX51q8/Nx5glyRBiREooHkhrUJHB7IlIstE9pep/vbKgRhGVhmOCWk9RMkzZMkzVN0fQnhIGoA25XVb9j4ax1JSJYbkFo08MPZnFQW8X+v0dm9zNz5ixgJkivyzNHu9eKgnz7bZ+Wry/R6PRaPn8gRUBxA5Po0NZ9njE+pP/UuLqnQ+vIDujd+gk4bw2FmGI47XcdOe5NrP17low/fZ+nUEq+8/gZypZzAVswnDHtz+R9d3apPbyQz1Guzd+ManT9+ILbWoH4cp8DJ/ZtcfP4Jnn72Ao889iTlSqNo94SX4EkJhvli9E5owKxJ7f4XqZ19AcUIvgsyLqR/8vF7D9HLIhIExQMWp3IlM1g4NrJTNSYsIwGrgsHNbQhexDiynA7SgYXRfTCkl81YhyqcrXcr7Ox2mKuU4cBvhbzZzmQ5HT05mwIQIYacDIrFWTF1zYtdGtzavj1z/g9N4BLSxpFseSmN74TBIpxe5ZMAxFrKxsYmi8eqh+7kRGH5P4VsGEj0B5i1AAAAAElFTkSuQmCC";

	public static String getType(String value) {
		final String os = value.toUpperCase();
		if(os.contains(WINDOWS) || os.contains(MICROSOFT) ) {
			return WINDOWS;
		}else if(os.contains(LINUX)) {
			return LINUX;
		}else if(os.contains(MACOS)) {
			return MACOS;
		}
		return "";
	}
	
	public static byte[] getIcon(String value) {
		switch(value) {
		case WINDOWS:
			return Base64.getDecoder().decode(WINDOWS_ICON);
		case LINUX:
			return Base64.getDecoder().decode(LINUX_ICON);
		case MACOS:
			return Base64.getDecoder().decode(MACOS_ICON);
		default:
			return Utils.getWhitePixel();
		}
	}
	
	public static boolean isLinux(String value) {
		return value != null && value.toUpperCase().contains(LINUX);
	}
	
	public static boolean isLinux() {
		return isUnix();
	}

    public static boolean isWindows() {
        return OS.contains("WIN");
    }

    public static boolean isMac() {
        return OS.contains("MAC");
    }

    public static boolean isUnix() {
        return (OS.contains("NIX") || OS.contains("NUX") || OS.contains("AIX"));
    }

    public static boolean isSolaris() {
        return OS.contains("SUNOS");
    }

    public static String getOS(){
        if (isWindows()) {
            return "win";
        } else if (isMac()) {
            return "osx";
        } else if (isUnix()) {
            return "uni";
        } else if (isSolaris()) {
            return "sol";
        } else {
            return "err";
        }
    }

    public static String getOSName(){
        if (isWindows()) {
            return "windows";
        } else if (isMac()) {
            return "macos";
        } else if (isUnix()) {
            return "linux";
        } else if (isSolaris()) {
            return "solaris";
        } else {
            return "err";
        }
    }

    public static String getArchiveExtension(){
        if (isWindows()) {
            return "zip";
        } else {
            return "tgz";
        }
    }
}