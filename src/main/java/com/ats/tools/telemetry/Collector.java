/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.tools.telemetry;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.api.common.Attributes;
import io.opentelemetry.api.common.AttributesBuilder;
import io.opentelemetry.api.metrics.LongCounter;
import io.opentelemetry.api.metrics.Meter;
import io.opentelemetry.sdk.OpenTelemetrySdk;
import io.opentelemetry.sdk.metrics.SdkMeterProvider;
import io.opentelemetry.sdk.metrics.SdkMeterProviderBuilder;
import io.opentelemetry.sdk.metrics.export.MetricExporter;
import io.opentelemetry.sdk.metrics.export.MetricReader;
import io.opentelemetry.sdk.metrics.export.PeriodicMetricReader;
import io.opentelemetry.sdk.resources.Resource;
import io.opentelemetry.sdk.resources.ResourceBuilder;

public class Collector {
    private String endpointUrl;
    private MetricReader metricReader;
    private OpenTelemetry openTelemetry;
    private Meter meter;  
    private Resource resource;

    private MetricExporter metricExporter;
    public MetricExporter getMetricExporter() {
        return metricExporter;
    }

    public static boolean isOpenTelemetryEnabled() {
    	return CollectorBuilder.getOpenTelemetryEndpoint() != null;
    }

    public static Collector getDefault() {
        return builder().build();
    }

    public static CollectorBuilder builder() {
        return new CollectorBuilder();
    }
    public static void main(String[] args) throws InterruptedException {
        if (args.length < 1) {
            System.out.println("Usage: com.ats.tools.telemetry.Collector [COUNTER_NAME]=[VALUE]  [COUNTER_NAME=VALUE]");
            System.exit(2);
        }

        Collector collector = Collector.getDefault();
        for(String metric:args) {
            String[] parts = metric.split("=");
            if(parts.length > 1) {
                String metricName = parts[0];
                String metricValue = parts[1];
                collector.addCount(metricName, Long.valueOf(metricValue));
                System.out.println("metric count "+metricName+" "+metricValue);
            }
        }
        collector.close();
    }



    public Collector(Map<String,String> resources, MetricExporter metricExporter) {
        this.resource = mapToResource(resources);
        this.metricExporter = metricExporter;
    }

    public void addCount(String counterName, long value) {
        getCounter(counterName).add(value);
    }

    public void addCount(String counterName, long value, Map attributes) {
        getCounter(counterName).add(value, mapToAttributes(attributes));
    }
    


    private static Resource mapToResource(Map<String,String> attributes) {
        ResourceBuilder resourceBuilder = Resource.getDefault().toBuilder();
        for (Map.Entry<String,String> entry : attributes.entrySet()) {
            resourceBuilder.put(entry.getKey(), entry.getValue());
        }
        return resourceBuilder.build();
    }

    private static Attributes mapToAttributes(Map<String,String> attributes) {
        AttributesBuilder attributesBuilder = Attributes.builder();
        for (Map.Entry<String,String> entry : attributes.entrySet()) {
            attributesBuilder.put(entry.getKey(), entry.getValue());
        }
        return attributesBuilder.build();
    }
    



    
    private LongCounter getCounter(String counterName) {
        return getMeter().counterBuilder(counterName)
                .setUnit("1")
                .build();
    }
    
    private Meter getMeter() {
        if(meter == null) {
            meter = getOpenTelemetry().meterBuilder("instrumentation-library-name")
                    .setInstrumentationVersion("1.0.0")
                    .build();
        }
        return meter;
    }
    public void close() {
        if(metricReader != null) {
            try {
                metricReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private OpenTelemetry getOpenTelemetry() {
        if(openTelemetry == null) {
            SdkMeterProviderBuilder sdkMeterProviderBuilder = SdkMeterProvider.builder();
            
            if(getMetricExporter() != null) {
                metricReader = PeriodicMetricReader.builder(getMetricExporter()).setInterval(100, TimeUnit.MILLISECONDS).build();
                sdkMeterProviderBuilder.registerMetricReader(metricReader);
            }
            if(resource != null) {
                sdkMeterProviderBuilder.setResource(resource);
            }

            SdkMeterProvider sdkMeterProvider = sdkMeterProviderBuilder.build();

            openTelemetry = OpenTelemetrySdk.builder()
                    .setMeterProvider(sdkMeterProvider)
                    .build();
        }
        return openTelemetry;
    }
    

}
