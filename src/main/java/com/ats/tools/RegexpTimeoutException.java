/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.tools;

public class RegexpTimeoutException extends RuntimeException {
	private static final long serialVersionUID = 6437153127902393756L;

	private final String regularExpression;
	private final String stringToMatch;
	private final long timeoutMillis;

	public RegexpTimeoutException() {
		super();

		regularExpression = null;
		stringToMatch = null;
		timeoutMillis = 0;
	}

	public RegexpTimeoutException(String message, Throwable cause) {
		super(message, cause);

		regularExpression = null;
		stringToMatch = null;
		timeoutMillis = 0;
	}

	public RegexpTimeoutException(String message) {
		super(message);

		regularExpression = null;
		stringToMatch = null;
		timeoutMillis = 0;
	}

	public RegexpTimeoutException(Throwable cause) {
		super(cause);

		regularExpression = null;
		stringToMatch = null;
		timeoutMillis = 0;
	}

	public RegexpTimeoutException(String regularExpression, String stringToMatch, long timeoutMillis) {
		super("Timeout occurred after " + timeoutMillis + "ms while processing regular expression '" + regularExpression + "' on input '" + stringToMatch + "'!");

		this.regularExpression = regularExpression;
		this.stringToMatch = stringToMatch;
		this.timeoutMillis = timeoutMillis;
	}

	public String getRegularExpression() {
		return regularExpression;
	}

	public String getStringToMatch() {
		return stringToMatch;
	}

	public long getTimeoutMillis() {
		return timeoutMillis;
	}
}
