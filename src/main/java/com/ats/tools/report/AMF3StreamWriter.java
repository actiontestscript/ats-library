package com.ats.tools.report;

import com.ats.flex.messaging.amf.io.AMF3Deserializer;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;

public class AMF3StreamWriter extends AMF3Deserializer {
	public AMF3StreamWriter(InputStream in) {
		super(in);
	}

	public void readAndProcessStream(Consumer<Object> objectProcessor) throws IOException {
		while (available() > 0) {
			Object obj = readObject();
			objectProcessor.accept(obj);

			storedObjects.clear();
		}
	}

}
