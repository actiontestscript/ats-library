package com.ats.tools.report;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.ats.executor.TestBound;
import com.ats.flex.messaging.amf.io.AMF3Deserializer;
import com.ats.recorder.TestError;
import com.ats.recorder.TestSummary;
import com.ats.recorder.VisualAction;
import com.ats.recorder.VisualElement;
import com.ats.recorder.VisualReport;
import com.ats.script.Script;
import com.ats.tools.ResourceContent;
import com.ats.tools.logger.ExecutionLogger;

public class HtmlPlayerReport {

    private static Map<String, String> executionData = new HashMap<>();
    private static Map<String, String> testData = new HashMap<>();
    private static Map<String, String> actionData = new HashMap<>();
    private static Map<String, String> actionsData = new HashMap<>();

    private static Map<String, String> iconsData = new HashMap<>();
    private static Map<String, String> customImg = new HashMap<>();

    private static Map<String, String> actionsImg = new HashMap<>();

    private static final String REPORT_FOLDER = "reports";
    //private static final String TEMPLATE_FOLDER = "templates";
    private static final String IMAGE_FOLDER = "images";
    private static final String HTML_FOLDER = "playerHTML";
    private static final String CUSTOM_TEMPLATES_FOLDER = "/" + REPORT_FOLDER + "/" + HTML_FOLDER + "/";
    private static final String CUSTOM_IMAGES_FOLDER = "/" + REPORT_FOLDER + "/" + IMAGE_FOLDER + "/";

    private static String c_testName = "";

    private static final Path reportPath = Paths.get(
            com.ats.script.Project.SRC_FOLDER,
            com.ats.script.Project.ASSETS_FOLDER,
            com.ats.script.Project.RESOURCES_FOLDER,
            REPORT_FOLDER);

    private static final Path imagesPath = Paths.get(reportPath.toString() ,"images");

    private static final String[] imagesList = {"logo.png", "true.png", "false.png", "warning.png", "noStop.png", "api.png", "pdf.png", "watermark.png"};

    private static final String CUSTOM_GSAP_FOLDER = CUSTOM_TEMPLATES_FOLDER + "gsap_file/";
    private static final String CSS_FILE_NAME = "open.css";
    private static final String GSAP_FILE_NAME = "gsap_3.11.1.min.js";
    private static final String GSAP_STP_FILE_NAME = "ScrollToPlugin_3.11.1.min.js";
    private static final String JSCODE_FILE_NAME = "script.js";

    private static boolean developper = false;
    private static PrintWriter devData = null;
    private static PrintWriter devImg = null;

    private static final String HTML_TEMPLATE_NAME = "player_tpl.html";
    private static final String HTML_PLAYER_NAME = "atsv.html";

    static Path outputFolder;
    static Path inputFolder;

    public static void main(String[] args) throws IOException {

        List<String> listArgs = Arrays.asList(args);

        if (args.length == 0 || (listArgs.contains("--help") || listArgs.contains("-h") || listArgs.contains("--h") || listArgs.contains("/?") || listArgs.contains("\\?"))) {
            usage();
            return;
        }

        String testName = null;
        boolean iconsConvert = false;
        boolean execution = false;

        for (int i = 0; i < args.length; i++) {
            String param = args[i];
            if (param.startsWith("--")) {
                switch (param.substring(2)) {
                    case "outputFolder":
                    case "output":
                        if (i + 1 < args.length) {
                            outputFolder = Path.of(args[i + 1].replaceAll("\"", ""));
                        }
                        execution = true;
                        break;
                    case "input":
                    case "inputFolder":
                        if (i + 1 < args.length) {
                            inputFolder = Path.of(args[i + 1].replaceAll("\"", ""));
                        }
                        execution = true;
                        break;
                    case "testName":
                        testName = args[i + 1];
                        c_testName = testName;
                        execution = true;
                        break;
                    case "iconsConvert":
                        iconsConvert = true;
                        break;
                    case "dev":
                        developper = true;
                        break;
                    default:
                        System.out.println("\u001B[31m" + "unrecognized parameter : " + "\u001B[0m" + param);
                        usage();
                        return;
                }
            }
        }

        if (iconsConvert) {
            System.out.println("iconsData Js file started");
            writeJSIconsFile();
            System.out.println("iconsData Js file ended");
        }

        if (execution) {
            if (inputFolder != null && inputFolder.toFile().isDirectory()) {
                if (outputFolder == null || !outputFolder.toFile().isDirectory()) {
                    System.out.println("output folder not specified or unavailabled \n\tinput folder used like output Folder");
                    outputFolder = inputFolder;
                }
                if (testName != null && inputFolder.resolve(testName + Script.ATS_VISUAL_FILE_EXTENSION).toFile().exists()) {
                    createHTMLPlayerReport(new ExecutionLogger(System.out), inputFolder, testName, outputFolder);
                } else {
                    System.out.println("[ERROR] invalid suite name. GENETRATION ABORTED");
                }
            } else {
                System.out.println("[ERROR] input folder doesn't exist. GENETRATION ABORTED");
            }
        }
    }

    private static void usage() {
        System.out.print("Usage : Experiment dev" + "\n" +
                "This class is use to extract data from ATSV, convert and generate to a html_player" + "\n\n" +
                "Parameters usage (case sensitive):" + "\n" +
                "\t--inputFolder" + "\t\t" + "(mandatory)(absolute or relative path) : parameter used to specify" + "\n" +
                "\t  input" + "\t\t\t\t" + "the input folder where the system can find the ATSV file" + "\n" +
                "\t--testName" + "\t\t\t" + "(mandatory) : ATSV file name without extension" + "\n" +
                "\t--outputFolder" + "\t\t" + "(optional)(absolute or relative path) : parameter used to" + "\n" +
                "\t  output" + "\t\t\t" + "specify the output folder of generated player" + "\n" +
                "\n\n\n\n");
    }

    public static void createHTMLPlayerReport(ExecutionLogger logger, Path in, String qualifiedName, Path... out) {

        actionsData = new HashMap<>();
        executionData = new HashMap<>();
        testData = new HashMap<>();
        actionsImg = new HashMap<>();

        if (in != null && in.toFile().isDirectory()) {
            outputFolder = out.length > 0 ? out[0] : in;
        } else {
        	logger.sendError("input folder doesn't exist", (in!=null)?in.toString():"");
            return;
        }

        if (qualifiedName != "") {
            c_testName = qualifiedName;
        }

        final File atsvFile = in.resolve(qualifiedName + Script.ATS_VISUAL_FILE_EXTENSION).toFile();

        if (atsvFile.exists()) {

            final File xmlFolder = in.resolve(qualifiedName + "_xml").toFile();
            logger.sendInfo("create Atsv-Html player report", xmlFolder.getAbsolutePath());

            try {

                FileInputStream fis;
                AMF3Deserializer amf3;

                try {

                    fis = new FileInputStream(atsvFile);
                    amf3 = new AMF3Deserializer(fis);

                    //------------------------------------------------------------------------------------------------------
                    // General data
                    //------------------------------------------------------------------------------------------------------

                    final VisualReport report = (VisualReport) amf3.readObject();
                    reportDataStruct(report);
                    testDataStruct(report);

                    //------------------------------------------------------------------------------------------------------
                    // Actions extract
                    //------------------------------------------------------------------------------------------------------

                    int actionsNum = 0;

                    try {
                    	amf3.readObject(); // should be startscript action
                    }catch (Exception e){}

                    while (amf3.available() > 0) {

                        final Object obj = amf3.readObject();

                        if (obj instanceof VisualAction) {

                            final VisualAction va = (VisualAction) obj;

                            actionData = new HashMap<>();
                            actionDataStruct(va);

                            VisualElement ve;
                            if ((ve = va.getElement()) != null) {
                                addActionData("element", elementDataStruct(ve));
                            }

                            addActionsDataStruct(String.valueOf(actionsNum), getStringElement(actionData, false));
                            addActionsImg(String.valueOf(actionsNum), imagesDataStruct(va));
                            actionsNum++;

                        } else if (obj instanceof TestSummary) {
                            final TestSummary reportSummary = (TestSummary) obj;
                            summaryStruct(reportSummary);
                        }
                    }
                } catch (IOException e1) {
                    logger.sendError("ATSV report file error ->", e1.getMessage());
                } catch (Exception e2) {
                    logger.sendError("ATSV report exception ->", e2.getMessage());
                }
            } catch (Exception e4) {
                logger.sendError("ATSV report parser error ->", e4.getMessage());
            }

            copyRessources(logger);
            extractFolderIcons("reports/playerHTML/icons", "png");
            extractFolderIcons("reports/playerHTML/icons", "gif");
            addDefaultRessources();
            writeHTMLFile();

            logger.sendInfo("HTML_player generated", xmlFolder.getAbsolutePath());
        } else {

            logger.sendError("no ATSV file, XML actions file cannot be generated", atsvFile.getAbsolutePath());
        }
    }

    private static void reportDataStruct(VisualReport report) {
        executionData.put("cpuSpeed", JSData(report.getCpuSpeed()));
        executionData.put("cpuCount", JSData(report.getCpuCount()));
        executionData.put("totalMemory", JSData(report.getTotalMemory()));
        executionData.put("osInfo", JSData(report.getOsInfo()));
    }

    private static void testDataStruct(VisualReport report) {

        testData.put("testId", JSData(report.getId()));
        testData.put("testName", JSData(report.getScript()));
        testData.put("externalId", JSData(report.getExternalId()));
        testData.put("description", JSData(report.getDescription()));
        testData.put("author", JSData(report.getAuthor()));
        testData.put("prerequisite", JSData(report.getPrerequisite()));
        testData.put("started", JSData(Long.valueOf(report.getStarted())));
        testData.put("quality", JSData(report.getQuality()));
        testData.put("groups", JSData(report.getGroups()));
    }

    private static void addTestData(String key, String value) {
        testData.put(key, value);
    }

    private static void addActionData(String key, String value) {
        actionData.put(key, value);
    }

    private static void actionDataStruct(VisualAction va) {

        actionData.put("index", JSData(va.getIndex()));
        actionData.put("type", JSData(va.getType()));
        actionData.put("line", JSData(va.getLine()));
        actionData.put("script", JSData(va.getScript()));
        actionData.put("timeLine", JSData(va.getTimeLine()));
        actionData.put("error", JSData(va.getError()));
        actionData.put("stop", JSData(va.isStop()));
        actionData.put("duration", JSData(va.getDuration()));
        actionData.put("passed", (JSData((va.getError() == 0))));
        actionData.put("value", JSData(va.getValue()));
        actionData.put("data", JSData(va.getData()));

//        if (channelList.contains(va.getType())) {
        actionData.put("Channel", channelDataStruct(va));
//        }

    }

    private static void addActionsDataStruct(String key, String value) {
        actionsData.put(key, value);
    }

    private static void addActionsImg(String key, String value) {
        actionsImg.put(key, value);
    }

    private static String imagesDataStruct(VisualAction va) {

        Map<String, String> imagesData = new HashMap<>();

        imagesData.put("imageType", JSData(va.getImageType()));

        Map<String, String> imageData = new HashMap<>();

        int index = 0;

        for (byte[] img : va.getImages()) {
            if(img !=null) {
                imageData.put(String.valueOf(index), JSData(Base64.getEncoder().encodeToString(img)));
                index++;
            }
        }

        imagesData.put("numImages", JSData(index));

        imagesData.put("imagesPool", getStringElement(imageData, false));

        //System.out.println("imagesData : " +  getStringElement(imagesData));

        return getStringElement(imagesData, false);
    }

    private static String channelDataStruct(VisualAction va) {

        Map<String, String> channelData = new HashMap<>();

        channelData.put("name", JSData(va.getChannelName()));
        channelData.put("bound", boundDataStruct(va.getChannelBound()));

        return getStringElement(channelData, false);
    }

    private static String boundDataStruct(TestBound bound) {

        Map<String, String> boundData = new HashMap<>();

        boundData.put("x", JSData(bound.getX().intValue()));
        boundData.put("y", JSData(bound.getY().intValue()));
        boundData.put("width", JSData(bound.getWidth().intValue()));
        boundData.put("height", JSData(bound.getHeight().intValue()));

        return getStringElement(boundData, false);
    }

    private static String elementDataStruct(VisualElement ve) {

        Map<String, String> elementData = new HashMap<>();

        elementData.put("tag", JSData(ve.getTag()));
        elementData.put("criterias", JSData(ve.getCriterias()));
        elementData.put("foundElements", JSData(ve.getFoundElements()));
        elementData.put("searchDuration", JSData(ve.getSearchDuration()));
        elementData.put("bound", boundDataStruct(ve.getRectangle()));

        if (ve.getHpos() != null && ve.getVpos() != null) {
            elementData.put("extra", "{hPos : " + JSData(ve.getHpos()) + ",hVal : " + JSData(ve.getHposValue()) +
                    ",vPos : " + JSData(ve.getVpos()) + ",vVal : " + JSData(ve.getVposValue()) + "}");
        }

        return getStringElement(elementData, false);
    }

    private static void summaryStruct(TestSummary summary) {

        addTestData("actions", JSData(summary.getActions()));
        addTestData("suiteName", JSData(summary.getSuiteName()));
        addTestData("status", JSData(summary.getStatus()));
        addTestData("data", JSData(summary.getSummary()));

        if (summary.getStatus() == 0 && summary.getError() != null) {
        	final TestError scriptError = summary.getError();
            addTestData("error", "{\nscript : " + JSData(scriptError.getScriptLine()) +
                    ", line : " + JSData(scriptError.getLine()) +
                    ", message : " + JSData(scriptError.getMessage()) + "\n}");
        }
    }

    private static String JSData(Object obj) {

        String data = "";

        if (obj instanceof java.lang.String) {
            String tmp = (String) obj;
            if ((tmp.lastIndexOf("\\") != -1) && (tmp.lastIndexOf("\\") == tmp.length() - 1)) {
                tmp = tmp.substring(0, tmp.length() - 2) + "&#92;";
            }
            StringBuilder SB = new StringBuilder();
            {
                SB.append("\"")
                        .append(tmp.replaceAll("\"", "'"))
                        .append("\"");
            }
            data = SB.toString();

        } else if (obj instanceof java.lang.Integer || obj instanceof java.lang.Long) {
            data = "" + obj + "";
        } else if (obj instanceof java.lang.Boolean) {
            data = (Boolean) obj ? "1" : "0";
        } else {
            data = "\"\"";
        }

        return StandardCharsets.UTF_8.decode(StandardCharsets.UTF_8.encode(data)).toString();
    }

    private static String getStringElement(final Map<String, String> data, boolean enter) {

        final StringBuilder stringData = new StringBuilder("{");
        final Set<String> elements = data.keySet();

        boolean first = true;

        for (String element : elements) {
            String value = data.get(element);
            if (!first) {
                stringData.append(",");
            } else {
                first = false;
            }
            if (enter) {
                stringData.append("\n");
            }
            stringData.append(element).append(" : ").append(value);
        }

        if(stringData.length() == 1) {
        	return null;
        }

        return stringData.append("}").toString();
    }

    private static void writeHTMLFile() {
        try {
            PrintWriter htmlOPS = new PrintWriter(new OutputStreamWriter(new FileOutputStream(outputFolder.resolve(getHtmlPlayerName(c_testName)).toFile()), StandardCharsets.UTF_8));

            if (developper) {
                String date = new java.text.SimpleDateFormat("yyyy-MM-dd_hh-mm").format(new java.util.Date(Long.valueOf(testData.get("started"))));
                Path devPath = Paths.get(outputFolder.toString() + "\\" + date);
                System.out.println("path dev folder :" + devPath.toString());
                Files.createDirectories(devPath);
                devData = new PrintWriter(new OutputStreamWriter(new FileOutputStream(devPath.resolve("data.js").toFile()), StandardCharsets.UTF_8));
                devImg = new PrintWriter(new OutputStreamWriter(new FileOutputStream(devPath.resolve("img.js").toFile()), StandardCharsets.UTF_8));
            }

            try {
                Files.deleteIfExists(outputFolder.resolve(HTML_TEMPLATE_NAME));
            } catch (IOException e) {
            	htmlOPS.close();
                throw new RuntimeException(e);
            }

            InputStream htmlIPStream = new FileInputStream(copyResource(HTML_TEMPLATE_NAME, outputFolder));
            BufferedReader htmlTemplate = new BufferedReader(new InputStreamReader(htmlIPStream));

            String line;

            while ((line = htmlTemplate.readLine()) != null) {

                if (line.contains("!%ATS%GSAP%ATS%!")) {
                    writeIntegratedFile(htmlOPS, "script", GSAP_FILE_NAME, CUSTOM_GSAP_FOLDER);
                } else if (line.contains("!%ATS%GSAP_STP%ATS%!")) {
                    writeIntegratedFile(htmlOPS, "script", GSAP_STP_FILE_NAME, CUSTOM_GSAP_FOLDER);
                } else if (line.contains("!%ATS%CSS%ATS%!")) {
                    writeIntegratedFile(htmlOPS, "style", CSS_FILE_NAME);
                } else if (line.contains("!%ATS%JSCODE%ATS%!")) {
                    writeIntegratedFile(htmlOPS, "script", JSCODE_FILE_NAME);
                } else if (line.contains("!%ATS%JSICONS%ATS%!")) {
                    //writeIntegratedFile(htmlOPS, "script", JSICONS_FILE_NAME, CUSTOM_ICONS_FOLDER);
                    StringBuilder data = new StringBuilder();
                    {
                        data.append("const icons = ").append(getStringElement(iconsData, true)).append("\n");
                    }
                    htmlOPS.write("\t<script > // auto-generated section\n");
                    htmlOPS.write(data.toString());
                    htmlOPS.write("\n\t /* end of auto-integrated section */ </script>\n\n");

                } else if (line.contains("!%ATS%JSDATA%ATS%!")) {
                    StringBuilder data = new StringBuilder();
                    {
                        data.append("const execution = " + getStringElement(executionData, false) + "\n");
                        data.append("const test = " + getStringElement(testData, false) + "\n");
                        data.append("const actions = " + getStringElement(actionsData, true) + "\n");
                        data.append("const customsImg = " + getStringElement(customImg, true) + "\n");
                        data.append("dataLoaded = true;\n");
                    }
                    htmlOPS.write("\t<script > // auto-generated section\n");
                    htmlOPS.write(data.toString());
                    htmlOPS.write("\n\t /* end of auto-integrated section */ </script>\n\n");
                    htmlOPS.flush();
                    if (developper && devData != null) {
                        devData.write(data.toString());
                        devData.flush();
                    }

                } else if (line.contains("!%ATS%JSIMG%ATS%!")) {
                    writeImgStock(htmlOPS);
                } else {
                    htmlOPS.write(line);
                    htmlOPS.write('\n');
                    htmlOPS.flush();
                }
            }

            htmlIPStream.close();
            htmlTemplate.close();
            htmlOPS.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            Files.deleteIfExists(outputFolder.resolve(HTML_TEMPLATE_NAME));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


        if (developper) {
            if (devData != null) {
                devData.close();
            }
            if (devImg != null) {
                devImg.close();
            }
        }
    }

    public static String getHtmlPlayerName(String name) {
        final StringBuilder sb =
                new StringBuilder(name).append("_").append(HTML_PLAYER_NAME);
        return sb.toString();
    }

    private static File copyResource(String resName, Path dest) {

        Path filePath = dest.resolve(resName);

        try {
            if (!Files.exists(filePath)) {
                InputStream is = ResourceContent.class.getResourceAsStream(CUSTOM_TEMPLATES_FOLDER + resName);

                InputStream in = new BufferedInputStream(is);

                File targetFile = dest.resolve(resName).toFile();
                OutputStream out = new BufferedOutputStream(new FileOutputStream(targetFile));
                byte[] buffer = new byte[1024];
                int lengthRead;

                while ((lengthRead = in.read(buffer)) > 0) {
                    out.write(buffer, 0, lengthRead);
                    out.flush();
                }

                in.close();
                is.close();
                out.close();
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return filePath.toFile();
    }

    private static long writeIntegratedFile(PrintWriter out, String type, String fileToIntegrate, String... specificInputPath) {

        String inputResourceFolder = "";
        inputResourceFolder = specificInputPath.length > 0 ? specificInputPath[0] : CUSTOM_TEMPLATES_FOLDER;

        InputStream is = ResourceContent.class.getResourceAsStream(inputResourceFolder + fileToIntegrate);
        InputStream in = new BufferedInputStream(is);
        byte[] buffer = new byte[1024];
        long byteRead = -1;
        int lengthRead;

        out.write(("\n\t<" + type + "> /* auto-integrated section */ \n"));
        out.flush();

        String line;
        try {
            while ((lengthRead = in.read(buffer)) > 0) {
                byteRead += lengthRead;
                line = new String(buffer);
                line = lengthRead != 1024 ? line.substring(0, lengthRead - 1) : line;
                out.write(line);
                out.flush();
                buffer = new byte[1024];
            }

            out.write(("\n\t/* end of auto-integrated section */ </" + type + ">\n"));
            out.flush();

            in.close();
            is.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return byteRead;
    }

    // function to convert icons to BASE64 files.
    private static void writeJSIconsFile() {

        OutputStream IconsOPS = null;
        try {
            IconsOPS = new BufferedOutputStream(new FileOutputStream(Path.of(
                    "T:\\env\\ATS\\ats-core\\src\\main\\resources\\reports\\playerHTML\\icons\\iconsData.js").toFile()));

            List<String> iconsPath = new ArrayList<>();

            Map<String, String> icons = new HashMap<>();
            Map<String, String> data = new HashMap<>();

            Files.list(new File(inputFolder + "player\\32").toPath()).forEach(path -> {
                iconsPath.add(path.toString());
                String fileNameExt = path.toString().substring(path.toString().lastIndexOf("\\") + 1);
                String fileName = fileNameExt.substring(0, fileNameExt.lastIndexOf("."));
                String format = fileNameExt.substring((fileNameExt.lastIndexOf(".") + 1));

                try {
                    icons.put(fileName.replaceAll("-", "_"),
                            JSData("data:image/" + format + ";base64," + Base64.getEncoder().encodeToString(Files.readAllBytes(path))));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });

            data.put("player_32", getStringElement(icons, true));

            Files.list(new File(inputFolder + "actions\\32").toPath()).forEach(path -> {
                String fileNameExt = path.toString().substring(path.toString().lastIndexOf("\\") + 1);
                String fileName = fileNameExt.substring(0, fileNameExt.lastIndexOf("."));
                String format = fileNameExt.substring((fileNameExt.lastIndexOf(".") + 1));

                try {
                    icons.put(fileName.replaceAll("-", "_"),
                            JSData("data:image/" + format + ";base64," + Base64.getEncoder().encodeToString(Files.readAllBytes(path))));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });

            data.put("actions_32", getStringElement(icons, true));

            Files.list(new File(inputFolder + "actions\\24").toPath()).forEach(path -> {
                String fileNameExt = path.toString().substring(path.toString().lastIndexOf("\\") + 1);
                String fileName = fileNameExt.substring(0, fileNameExt.lastIndexOf("."));
                String format = fileNameExt.substring((fileNameExt.lastIndexOf(".") + 1));

                try {
                    icons.put(fileName.replaceAll("-", "_"),
                            JSData("data:image/" + format + ";base64," + Base64.getEncoder().encodeToString(Files.readAllBytes(path))));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });

            data.put("actions_24", getStringElement(icons, true));

            Files.list(new File(inputFolder + "actions\\20").toPath()).forEach(path -> {
                String fileNameExt = path.toString().substring(path.toString().lastIndexOf("\\") + 1);
                String fileName = fileNameExt.substring(0, fileNameExt.lastIndexOf("."));
                String format = fileNameExt.substring((fileNameExt.lastIndexOf(".") + 1));

                try {
                    icons.put(fileName.replaceAll("-", "_"),
                            JSData("data:image/" + format + ";base64," + Base64.getEncoder().encodeToString(Files.readAllBytes(path))));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });

            data.put("actions_20", getStringElement(icons, true));

            Files.list(new File(inputFolder + "actions\\16").toPath()).forEach(path -> {
                String fileNameExt = path.toString().substring(path.toString().lastIndexOf("\\") + 1);
                String fileName = fileNameExt.substring(0, fileNameExt.lastIndexOf("."));
                String format = fileNameExt.substring((fileNameExt.lastIndexOf(".") + 1));

                try {
                    icons.put(fileName.replaceAll("-", "_"),
                            JSData("data:image/" + format + ";base64," + Base64.getEncoder().encodeToString(Files.readAllBytes(path))));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });

            data.put("actions_16", getStringElement(icons, true));


            IconsOPS.write("const icons = ".getBytes());
            IconsOPS.write(getStringElement(data, true).getBytes());
            IconsOPS.write('\n');
            IconsOPS.flush();
            IconsOPS.write('\n');
            IconsOPS.flush();

            IconsOPS.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void writeImgStock(PrintWriter outputFile) {

        int size = actionsImg.size();
        StringBuilder imgData = new StringBuilder();
        for (int index = 0; index < size; index++) {
            imgData = new StringBuilder();
            outputFile.write("<script>");
            imgData.append("imgStock[" + String.valueOf(index) + "] = " + actionsImg.get(String.valueOf(index)) + ";\n");
            imgData.append("imgStockLoaded = " + String.valueOf(index + 1) + ";");
            outputFile.write(imgData.toString());
            outputFile.write("</script>\n");
            outputFile.flush();
            if (developper && devImg != null) {
                devImg.write(imgData.toString());
                devImg.flush();
            }
        }
    }

    private static void copyRessources(ExecutionLogger logger) {
        if (Files.exists(imagesPath)) {
            for (String img : imagesList) {
                copyRessourcesImage(logger, imagesPath.resolve(img));
            }

        } else {
        	logger.sendInfo("No image folder found", imagesPath.toString());
        }
    }

    private static void extractFolderIcons(String prePath, String ext) {

        List<String> resToCopy = extractJarFolderRessourcesPath(prePath, ext);

        while (!resToCopy.isEmpty()) {
            String subPath = resToCopy.get(0).substring(0, resToCopy.get(0).lastIndexOf("/"));
            int loopSize = resToCopy.size();
            int index = 0;

            Map<String, String> icons = new HashMap<>();

            while (index < loopSize) {

                String path = resToCopy.get(index);
                String subPathData = path.substring(0, path.lastIndexOf("/"));
                if (subPathData.startsWith(subPath)) {
                    byte[] imgBytes = null;
                    try {
                        imgBytes = ResourceContent.class.getResourceAsStream("/" + path).readAllBytes();
                        String fileName = path.substring(path.lastIndexOf("/") + 1, path.lastIndexOf("."));
                        icons.put(fileName.replaceAll("/", "_").replaceAll("-", "_"),
                                JSData("data:image/" + ext + ";base64," + getBase64DefaultImages(imgBytes)));
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    resToCopy.remove(path);
                    loopSize--;
                } else {
                    index++;
                }
            }

            String key = subPath.replace(prePath, "").substring(1).replaceAll("/", "_");

            if (iconsData.containsKey(key)) {
                String data = iconsData.get(key).substring(2, iconsData.get(key).lastIndexOf("}"));
                String[] table = data.split(",\n");
                for (int iconInd = 0; iconInd < table.length; iconInd++) {
                    String ligne = table[iconInd];
                    int indCar = ligne.indexOf(":");
                    String iconName = ligne.substring(0, indCar - 1);
                    String iconData = iconInd == table.length - 1 ? ligne.substring(indCar + 1) : ligne.substring(indCar + 2);
                    icons.put(iconName, iconData);
                }
            }
            iconsData.put(key, getStringElement(icons, true));
        }
    }

    /*private static void copyRessourcesFolder(Path path) {
        if (Files.exists(path)) {
            for (File f : path.toFile().listFiles()) {
                if (f.isFile()) {
                    String fileName = f.getName().substring(0, f.getName().lastIndexOf("."));
                    String imgData = getImg64(path, f.getName());
                    customImg.put(fileName.replaceAll("-", "_"), JSData(imgData));
                } else if (f.isDirectory()) {
                    try {
                        Files.createDirectories(outputFolder.resolve(f.getName()));
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    copyRessourcesFolder(path.resolve(f.getName()));
                }
            }
        }
    }*/

    private static void copyRessourcesImage(ExecutionLogger logger, Path path) {
        if (Files.exists(path)) {
            File f = path.toFile();
            if (f.isFile()) {
                final String fileName = f.getName().substring(0, f.getName().lastIndexOf("."));
                final String imgData = getImg64(path.getParent(), f.getName());
                customImg.put(fileName.replaceAll("-", "_"), JSData(imgData));
            } else {
            	logger.sendInfo("path is not a file", path.toString());
            }
        }
    }

    private static List<String> extractJarFolderRessourcesPath(String prePathFolder, String ext) {
        final CodeSource src = ResourceContent.class.getProtectionDomain().getCodeSource();
        final List<String> list = new ArrayList<>();

        if (src != null) {
            final URL jar = src.getLocation();
            ZipInputStream zip = null;
            try {
                zip = new ZipInputStream(jar.openStream());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            ZipEntry ze = null;

            while (true) {
                try {
                    if (!((ze = zip.getNextEntry()) != null)) break;
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                final String entryName = ze.getName();
                if (entryName.startsWith(prePathFolder) && entryName.endsWith("." + ext)) {
                    list.add(entryName);
                }
            }
        }
        return list;
    }

    private static String getImg64(Path path, String imgFileName) {

        String imgFile = "";

        byte[] imgBytes = null;
        String format = imgFileName.substring((imgFileName.lastIndexOf(".") + 1));
        if (Files.exists(path.resolve(imgFileName))) {
            try {
                format = imgFileName.substring((imgFileName.lastIndexOf(".") + 1));
                imgBytes = Files.readAllBytes(path.resolve(imgFileName));
                imgFile = "data:image/" + format + ";base64," + getBase64DefaultImages(imgBytes);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            try {
                imgBytes = ResourceContent.class.getResourceAsStream(CUSTOM_IMAGES_FOLDER + imgFileName).readAllBytes();
                imgFile = "data:image/" + format + ";base64," + getBase64DefaultImages(imgBytes);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        return imgFile;
    }

    private static String getBase64DefaultImages(byte[] b) throws IOException {
        return Base64.getEncoder().encodeToString(b);
    }

    private static void addDefaultRessources() {

        byte[] imgBytes = null;
        try {
            imgBytes = ResourceContent.class.getResourceAsStream("/reports/images/watermark.png").readAllBytes();
            customImg.put("defaultWM", JSData("data:image/png;base64," + getBase64DefaultImages(imgBytes)));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}