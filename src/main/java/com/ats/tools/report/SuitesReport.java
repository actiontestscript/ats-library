package com.ats.tools.report;

import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ats.script.Project;

public class SuitesReport {
	public String projectId;
	public String projectUuid;
	public String projectName;
	public String projectVersion;
	public String projectDescription;
	public SuitesReportItem[] suites;
	public long started;

	private static final Pattern VERSION_REGEX = Pattern.compile("(.*)\\(([^)]+)\\)");

	public SuitesReport(SuitesReportItem suite) {

		this.suites = new SuitesReportItem[] {suite};
		this.projectId = suite.getProjectId();
		this.projectUuid = suite.getProjectUuid();
		this.started = suite.getStarted();

		final Matcher m = VERSION_REGEX.matcher(this.projectId);
		if(m.find()) {
			this.projectName = m.group(1);
			this.projectVersion = m.group(2);
		}
		
		try {
			final Document doc = Project.getProjectProperties();
			if(doc != null) {
				final Element root = doc.getDocumentElement();
				final NodeList desc = root.getElementsByTagName("description");
				if(desc != null && desc.getLength() > 0) {
					this.projectDescription = desc.item(0).getTextContent();
				}
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {	}
	}

	public void add(SuitesReportItem suite) {
		this.suites = Stream.concat(Arrays.stream(this.suites), Arrays.stream(new SuitesReportItem[] {suite})).toArray(SuitesReportItem[]::new);
	}
}