package com.ats.tools.report.models;

import com.ats.recorder.TestError;

import javax.xml.stream.XMLStreamReader;
import java.util.List;

public class Summary {
	private String actions;
	private String duration;
	private String status;
	private String suiteName;
	private String testName;
	private String data;
	private String error;
	private String errorLine;
	private String errorScriptName;
	private List<TestError> testErrors;

	public Summary() {
	}

	;

	public Summary(XMLStreamReader reader) {
		for (int i = 0; i < reader.getAttributeCount(); i++) {
			String attributeName = reader.getAttributeName(i).getLocalPart();
			switch (attributeName) {
				case "actions": {
					this.actions = reader.getAttributeValue("", attributeName);
					break;
				}
				case "duration": {
					this.duration = reader.getAttributeValue("", attributeName);
					break;
				}
				case "status": {
					this.status = reader.getAttributeValue("", attributeName);
					break;
				}
				case "suiteName": {
					this.suiteName = reader.getAttributeValue("", attributeName);
					break;
				}
			}
		}
	}

	public String getActions() {
		return actions;
	}

	public void setActions(String actions) {
		this.actions = actions;
	}

	public String getDuration() {
		return duration == null ? "0" : duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSuiteName() {
		return suiteName;
	}

	public void setSuiteName(String suiteName) {
		this.suiteName = suiteName;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getError() {
		return this.error;
	}

	public void setErrorLine(String errorLine) {
		this.errorLine = errorLine;
	}

	public String getErrorLine() {
		return this.errorLine;
	}

	public void setErrorScriptLine(String errorScriptName) {
		this.errorScriptName = errorScriptName;
	}

	public String getErrorScriptName() {
		return errorScriptName;
	}

	public List<TestError> getTestErrors() {
		return testErrors;
	}

	public void setTestErrors(List<TestError> testErrors) {
		this.testErrors = testErrors;
	}
}