package com.ats.tools.report.models;

import com.ats.tools.Utils;
import org.apache.commons.lang3.StringUtils;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class TestInfo {
    public static final String DATE_TIME_PATTERN = "yyyy-MM-dd 'at' HH:mm:ss";
    private String cpuCount;
    private String cpuSpeed;
    private String externalId;
    private String osInfo;
    private String testId;
    private String testName;
    private String totalMemory;
    private String description;
    private String author;
    private String prerequisite;
    private String started;
    private String startedFormatted;
    private String quality;
    private String durationChart;
    private String modifiedBy;
    private String modifiedAt;
    private String createdAt;
    private List<String> groups = new ArrayList<>();
    private HtmlReportProject project;
    private Summary summary;

    @Override
    public String toString() {
        return "TestRecord{" +
                "cpuCount=" + cpuCount +
                ", cpuSpeed=" + cpuSpeed +
                ", externalId='" + externalId + '\'' +
                ", osInfo='" + osInfo + '\'' +
                ", testId='" + testId + '\'' +
                ", testName='" + testName + '\'' +
                ", totalMemory=" + totalMemory +
                ", description='" + description + '\'' +
                ", author='" + author + '\'' +
                ", prerequisite='" + prerequisite + '\'' +
                ", started=" + started +
                ", startedFormatted='" + startedFormatted + '\'' +
                ", quality=" + quality +
                ", groups=" + groups +
                ", project=" + project +
                ", summary=" + summary +
                '}';
    }

    public String getCpuCount() {
        return cpuCount;
    }

    public void setCpuCount(String cpuCount) {
        this.cpuCount = cpuCount;
    }

    public String getCpuSpeed() {
        return cpuSpeed;
    }

    public void setCpuSpeed(String cpuSpeed) {
        this.cpuSpeed = cpuSpeed;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getOsInfo() {
        return osInfo;
    }

    public void setOsInfo(String osInfo) {
        this.osInfo = osInfo;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getTotalMemory() {
        return totalMemory;
    }

    public void setTotalMemory(String totalMemory) {
        this.totalMemory = totalMemory;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPrerequisite() {
        return prerequisite;
    }

    public void setPrerequisite(String prerequisite) {
        this.prerequisite = prerequisite;
    }

    public String getStarted() {
        return started;
    }

    public void setStarted(String started) {
        this.started = started;
    }

    public String getStartedFormatted() {
        return startedFormatted;
    }

    public void setStartedFormatted(String startedFormatted) {
        this.startedFormatted = startedFormatted;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    public HtmlReportProject getProject() {
        return project;
    }

    public void setProject(HtmlReportProject project) {
        this.project = project;
    }

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    public String getDurationChart() {
        return durationChart;
    }

    public void setDurationChart(String durationChart) {
        this.durationChart = durationChart;
    }

    public String getFinishedFormated() {
         return OffsetDateTime.ofInstant(Instant.ofEpochMilli(Utils.string2Long(getStarted()) + Utils.string2Long(summary.getDuration())),
                ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern(DATE_TIME_PATTERN));
    }

    public String getModifiedBy() {
        String modifiedByUser = null;
        if (StringUtils.isNotEmpty(modifiedBy)) {
            String[] userNames = modifiedBy.split(":");
            if (userNames.length > 2) {
                modifiedByUser = userNames[2];
            } else {
                modifiedByUser = userNames[1];
            }
        }
        return modifiedByUser;
    }

    public String getSystemUser() {
        return modifiedBy.split(":").length > 1 ? modifiedBy.split(":")[1] : null;
    }



    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModifiedAt() {
        return modifiedAt == null ? null : OffsetDateTime.ofInstant(Instant.ofEpochMilli(Utils.string2Long(modifiedAt)),
                ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern(DATE_TIME_PATTERN));
    }

    public void setModifiedAt(String modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public String getCreatedAt() {
        return createdAt == null ? null : OffsetDateTime.ofInstant(Instant.ofEpochMilli(Utils.string2Long(createdAt)),
                ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern(DATE_TIME_PATTERN));
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}

