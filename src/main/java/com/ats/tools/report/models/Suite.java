package com.ats.tools.report.models;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

public class Suite {
    private String devReportLvl;
    private String mgtReportLvl;
    private String validReportLvl;
    private List<String> tests;
    private String name;
    private String description;
    private int visualQuality;
    private boolean atsvHtml;
    private String dateOrder;
    private Map<String, String> parameters;
    private List<String> includedGroups;
    private List<String> excludedGroups;
    private boolean reporting;
    private long started;
    private SuiteInfo suiteInfo;
    private boolean useWebpFormat;

	/*public void create(String noSuiteName, String noSuiteTestName) {
		this.name = noSuiteTestName;
		this.tests = Arrays.asList(noSuiteTestName);
		this.devReportLvl = "3";
		this.mgtReportLvl = "3";
		this.validReportLvl = "1";
		this.description = "";
		this.visualQuality = 4;
		this.atsvHtml = true;
		this.reporting = true;
		
		this.suiteInfo = new SuiteInfo();
		this.suiteInfo.setStatus("PASSED");
		
		this.dateOrder = "";

		this.parameters = new HashMap<String, String>();
		this.includedGroups = Collections.emptyList();
		this.excludedGroups = Collections.emptyList();
	}*/

	public boolean isUseWebpFormat() {
		return useWebpFormat;
	}

	public void setUseWebpFormat(boolean useWebpFormat) {
		this.useWebpFormat = useWebpFormat;
	}

	public String getDevReportLvl() {
        return devReportLvl;
    }

    public void setDevReportLvl(String devReportLvl) {
        this.devReportLvl = devReportLvl;
    }

    public String getMgtReportLvl() {
        return mgtReportLvl;
    }

    public void setMgtReportLvl(String mgtReportLvl) {
        this.mgtReportLvl = mgtReportLvl;
    }

    public String getValidReportLvl() {
        return validReportLvl;
    }

    public void setValidReportLvl(String validReportLvl) {
        this.validReportLvl = validReportLvl;
    }

    public List<String> getTests() {
        return tests;
    }

    public void setTests(List<String> tests) {
        this.tests = tests;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getVisualQuality() {
        return visualQuality;
    }

    public void setVisualQuality(int visualQuality) {
        this.visualQuality = visualQuality;
    }

    public boolean isAtsvHtml() {
		return atsvHtml;
	}

	public void setAtsvHtml(boolean atsvHtml) {
		this.atsvHtml = atsvHtml;
	}

	public String getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(String dateOrder) {
        this.dateOrder = dateOrder;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public List<String> getIncludedGroups() {
        return includedGroups;
    }

    public void setIncludedGroups(List<String> includedGroups) {
        this.includedGroups = includedGroups;
    }

    public List<String> getExcludedGroups() {
        return excludedGroups;
    }

    public void setExcludedGroups(List<String> excludedGroups) {
        this.excludedGroups = excludedGroups;
    }

    public boolean isReporting() {
        return reporting;
    }

    public void setReporting(boolean reporting) {
        this.reporting = reporting;
    }

    public long getStarted() {
        return started;
    }

    public void setStarted(long started) {
        this.started = started;
    }

    public SuiteInfo getSuiteInfo() {
        return suiteInfo;
    }

    public void setSuiteInfo(SuiteInfo suiteInfo) {
        this.suiteInfo = suiteInfo;
    }

    public String getStartedFormatted() {
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(started), ZoneId.systemDefault());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd - HH:mm:ss");
        return localDateTime.format(formatter);
    }

    public String getStartedFormattedZulu() {
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(started), ZoneId.of("UTC"));
        return localDateTime.toString() + "Z";
    }
}
