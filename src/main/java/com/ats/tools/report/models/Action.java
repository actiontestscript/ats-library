package com.ats.tools.report.models;

public class Action {
    private int index;
    private String type;
    private int line;
    private String script;
    private long timeLine;
    private String error;
    private boolean stop;
    private int duration;
    private boolean passed;
    private String value;
    private String data;
    private Image image;
    private Channel channel;
    private AppDataJson appDataJson;
    private ActionElement actionElement;

    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return "Action{" +
                "index=" + index +
                ", type='" + type + '\'' +
                ", line=" + line +
                ", script='" + script + '\'' +
                ", timeLine=" + timeLine +
                ", error=" + error +
                ", stop=" + stop +
                ", duration=" + duration +
                ", passed=" + passed +
                ", value='" + value + '\'' +
                ", data='" + data + '\'' +
                ", image=" + image +
                ", channel=" + channel +
                ", appDataJson=" + appDataJson +
                ", actionElement=" + actionElement +
                '}';
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public long getTimeLine() {
        return timeLine;
    }

    public void setTimeLine(long timeLine) {
        this.timeLine = timeLine;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean isStop() {
        return stop;
    }

    public void setStop(boolean stop) {
        this.stop = stop;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isPassed() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public AppDataJson getAppDataJson() {
        return appDataJson;
    }

    public void setAppDataJson(AppDataJson appDataJson) {
        this.appDataJson = appDataJson;
    }

    public ActionElement getActionElement() {
        return actionElement;
    }

    public void setActionElement(ActionElement actionElement) {
        this.actionElement = actionElement;
    }
}