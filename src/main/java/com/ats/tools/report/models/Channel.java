package com.ats.tools.report.models;

public class Channel {
    private String name;
    private Bound bound;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bound getBound() {
        return bound;
    }

    public void setBound(Bound bound) {
        this.bound = bound;
    }

    @Override
    public String toString() {
        return "Channel{" +
                "name='" + name + '\'' +
                ", bound=" + bound +
                '}';
    }
}