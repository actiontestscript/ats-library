package com.ats.tools.report.models;

import java.util.List;

public class Results {
    private List<SuiteInfo> suites;
    private int suitesPassed;
    private int suitesFailed;
    private int suitesCount;
    private int testsCount;
    private long duration;
    private long started;
    private String status;

    public List<SuiteInfo> getSuites() {
        return suites;
    }
    public void setSuites(List<SuiteInfo> suites) {
        this.suites = suites;
    }

    public int getSuitesPassed() {
        return suitesPassed;
    }

    public void setSuitesPassed(int suitesPassed) {
        this.suitesPassed = suitesPassed;
    }

    public int getSuitesFailed() {
        return suitesFailed;
    }

    public void setSuitesFailed(int suitesFailed) {
        this.suitesFailed = suitesFailed;
    }

    public int getSuitesCount() {
        return suitesCount;
    }

    public void setSuitesCount(int suitesCount) {
        this.suitesCount = suitesCount;
    }

    public int getTestsCount() {
        return testsCount;
    }

    public void setTestsCount(int testsCount) {
        this.testsCount = testsCount;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getStarted() {
        return started;
    }

    public void setStarted(long started) {
        this.started = started;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
