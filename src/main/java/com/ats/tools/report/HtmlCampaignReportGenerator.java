package com.ats.tools.report;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.ats.recorder.TestError;
import com.ats.script.actions.ActionApi;
import com.ats.script.actions.ActionCallscript;
import com.ats.script.actions.ActionComment;
import com.ats.tools.ResourceContent;
import com.ats.tools.logger.levels.AtsLogger;
import com.ats.tools.report.actions.HtmlReportAction;
import com.ats.tools.report.analytics.ActionTypesEnum;
import com.ats.tools.report.general.HtmlReport;
import com.ats.tools.report.general.HtmlReportExecution;
import com.ats.tools.report.general.HtmlReportPlaylist;
import com.ats.tools.report.general.HtmlReportTestInfo;
import com.ats.tools.report.models.Action;
import com.ats.tools.report.models.ActionElement;
import com.ats.tools.report.models.AppDataJson;
import com.ats.tools.report.models.Bound;
import com.ats.tools.report.models.Channel;
import com.ats.tools.report.models.HtmlReportProject;
import com.ats.tools.report.models.Image;
import com.ats.tools.report.models.Results;
import com.ats.tools.report.models.Script;
import com.ats.tools.report.models.Suite;
import com.ats.tools.report.models.SuiteInfo;
import com.ats.tools.report.models.Summary;
import com.ats.tools.report.models.TestInfo;
import com.ats.tools.report.utils.AtsReportXmlGenerator;
import com.ats.tools.report.utils.FileUtils;
import com.ats.tools.report.utils.HtmlSummaryGenerator;
import com.ctc.wstx.api.WstxInputProperties;
import com.ctc.wstx.stax.WstxInputFactory;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

public class HtmlCampaignReportGenerator {

    public static final String FAIL_STATUS = "FAIL";
    public static final String PASS_STATUS = "PASS";
    private final Path outputFolderPath;
    private final File jsonSuiteFilesFile;
    private final int devReportLevel;
    private final int validReportLevel;

    private final SuitesReportItem suiteItem;

    private final boolean isValidationReport;
    private static final String styles = ResourceContent.getHtmlReportCss();
    private static final String javascript = ResourceContent.getHtmlReportJavascript();
    private static final String actionTemplate = ResourceContent.getHtmlReportActionHtmlTemplate();
    private static final String mainTemplate = ResourceContent.getHtmlReportTemplate();
    private static final String playlistTemplate = ResourceContent.getHtmlReportPlaylistTemplate();
    private static final String reportSuiteTemplate = ResourceContent.getHtmlReportSuiteTemplate();
    private static final String mainExecutionTemplate = ResourceContent.getHtmlReportExecutionTemplate();
    private static final String htmlSummaryTemplate = ResourceContent.getHtmlReportSummaryTemplate();
    private AtsReportXmlGenerator atsReportXmlGenerator;
    private Map<String, Integer> testSuiteActionsCount = new HashMap<>();
    private HtmlReportPlaylist htmlReportPlaylist;
    private ObjectMapper objectMapper = new ObjectMapper();
    private List<Summary> testSummariesForExecutions;
    private Map<String, Long> testDurations = new HashMap<>();
    // List of playlist data
    private List<PlaylistData> playlistsDataLst = new ArrayList<>();
    private PlaylistData playlistsDataCurrent = null;

    public static class InputFactory extends WstxInputFactory {
        public InputFactory() {
            super();
            setProperty(WstxInputProperties.P_MAX_ATTRIBUTE_SIZE, Integer.MAX_VALUE);
        }
    }

    public HtmlCampaignReportGenerator(Path outputFolderPath, File jsonSuiteFilesFile, int devReportLevel, int validReport, SuitesReportItem suiteItem) {
        this.outputFolderPath = outputFolderPath;
        this.jsonSuiteFilesFile = jsonSuiteFilesFile;
        this.devReportLevel = devReportLevel;
        this.validReportLevel = validReport;
        this.isValidationReport = validReport == 1;
        this.suiteItem = suiteItem;
    }

    public void generateSuitsHtmlReports() throws IOException, XMLStreamException {

        long startTime = System.nanoTime();

        List<Script> scripts = FileUtils.getScripts(outputFolderPath.toString());
        HtmlReportProject project = objectMapper.readValue(Files.readString(jsonSuiteFilesFile.toPath()), HtmlReportProject.class);
        Results results = objectMapper.readValue(outputFolderPath.resolve(AtsReport.ATS_RESULTS_JSON_FILE_NAME).toFile(), Results.class);

        atsReportXmlGenerator = new AtsReportXmlGenerator(project, results, scripts, devReportLevel, outputFolderPath, validReportLevel, AtsReport.ATS_TEST_REPORT_XML_NAME);
        atsReportXmlGenerator.writeProjectInfo();
        testSummariesForExecutions = atsReportXmlGenerator.getTestSummariesForExecutionsResult();

        HtmlReportExecution htmlReportExecution = new HtmlReportExecution(mainExecutionTemplate, project, outputFolderPath.toString(), devReportLevel, isValidationReport, suiteItem);
        htmlReportExecution.processMainExecutionsFile();

        project.getSuites().stream().forEach(suite -> {
            SuiteInfo suiteInfo = results.getSuites().stream()
                    .sorted((o1, o2) -> Math.toIntExact(o2.getStarted() - o1.getStarted()))
                    .filter(s -> s.getName().equals(suite.getName())).findFirst().orElse(null);
            if (suiteInfo != null) {
                suite.setSuiteInfo(suiteInfo);
            }
        });
        project.getSuites().forEach(suite -> generateHtmlReports(suite, htmlReportExecution.getFileWriter(), htmlReportExecution.getValidationFileWriter(), project, scripts));

        AddDataGraph(playlistsDataLst, htmlReportExecution.getFileWriter());
        htmlReportExecution.closeWriter();
        atsReportXmlGenerator.writeFileEnd();

        if (!suiteItem.isNoSuiteLaunch()) {
            HtmlSummaryGenerator.generateHtmlSummary(htmlSummaryTemplate, project,
                    outputFolderPath.toString(), devReportLevel, testSuiteActionsCount, results, scripts, testDurations);
        }


        AtsLogger.printLog(
                new StringBuilder("report generated in ").
                        append((System.nanoTime() - startTime) / 1000000000).
                        append(" second(s)")
        );
    }

    private void generateHtmlReports(Suite suite, OutputStream fileWriter, OutputStream validationFileWriter, HtmlReportProject project, List<Script> scripts) {
        List<Script> suiteScripts = scripts.stream().filter(script -> script.getSuite().equals(suite.getName())).collect(Collectors.toList());

        String scriptsStatus = scripts.stream()
                .filter(script -> script.getSuite().equals(suite.getName()))
                .anyMatch(script -> !script.isPassed()) ? FAIL_STATUS : PASS_STATUS;
        //Creating the playlist data reservoir for graphs
        playlistsDataCurrent = new PlaylistData(suite.getName());

        //Create your playlist
        htmlReportPlaylist = new HtmlReportPlaylist(playlistTemplate, suite,
                fileWriter, validationFileWriter, project, suiteScripts,
                isValidationReport, suiteItem.isNoSuiteLaunch(), scriptsStatus);
        htmlReportPlaylist.processPlaylistData();
        playlistsDataCurrent.addTest(htmlReportPlaylist.getPassed(), htmlReportPlaylist.getFailed(), htmlReportPlaylist.getFiltered());
        playlistsDataCurrent.addDurationInSecond(htmlReportPlaylist.getDurationInSecond());
        atsReportXmlGenerator.writeSuiteData(suite, scripts);

        suite.getTests().forEach(test -> {
            generateHtmlReport(suite.getName(), test);
        });

        playlistsDataLst.add(playlistsDataCurrent);

        atsReportXmlGenerator.writeSuiteEnd();
        htmlReportPlaylist.processPlaylistFooter();
    }

    private void generateHtmlReport(String suiteName, String testName) {
        final Path targetPath = outputFolderPath.resolve(suiteName).resolve(testName + "_xml");

        if (!Files.exists(targetPath)) {//test has been filtered
            return;
        }

        Map<String, String> appIcons = new HashMap<>();
        boolean creatingObject = false;
        Action action = new Action();
        AppDataJson appDataJson = new AppDataJson();
        ActionElement actionElement = new ActionElement();
        TestInfo testInfo = new TestInfo();
        testInfo.setTestName(testName);

        try {
            System.getProperties().put("javax.xml.stream.XMLInputFactory", InputFactory.class.getName());

            Optional<Summary> first = testSummariesForExecutions.stream().filter(summary -> summary.getTestName().equals(testName)).findFirst();

            XMLInputFactory factory = XMLInputFactory.newInstance();
            InputStream inputStream = new FileInputStream(targetPath.resolve(XmlReport.REPORT_DATA_FILE).toFile());
            if (first.isPresent()) {
                atsReportXmlGenerator.writeScript(targetPath.resolve(AtsReport.ATS_TEST_REPORT_HTML_DATA_FILE_NAME).toFile(), devReportLevel);
            }
            XMLStreamReader reader = factory.createXMLStreamReader(inputStream);
            OutputStream outputStream = new FileOutputStream(targetPath.resolve(AtsReport.ATS_TEST_REPORT_HTML).toFile());

            HtmlReport htmlReport = createReportHeader();
            HtmlReportAction htmlReportAction = new HtmlReportAction();
            boolean isTestInfo = false;

            while (reader.hasNext()) {
                int event = reader.next();
                if (event == XMLStreamConstants.START_DOCUMENT) {
                    continue;
                }

                if (event == XMLStreamConstants.START_ELEMENT) {
                    if (reader.getLocalName().equals("action")) {
                        isTestInfo = false;
                        action.setIndex(Integer.parseInt(reader.getAttributeValue(0)));
                        String type = reader.getAttributeValue(1).split("\\.")[reader.getAttributeValue(1).split("\\.").length - 1];
                        action.setType(type);
                        creatingObject = true;
                    } else if (!creatingObject && reader.getLocalName().equals("script")) {
                        creatingObject = true;
                        isTestInfo = true;
                    }
                    if (creatingObject) {
                        if (isTestInfo) {
                            switch (reader.getLocalName()) {
                                case "script": {
                                    testInfo.setExternalId(reader.getAttributeValue("", "externalId"));
                                    testInfo.setTestId(reader.getAttributeValue("", "testId"));
                                    testInfo.setOsInfo(reader.getAttributeValue("", "osInfo"));
                                    break;
                                }
                                case "description": {
                                    testInfo.setDescription(reader.getElementText());
                                    break;
                                }
                                case "author": {
                                    testInfo.setAuthor(reader.getElementText());
                                    break;
                                }
                                case "modifiedBy": {
                                    testInfo.setModifiedBy(reader.getElementText());
                                    break;
                                }
                                case "modifiedAt": {
                                    testInfo.setModifiedAt(reader.getElementText());
                                    break;
                                }
                                case "createdAt": {
                                    testInfo.setCreatedAt(reader.getElementText());
                                    break;
                                }
                                case "prerequisite": {
                                    testInfo.setPrerequisite(reader.getElementText());
                                    break;
                                }
                                case "startedFormated": {
                                    testInfo.setStartedFormatted(reader.getElementText());
                                    break;
                                }
                                case "started": {
                                    testInfo.setStarted(reader.getElementText());
                                    break;
                                }
                                case "quality": {
                                    testInfo.setQuality(reader.getElementText());
                                    break;
                                }
                                case "group": {
                                    testInfo.getGroups().add(reader.getElementText());
                                    break;
                                }
                                case "project": {
                                    HtmlReportProject project = new HtmlReportProject();
                                    testInfo.setProject(project);
                                    break;
                                }
                                case "id": {
                                    testInfo.getProject().setProjectId(reader.getElementText());
                                    break;
                                }
                                case "name": {
                                    testInfo.getProject().setProjectName(reader.getElementText());
                                    break;
                                }
                                case "summary": {
                                    Summary summary = new Summary(reader);
                                    testInfo.setSummary(summary);
                                    break;
                                }
                                case "errors": {
                                    testInfo.getSummary().setTestErrors(new ArrayList<>());
                                    break;
                                }
                                case "error": {
                                    TestError error = new TestError();
                                    error.setLine(Integer.parseInt(reader.getAttributeValue("", "line")));
                                    error.setScript(reader.getAttributeValue("", "script"));
                                    error.setTestErrorStatus(TestError.TestErrorStatus.valueOf(reader.getAttributeValue("", "testErrorStatus")));
                                    error.setMessage(reader.getElementText());
                                    testInfo.getSummary().getTestErrors().add(error);
                                    break;
                                }
                                case "data": {
                                    testInfo.getSummary().setData(reader.getElementText());
                                    break;
                                }
                            }
                        }
                        if (!isTestInfo) {
                            switch (reader.getLocalName()) {
                                case "line": {
                                    action.setLine(Integer.parseInt(reader.getElementText()));
                                    break;
                                }
                                case "script": {
                                    action.setScript(reader.getElementText());
                                    break;
                                }
                                case "timeLine": {
                                    action.setTimeLine(Long.parseLong(reader.getElementText()));
                                    break;
                                }
                                case "error": {
                                    action.setError(reader.getElementText());
                                    break;
                                }
                                case "stop": {
                                    action.setStop(Boolean.parseBoolean(reader.getElementText()));
                                    break;
                                }
                                case "duration": {
                                    action.setDuration(Integer.parseInt(reader.getElementText()));
                                    break;
                                }
                                case "passed": {
                                    action.setPassed(Boolean.parseBoolean(reader.getElementText()));
                                    break;
                                }
                                case "value": {
                                    action.setValue(reader.getElementText());
                                    break;
                                }
                                case "data": {
                                    if (isTestInfo) {
                                        testInfo.getSummary().setData(reader.getElementText());
                                    } else {
                                        action.setData(reader.getElementText());
                                    }
                                    break;
                                }
                                case "parameter": {

                                    if (ActionApi.class.getSimpleName().equals(action.getType()) || ActionCallscript.class.getSimpleName().equals(action.getType())) {
                                        switch (reader.getAttributeCount()) {
                                            case 2: {
                                                appDataJson.getData().put(reader.getAttributeValue(0), reader.getAttributeValue(1));
                                                break;
                                            }
                                            case 3: {
                                                appDataJson.getData().put(reader.getAttributeValue(0), reader.getAttributeValue(2));
                                                break;
                                            }
                                            default: {
                                                appDataJson.getData().put(reader.getAttributeValue(0), "Ø");

                                            }
                                        }
                                    } else {
                                        switch (reader.getAttributeValue(0)) {
                                            case "name": {
                                                appDataJson.setName(reader.getAttributeValue(1));
                                                action.setAppDataJson(appDataJson);
                                                break;
                                            }
                                            case "app": {
                                                appDataJson.setApp(reader.getAttributeValue(1));
                                                break;
                                            }
                                            case "appVersion": {
                                                appDataJson.setAppVersion(reader.getAttributeValue(1));
                                                break;
                                            }
                                            case "os": {
                                                appDataJson.setOs(reader.getAttributeValue(1));
                                                break;
                                            }
                                            case "appIcon": {
                                                appDataJson.setAppIcon(reader.getAttributeValue(1));
                                                appIcons.put(appDataJson.getApp(), appDataJson.getAppIcon());
                                                break;
                                            }
                                        }
                                    }
                                    break;
                                }
                                case "img": {
                                    if (reader.getAttributeCount() > 0) {
                                        Image image = new Image(reader);
                                        if (image.getType().equals("api")) {
                                            image.useApiIcon();
                                        }
                                        action.setImage(image);
                                    }
                                    break;
                                }
                                case "channel": {
                                    Channel channel = new Channel();
                                    channel.setName(reader.getAttributeValue(0));
                                    action.setChannel(channel);
                                    break;
                                }
                                case "bound": {
                                    Bound bound = new Bound();
                                    action.getChannel().setBound(bound);
                                    break;
                                }
                                case "x": {
                                    action.getChannel().getBound().setX(Integer.parseInt(reader.getElementText()));
                                    break;
                                }
                                case "y": {
                                    action.getChannel().getBound().setY(Integer.parseInt(reader.getElementText()));
                                    break;
                                }
                                case "width": {
                                    action.getChannel().getBound().setWidth(Integer.parseInt(reader.getElementText()));
                                    break;
                                }
                                case "height": {
                                    action.getChannel().getBound().setHeight(Integer.parseInt(reader.getElementText()));
                                    break;
                                }

                                case "element": {
                                    actionElement.setTag(reader.getAttributeValue(0));
                                    break;
                                }

                                case "criterias": {
                                    actionElement.setCriterias(reader.getElementText());
                                    break;
                                }

                                case "foundElements": {
                                    actionElement.setFoundElements(Integer.parseInt(reader.getElementText()));
                                    break;
                                }

                                case "searchDuration": {
                                    actionElement.setSearchDuration(Integer.parseInt(reader.getElementText()));
                                    break;
                                }
                            }
                        }
                    }
                }

                if (event == XMLStreamConstants.END_ELEMENT && reader.getLocalName().equals("script") && isTestInfo) {
                    htmlReport.setMainReport(new HtmlReportTestInfo(testInfo, htmlReport.getMainReport(), null, suiteName, suiteItem).getResult());
                    String _actions = testInfo.getSummary().getActions();
                    playlistsDataCurrent.addNbTests(Integer.parseInt(_actions));
                    testSuiteActionsCount.put(testInfo.getTestName() + "|" + testInfo.getSummary().getSuiteName(), Integer.valueOf(testInfo.getSummary().getActions()));
                    testDurations.put(testInfo.getTestName() + "|" + testInfo.getSummary().getSuiteName(), Long.parseLong(testInfo.getSummary().getDuration()));
                    outputStream.write(htmlReport.getMainReport().getBytes());
                    htmlReportPlaylist.processPlaylistTestInfo(new HtmlReportTestInfo(testInfo, reportSuiteTemplate, targetPath.resolve(AtsReport.ATS_TEST_REPORT_HTML).toString(), suiteName, suiteItem).getResult());
                }

                if (event == XMLStreamConstants.END_ELEMENT && com.ats.script.actions.Action.DATA_JSON.equals(reader.getLocalName())) {
                    action.setAppDataJson(appDataJson);
                    appDataJson = new AppDataJson();
                }

                if (event == XMLStreamConstants.END_ELEMENT && reader.getLocalName().equals("element")) {
                    action.setActionElement(actionElement);
                    actionElement = new ActionElement();
                }

                if (event == XMLStreamConstants.END_ELEMENT && reader.getLocalName().equals("action")) {
                    String processedAction = htmlReportAction.processAction(action, appIcons, testInfo, devReportLevel);
                    outputStream.write(processedAction.getBytes());
                    htmlReportPlaylist.processPlaylistTestAction(processedAction, isValidationReport, (!ActionComment.class.getSimpleName().equals(action.getType()) && !ActionCallscript.class.getSimpleName().equals(action.getType())));
                    htmlReportAction.setActionTemplate(actionTemplate);

                    playlistsDataCurrent.addAction(action.getType());

                    action = new Action();
                }
            }

            outputStream.write(htmlReport.getFooter().getBytes());
            htmlReportPlaylist.processTestCaseFooter();

            reader.close();
            inputStream.close();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private HtmlReport createReportHeader() {
        return new HtmlReport(mainTemplate, styles, javascript);
    }

    // Add graph data in json
    private void AddDataGraph(List<PlaylistData> playlists, OutputStream fileWriter) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String jsonData = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(playlists);
            String baliseJson = "<script type=\"application/json\" id=\"playlist-data\">\n" +
                    jsonData +
                    "\n</script>";
            fileWriter.write(baliseJson.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Data structures for a playlist
    private static class PlaylistData {
        private String name;
        @JsonProperty("passed")
        private long _passed = 0;
        @JsonProperty("failed")
        private long _failed = 0;
        @JsonProperty("filtered")
        private long _filtered = 0;

        @JsonProperty("nbActions")
        private int _nbActions = 0;

        @JsonProperty("durationInSecond")
        private long _durationInSecond;
        private List<ActionData> actionsData;

        public PlaylistData(String name) {
            this.name = name;
            this.actionsData = new ArrayList<>();
            this.actionsData.add(new ActionData("Navigation", 0));
            this.actionsData.add(new ActionData("Calls", 0));
            this.actionsData.add(new ActionData("UserAction", 0));
            this.actionsData.add(new ActionData("Assertion", 0));
            this.actionsData.add(new ActionData("Technical", 0));
            this.actionsData.add(new ActionData("Others", 0));
        }

        public void addAction(String actionType) {
            String actionName = "";
            switch (ActionTypesEnum.of(actionType)) {
                case NAVIGATION: {
                    actionName = "Navigation";
                    break;
                }
                case CALL_SCRIPT: {
                    actionName = "Calls";
                    break;
                }
                case USER_ACTION: {
                    actionName = "UserAction";
                    break;
                }
                case ASSERT: {
                    actionName = "Assertion";
                    break;
                }
                case TECHNICAL: {
                    actionName = "Technical";
                    break;
                }
                default:
                    actionName = "Others";
                    break;
            }

            for (ActionData action : actionsData) {
                if (action.name.equalsIgnoreCase(actionName)) {
                    action.value += 1;
                    return;
                }
            }
        }

        public void addTest(long passed, long failed, long filtered) {
            _passed = passed;
            _failed = failed;
            _filtered = filtered;
        }

        public void addNbTests(int nbActions) {
            _nbActions += nbActions;
        }

        public void addDurationInSecond(long duration) {
            _durationInSecond = duration;
        }

        public String getName() {
            return name;
        }

        public List<ActionData> getActions() {
            return actionsData;
        }
    }

    private static class ActionData {
        public String name;
        public int value;

        public ActionData(String name, int value) {
            this.name = name;
            this.value = value;
        }
    }
}