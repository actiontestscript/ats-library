package com.ats.tools.report.actions;

import com.ats.tools.report.models.Action;
import com.ats.tools.report.models.TemplateConstants;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

public class HtmlReportActionWindowState {
	private static final String URL_HEADER = TemplateConstants.HEADER_ONE;
	private static final String URL = TemplateConstants.VALUE_ONE;
	private static final String RESIZING_PROPERTIES_TEMPLATE = "Location : (${pos1} , ${pos2})    Size : (${size1} x ${size2}) => Location : (${pos3},${pos4})      Size : (${size3},${size4})";
	private static final String WINDOW_PROPERTIES_HEADER = TemplateConstants.HEADER_TWO;
	private static final String WINDOW_PROPERTIES = TemplateConstants.VALUE_TWO;
	private String template;
	private String result;

	public HtmlReportActionWindowState(String template, Action action, boolean isResize) {
		this.template = template;
		if (isResize) {
			if (StringUtils.isNoneEmpty(action.getValue())) {
				String[] windowProperties = action.getValue().split(",");
				String resizingProperties = getResizingProperties(action, windowProperties);
				this.result = template.replace(URL_HEADER, "Channel");
				if (action.getChannel() != null) {
					this.result = result.replace(URL, action.getChannel().getName());
				}
				this.result = result.replace(WINDOW_PROPERTIES_HEADER, "Resizing");
				this.result = result.replace(WINDOW_PROPERTIES, resizingProperties);
			} else {
				this.result = template.replace(URL_HEADER, "Channel");
				if (action.getChannel() != null) {
					this.result = result.replace(URL, action.getChannel().getName());
				} else {
					this.result = result.replace(URL, "Ø");
				}
				this.result = result.replace(WINDOW_PROPERTIES_HEADER, "Resizing");
				this.result = result.replace(WINDOW_PROPERTIES, "");
			}
		} else {
			this.result = template.replace(URL_HEADER, "Window");
			if (StringUtils.isNoneEmpty(action.getValue())) {
				this.result = result.replace(URL, action.getValue());
			} else {
				this.result = result.replace(URL, "Ø");
			}
		}
	}

	@NotNull
	private static String getResizingProperties(Action action, String[] windowProperties) {
		String resizingProperties = RESIZING_PROPERTIES_TEMPLATE.replace("${pos1}", String.valueOf(action.getChannel().getBound().getX()));
		resizingProperties = resizingProperties.replace("${pos2}", String.valueOf(action.getChannel().getBound().getY()));
		resizingProperties = resizingProperties.replace("${size1}", String.valueOf(action.getChannel().getBound().getWidth()));
		resizingProperties = resizingProperties.replace("${size2}", String.valueOf(action.getChannel().getBound().getHeight()));
		resizingProperties = resizingProperties.replace("${pos3}", windowProperties[0]);
		resizingProperties = resizingProperties.replace("${pos4}", windowProperties[1]);
		resizingProperties = resizingProperties.replace("${size3}", windowProperties[2]);
		resizingProperties = resizingProperties.replace("${size4}", windowProperties[3]);
		return resizingProperties;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getResult() {
		return result;
	}
}
