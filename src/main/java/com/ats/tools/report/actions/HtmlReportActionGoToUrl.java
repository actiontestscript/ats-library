package com.ats.tools.report.actions;

import com.ats.tools.report.models.Action;
import com.ats.tools.report.models.TemplateConstants;
import org.apache.commons.lang3.StringUtils;

public class HtmlReportActionGoToUrl {

    private static final String URL_HEADER = TemplateConstants.HEADER_ONE;
    private static final String URL = TemplateConstants.VALUE_ONE;
    private String template;
    private String result;

    public HtmlReportActionGoToUrl(String template, Action action) {
        this.template = template;
        this.result = template.replace(URL_HEADER, "Navigation url");
        this.result = result.replace(URL, StringUtils.isEmpty(action.getValue()) ? "" : action.getValue());
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getResult() {
        return result;
    }
}
