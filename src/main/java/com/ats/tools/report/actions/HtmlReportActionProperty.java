package com.ats.tools.report.actions;

import com.ats.tools.report.models.Action;
import com.ats.tools.report.models.TemplateConstants;
import com.ats.tools.report.utils.HtmlActionUtils;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;

public class HtmlReportActionProperty {
    private static final String ACTION_PROPERTY_HEADER = TemplateConstants.HEADER_ONE;
    private static final String ACTION_PROPERTY = TemplateConstants.VALUE_ONE;
    private static final String ELEMENT_HEADER = TemplateConstants.HEADER_TWO;
    private static final String ELEMENT = TemplateConstants.VALUE_TWO;
    private String template;
    private String result;

    public HtmlReportActionProperty(String template, Action action) {
        if (action.getActionElement() != null && action.getActionElement().getCriterias() != null) {
            String actionPropertyValue = HtmlReportActionKeyValueTemplate.buildEntireKeyValueString(ImmutableMap.of(StringUtils.isEmpty(action.getValue()) ? "Ø" : action.getValue(), StringUtils.isEmpty(action.getData()) ? "Ø" : action.getData()), true);
            this.template = template;
            this.result = template.replace(ELEMENT, HtmlActionUtils.buildSearchedElementData(action));
            this.result = result.replace(ELEMENT_HEADER, HtmlActionUtils.buildSearchedElementHeader(action));
            this.result = result.replace(ACTION_PROPERTY_HEADER, "Property");
            this.result = result.replace(ACTION_PROPERTY, actionPropertyValue);
        } else {
            this.template = template;
            this.result = template.replace(ELEMENT_HEADER, HtmlActionUtils.buildSearchedElementHeader(action));
            this.result = result.replace(ELEMENT, "");
            this.result = result.replace(ACTION_PROPERTY_HEADER, "Property");
            this.result = result.replace(ACTION_PROPERTY, "");
        }
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getResult() {
        return result;
    }
}
