package com.ats.tools.report.actions;

import com.ats.tools.logger.levels.AtsLogger;
import com.ats.tools.report.models.Action;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class HtmlReportActionApi {

    private static final String WEBSERVICE_NAME = "${webserviceName}";
    private static final String API_CALL_PROPERTIES = "${apiCallProperties}";
    private static final String REQUEST_PAYLOAD = "${requestPayload}";
    private static final String REQUEST_HEADERS = "${requestHeaders}";
    private static final String REQUEST_METHOD = "${requestMethod}";
    private static final String REQUEST_URL = "${requestUrl}";
    private static final String PAYLOAD_TYPE = "${payloadType}";
    private static final String PAYLOAD_DISPLAY_PLAYS_HOLDER = "style='display: none'";
    private static final String SOAP_ICON = "iVBORw0KGgoAAAANSUhEUgAAAC4AAAAYCAYAAACFms+HAAAA8HpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjajVFbbsQgDPznFHsEv2LgOGSTlXqDHn8HME2zUqVawtiDsYchnd9fr/ToJkWSbbl4dSeYVavSEBSatg/PZMMPkzPO+I4nXpcEkGLXmVYJ/ASOmCOvUc+rfjVaATdE23XQWuD7Hd+joZTPRsFAeU6mIy5EI5VgZDN/BiOvJd+edjzpbuVapll8c84Gb0I5e0VchCxDz6MT1TzGQ6OYtICVr1IBJzmVFTKrqk2W2pdpw67DS0IhqyPphx3yITzhK0EBjets/Gr0I+ZvbS6N/rD/PCu9AZrfda5cnwy4AAABg2lDQ1BJQ0MgcHJvZmlsZQAAeJx9kT1Iw0AcxV9TpSKVDu0gopChOtlFRRxLFYtgobQVWnUwufQLmjQkKS6OgmvBwY/FqoOLs64OroIg+AHi7OCk6CIl/i8ptIjx4Lgf7+497t4BQqvGVLMvDqiaZWSSCTFfWBUDr/BDQAhhjEnM1FPZxRw8x9c9fHy9i/Es73N/jiGlaDLAJxLHmW5YxBvEs5uWznmfOMIqkkJ8Tjxp0AWJH7kuu/zGueywwDMjRi4zTxwhFss9LPcwqxgq8QxxVFE1yhfyLiuctzirtQbr3JO/MFjUVrJcpzmKJJaQQhoiZDRQRQ0WYrRqpJjI0H7Cwz/i+NPkkslVBSPHAupQITl+8D/43a1Zmp5yk4IJoP/Ftj/GgcAu0G7a9vexbbdPAP8zcKV1/fUWMPdJerOrRY+A0DZwcd3V5D3gcgcYftIlQ3IkP02hVALez+ibCkD4Fhhcc3vr7OP0AchRV8s3wMEhMFGm7HWPdw/09vbvmU5/P2d2cqJ2uj8yAAANdmlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNC40LjAtRXhpdjIiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iCiAgICB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIgogICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICAgeG1sbnM6R0lNUD0iaHR0cDovL3d3dy5naW1wLm9yZy94bXAvIgogICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iCiAgICB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iCiAgIHhtcE1NOkRvY3VtZW50SUQ9ImdpbXA6ZG9jaWQ6Z2ltcDowNjZiMWUyMy03NTFjLTQwYmUtYWNiMS05YjZjNGJmYzRjODYiCiAgIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6M2Y1ODg4MDAtNzEyMi00NDJhLTkwOGMtYWUwYzM5ZGUzOTk1IgogICB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6YWRhZWI3Y2EtZGIwOC00ZjkxLWEyNTgtNjhiY2NiNDJkM2E5IgogICBkYzpGb3JtYXQ9ImltYWdlL3BuZyIKICAgR0lNUDpBUEk9IjIuMCIKICAgR0lNUDpQbGF0Zm9ybT0iV2luZG93cyIKICAgR0lNUDpUaW1lU3RhbXA9IjE3MDkzOTI4MDYxODkwMzEiCiAgIEdJTVA6VmVyc2lvbj0iMi4xMC4zNCIKICAgdGlmZjpPcmllbnRhdGlvbj0iMSIKICAgeG1wOkNyZWF0b3JUb29sPSJHSU1QIDIuMTAiCiAgIHhtcDpNZXRhZGF0YURhdGU9IjIwMjQ6MDM6MDJUMTY6MjA6MDYrMDE6MDAiCiAgIHhtcDpNb2RpZnlEYXRlPSIyMDI0OjAzOjAyVDE2OjIwOjA2KzAxOjAwIj4KICAgPHhtcE1NOkhpc3Rvcnk+CiAgICA8cmRmOlNlcT4KICAgICA8cmRmOmxpCiAgICAgIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiCiAgICAgIHN0RXZ0OmNoYW5nZWQ9Ii8iCiAgICAgIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6ODhiZDRlOTMtZWIxMy00OTU5LWI5MTctMTVmNTNjZWNmMmFjIgogICAgICBzdEV2dDpzb2Z0d2FyZUFnZW50PSJHaW1wIDIuMTAgKFdpbmRvd3MpIgogICAgICBzdEV2dDp3aGVuPSIyMDI0LTAzLTAyVDE2OjIwOjA2Ii8+CiAgICA8L3JkZjpTZXE+CiAgIDwveG1wTU06SGlzdG9yeT4KICA8L3JkZjpEZXNjcmlwdGlvbj4KIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAKPD94cGFja2V0IGVuZD0idyI/Po6rpcIAAAAGYktHRADDAMAAyFPm9bYAAAAJcEhZcwAACxMAAAsTAQCanBgAAAAHdElNRQfoAwIPFAYX+7RJAAAAGXRFWHRDb21tZW50AENyZWF0ZWQgd2l0aCBHSU1QV4EOFwAABTlJREFUWMPtl1lvG0cWhb9b1QsXkVpIaqMWWrJjY14CxMg8zADzqwf5BQGSByOBJ5PVSCSNLMmmtXERyWZ31Z2HpiRKspRg5klA6qHRXdVdde6pOufelrN/fqE8wmZ4pO1P4H8C/1+By9SdquLxKArc1nDel48JquDFoeLvLKKTSY0qogL4j0BR5Godj6pH5X7fCG53eIGsXGTmyRNsaQYvjrTTI/vtAEkGk4kNWqkQbSxjZ8pYArJxwvj9O7KjNhZ/gwJFCJ5uEM3Poj5g+Po7JEuvRytVoufPENykS/HpmOTkDH94glF/RdS9wKVUYeHlSyhHGAwCRAsNXL1G/8uvUO+Jnm5RfLYF1uQAVQiBeGWJpPmB8bev0cxdg7cR5c1tTCHEe0O2fEy6v4dIPm6iiHhpCVWPIKCgOIqr64xWjxm9+g5R9zDj4fYalCLSTofhv3+CICBqbeDbbcSDXVun9MkzBEjabZLDA2yiSGOB0sYahcYy8qkwfPUNIiAIcXMJChGaZWANQbNBsr+HuQxNJxfxdL/5Ht8fEq0uUX6yTrFRJ1tZJD08vHGug9un1hSKGBH8KMOfn+EJcKcfMMaDBJRbLUSUcafL4NW3GIUM4LjNxXhI+flfiBcXGc1VodslU6HUXMarp/9mh8p2i2h+hkFlFn/Rxeq0BgQuzpDuBeNuj2Kzji3OYOerpEcH1y/eFqcBsn4fj1JcqlP5x98pfLKFxkWMEygVCKplBBgcvCNwN2Wd/OcInAdRgvp8LtxqBTM7i3EOd3BIcn6KSEBhrYn1cqWrS2NwEuCMxVgDQa4WzRxGHxCnF0h/3SeqzhIuLBBUZzHVGUpb6wx+2IHeGfn+CzoYIUYw1ySQZWPUZZggxIZFUoRofQXFkJ2eY4cD/MEHqNUIl+ukP1tU/Q37KWxuo1lKPDeHBDF4ZXzQnpL6R4CLQpAMGXz9ClOrY1cWKS41sHFI+UWL/lcnoLmrRFFMJg6DvQwbAkGNAQzeOZwYKo1FwDPu9fC1ecZpRkk9Ui4g9Rrafs+l63lRimv1CfsGnwzpvfkNvTh/GPjlOXfqkfY7RsfvSd42qP3tc0wIqXO44YigVCZcbZC83c230CgqYJeXkcCg3jM67WCWG1CICbyh0tqEJ5u5YwgYhbi5yuBDG0RRBes9x7/8ihkMCUYpafcM693kPMv9wBUwT1sU60uM3vyMdM8Jq2VA8JmHsaO395a5F88I6gsUP31OstMGByzOMbu9gVFlfNZBTjoUXr4AgaR/Rnp0kqcWUYJiifJ6k3Cxho9jzKU/O8UdnyDdHh6wk+T2uwlIrVBuriDlEtFfPwNvEDGoQn/vLZKN8Tt79Ishpc114maLYrMFmgNykpF1z+m+/hFbgEJtAY9htLtLuN/GiYLCOIyJV2tYWyBcXcR3eyAeH3oC9QiKIrmno38AuCqdr/9FcatJWJ/DhAWy0ZiLwyP8zg4Gg9WU9Ifv6b4/J95cIarMIAayZMSofUK2e4DNMuzKPO6ij0uVtH2C2qncl6Uku/vEtQa2WiI7PcV3zlEyjPe/W6eogNz9kRBksnmeDIdiEIxylemm6xoVUM2/ERRUUVG8gEMIvUckZ0+n2FMVHIqafEVVQVQxoiDwQJmCykfEmRdODhEhUIPVHITIPZFPvjGXxZVMPGEiQGPMdUmmNy3MTpxM0euCagL6/kOSzxY8uCeSs3xleKp3LEhE7gQlPMzYbUNg6v3pZ5lKTjdtW+4Hrqr8kbXvBPN/tmtPfyhYeaw/Eo8UuKCPlXHlv/dtc16vtOQ8AAAAAElFTkSuQmCC";
    private static final String REST_ICON = "iVBORw0KGgoAAAANSUhEUgAAAC4AAAAYCAYAAACFms+HAAAABmJLR0QArgDOAOl0s3seAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH5wUdFA0Thzm1lwAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAARuSURBVFjD7ZfdTxtXEMV/c3dtL2CbrwRs45QgElVNmqZRVeWlf3bfq740alHTJKrShgSIDcZgggGDbbC9M33Y9UcSaCFPjcSVLO/a9+49M+fMmbvy05Mfjc9wOD7TcQ38Gvglh3/RHxoKmAx/EBARROJaFsUMTEfnGQjRvRgiiogDHKqGiBBPQATMQsw0yp+dn0PxFJFLAjeDr+89ZGI8i6CYOcyMVuuE3VqVer2GuCiQbx89xrlUvEs4CKLbO+PZ819xeBTyt8jnF0gkEqiFNI4abFXKZDMZ8rnlaB3hMENY/K08fbFCL+x8BP5c4IIRJAPGUmnUuqiCcx5BEDA7M0t5c4Py5jriCUEqHQMPRzYEJ1EWFxfvUizewkxRNXwvwezsGCKO45MjxsYCQCMWzAMMk+hJWIiId3mpRGoQBKFxdMiffz3D9xLcv/eAbHqKQq5IeXMDzBBABCrbJd6WVvu4wcA5n0J+AXBsVdYob66TSo6TzxWp7m7T6Zyys1MBlIXcEotf3MFQVlZ+IeQME6Xb64ww8V9SkcHecf326Ha77O+/I5uexXk+zkWU2gCnodaLdIYBHp7zcc7F9w5VpX16zHrpbxAHKKoKCqHqgO1ueErPulGpnCfwfyvOYfoFUw/f95iZvgEIzeYxoSnORayIwVR6mtuLXwFCt3tKpVqipz2arRMmJjIUC0vcmClwcPiO6m6ZZrsJ/aCEQRIMw3C8R93VpBItzWayPP7+B5zz8Tyfs06bN6WXmPQwSYI5BMhkJplIZzERzs6alLbXcU549eY5X955yPjYJEEQkM8vkMvNs7bxmuruFuYJWF/XGjEnURBiHynk8lIxM1SNVCIJZqyvvebk+CSyx/jBBtQP9qntbWMisQsYgtBqNXn6/AmZ7DQ3Z+aYv5nH93xuF5fYrVUws/dsYWCX2MBfLt2ARic3Gkes/P4zjcYBIrC8vEzST+HMIQN6odVuUtuvUHu3yf7hbpwoQcxDDRqNA9bfrlJ6u4YgeH4C3/PimgAxiQ2hf22Y2NU6pxuRijgFMTbKq6j0SCSTLC3eBfNjyCFI5CBBKk0QjBOkxgmSE2A+Dx98xzf3HzGZnSaVDMhMTWICnU6Xbq+HxowNQcZZN/cpndONZF4wcxwe1ant1Zi7mWd+boHqTpVm+zD2Acjnb1HIFwcF1um2ebX6knR6ChGP2akcBqgZakp5qxR114+K0L1faFcBrmIcN49QDWm1T6KObI6N8hpJ3+H5Cebm5nhTqtM4OcC5BA5BCDETTBxnnVPqh3v88eI3CvNFsplJxPNotlvs7GxSP9yLgTtElE6nTaNRxwhBegMJXWh2F71ImOmw+UpsTyZx0RlqHsjwrBElSGMGJMqcKIL2lYva0Poie5aR840XBYFiopjJpx2yIrCjbmQQ21S0aziYZ7GdgUQHMfpBRNQP1Cuj55D+J4yfqXFhXuwkV2tA57JhHwQpV1gtH/iXxC4S1UY46sf/7/O4fNKq6zega+CXHP8AKhYWfO2tuKQAAAAASUVORK5CYII=";
    private static final String APPLICATION_ICON_TEMPLATE = "<img loading='lazy' style='margin-right: 10px' src='data:image/png;base64,${appIcon}' alt='Application icon'>";

    private static final String REQUEST_HEADERS_HTML_TEMPLATE = "<div class='api-call-header'>\n" +
            "                <div class='api-call-header-name'>${headerName}</div>\n" +
            "                <div class='api-call-header-value'>${headerValue}</div>\n" +
            "            </div>";

    private String template;
    private String result;

    public HtmlReportActionApi(String template, Action action) {
        this.template = template;
        HtmlReportActionApiRequest request = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            request = objectMapper.readValue(action.getValue(), HtmlReportActionApiRequest.class);
        } catch (Exception e) {
            AtsLogger.printLog("cannot parse request data.");
        }

        if (request != null) {
            this.result = template.replace(WEBSERVICE_NAME, buildWebServiceName(request));
            this.result = result.replace(REQUEST_METHOD, request.getType());
            this.result = result.replace(REQUEST_URL, request.getMethod());
            CharSequence replacement = buildRequestHeaders(request);
            this.result = result.replace(REQUEST_HEADERS, replacement);
            String payload = buildRequestPayload(request, action);
            this.result = result.replace(REQUEST_PAYLOAD, payload);
            if (!request.getHeaders().isEmpty()) {
                this.result = result.replace("headers style='display: none;'", "");
            }
        } else {
            this.result = template.replace(WEBSERVICE_NAME, "API Request");
            this.result = result.replace(REQUEST_METHOD, "").replace(REQUEST_URL, "");
        }

    }

    private CharSequence buildRequestHeaders(HtmlReportActionApiRequest request) {
        if (request.getHeaders().isEmpty()) {
            return "No headers";
        } else {
            return buildHeadersString(request.getHeaders());
        }
    }

    private CharSequence buildHeadersString(Map<String, String> headers) {
        List<String> headersStrings = headers.entrySet().stream().map(stringStringEntry -> REQUEST_HEADERS_HTML_TEMPLATE.replace("${headerName}", stringStringEntry.getKey()).replace("${headerValue}", stringStringEntry.getValue())).collect(Collectors.toList());
        headersStrings.set(headersStrings.size() - 1, headersStrings.getLast().replace("class='api-call-header'", "class='api-call-header no-bottom-border'"));
        return String.join("", headersStrings);
    }

    private CharSequence buildApiCallProperties(HtmlReportActionApiRequest request, Action action) {
        String webServiceProperties;
        webServiceProperties = request.getType() + " " + request.getMethod();
        return webServiceProperties;
    }

    private String buildWebServiceName(HtmlReportActionApiRequest request) {
        String webServiceNameIcon;
        String webServiceName;

        if (request.getType().equals("SOAP")) {
            webServiceNameIcon = APPLICATION_ICON_TEMPLATE.replace("${appIcon}", SOAP_ICON);
        } else {
            webServiceNameIcon = APPLICATION_ICON_TEMPLATE.replace("${appIcon}", REST_ICON);
        }
        webServiceName = webServiceNameIcon + " " + "Webservice";
        return webServiceName;
    }

    private String buildRequestPayload(HtmlReportActionApiRequest request, Action action) {
        ObjectMapper objectMapper = new ObjectMapper();
        String requestParametersString = "";
        String requestPayloadString = "";


        if (action.getAppDataJson() != null) {
            showRequestPayload();
            try {
                requestPayloadString = objectMapper.writeValueAsString(action.getAppDataJson().getData());
                this.result = result.replace(PAYLOAD_TYPE, "JSON");
                showRequestPayload();

            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        } else if (StringUtils.isNotEmpty(action.getData()) && !request.getType().equals("SOAP")) {
            requestPayloadString = action.getData();
            result = result.replace(PAYLOAD_TYPE, "URL");
            showRequestPayload();

        } else if (request.getType().equals("SOAP")) {
            requestPayloadString = action.getData().replaceAll("<", "&lt;").replaceAll(">", "&gt;");
            result = result.replace(PAYLOAD_TYPE, "XML");
            showRequestPayload();

        }

        return StringUtils.isBlank(requestParametersString) && StringUtils.isBlank(requestPayloadString) ? "No data" : requestParametersString + requestPayloadString;
    }

    private void showRequestPayload() {
        result = result.replace(PAYLOAD_DISPLAY_PLAYS_HOLDER, "style='display: flex;'");
    }


    public void setTemplate(String template) {
        this.template = template;
    }

    public String getResult() {
        return this.result;
    }
}
