package com.ats.tools.report.actions;

import com.ats.tools.report.models.Action;
import com.ats.tools.report.models.OperatorsWithIcons;
import com.ats.tools.report.models.TemplateConstants;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HtmlReportActionChannel {

    private static final String CHANNEL_NAME_HEADER = TemplateConstants.HEADER_ONE;
    private static final String CHANNEL_NAME = TemplateConstants.VALUE_ONE;
    private static final String APPLICATION_HEADER = TemplateConstants.HEADER_TWO;
    private static final String APPLICATION = TemplateConstants.VALUE_TWO;
    private static final String APPLICATION_VERSION_HEADER = TemplateConstants.HEADER_THREE;
    private static final String APPLICATION_VERSION = TemplateConstants.VALUE_THREE;
    private static final String APPLICATION_ICON_TEMPLATE = "<img loading='lazy' style='margin-right: 10px' src='data:image/png;base64,${appIcon}' alt='Application icon'>";
    private static final String APPLICATION_TEMPLATE = "<div style='display: flex; flex-direction: column; justify-content: space-evenly; height: 100%;'><div style='display: flex; align-items: center;'><div class='${osIcon}'></div>${osInfo}</div><div style=' display: flex; align-items: center;'>" +
            "<img loading='lazy' style='margin-right: 10px' src='data:image/png;base64,${appIcon}' alt='Application icon'>${appName}</div></div>";

    private static final String SWITCH_CHANNEL_HTML_TEMPLATE = "<div>${oldChannel}</div>${operator}<div>${newChannel}</div>";
    public static final String LINUX_ICON_CLASS = "linux-icon";
    public static final String WINDOWS_ICON_CLASS = "windows-icon";
    public static final String VERSION_TEMPLATE = "<div style='display: flex;\n" +
            "    flex-direction: column;\n" +
            "    justify-content: space-evenly;\n" +
            "    height: 100%;'><div>${osVersion}</div><div>${appVersion}</div></div>";
    public static final String OS_INFO_SECTION_TEMPLATE = "<div style='display: flex; align-items: center;'><div class='${osIcon}'></div>${osInfo}</div>";


    public static enum ActionChannelOperation {
        CHANNEL_START, CHANNEL_CLOSE, CHANNEL_CHANGE
    }

    private static final String APP_ICON = "${appIcon}";
    private static final String APP_NAME = "${appName}";
    private String template;
    private String result;

    public HtmlReportActionChannel(String template, Action action, ActionChannelOperation actionChannelOperation, Map<String, String> appIcons, String osInfo) {
        this.template = template;
        if (actionChannelOperation.equals(ActionChannelOperation.CHANNEL_CHANGE)) {
            this.result = template.replace(CHANNEL_NAME_HEADER, "Switch channel");
            if (action.getChannel() != null) {
                this.result = result.replace(CHANNEL_NAME, buildSwitchChannelString(action));
            } else {
                this.result = result.replace(CHANNEL_NAME, "");
            }
        } else {
            if (action.getAppDataJson() != null && action.getChannel() != null) {
                String applicationName = action.getAppDataJson().getApp();
                String appIcon = appIcons.get(action.getAppDataJson().getApp());
                applicationName = APPLICATION_TEMPLATE.replace("${appName}", applicationName);
                String version = "";
                if (ActionChannelOperation.CHANNEL_START.equals(actionChannelOperation)) {
                    if (StringUtils.isNoneEmpty(action.getAppDataJson().getOs()) && (action.getAppDataJson().getOs().toLowerCase().contains("windows") || action.getAppDataJson().getOs().toLowerCase().contains("linux"))) {
                        String osName = action.getAppDataJson().getOs().toLowerCase().contains("windows") ? "Windows" : "Linux";
                        applicationName = applicationName.replace("${osInfo}", osName);
                        applicationName = applicationName.replace("${osIcon}", "Windows".equals(osName) ? WINDOWS_ICON_CLASS : LINUX_ICON_CLASS);
                        version = VERSION_TEMPLATE.replace("${osVersion}", action.getAppDataJson().getOs());
                    } else {
                        applicationName = applicationName.replace(OS_INFO_SECTION_TEMPLATE,"");
                    }
                } else {
                    applicationName = applicationName.replace(OS_INFO_SECTION_TEMPLATE,"");
                }
                if (StringUtils.isNoneBlank(appIcon)) {
                    applicationName = applicationName.replace("${appIcon}", appIcon);
                } else {
                    applicationName = applicationName.replace(APPLICATION_ICON_TEMPLATE, "");
                }
                this.result = template.replace(CHANNEL_NAME_HEADER, "Channel name");
                this.result = result.replace(CHANNEL_NAME, action.getAppDataJson().getName());
                this.result = result.replace(APPLICATION_HEADER, "Application");
                this.result = result.replace(APPLICATION, applicationName);
                if (actionChannelOperation.equals(ActionChannelOperation.CHANNEL_START)) {
                    this.result = result.replace(APPLICATION_VERSION_HEADER, "Version");
                    if (action.getAppDataJson().getAppVersion() != null) {
                        this.result = result.replace(APPLICATION_VERSION, version.replace("${appVersion}", action.getAppDataJson().getAppVersion()));
                    }
                }
            } else {
                this.result = template.replace(CHANNEL_NAME_HEADER, "Channel name");
                this.result = result.replace(CHANNEL_NAME, "");
                this.result = result.replace(APPLICATION_HEADER, "Application");
                this.result = result.replace(APPLICATION, "");
                if (actionChannelOperation.equals(ActionChannelOperation.CHANNEL_START)) {
                    this.result = result.replace(APPLICATION_VERSION_HEADER, "Version");
                    this.result = result.replace(APPLICATION_VERSION, "");
                }

            }
        }

    }

    private CharSequence buildSwitchChannelString(Action action) {
        return SWITCH_CHANNEL_HTML_TEMPLATE.replace("${oldChannel}", action.getChannel().getName())
                .replace("${newChannel}", action.getValue())
                .replace("${operator}", OperatorsWithIcons.getIconForOperator("ARROW"));
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getResult() {
        return result;
    }
}
