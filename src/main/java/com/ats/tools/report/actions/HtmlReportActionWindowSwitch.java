package com.ats.tools.report.actions;

import com.ats.tools.report.models.Action;
import com.ats.tools.report.models.OperatorsWithIcons;
import com.ats.tools.report.models.TemplateConstants;
import org.apache.commons.lang3.StringUtils;

public class HtmlReportActionWindowSwitch {
    private static final String SWITCH_WINDOW_HEADER = TemplateConstants.HEADER_ONE;
    private static final String SWITCH_WINDOW_DATA = TemplateConstants.VALUE_ONE;
    private static final String SWITCH_WINDOW_HTML_TEMPLATE = "<div>${oldChannel}</div>${operator}<div>${newChannel}</div>";
    private String template;
    private String result;

    public HtmlReportActionWindowSwitch(String template, Action action) {
        this.template = template;
        this.result = template.replace(SWITCH_WINDOW_HEADER, "Switch window");
        if (StringUtils.isNotEmpty(action.getValue())) {
            this.result = result.replace(SWITCH_WINDOW_DATA, buildSwitchWindowString(action));
        } else {
            this.result = result.replace(SWITCH_WINDOW_DATA, "");
        }
    }

    private CharSequence buildSwitchWindowString(Action action) {
        return SWITCH_WINDOW_HTML_TEMPLATE.replace("${oldChannel}", "")
                .replace("${newChannel}", action.getValue())
                .replace("${operator}", OperatorsWithIcons.getIconForOperator("ARROW"));
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getResult() {
        return result;
    }
}
