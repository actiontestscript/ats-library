package com.ats.tools.report.actions;

import com.ats.tools.report.models.Action;
import com.ats.tools.report.models.TemplateConstants;
import com.ats.tools.report.utils.HtmlActionUtils;

public class HtmlReportActionScripting {
    private static final String ACTION_SCRIPTING_HEADER = TemplateConstants.HEADER_ONE;
    private static final String ACTION_SCRIPTING = TemplateConstants.VALUE_ONE;
    private static final String ELEMENT_HEADER = TemplateConstants.HEADER_TWO;
    private static final String ELEMENT = TemplateConstants.VALUE_TWO;

    private String template;
    private String result;

    public HtmlReportActionScripting(String template, Action action) {
        this.template = template;
        this.result = template.replace(ELEMENT_HEADER, HtmlActionUtils.buildSearchedElementHeader(action));
        if (action.getActionElement() != null) {
            this.result = result.replace(ELEMENT, HtmlActionUtils.buildSearchedElementData(action));
            this.result = result.replace(ACTION_SCRIPTING_HEADER, "Scripting");
            this.result = result.replace(ACTION_SCRIPTING, action.getValue());
        } else {
            this.result = result.replace(ELEMENT, "");
            this.result = result.replace(ACTION_SCRIPTING_HEADER, "Scripting");
            this.result = result.replace(ACTION_SCRIPTING, "");
        }
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getResult() {
        return result;
    }
}
