package com.ats.tools.report.actions;

import com.ats.tools.Operators;

public class HtmlReportActionAssertCondition {
    private String name;
    private Operators operator;
    private String expected;
    private String actual;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Operators getOperator() {
        return operator;
    }

    public void setOperator(Operators operator) {
        this.operator = operator;
    }

    public String getExpected() {
        return expected;
    }

    public void setExpected(String expected) {
        this.expected = expected;
    }

    public String getActual() {
        return actual;
    }

    public void setActual(String actual) {
        this.actual = actual;
    }
}
