package com.ats.tools.report.general;

import com.ats.tools.ResourceContent;

public class HtmlReport {
    public static final String CSS = "${styles}";
    public static final String JAVASCRIPT = "${javascript}";
    public static final String FOOTER = "</div>\n</div>\n</body>\n</html>";
    private String report;

    public HtmlReport(String report, String css, String javascript) {
        this.report = report.replace(CSS, css).replace(JAVASCRIPT, javascript);
    }

    public String getMainReport() {
        return report;
    }

    public String getFooter() {
        return ResourceContent.getHtmlFooter();
    }

    public void setMainReport(String report) {
        this.report = report;
    }
}
