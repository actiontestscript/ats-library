package com.ats.tools.report.general;

import static com.ats.tools.report.HtmlCampaignReportGenerator.PASS_STATUS;
import static com.ats.tools.report.general.HtmlReportExecution.DISPLAY_NONE_CSS;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ats.tools.Utils;
import com.ats.tools.report.AtsReport;
import com.ats.tools.report.models.HtmlReportProject;
import com.ats.tools.report.models.Script;
import com.ats.tools.report.models.Suite;
import com.ats.tools.report.models.SuiteInfo;

public class HtmlReportPlaylist {
    public static final String PLAYLIST_INFO_ONLY_CSS = "playlist-info-only";
    public static final String PLAYLIST_INFO_DATA_FRAME_WITH_HEADER_CSS = "playlist-info-data-frame-with-header";
    public static final String PLAYLIST_INFO_DATA_FRAME_WITH_HEADER_WIDTH_100_CSS = "playlist-info-data-frame-with-header width-100";
    public static final String GROUP_FILTERS_CSS = "group-filters";
    private final String PLAYLIST_NAME = "${playlistName}";
    private final String PLAYLIST_NAME_URI = "${playlistNameUri}";
    private final String PLAYLIST_TESTS_PASSED = "${playlistTestsPassed}";
    private final String PLAYLIST_TESTS_FAILED = "${playlistTestsFailed}";
    private final String PLAYLIST_TESTS_FILTERED = "${playlistTestsFiltered}";
    private final String PLAYLIST_EXECUTION_TIME_SEC = "${playlistExecutionTimeSec}";
    private final String PLAYLIST_EXECUTION_TIME_MIN = "${playlistExecutionTimeMin}";
    private final String PLAYLIST_EXECUTION_TIME_HRS = "${playlistExecutionTimeHrs}";
    private final String PLAYLIST_DESCRIPTION = "${playlistDescription}";
    private final String PLAYLIST_PARAMETERS = "${playlistParameters}";
    private final String PLAYLIST_GROUPS_INCLUDED = "${playlistGroupsIncluded}";
    private final String PLAYLIST_GROUPS_EXCLUDED = "${playlistGroupsExcluded}";
    private final String PLAYLIST_EXECUTION_RESULT = "result-pass-icon-transparent";
    private final String PLAYLIST_EXECUTION_DETAILS = "suite-execution-details";

    private final String GROUP_DISPLAY_PLACEHOLDER_NOT_EXECUTE = "${donotexecutedisplayplaceholder}";
    private final String GROUP_DISPLAY_PLACEHOLDER_EXECUTE = "${executedisplayplaceholder}";
    private final List<Script> scripts;
    public static final String PLAYLIST_NAME_HTML_TEMPLATE = "<a style='color: white;display: flex;align-items: center;' class='hover-underline' href='ats://${playlistNameUri}' target='_self'><div class='external-link-icon external-link-icon-large'></div><div style='font-weight: 100; color: rgba(194, 200, 209)'>Playlist:&nbsp;</div><div class='playlist-info-name-text'>${playListName}</div></a>";
    public static final String GROUP_NAME_HTML_TEMPLATE = "<a style='text-decoration: none; color: rgba(56, 63, 79, 1); display: flex;' href='ats://${groupUri}' target='_self'><div class='external-link-icon'></div>${groupName}</a>";
    private final String PLAYLIST_PARAMETER_HTML_TEMPLATE = "<div class=\"playlist-parameter\">\n" +
            "                            <div class=\"pp-title\">${title}</div>\n" +
            "                            <div class=\"pp-value\">${value}</div>\n" +
            "                        </div>";
    public static final String GROUP_TEMPLATE = "<div class='item-with-frame hover-underline'>${groupWithUri}</div>";
    private final boolean isValidationReport;
    private Suite suite;
    private String playlistTemplate;
    private String result;
    private OutputStream fileWriter;
    private OutputStream validationFileWriter;
    private HtmlReportProject project;
    private boolean isNoSuiteLunch;
    private boolean isCommandLineExecution;
    private String scriptsStatus;
    private long passed = 0;
    private long failed = 0;
    private long filtered = 0;

    private long durationInSecond = 0;

    public HtmlReportPlaylist(String playlistTemplate, Suite suite, OutputStream fileWriter,
                              OutputStream validationFileWriter, HtmlReportProject project, List<Script> scripts,
                              boolean isValidationReport, boolean noSuiteLaunch, String scriptsStatus) {
        this.suite = suite;
        this.playlistTemplate = playlistTemplate;
        this.fileWriter = fileWriter;
        this.validationFileWriter = validationFileWriter;
        this.project = project;
        this.scripts = scripts;
        this.isValidationReport = isValidationReport;
        this.isNoSuiteLunch = noSuiteLaunch;
        this.isCommandLineExecution = suite.getName().toLowerCase().contains("command line suite");
        this.scriptsStatus = scriptsStatus;
    }

    public void processPlaylistFooter() {
        try {
            fileWriter.write("</div></div></div>".getBytes());
            if (isValidationReport) {
                validationFileWriter.write("</div></div></div>".getBytes());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void processPlaylistData() {
        passed = scripts.stream().filter(Script::isPassed).count();
        failed = scripts.stream().filter(script -> !script.isPassed()).count();
        filtered = suite.getTests().size() - passed - failed;

        this.result = StringUtils.replace(playlistTemplate, PLAYLIST_NAME, suite.getName());
        this.result = result.replace(PLAYLIST_NAME_URI, buildPlaylistName(suite));
        this.result = result.replace(PLAYLIST_TESTS_PASSED, String.valueOf(passed));
        this.result = result.replace(PLAYLIST_TESTS_FAILED, String.valueOf(failed));
        this.result = result.replace(PLAYLIST_TESTS_FILTERED, String.valueOf(filtered));
        //Avoiding null pointer exception here
        if (suite.getSuiteInfo() == null) {
            SuiteInfo suiteInfo = new SuiteInfo();
            suiteInfo.setDuration(0);
            suiteInfo.setStatus("");
            suite.setSuiteInfo(suiteInfo);
        }

        if (!isCommandLineExecution) {
            if (!suite.getSuiteInfo().getStatus().equals(scriptsStatus) || testsCountsMismatch(suite, scripts)) {
                suite.setDescription(AtsReport.INCONSISTENCY_WARNING + suite.getDescription());
            }
        }
        if (isNoSuiteLunch && isCommandLineExecution) {
            this.result = result.replace(PLAYLIST_EXECUTION_DETAILS, DISPLAY_NONE_CSS);
            if (!suite.getDescription().contains(AtsReport.INCONSISTENCY_WARNING)) {
                this.result = result.replace(PLAYLIST_INFO_ONLY_CSS, DISPLAY_NONE_CSS);
            }
            this.result = result.replace(GROUP_FILTERS_CSS, DISPLAY_NONE_CSS);
            this.result = result.replace(PLAYLIST_INFO_DATA_FRAME_WITH_HEADER_CSS, PLAYLIST_INFO_DATA_FRAME_WITH_HEADER_WIDTH_100_CSS);
            suite.setParameters(buildParametersForNoSuiteLunch());
        }
        durationInSecond = suite.getSuiteInfo().getDuration() / 1000;
        this.result = result.replace(PLAYLIST_EXECUTION_TIME_SEC, (suite.getSuiteInfo().getDuration() / 1000) % 60 + " sec");
        this.result = result.replace(PLAYLIST_EXECUTION_TIME_MIN, ((suite.getSuiteInfo().getDuration() / 1000) % 3600) / 60 + " min");
        this.result = result.replace(PLAYLIST_EXECUTION_TIME_HRS, suite.getSuiteInfo().getDuration() / 3600000 + " hr");
        this.result = result.replace(PLAYLIST_DESCRIPTION, suite.getDescription());
        this.result = result.replace(PLAYLIST_PARAMETERS, processParameters(suite.getParameters()));
        CharSequence includedGroupsString = processGroups(suite.getIncludedGroups());
        CharSequence excludedGroupsString = processGroups(suite.getExcludedGroups());

        this.result = result.replace(GROUP_DISPLAY_PLACEHOLDER_EXECUTE, buildDisplayGroupsBlockProperty(includedGroupsString));
        this.result = result.replace(GROUP_DISPLAY_PLACEHOLDER_NOT_EXECUTE, buildDisplayGroupsBlockProperty(excludedGroupsString));

        this.result = result.replace(PLAYLIST_GROUPS_INCLUDED, includedGroupsString);
        this.result = result.replace(PLAYLIST_GROUPS_EXCLUDED, excludedGroupsString);
        if (suite.getSuiteInfo() == null || !suite.getSuiteInfo().getStatus().equals(PASS_STATUS)) {
            this.result = result.replace(PLAYLIST_EXECUTION_RESULT, "result-failed-icon-transparent");
        }

        try {
            fileWriter.write(result.getBytes());
            if (isValidationReport) {
                validationFileWriter.write(result.getBytes());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private boolean testsCountsMismatch(Suite suite, final List<Script> scripts) {
        if (suite.getSuiteInfo().getTestsFiltered() != 0) {

        }
        List<Script> filteredScripts = scripts.stream().filter(script -> script.getSuite().equals(suite.getName())).collect(Collectors.toList());
        long passedScriptsCount = filteredScripts.stream().filter(Script::isPassed).count();
        long failedScriptsCount = filteredScripts.stream().filter(script -> !script.isPassed()).count();

        boolean isPassedMismatch = suite.getSuiteInfo().getTestsPassed() != passedScriptsCount;
        boolean isFailedMismatch = suite.getSuiteInfo().getTestsFailed() != failedScriptsCount;

        return isPassedMismatch || isFailedMismatch;
    }

    private Map<String, String> buildParametersForNoSuiteLunch() {
        List<String> parametersNames = new ArrayList<>();
        Map<String, String> parameters = new HashMap<>();
        NodeList nodeList = null;
        try {
            nodeList = com.ats.script.Project.getProjectProperties().getElementsByTagName("environment");
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new RuntimeException(e);
        }


        Node node = nodeList.item(0); // Get the first matching node
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            NodeList childNodes = node.getChildNodes();

            for (int i = 0; i < childNodes.getLength(); i++) {
                Node child = childNodes.item(i);
                if (child.getNodeType() == Node.ELEMENT_NODE) {
                    parametersNames.add(child.getTextContent().trim());
                }
            }
        }
        parametersNames.forEach(parameterName -> parameters.put(parameterName, System.getProperty(parameterName)));
        parameters.entrySet().removeIf(stringStringEntry -> stringStringEntry.getValue() == null);
        return parameters;
    }

    private CharSequence buildDisplayGroupsBlockProperty(CharSequence groups) {
        return StringUtils.isEmpty(groups) ? "style='display: none;'" : "";
    }

    private CharSequence buildPlaylistName(Suite suite) {
        return isNoSuiteLunch && isCommandLineExecution ? "Single test execution" : PLAYLIST_NAME_HTML_TEMPLATE.replace("${playlistNameUri}", project.getProjectUuid() + "/exec/" + suite.getName()).replace("${playListName}", suite.getName());
    }

    private CharSequence buildGroupWithUri(String group) {
        return GROUP_NAME_HTML_TEMPLATE.replace("${groupUri}", project.getProjectUuid() + "/group/" + group).replace("${groupName}", group);
    }

    public void processPlaylistTestInfo(String testInfo) {
        try {
            String finalTestInfo = testInfo.replace("class='general-report-layout-single'", "class='general-report-layout'");
            fileWriter.write(finalTestInfo.getBytes());
            if (isValidationReport) {
                validationFileWriter.write(finalTestInfo.getBytes());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void processPlaylistTestAction(String testAction, boolean isValidationReportEnabled, boolean writeAction) {
        try {
            fileWriter.write(testAction.getBytes());
            if (isValidationReportEnabled && writeAction) {
                validationFileWriter.write(testAction.getBytes());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private CharSequence processGroups(List<String> groups) {
        return groups.stream().map(group -> GROUP_TEMPLATE.replace("${groupWithUri}", buildGroupWithUri(group))).collect(Collectors.joining());
    }

    private CharSequence processParameters(Map<String, String> parameters) {
        return parameters.entrySet().stream().map(stringStringEntry -> {
            String parametersHtml = PLAYLIST_PARAMETER_HTML_TEMPLATE.replace("${title}", stringStringEntry.getKey());
            parametersHtml = parametersHtml.replace("${value}", stringStringEntry.getValue());
            return parametersHtml;
        }).collect(Collectors.joining());
    }

    public void processTestCaseFooter() throws IOException {
        fileWriter.write("</div></div></div></div></div>".getBytes());
        if (isValidationReport) {
            validationFileWriter.write("</div></div></div></div></div>".getBytes());
        }
    }

    public long getPassed() {
        return passed;
    }

    public long getFailed() {
        return failed;
    }

    public long getFiltered() {
        return filtered;
    }

    public long getDurationInSecond() {
        return durationInSecond;
    }

}
