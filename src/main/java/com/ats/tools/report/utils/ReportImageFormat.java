package com.ats.tools.report.utils;

import java.util.ArrayList;

import org.w3c.dom.Element;

import com.ats.recorder.VisualElement;
import com.ats.tools.Utils;

public class ReportImageFormat {
	public void format(Element img, boolean isError, String imageType, ArrayList<byte[]> images, VisualElement element, int ref) {
		img.setAttribute("type", imageType);
		img.setAttribute("src", Utils.getImageData(isError, imageType, images, element, ref));
	}
}