package com.ats.tools.report.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import com.ats.generator.ATS;
import com.ats.tools.logger.levels.AtsLogger;
import com.ats.tools.report.AtsReport;
import com.ats.tools.report.models.HtmlReportProject;
import com.ats.tools.report.models.Results;
import com.ats.tools.report.models.Script;
import com.ats.tools.report.models.Suite;
import com.ats.tools.report.models.Summary;

public class AtsReportXmlGenerator {
	private XMLStreamWriter xmlStreamWriter;
	private FileOutputStream fileOutputStream;
	private XMLOutputFactory outputFactory;
	private List<Summary> testSummaries;
	private HtmlReportProject project;
	private Results results;
	private List<Script> scripts;
	private int devReportLevel;
	private Path outputFolderPath;
	private int validReportLevel;
	private List<Summary> testSummariesForExecution;
	private String outputFilename;

	public AtsReportXmlGenerator(HtmlReportProject project, Results results, List<Script> scripts, int devReportLevel, Path outputFolderPath, int validReportLevel, String outputFilename) {
		this.project = project;
		this.results = results;
		this.scripts = scripts;
		this.devReportLevel = devReportLevel;
		this.outputFolderPath = outputFolderPath;
		this.validReportLevel = validReportLevel;
		this.testSummariesForExecution = getTestSummariesForExecutions(outputFolderPath, project.getSuites());
		this.outputFilename = outputFilename;
	}

	public void writeProjectInfo() throws FileNotFoundException, XMLStreamException {
		initialize(outputFolderPath);
		testSummaries = testSummariesForExecution;
		long testsFailed = scripts.stream().filter(script -> !script.isPassed()).count();
		long testsPassed = scripts.stream().filter(Script::isPassed).count();
		String base64LogoProperty = FileUtils.getBase64LogoProperty(project, outputFolderPath.toString());
		int actionsCount = testSummariesForExecution.stream().mapToInt(summary -> Integer.parseInt(summary.getActions())).sum();

		xmlStreamWriter.writeStartDocument();
		xmlStreamWriter.writeStartElement("ats-report");
		xmlStreamWriter.writeAttribute("actions", Integer.toString(actionsCount));
		xmlStreamWriter.writeAttribute("atsVersion", ATS.getAtsVersion());
		xmlStreamWriter.writeAttribute("devReportLevel", String.valueOf(devReportLevel));
		xmlStreamWriter.writeAttribute("duration", String.valueOf(results.getDuration()));
		xmlStreamWriter.writeAttribute("path", "file:" + File.separator + outputFolderPath);
		xmlStreamWriter.writeAttribute("projectDescription", project.getProjectDescription());
		xmlStreamWriter.writeAttribute("projectId", project.getProjectId());
		xmlStreamWriter.writeAttribute("projectName", project.getProjectName());
		xmlStreamWriter.writeAttribute("projectUuid", project.getProjectUuid());
		xmlStreamWriter.writeAttribute("projectVersion", project.getProjectVersion());
		xmlStreamWriter.writeAttribute("started", String.valueOf(project.getStarted()));
		xmlStreamWriter.writeAttribute("startedFormated", project.getStartedFormatted());
		xmlStreamWriter.writeAttribute("startedFormatedZulu", project.getStartedFormattedZulu());
		xmlStreamWriter.writeAttribute("suitesCount", String.valueOf(results.getSuitesCount()));
		xmlStreamWriter.writeAttribute("suitesPassed", String.valueOf(results.getSuitesPassed()));
		xmlStreamWriter.writeAttribute("tests", String.valueOf(testsFailed + testsPassed));
		xmlStreamWriter.writeAttribute("testsFailed", String.valueOf(testsFailed));
		xmlStreamWriter.writeAttribute("testsPassed", String.valueOf(testsPassed));
		xmlStreamWriter.writeAttribute("validReportLevel", String.valueOf(validReportLevel));
		xmlStreamWriter.writeStartElement("pics");

		xmlStreamWriter.writeStartElement("pic");
		xmlStreamWriter.writeAttribute("name", "logo");
		xmlStreamWriter.writeCharacters(base64LogoProperty);
		xmlStreamWriter.writeEndElement();

		xmlStreamWriter.writeStartElement("pic");
		xmlStreamWriter.writeAttribute("name", "true");
		xmlStreamWriter.writeCharacters(project.getICON_TRUE());
		xmlStreamWriter.writeEndElement();

		xmlStreamWriter.writeStartElement("pic");
		xmlStreamWriter.writeAttribute("name", "false");
		xmlStreamWriter.writeCharacters(project.getICON_FALSE());
		xmlStreamWriter.writeEndElement();

		xmlStreamWriter.writeEndElement();

		xmlStreamWriter.flush();
	}

	public void writeSuiteData(Suite suite, List<Script> scripts) {
		List<Script> suiteScripts = scripts.stream().filter(script -> script.getSuite().equals(suite.getName())).collect(Collectors.toList());
		long scriptsPassed = suiteScripts.stream().filter(Script::isPassed).count();
		long scriptsFailed = suiteScripts.stream().filter(script -> !script.isPassed()).count();

		int suiteActionsCount = testSummaries.stream()
				.filter(summary -> summary.getSuiteName().equals(suite.getName()))
				.mapToInt(summary -> Integer.parseInt(summary.getActions()))
				.sum();

		try {
			xmlStreamWriter.writeStartElement("suite");
			xmlStreamWriter.writeAttribute("actions", String.valueOf(suiteActionsCount));
			xmlStreamWriter.writeAttribute("dateOrder", suite.getDateOrder());
			xmlStreamWriter.writeAttribute("description", suite.getDescription());
			xmlStreamWriter.writeAttribute("duration", String.valueOf(suite.getSuiteInfo().getDuration()));
			xmlStreamWriter.writeAttribute("name", suite.getName());
			xmlStreamWriter.writeAttribute("passed", String.valueOf(suite.getSuiteInfo().getStatus().equals("PASS")));
			xmlStreamWriter.writeAttribute("started", String.valueOf(suite.getStarted()));
			xmlStreamWriter.writeAttribute("startedFormated", suite.getStartedFormatted());
			xmlStreamWriter.writeAttribute("startedFormatedZulu", suite.getStartedFormattedZulu());
			xmlStreamWriter.writeAttribute("testsCount", String.valueOf(suite.getTests().size()));
			xmlStreamWriter.writeAttribute("testsFailed", String.valueOf(scriptsFailed));
			xmlStreamWriter.writeAttribute("testsPassed", String.valueOf(scriptsPassed));

			xmlStreamWriter.writeStartElement("parameters");

			xmlStreamWriter.writeStartElement("parameter");
			xmlStreamWriter.writeAttribute("name", "ats-image-quality");
			xmlStreamWriter.writeAttribute("value", getVisualQualityLabel(suite.getVisualQuality()));
			xmlStreamWriter.writeEndElement();

			writeSuiteParameters(suite.getParameters());

			xmlStreamWriter.writeEndElement();

			writeSuiteGroups(suite);

			xmlStreamWriter.writeCharacters(" ");
			xmlStreamWriter.writeStartElement("tests");
			xmlStreamWriter.writeCharacters(" ");
			xmlStreamWriter.flush();
		} catch (XMLStreamException e) {
			throw new RuntimeException(e);
		}
	}

	private void writeSuiteGroups(Suite suite) throws XMLStreamException {
		if (!suite.getIncludedGroups().isEmpty() || !suite.getExcludedGroups().isEmpty()) {
			xmlStreamWriter.writeStartElement("groups");
			if (!suite.getIncludedGroups().isEmpty()) {
				suite.getIncludedGroups().stream().forEach(includedGroup -> writeGroup(includedGroup, "included"));
			}

			if (!suite.getExcludedGroups().isEmpty()) {
				suite.getExcludedGroups().stream().forEach(excludedGroup -> writeGroup(excludedGroup, "excluded"));
			}
			xmlStreamWriter.writeEndElement();
		}
	}

	private void writeSuiteParameters(Map<String, String> parameters) {
		parameters.entrySet().stream().forEach(parameterEntry -> {
			try {
				xmlStreamWriter.writeStartElement("parameter");
				xmlStreamWriter.writeAttribute("name", parameterEntry.getKey());
				xmlStreamWriter.writeAttribute("value", parameterEntry.getValue());
				xmlStreamWriter.writeEndElement();
			} catch (XMLStreamException e) {
				throw new RuntimeException(e);
			}
		});
	}

	public void writeScript(File filePath, int devReportLVL) {
		byte[] searchString;
		byte[] replacement;

		byte[] buffer = new byte[4096];
		boolean devReportLvlReplaced = false;
		boolean startElementReplaced = false;
		try (FileInputStream fs = new FileInputStream(filePath)) {
			int content;
			while ((content = fs.read(buffer)) != -1) {
				if (!devReportLvlReplaced) {
					searchString = "devReportLVL=\"0\"".getBytes();
					replacement = ("devReportLVL=\"" + devReportLVL + "\"").getBytes();
					devReportLvlReplaced = replaceSpecificString(content, searchString, buffer, replacement);
				}
				if (!startElementReplaced) {
					searchString = "<?xml version='1.0' encoding='UTF-8'?>".getBytes();
					replacement = "                                      ".getBytes();
					startElementReplaced = replaceSpecificString(content, searchString, buffer, replacement);
					if (!startElementReplaced) {
						searchString = "<?xml version='1.0' encoding='UTF-8' standalone='no'?>".getBytes();
						replacement = "                                                      ".getBytes();
						startElementReplaced = replaceSpecificString(content, searchString, buffer, replacement);
						if (!startElementReplaced) {
							searchString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>".getBytes();
							replacement = "                                      ".getBytes();
							startElementReplaced = replaceSpecificString(content, searchString, buffer, replacement);
						}
					}
				}
				fileOutputStream.write(buffer, 0, content);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean replaceSpecificString(int content, byte[] searchString, byte[] buffer, byte[] replacement) {
		boolean findMatch = false;
		for (int i = 0; i <= content - searchString.length; i++) {
			boolean match = true;
			for (int j = 0; j < searchString.length; j++) {
				if (buffer[i + j] != searchString[j]) {
					match = false;
					break;
				}
			}
			if (match) {
				System.arraycopy(replacement, 0, buffer, i, replacement.length);
				i += searchString.length - 1;
				findMatch = true;
			}
		}
		return findMatch;
	}


	private void initialize(Path outputFilePath) throws FileNotFoundException, XMLStreamException {
		outputFactory = XMLOutputFactory.newInstance();
		fileOutputStream = new FileOutputStream(outputFilePath.resolve(outputFilename).toFile());
		xmlStreamWriter = outputFactory.createXMLStreamWriter(fileOutputStream);
	}

	public void writeFileEnd() throws XMLStreamException {
		xmlStreamWriter.writeEndDocument();
		xmlStreamWriter.flush();
		xmlStreamWriter.close();
	}

	public void writeSuiteEnd() {
		try {
			xmlStreamWriter.writeEndElement();
			xmlStreamWriter.writeEndElement();
			xmlStreamWriter.flush();
		} catch (XMLStreamException e) {
			throw new RuntimeException(e);
		}
	}

	private String getVisualQualityLabel(int visualQuality) {
		switch (visualQuality) {
			case 1:
				return "size-optimized";
			case 2:
				return "speed-optimized";
			case 3:
				return "quality";
			case 4:
				return "max-quality";
		}
		return "undefined";
	}

	private void writeGroup(String group, String groupType) {
		try {
			xmlStreamWriter.writeStartElement(groupType);
			xmlStreamWriter.writeCharacters(group);
			xmlStreamWriter.writeEndElement();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
	}

	public List<Summary> getTestSummariesForExecutions(Path outputFolderPath, List<Suite> suites) {
		List<Summary> summaries = new ArrayList<>();

		suites.forEach(suite -> {
			suite.getTests().forEach(testName -> {
				final Path targetPath = outputFolderPath.resolve(suite.getName()).resolve(testName + "_xml");
				final File testReportFolder = targetPath.resolve(AtsReport.ATS_TEST_REPORT_HTML_DATA_FILE_NAME).toFile();

				if (testReportFolder.exists()) {

					try {
						XMLInputFactory factory = XMLInputFactory.newInstance();
						XMLEventReader eventReader = factory.createXMLEventReader(new FileInputStream(testReportFolder));
						boolean summaryAdded = false;

						while (eventReader.hasNext() && !summaryAdded) {
							XMLEvent event = eventReader.nextEvent();

							if (event.isStartElement()) {
								StartElement startElement = event.asStartElement();

								if (startElement.getName().getLocalPart().equals("summary")) {
									Summary summary = new Summary();
									Attribute actionsAttr = startElement.getAttributeByName(new javax.xml.namespace.QName("actions"));
									Attribute durationAttr = startElement.getAttributeByName(new javax.xml.namespace.QName("duration"));
									Attribute statusAttr = startElement.getAttributeByName(new javax.xml.namespace.QName("status"));

									summary.setActions(actionsAttr != null ? actionsAttr.getValue() : "");
									summary.setDuration(durationAttr != null ? durationAttr.getValue() : "");
									summary.setStatus(statusAttr != null ? statusAttr.getValue() : "");
									summary.setSuiteName(suite.getName());
									summary.setTestName(testName);
									summaries.add(summary);
									summaryAdded = true;
								}
							}
						}
						eventReader.close();
					} catch (Exception e) {
						e.printStackTrace();
					}

				} else {
					AtsLogger.printLog("test case filtered and not executed -> " + testName);
				}
			});
		});
		return summaries;
	}

	public List<Summary> getTestSummariesForExecutionsResult() {
		return this.testSummariesForExecution;
	}
}
