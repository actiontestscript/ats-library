package com.ats.tools.report.utils;

import com.ats.tools.logger.levels.AtsLogger;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Class which provides the possibility to use advanced functionality for Ats logs.
 */
public class AdvancedAtsLogger {

    /**
     * Method allows to print ATS logs with period and additional data such as execution time and memory used.
     *
     * @param message   Message to be printed.
     * @param period    Time span for message to be printed.
     * @param startTime execution start point of time, used to determine execution time.
     * @return instance of {@link Timer} to be able to close it when needed.
     */
    public static Timer displayMessageWithPeriodExecutionTimeAndMemoryUsed(String message, int period, long startTime) {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                long usedMemoryInMB = Runtime.getRuntime().totalMemory() / 1000000;
                AtsLogger.printLog(message + " " + (System.nanoTime() - startTime) / 1000000000 + " seconds, " + usedMemoryInMB + "MB of memory used");
            }
        }, 0, period * 1000L);
        return timer;
    }

}
