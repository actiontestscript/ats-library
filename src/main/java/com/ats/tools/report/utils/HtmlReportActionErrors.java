package com.ats.tools.report.utils;

public enum HtmlReportActionErrors {

	ELEMENT_NOT_FOUND(-1), ELEMENT_NOT_VISIBLE(-2),ELEMENT_NOT_INTERACTABLE(-3);

	private int errorCode;

	HtmlReportActionErrors(int errorCode) {
		this.errorCode = errorCode;
	}
}
