package com.ats.tools.report.utils;

import com.ats.tools.report.analytics.ActionTypesEnum;

public class HtmlReportActionLabel {
	private String label;
	private String cssClass;

	public HtmlReportActionLabel(String actionType) {
		switch (ActionTypesEnum.of(actionType)) {
			case NAVIGATION: {
				this.label = "Navigation";
				this.cssClass = "atl-navigation";
				break;
			}
			case CALL_SCRIPT: {
				this.label = "Calls";
				this.cssClass = "atl-calls";
				break;
			}
			case USER_ACTION: {
				this.label = "User action";
				this.cssClass = "atl-user-actions";
				break;
			}
			case ASSERT: {
				this.label = "Assertion";
				this.cssClass = "atl-assert";
				break;

			}
			case TECHNICAL: {
				this.label = "Technical";
				this.cssClass = "atl-technical";
				break;
			}
			default:
				this.label = "Others";
				this.cssClass = "atl-others";
				break;
		}
	}

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getCssClass() {
		return cssClass;
	}
	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}
}
