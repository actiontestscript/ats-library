package com.ats.tools.report.utils;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Simple memory monitor class
 *
 */
public class MemoryMonitor {
    Timer timer = new Timer();
    long maxMemoryUsed = 0;

    public MemoryMonitor startMonitoring() {
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                long usedMemoryInMB = Runtime.getRuntime().totalMemory() / 1000000;
                if (usedMemoryInMB > maxMemoryUsed) {
                    maxMemoryUsed = usedMemoryInMB;
                }
            }
        }, 0, 2000L);
        return this;
    }

    public long stopMonitoring() {
        timer.cancel();
        return maxMemoryUsed;
    }
}
