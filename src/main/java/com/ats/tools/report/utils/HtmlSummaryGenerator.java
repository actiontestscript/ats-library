package com.ats.tools.report.utils;

import com.ats.generator.ATS;
import com.ats.tools.ResourceContent;
import com.ats.tools.logger.levels.AtsLogger;
import com.ats.tools.report.models.HtmlReportProject;
import com.ats.tools.report.models.Results;
import com.ats.tools.report.models.Script;
import com.ats.tools.report.models.SuiteInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class HtmlSummaryGenerator {

    private static final String summarySuiteTemplate = ResourceContent.getHtmlReportSummarySuiteTemplate();
    private static final String summaryTestTemplate = "<div class='header'>\n" +
            "                    <div class='status-line test-details-header-test-case-name font-weight-100 no-bottom-border ${background-color} ${border-radius}'>\n" +
            "                        <div style='display: flex; align-items: center; margin-left: 60px;'><div class='test-name-prefix ${tcp-border-radius}'>test case</div> <div style='display: flex;flex-direction: column;'>\n" +
            "                           <div style='font-size: 13px;font-style: italic;'>${testPath}</div>\n" +
            "                           <div style='word-wrap: break-word;max-width: 770px;'>${testName}</div>\n" +
            "                           </div></div>\n" +
            "                        <div style='display: flex; align-items: center;'>" +
            "                        <div class='details-header-action-details-item'>\n" +
            "<div style='display: flex; align-items: center;'>\n" +
            "    <div style='padding-right: 25px;display: flex;flex-direction: column;align-items: flex-start;'>\n" +
            "        <div style='display: flex; justify-content: flex-start;'>${actionsCount} action${teem}</div>\n" +
            "        <div style='font-size: 14px;'>executed in ${testExecutionTime}</div>\n" +
            "    </div>\n" +
            "</div>                            " +
            "<div class='${resultIcon}'></div></div>\n" +
            "                        </div>\n" +
            "                    </div>\n" +
            "                </div>";

    public static void generateHtmlSummary(String htmlSummaryTemplate, HtmlReportProject project, String outputDirectory, int devReportLevel, Map<String, Integer> testSuiteActionsCount, Results results, List<Script> scripts, Map<String, Long> testDurations) {
        ObjectMapper mapper = new ObjectMapper();
        int executedActionsCount = testSuiteActionsCount.values().stream().mapToInt(integer -> integer).sum();
        String result = "";
        result = htmlSummaryTemplate.replace("${atsTestScriptVersion}", ATS.getAtsVersion());
        result = result.replace("${projectLogo}", FileUtils.getBase64LogoProperty(project, outputDirectory));
        result = result.replace("${projectNameFormatted}", project.getProjectId());
        result = result.replace("${suitsLaunched}", String.valueOf(project.getSuites().size()));
        result = result.replace("${createdAt}", String.valueOf(project.getStartedFormatted()));
        result = result.replace("${reportType}", HtmlActionUtils.getReportType(devReportLevel));
        result = result.replace("${executedSuitsCount}", String.valueOf(results.getSuitesCount()));
        result = result.replaceAll(Pattern.quote("${passedSuitsCount}"), String.valueOf(results.getSuitesPassed()));
        result = result.replaceAll(Pattern.quote("${failedSuitsCount}"), String.valueOf(results.getSuitesFailed()));
        result = result.replace("${totalDuration}", buildDuration(results.getDuration()));
        result = result.replace("${executedTestsCount}", String.valueOf(getExecutedTestsCount(results)));
        result = result.replaceAll(Pattern.quote("${passedTestCount}"), String.valueOf(getTestsPassed(results)));
        result = result.replaceAll(Pattern.quote("${failedTestCount}"), String.valueOf(getTestsFailed(results)));
        result = result.replace("${executedActionsCount}", String.valueOf(executedActionsCount));
        result = result.replaceAll(Pattern.quote("${plm}"), project.getSuites().size() > 1 ? "s" : "");
        result = result.replaceAll(Pattern.quote("${tem}"), getExecutedTestsCount(results) > 1 ? "s" : "");
        result = result.replace("${peem}", testSuiteActionsCount.values().stream().mapToInt(integer -> integer).sum() > 1 ? "s" : "");

        try {
            String suiteChartsData = mapper.writeValueAsString(results.getSuites().stream()
                    .collect(Collectors.toMap(
                            SuiteInfo::getName,
                            suiteInfo -> new int[]{suiteInfo.getTestsPassed(), suiteInfo.getTestsFailed(), suiteInfo.getTestsFiltered()})
                    ));
            result = result.replaceAll(Pattern.quote("${suiteChartsData}"), suiteChartsData);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }


        result = result.replace("${suites}", buildSuites(results, scripts, testSuiteActionsCount, testDurations));

        try (FileWriter writer = new FileWriter(outputDirectory + "/summary.html")) {
            writer.write(result);
            AtsLogger.printLog("html summary report generated");
        } catch (IOException e) {
            AtsLogger.printLog("an error occurred during html summary processing: " + e.getMessage());
        }
    }

    private static int getExecutedTestsCount(Results results) {
        return results.getSuites().stream().mapToInt(suiteInfo -> suiteInfo.getTestsPassed() + suiteInfo.getTestsFailed()).sum();
    }


    private static String buildSuites(Results results, List<Script> scripts, Map<String, Integer> testSuiteActionsCount, Map<String, Long> testDurations) {
        return results.getSuites().stream().sorted((suiteInfo, t1) -> Math.toIntExact(suiteInfo.getStarted() - t1.getStarted())).map(suite -> buildSuite(suite, scripts, testSuiteActionsCount, testDurations)).collect(Collectors.joining());
    }

    private static String buildSuite(SuiteInfo suite, List<Script> scripts, Map<String, Integer> testSuiteActionsCount, Map<String, Long> testDurations) {
        String result = summarySuiteTemplate.replace("${playlistName}", suite.getName());
        int testCount = suite.getTestsFailed() + suite.getTestsPassed() + suite.getTestsFiltered();
        int actionsCountForSuite = getActionsCountForSuite(suite.getName(), testSuiteActionsCount);
        result = result.replaceAll(Pattern.quote("${suiteTestCount}"), String.valueOf(testCount));
        result = result.replace("${suiteTestsPassed}", String.valueOf(suite.getTestsPassed()));
        result = result.replace("${suiteTestsFailed}", String.valueOf(suite.getTestsFailed()));
        result = result.replace("${suiteTestsSkipped}", String.valueOf(suite.getTestsFiltered()));
        result = result.replace("${suiteActionsCount}", String.valueOf(actionsCountForSuite));
        result = result.replace("${suiteExecutionTime}", buildDuration(suite.getDuration()));
        result = result.replace("${resultClass}", getResultClass(suite.getStatus()));
        result = result.replace("${idSuiteName}", suite.getName());
        result = result.replace("${tests}", buildTests(suite, scripts, testDurations, testSuiteActionsCount));
        result = result.replace("${stem}", testCount > 1 ? "s" : "");
        result = result.replace("${eem}", actionsCountForSuite > 1 ? "s" : "");
        return result;
    }

    private static String buildTests(SuiteInfo suite, List<Script> scripts, Map<String, Long> testDurations, Map<String, Integer> testSuiteActionsCount) {
        StringBuilder result = new StringBuilder();
        int counter = 0;
        int totalPlaylistScripts = (int) scripts.stream().filter(script -> script.getSuite().equals(suite.getName())).count();

        for (Script script : scripts) {
            if (script.getSuite().equals(suite.getName())) {
                counter++;
                boolean darkBackground = counter % 2 == 0;
                boolean isLast = counter == totalPlaylistScripts;
                boolean isFirst = counter == 1;
                result.append(buildTest(script, testDurations, testSuiteActionsCount, isFirst, isLast, darkBackground));
            }
        }

        return result.toString();
    }

    private static String buildTest(Script script, Map<String, Long> testDurations, Map<String, Integer> testSuiteActionsCount, boolean isFirst, boolean isLast, boolean darkBackground) {
        Integer actionsCount = testSuiteActionsCount.entrySet().stream().filter(entry -> script.getName().equals(entry.getKey().split("\\|")[0])).map(Map.Entry::getValue).findFirst().orElse(0);
        String testPath = "";
        String testName = "";
        int lastDotIndex = script.getName().lastIndexOf('.');

        if (lastDotIndex != -1) {
            testPath = script.getName().substring(0, lastDotIndex);
            testName = script.getName().substring(lastDotIndex + 1);
        } else {
            testPath = "...";
            testName = script.getName();
        }

        String testInfo = summaryTestTemplate.replace("${actionsCount}", String.valueOf(actionsCount))
                .replace("${testExecutionTime}", buildDuration(geDurationForTest(script.getName(), testDurations)))
                .replace("${resultIcon}", getResultClass(script.isPassed() ? "PASS" : "FAIL"))
                .replace("${teem}", actionsCount > 1 ? "s" : "")
                .replace("${testPath}", testPath)
                .replace("${testName}", testName);
        if (isFirst && isLast) {
            testInfo = testInfo.replace("${border-radius}", "full-border-radius");
        }

        if (isLast) {
            testInfo = testInfo.replace("no-bottom-border", "");
            testInfo = testInfo.replace("${border-radius}", "bottom-border-radius");
        }

        if (isFirst) {
            testInfo = testInfo.replace("${tcp-border-radius}", "test-name-prefix-br");
            testInfo = testInfo.replace("${border-radius}", "top-border-radius");
        }

        if (darkBackground) {
            testInfo = testInfo.replace("${background-color}", "test-background-color");
        }
        return testInfo;
    }

    private static Long geDurationForTest(String name, Map<String, Long> testDurations) {
        return testDurations.entrySet().stream().filter(entry -> entry.getKey().contains(name)).map(Map.Entry::getValue).findFirst().orElse(0L);
    }

    private static String getResultClass(String status) {
        return status.equals("PASS") ? "result-pass-icon-transparent" : "result-failed-icon";
    }

    private static int getActionsCountForSuite(String name, Map<String, Integer> testSuiteActionsCount) {
        return testSuiteActionsCount.entrySet().stream().filter(stringIntegerEntry -> name.equals(stringIntegerEntry.getKey().split("\\|")[1])).mapToInt(Map.Entry::getValue).sum();
    }

    private static int getTestsPassed(Results results) {
        return results.getSuites().stream().mapToInt(SuiteInfo::getTestsPassed).sum();
    }

    private static int getTestsFailed(Results results) {
        return results.getSuites().stream().mapToInt(SuiteInfo::getTestsFailed).sum();
    }

    private static String buildDuration(long durationInMillis) {
        Duration duration = Duration.ofMillis(durationInMillis);

        long hours = duration.toHours();
        long minutes = duration.toMinutesPart();
        long seconds = duration.toSecondsPart();

        return String.format("%02d:%02d:%02d", hours, minutes, seconds);

    }

}
