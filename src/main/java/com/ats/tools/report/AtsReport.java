package com.ats.tools.report;

public class AtsReport {

	public static final String ATS_JSON_SUITES = "ats-suites.json";
	
	public static final String ATS_REPORT = "ats-report";
	public static final String ATS_TEST_REPORT = "ats-test-report";
	
    public static final String ATS_TEST_REPORT_XML_NAME = ATS_REPORT + ".xml";
	public final static String ATS_REPORT_HTML = ATS_REPORT + ".html";
	
	public final static String ATS_TEST_REPORT_HTML = ATS_TEST_REPORT + ".html";
    
    public static final String ATS_TEST_REPORT_HTML_DATA_FILE_NAME = "actions.xml";
    public static final String INCONSISTENCY_WARNING = "<div class='inconsistency-warning'><div class='warning-icon'></div><div>Inconsistency in execution results has been found! Please contact support.</div></div>";
    public static final String ATS_RESULTS_JSON_FILE_NAME = "ats-results.json";
	
	public static final String ATS_REPORT_ENV = "ATS_REPORT";
	public static final String MGT_REPORT = "mgt-report";

	public static final String VALID_REPORT = "validation-report";
	public static final String TEMPLATE_EXTENSION = "_html.xml";
		
	private AtsReport() {
		//prevent instanciation
	}
}