package com.ats.tools.report.analytics;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public enum ActionTypesEnum {
	NAVIGATION("ActionChannel", "ActionWindow", "GotoUrl", "ActionApi"), CALL_SCRIPT("ActionCallscript"), USER_ACTION("ActionText", "ActionMouse", "ActionSelect"), ASSERT("ActionAssert", "ActionProperty"), TECHNICAL("ActionScripting", "ActionButton", "ActionPropertySet"), SSA("StartScriptAction"), OTHERS;
	private final List<String> actionTypesList;

	ActionTypesEnum(String... actionTypes) {
		actionTypesList = List.of(actionTypes);
	}

	public static ActionTypesEnum of(String actionType) {
		return Arrays.stream(ActionTypesEnum.values()).filter(actionTypesEnum -> actionTypesEnum.actionTypesList.stream().anyMatch(s -> {
			String[] actionTypeArray = actionType.split("\\.");
			String actionTypeName = actionTypeArray[actionTypeArray.length - 1];
			return actionTypeName.contains(s);
		})).findFirst().orElse(OTHERS);
	}

	public void executeWithEnum() {

	}

	public static void executeWithEnum(String actionTypeString, Consumer<ActionTypesEnum> enumProcessor) {
		ActionTypesEnum actionType = ActionTypesEnum.OTHERS;
		switch (ActionTypesEnum.of(actionTypeString)) {
			case NAVIGATION: {
				actionType = NAVIGATION;
				break;
			}
			case CALL_SCRIPT: {
				actionType = CALL_SCRIPT;
				break;
			}
			case USER_ACTION: {
				actionType = USER_ACTION;
				break;
			}
			case ASSERT: {
				actionType = ASSERT;
				break;

			}
			case TECHNICAL: {
				actionType = TECHNICAL;
				break;
			}
			case SSA: {
				break;
			}
			default:
				break;
		}
		enumProcessor.accept(actionType);
	}
}
