package com.ats.tools.report;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.w3c.dom.DOMException;

import com.ats.driver.AtsManager;
import com.ats.tools.Utils;

public class PrepareProjectExecution {

	public static void main(String[] args) throws DOMException, Exception {

		String targetFolder = "target";
		if(args.length > 0) {
			targetFolder = args[0];
		}

		final Path pomFilePath = Paths.get("pom.xml");

		if(pomFilePath.toAbsolutePath().toFile().exists()) {
			try {

				final Path targetPath = Paths.get(targetFolder);
				final File targetFile = targetPath.toFile();
				if(targetFile.exists()) {
					try {
						Utils.deleteRecursiveFiles(targetFile);
					}catch(Exception e) {}
					targetFile.mkdir();
				}

				final File buildFile = targetPath.resolve("build.xml").toFile();
				final FileWriter fw = new FileWriter(buildFile);
				fw.write("<project basedir=\".\" default=\"compile\">");
				fw.write("<copy todir=\"classes\"> ");
				fw.write("<fileset dir=\"..\\src\" includes='assets/**'/>");
				fw.write("</copy>");
				fw.write("<property name=\"lib.dir\" value=\"lib\"/>");
				fw.write("<path id=\"classpath\">");
				fw.write("<fileset dir=\"" + AtsManager.getAtsHomeFolder() + "\\libs\" includes=\"**/*.jar\"/>");
				fw.write("</path>");
				fw.write("<target name=\"compile\">");
				fw.write("<mkdir dir=\"classes\"/>");
				fw.write("<javac srcdir=\"generated\" destdir=\"classes\" classpathref=\"classpath\"/>");
				fw.write("</target>");
				fw.write("</project>");

				fw.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
