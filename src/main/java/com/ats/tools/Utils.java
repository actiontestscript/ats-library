/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.tools;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.module.ModuleDescriptor.Version;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import com.ats.executor.ActionStatus;
import com.ats.generator.variables.parameter.ParameterDataFile;
import com.ats.recorder.VisualElement;
import com.google.gson.JsonArray;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

public class Utils {

	public static final String EMPTY_SCREEN = "iVBORw0KGgoAAAANSUhEUgAAAJYAAABkCAYAAABkW8nwAAAAxnpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjabVBbDsMgDPvPKXYE8uCR49CVSbvBjr9A0qndZgnH4NaEwHg9H3CbIBSQXFvRUpJBVJS6iZYcfTEmWbygJTy8nkORMMiO2Cr7tsUPeJzjJ8BLN5VPQe0exnY1NC6g9hVEXnh2NPUeQRpBTG5gBHR/Vira6vkJ20hXNF8wSdA/k+rez77a9PZs9zDRYORkzCzeAM8lwN0EGiMLTZVM8+LMJTqxgfyb0wF4A/k4WSK+t3bkAAABhWlDQ1BJQ0MgcHJvZmlsZQAAeJx9kT1Iw1AUhU9TRdGqgx1EHCJUJ7tUEcdSxSJYKG2FVh1MXvoHTRqSFBdHwbXg4M9i1cHFWVcHV0EQ/AFxdnBSdJES70sKLWK88Hgf591zeO8+QGhUmGp2RQFVs4xUPCZmc6tizyt86McgxhGRmKkn0osZeNbXPXVT3YV5lnffnzWg5E0G+ETiKNMNi3iDeHbT0jnvEwdZSVKIz4mnDLog8SPXZZffOBcdFnhm0Mik5omDxGKxg+UOZiVDJZ4hDimqRvlC1mWF8xZntVJjrXvyFwby2kqa67TGEMcSEkhChIwayqjAQph2jRQTKTqPefhHHX+SXDK5ymDkWEAVKiTHD/4Hv2drFqYjblIgBnS/2PbHBNCzCzTrtv19bNvNE8D/DFxpbX+1Acx9kl5va6EjYGgbuLhua/IecLkDjDzpkiE5kp+WUCgA72f0TTlg+BboW3Pn1jrH6QOQoVkt3wAHh8BkkbLXPd7d2zm3f3ta8/sBzblyy7jWGv0AAA12aVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/Pgo8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA0LjQuMC1FeGl2MiI+CiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIKICAgIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiCiAgICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iCiAgICB4bWxuczpHSU1QPSJodHRwOi8vd3d3LmdpbXAub3JnL3htcC8iCiAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIKICAgeG1wTU06RG9jdW1lbnRJRD0iZ2ltcDpkb2NpZDpnaW1wOjJhMmU1YmQ4LTAxOWUtNDUzMC05NzU0LTUyNmNiNmQzOTdmNCIKICAgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpjOTkyY2NmNy0xYTk2LTRjZWEtOGJlMi00ZTc1NWI0NzhlNWYiCiAgIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDoyMDJlNzM4OC1lMGU2LTQ2MTMtYjI3NC1lMmY3MzcwNDAyZjQiCiAgIGRjOkZvcm1hdD0iaW1hZ2UvcG5nIgogICBHSU1QOkFQST0iMi4wIgogICBHSU1QOlBsYXRmb3JtPSJXaW5kb3dzIgogICBHSU1QOlRpbWVTdGFtcD0iMTcwNTIyNDY1Njg3MTA1NCIKICAgR0lNUDpWZXJzaW9uPSIyLjEwLjM0IgogICB0aWZmOk9yaWVudGF0aW9uPSIxIgogICB4bXA6Q3JlYXRvclRvb2w9IkdJTVAgMi4xMCIKICAgeG1wOk1ldGFkYXRhRGF0ZT0iMjAyNDowMToxNFQxMDozMDo1NiswMTowMCIKICAgeG1wOk1vZGlmeURhdGU9IjIwMjQ6MDE6MTRUMTA6MzA6NTYrMDE6MDAiPgogICA8eG1wTU06SGlzdG9yeT4KICAgIDxyZGY6U2VxPgogICAgIDxyZGY6bGkKICAgICAgc3RFdnQ6YWN0aW9uPSJzYXZlZCIKICAgICAgc3RFdnQ6Y2hhbmdlZD0iLyIKICAgICAgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDphODQ1OTVhOC00MDkyLTQwZjYtYmFlZC05YzUxYjEzZmE0YTciCiAgICAgIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkdpbXAgMi4xMCAoV2luZG93cykiCiAgICAgIHN0RXZ0OndoZW49IjIwMjQtMDEtMTRUMTA6MzA6NTYiLz4KICAgIDwvcmRmOlNlcT4KICAgPC94bXBNTTpIaXN0b3J5PgogIDwvcmRmOkRlc2NyaXB0aW9uPgogPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgIAo8P3hwYWNrZXQgZW5kPSJ3Ij8+i5F77gAAAAlwSFlzAAAAsgAAALIBa5Ro4AAAAAd0SU1FB+gBDgkeOBju0QIAAAHZSURBVHja7du/axNhHAfg79WrHRSxBVHp4GLrZDfBTXBV8D8QcXERRdBZR3FzD3RzcfPfcBIsOlZtMdb0R1JC0ktyr4MUSyA0PzqZ59nuuHe4D5/3vffgLgIAAAAAAAAAAAAAAAAAAAAAAAAAAPifZdN0s+/e770ad+xMlnUvL+b5+Emn9q2bZ19PS9b5lE2kl+MPTY0s4tz4w7N6RExNsWYs2igWioVigWKhWCgWKBaKhWKBYqFYKBaDpYhSCop14n7XOmvNZvlZEop1ojY3irlqtWhIYji5CI5X3++tFd20VBTlmaKbqqfz7JJUrFgTW19v17O/X9vmWz87XyWiWBMrivJXq1XeODzeb/ZWUkptySjWZKvVt+JLRMz+ez1MC7Va96NkFGtsZZkOdnY71/vPb293L0rH5v2otyOtVt8P5iPifv/5lNLy7k6nsjCfN0eYw61pCjoztwZ78uLHp4hYiYjIT2WNa8tzR//S+fDo4YV7UvIoHMnj5xu3D0s1wN1KZeuqpBRrtGCy9Oy4S3oz8VRSijXKI3ApIu4MsZN4sLq6e15iNu/DzbYUiyniTd+GvZNFmu2/9qAsrkTEntQAAAAAAAAAAAAAAAAAAAAAAAAAAIBp8ge5OnzzSBa+EwAAAABJRU5ErkJggg==";

	private static final String WHITE_PIXEL = "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH5wYKDCwDjkpXLgAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAAMSURBVAjXY/j//z8ABf4C/tzMWecAAAAASUVORK5CYII=";
	private static final String VALID_JAVA_CHAR = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_$";

	public static final okhttp3.MediaType JSON_MEDIA = okhttp3.MediaType.get("application/json; charset=utf-8");
	public static final okhttp3.MediaType TEXT_MEDIA = okhttp3.MediaType.get("text/plain");
	
	private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	private static final String[] DATES_PATTERN = new String[] {DATE_FORMAT, "yyyy-MM-dd 'at' HH:mm:ss"};

	//-----------------------------------------------------------------------------------------------------------
	// Dates util
	//-----------------------------------------------------------------------------------------------------------
	
	public static Date parseDateFormat(String value) {
		if(value != null && !value.isBlank()) {
			try {
				 return DateUtils.parseDate(value, DATES_PATTERN);
			} catch (Exception e) {	}
		}

		final Calendar today = Calendar.getInstance();
		today.clear(Calendar.HOUR); today.clear(Calendar.MINUTE); today.clear(Calendar.SECOND);

		return today.getTime();
	}

	public static String dateFormat(Date value) {
		return DateFormatUtils.format(value, DATE_FORMAT);
	}
	
	//-----------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------
	
	public static String unescapeAts(String data) {
		return data.replaceAll("&sp;", " ").replaceAll("&co;", ",").replaceAll("&eq;", "=").replaceAll("&rb;", "]").replaceAll("&lb;", "[");
	}

	public static String escapeHTML(String str) {
		return str.codePoints().mapToObj(c -> c > 127 || "\"'<>&".indexOf(c) != -1 ?
				"&#" + c + ";" : new String(Character.toChars(c)))
				.collect(Collectors.joining());
	}

	public static boolean isNumeric(String numString) {
		if (numString == null) {
			return false;
		}
		try {
			Double.parseDouble(numString);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	public static int string2Int(String value){
		return string2Int(value, 0);
	}

	public static int string2Int(String value, int defaultValue){
		try {
			return Integer.parseInt(value.replaceAll("\\s", ""));
		} catch (NullPointerException | NumberFormatException e) {
			return defaultValue;
		}
	}

	public static long string2Long(String value){
		try {
			return Long.parseLong(value.replaceAll("\\s", ""));
		} catch (NullPointerException | NumberFormatException e) {
			return 0L;
		}
	}

	public static boolean isNotEmpty(String value){
		return value != null && !value.isBlank();
	}

	public static double string2Double(String value){
		try {
			return Double.parseDouble(value.replaceAll("\\s", ""));
		} catch (NullPointerException | NumberFormatException e) {
			return 0D;
		}
	}

	public static String truncateString(String value, int length)
	{
		if (value != null && value.length() > length)
			value = value.substring(0, length);
		return value;
	}

	public static JsonArray string2JsonArray(String value){
		final char[] letters = value.toCharArray();
		JsonArray array = new JsonArray();
		for (final char ch : letters) {
			array.add(new JsonPrimitive((int)ch));
		}
		return array;
	}

	public static String getShortUid() {
		char[] chars = "ABSDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
		Random r = new Random(System.currentTimeMillis());
		char[] id = new char[8];
		for (int i = 0;  i < 8;  i++) {
			id[i] = chars[r.nextInt(chars.length)];
		}
		return new String(id);
	}

	public static String checkJsonData(String jsonStr) {
		if(jsonStr != null && jsonStr.length() > 0 && ((jsonStr.startsWith("{") && jsonStr.endsWith("}")) || (jsonStr.startsWith("[") && jsonStr.endsWith("]")))) {
			try {
				JsonParser.parseString(jsonStr) ;
				return jsonStr;
			} catch (JsonParseException e) {}
		}
		return null;
	}

	public static boolean isXmlLike(String inXMLStr) {

		boolean retBool = false;
		inXMLStr = inXMLStr.trim();

		if(inXMLStr != null && inXMLStr.length() > 0 && inXMLStr.startsWith("<") && inXMLStr.endsWith(">")) {

			Pattern pattern;
			Matcher matcher;

			final String XML_PATTERN_STR = "<(\\S+?)(.*?)>(.*?)(</\\1>)?";

			pattern = Pattern.compile(XML_PATTERN_STR,
					Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE);

			matcher = pattern.matcher(inXMLStr);
			retBool = matcher.matches();
		}
		return retBool;
	}

	public static String cleanHtmlCode(String value) {
		return value.replaceAll("\\r?\\n", "").replaceAll(" ?<!----> ?", "");
	}

	//-------------------------------------------------------------------------------------------------------------------------------------------
	//  Image utils
	//-------------------------------------------------------------------------------------------------------------------------------------------

	public static String getImageData(boolean isError, String imgType, ArrayList<byte[]> images, VisualElement element, int ref) {

		if (images.size() > 0 && images.get(images.size() - 1) != null) {
			/*if (element != null && element.getRectangle() != null) {
				int imageRef = 0;
				if (isError && images.size() > 0) {
					imageRef = images.size() - 1;
				} else if (images.size() > ref) {
					imageRef = ref;
				}

				final TestBound bound = element.getRectangle();

				try {

					final BufferedImage image = ImageIO.read(new ByteArrayInputStream(images.get(imageRef)));

					final Graphics2D g1 = image.createGraphics();
					g1.setColor(new Color(255, 0, 255));
					g1.setStroke(new BasicStroke(3));
					g1.drawRect(bound.getX().intValue(), bound.getY().intValue() - 10, bound.getWidth().intValue(), bound.getHeight().intValue());
					g1.dispose();

					ByteArrayOutputStream baos = new ByteArrayOutputStream();

					ImageIO.write(image, imgType, baos);

					return Base64.getEncoder().encodeToString(baos.toByteArray());

				} catch (Exception e) {
					System.out.println(e.getMessage());
					
				}

			} else {*/
				return Base64.getEncoder().encodeToString(images.get(images.size() - 1));
			//}
		}

		return EMPTY_SCREEN;
	}	

	public static byte[] loadImage(URL url) {

		ByteArrayOutputStream output = new ByteArrayOutputStream();

		try (InputStream inputStream = url.openStream()) {

			int n = 0;
			byte [] buffer = new byte[ 1024 ];

			while (-1 != (n = inputStream.read(buffer))) {
				output.write(buffer, 0, n);
			}

			return output.toByteArray();

		} catch (IOException e) {

		}

		return null;
	}

	public static byte[] getWhitePixel() {
		return Base64.getDecoder().decode(WHITE_PIXEL);
	}

	//-------------------------------------------------------------------------------------------------------------------------------------------
	//  Files utils
	//-------------------------------------------------------------------------------------------------------------------------------------------

	public static boolean deleteRecursive(File path) throws FileNotFoundException{
		if (!path.exists()) throw new FileNotFoundException(path.getAbsolutePath());
		boolean ret = true;
		if (path.isDirectory()){
			for (final File f : path.listFiles()){
				ret = ret && Utils.deleteRecursive(f);
			}
		}
		return ret && path.delete();
	}

	public static void deleteRecursiveFiles(File f) throws FileNotFoundException{
		if (!f.exists()) throw new FileNotFoundException(f.getAbsolutePath());
		if (f.isDirectory()){
			for (final File f0 : f.listFiles()){
				if(f0.isDirectory()) {
					deleteRecursiveFiles(f0);
					if(f0.listFiles().length == 0) {
						f0.delete();
					}
				}else if(f0.isFile()) {
					f0.delete();
				}
			}
		}
	}

	public static void deleteRecursiveJavaFiles(File f) throws FileNotFoundException{
		if (!f.exists()) throw new FileNotFoundException(f.getAbsolutePath());
		if (f.isDirectory()){
			for (final File f0 : f.listFiles()){
				if(f0.isDirectory()) {
					deleteRecursiveJavaFiles(f0);
					if(f0.listFiles().length == 0) {
						f0.delete();
					}
				}else if(f0.isFile() && f0.getName().toLowerCase().endsWith(".java")) {
					f0.delete();
				}
			}
		}
	}

	public static void copyFiles(String src, String dest, boolean overwrite) {
		try {
			Files.walk(Paths.get(src)).forEach(a -> {
				final String aPath = a.toString();
				final Path b = Paths.get(dest, aPath.substring(src.length()));

				final String fileName = a.getFileName().toString();

				try {

					if (!aPath.equals(src)) {
						if(Files.isDirectory(a) || fileName.endsWith(".java") && fileName.substring(0, fileName.length()-5).chars().allMatch(c -> VALID_JAVA_CHAR.indexOf(c) != -1)) {
							Files.copy(a, b, overwrite ? new CopyOption[]{StandardCopyOption.REPLACE_EXISTING} : new CopyOption[]{});
						}
					}

				} catch (IOException e) {
					e.printStackTrace();
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------------------------------------------------------------------------------
	//  Files utils
	//-------------------------------------------------------------------------------------------------------------------------------------------

	public static boolean checkUrl(ActionStatus status, String urlPath){

		URL url = null;

		try {
			url = new URI(urlPath).toURL();
		}catch(MalformedURLException | URISyntaxException e) {
			status.setPassed(false);
			status.setCode(ActionStatus.MALFORMED_GOTO_URL);
			status.setData(urlPath);
			return false;
		}

		int responseCode;
		try {
			if(urlPath.startsWith("https")) {
				HttpsURLConnection con =	(HttpsURLConnection)url.openConnection();
				con.setRequestMethod("HEAD");
				responseCode = con.getResponseCode();
			}else {
				HttpURLConnection con =	(HttpURLConnection)url.openConnection();
				con.setRequestMethod("HEAD");
				responseCode = con.getResponseCode();
			}
		}catch (IOException e) {
			status.setPassed(false);
			status.setCode(ActionStatus.UNKNOWN_HOST_GOTO_URL);
			status.setData(e.getMessage());
			return false;
		}

		if(responseCode == HttpURLConnection.HTTP_OK) {
			return true;
		}else {
			status.setPassed(false);
			status.setCode(ActionStatus.UNREACHABLE_GOTO_URL);
			status.setData(urlPath);
			return false;
		}
	}

	public static String removeExtension(String s) {

		String separator = System.getProperty("file.separator");
		String filename;

		int lastSeparatorIndex = s.lastIndexOf(separator);
		if (lastSeparatorIndex == -1) {
			filename = s;
		} else {
			filename = s.substring(lastSeparatorIndex + 1);
		}

		int extensionIndex = filename.lastIndexOf(".");
		if (extensionIndex == -1)
			return filename;

		return filename.substring(0, extensionIndex);
	}

	public static String leftString(final String str, final int len) {
		if (str == null) {
			return null;
		}
		if (len < 0) {
			return "";
		}
		if (str.length() <= len) {
			return str;
		}
		return str.substring(0, len);
	}

	public static String getCauseStackTraceString(Throwable e, String indent) {

		StringBuilder sb = new StringBuilder();

		Throwable cause = e.getCause();
		if (cause != null) {
			sb.append(indent);
			sb.append("Java exception caused by -> ");
			sb.append(getCauseStackTraceString(cause, indent));
		}else {
			sb.append(e.toString());
			sb.append("\n");

			StackTraceElement[] stack = e.getStackTrace();
			if (stack != null) {
				for (StackTraceElement stackTraceElement : stack) {
					sb.append(indent);
					sb.append("\tat ");
					sb.append(stackTraceElement.toString());
					sb.append("\n");
				}
			}

			Throwable[] suppressedExceptions = e.getSuppressed();
			// Print suppressed exceptions indented one level deeper.
			if (suppressedExceptions != null) {
				for (Throwable throwable : suppressedExceptions) {
					sb.append(indent);
					sb.append("\tSuppressed: ");
					sb.append(getCauseStackTraceString(throwable, indent + "\t"));
				}
			}
		}

		return sb.toString();
	}

	//-------------------------------------------------------------------------------------------------------------------------------------------
	//  Groovy script
	//-------------------------------------------------------------------------------------------------------------------------------------------

	/*public static String[] runGroovyScript(String scriptName, String[] libsPath, String[] parameters) throws IOException, ResourceException, ScriptException {

		final Binding binding = new Binding();
		binding.setVariable("args", parameters);

	    final GroovyScriptEngine engine = new GroovyScriptEngine(libsPath);

	    final Object result = engine.run(scriptName, binding);

	    if(result == null) {
	    	return new String[0];
	    }else if(result instanceof String[]) {
	    	return (String[])result;
	    }else{
	    	return new String[] {result.toString()};
	    }
	}*/

	//-------------------------------------------------------------------------------------------------------------------------------------------
	//  JSON CSV utils
	//-------------------------------------------------------------------------------------------------------------------------------------------

	public static String cutBOM(String value) {
		if (value.length() > 1) {
	        byte[] bytes = value.substring(0, 1).getBytes();
	        if (bytes.length >= 3 && bytes[0] == (byte) 0xEF && bytes[1] == (byte) 0xBB && bytes[2] == (byte) 0xBF) {
	            return value.substring(1);
	        }
	    }
		return value;
	}
	
	public static ParameterDataFile loadData(String url) throws MalformedURLException, URISyntaxException{
		return loadData(new ActionStatus(), new URI(url).toURL());
	}

	public static ParameterDataFile loadData(ActionStatus status,String url) throws MalformedURLException, URISyntaxException{
		return loadData(status, new URI(url).toURL());
	}

	public static ParameterDataFile loadData(ActionStatus status, URL url){
		return new ParameterDataFile(status, url);
	}

	//-------------------------------------------------------------------------------------------------------------------------------------------
	//  ATS artifacts utils
	//-------------------------------------------------------------------------------------------------------------------------------------------

	private final static Pattern SYS_VERSION_PATTERN = Pattern.compile("<a href\\s?=\\s?\"([^\"]+\\.(zip|tgz))\">");

	public static String getArtifactLastVersion(String folderUrl) {

		String version = null;

		try {
			final HttpURLConnection con = (HttpURLConnection) new URI(folderUrl).toURL().openConnection();
			if(con.getResponseCode() == 200) {

				final InputStream inputStream = con.getInputStream();
				final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

				final StringBuilder builder = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
				inputStream.close();

				final ArrayList<String> versions = new ArrayList<>();

				final Matcher matcher = SYS_VERSION_PATTERN.matcher(builder.toString());
				int index = 0;
				while (matcher.find(index)) {
					versions.add(matcher.group(1));
					index = matcher.end();
				}

				if(versions.size() > 0) {
					final Version v = versions.stream()
							.map(Version::parse)
							.sorted(Collections.reverseOrder())
							.findFirst().get();

					if(v != null) {
						version = v.toString().replace(".zip", "").replace(".tgz", "");
					}
				}
			}
		}catch(IOException | URISyntaxException e) {}

		return version;
	}

	public static String escapeMathSymbolsOnce(String data) {
		return data.replace("<", "").replace(">", "").replace("=","").replace("~","");
	}
}