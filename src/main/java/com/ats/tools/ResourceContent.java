/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.tools;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import com.google.common.io.Resources;

public class ResourceContent {

	private static String scrollElementJavaScript;
	private static String searchElementsJavaScript;
	private static String searchShadowElementsJavaScript;
	private static String documentSizeJavaScript;
	private static String elementAttributesJavaScript;
	private static String elementKeysAndAttributesJavaScript;
	private static String elementFunctionsJavaScript;
	private static String elementTextDataJavaScript;
	private static String parentElementJavaScript;
	private static String parentElementHtmlCode;
	private static String parentElementJavaScriptIE;
    private static String jasperReportScript;
    private static String jasperReportCss;
    private static String jasperReportHtmlContent;

	private static String htmlReportTemplate;
	private static String htmlReportActionHtmlTemplate;
	private static String htmlReportCss;
	private static String htmlReportJavascript;
	private static String htmlReportOneElementDataActionTemplate;
	private static String htmlReportTwoElementsDataActionTemplate;
	private static String htmlReportThreeElementsDataActionTemplate;
	private static String htmlReportApiCallActionTemplate;
	private static String htmlReportAssertPropertyActionTemplate;
	private static String htmlReportAssertOccurrenceActionTemplate;
	private static String htmlReportAssertValueActionTemplate;
	private static String htmlReportPlaylistTemplate;
	private static String htmlReportSuiteTemplate;
	private static String htmlReportExecutionTemplate;
	private static String atsLogo;
	private static String pageStyle;
	private static String htmlReportSummaryTemplate;
	private static String htmlReportSummarySuiteTemplate;

	private static String htmlReportFooter;

	static {
		documentSizeJavaScript = getScript("documentSize");
		elementAttributesJavaScript = getScript("elementAttributes");
		elementKeysAndAttributesJavaScript = getScript("elementKeysAndAttributes");
		elementFunctionsJavaScript = getScript("elementFunctions");
		parentElementJavaScript = getScript("parentElement");
		parentElementHtmlCode = getScript("parentElementHtmlCode");
		parentElementJavaScriptIE = getScript("parentElementIE");
		scrollElementJavaScript = getScript("scrollElement");
		searchElementsJavaScript = getScript("searchElements");
		elementTextDataJavaScript = getScript("elementTextData");
		searchShadowElementsJavaScript = getScript("searchShadowElements");
        jasperReportScript = getScript("jasperreport/report");
        jasperReportCss = getCss("jasperreport/report");
        jasperReportHtmlContent = getHtml("jasperreport/report");

		htmlReportTemplate = getHtml("htmlreport/html-report");
		htmlReportActionHtmlTemplate = getHtml("htmlreport/new-report-action");
		htmlReportCss = getCss("htmlreport/html-report");
		htmlReportJavascript = getScript("htmlreport/html-report");
		htmlReportOneElementDataActionTemplate = getHtml("htmlreport/1-tier-body-action");
		htmlReportTwoElementsDataActionTemplate = getHtml("htmlreport/2-tier-body-action");
		htmlReportThreeElementsDataActionTemplate = getHtml("htmlreport/3-tier-body-action");
		htmlReportApiCallActionTemplate = getHtml("htmlreport/api-call-action-template");
		htmlReportPlaylistTemplate = getHtml("htmlreport/playlist-template");
		htmlReportSuiteTemplate = getHtml("htmlreport/new-suite-report");
		htmlReportExecutionTemplate = getHtml("htmlreport/executions-template");
		htmlReportAssertPropertyActionTemplate = getHtml("htmlreport/action-assert-property-template");
		htmlReportAssertOccurrenceActionTemplate = getHtml("htmlreport/action-assert-occurrence-template");
		htmlReportAssertValueActionTemplate = getHtml("htmlreport/action-assert-value-template");
		htmlReportSummaryTemplate = getHtml("htmlreport/summary");
		htmlReportSummarySuiteTemplate = getHtml("htmlreport/summarySuite");

		htmlReportFooter = getHtml("htmlreport/footer");

		atsLogo = Base64.getEncoder().encodeToString(getAtsByteLogo());
		pageStyle = getData("jsStyle");
	}

	//-----------------------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------

    private static String getResource(String type, String resourceName) {
        try {
            final String content = Resources.toString(ResourceContent.class.getResource("/" + type + "/" + resourceName + "." + (type.equals("javascript") ? "js" : type)), StandardCharsets.UTF_8);
            return content.replaceAll("[\n\t\r]", "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getScript(String resourceName) {
        return getResource("javascript", resourceName);
    }

    public static String getCss(String resourceName) {
        return getResource("css", resourceName);
    }

    public static String getHtml(String resourceName) {
        return getResource("html", resourceName);
    }

	/*private static byte[] getIconByName(String iconName, int iconSize){
		try {
			return ByteStreams.toByteArray(ResourceContent.class.getResourceAsStream("/icon/" + iconSize + "/" + iconName + ".png"));
		} catch (IOException e) {
			return new byte[0];
		}
	}*/

	public static byte[] getAtsByteLogo(){
		try {
			return Resources.toByteArray(ResourceContent.class.getResource("/icons/ats_logo.png"));
		} catch (IOException e1) {
			return new byte[] {};
		}
	}

	public static String getData(String name) {
		try {
			return Resources.toString(ResourceContent.class.getResource("/" + name), StandardCharsets.UTF_8);
		} catch (IOException e1) {
			return "";
		}
	}

	//-----------------------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------

	public static String getPageStyle() {
		return pageStyle;
	}

	public static String getAtsLogo() {
		return atsLogo;
	}

	public static String getScrollElementJavaScript() {
		return scrollElementJavaScript;
	}

	public static String getSearchElementsJavaScript() {
		return searchElementsJavaScript;
	}

	public static String getSearchShadowElementsJavaScript() {
		return searchShadowElementsJavaScript;
	}

	public static String getDocumentSizeJavaScript() {
		return documentSizeJavaScript;
	}

	public static String getElementAttributesJavaScript() {
		return elementAttributesJavaScript;
	}

	public static String getElementKeysAndAttributesJavaScript() {
		return elementKeysAndAttributesJavaScript;
	}

	public static String getElementFunctionsJavaScript() {
		return elementFunctionsJavaScript;
	}

	public static String getElementTextDataJavaScript() {
		return elementTextDataJavaScript;
	}

	public static String getParentElementJavaScript() {
		return parentElementJavaScript;
	}

	public static String getParentElementHtmlCode() {
		return parentElementHtmlCode;
	}

	public static String getParentElementJavaScriptIE() {
		return parentElementJavaScriptIE;
	}

    public static String getJasperReportScript() {
        return jasperReportScript;
    }

    public static String getJasperReportCss() {
        return jasperReportCss;
    }

    public static String getJasperReportHtmlContent() {
        return jasperReportHtmlContent;
    }

	public static String getHtmlReportActionHtmlTemplate() {
		return htmlReportActionHtmlTemplate;
	}

	public static String getHtmlReportCss() {
		return htmlReportCss;
	}

	public static String getHtmlReportTemplate() {
		return htmlReportTemplate;
	}

	public static String getHtmlReportJavascript() {
		return htmlReportJavascript;
	}
	public static String getHtmlReportOneElementDataActionTemplate() {
		return htmlReportOneElementDataActionTemplate;
	}
	public static String getHtmlReportTwoElementsDataActionTemplate() {
		return htmlReportTwoElementsDataActionTemplate;
	}
	public static String getHtmlReportThreeElementsDataActionTemplate() {
		return htmlReportThreeElementsDataActionTemplate;
	}
	public static String getHtmlReportPlaylistTemplate() {
		return htmlReportPlaylistTemplate;
	}
	public static String getHtmlReportSuiteTemplate() {
		return htmlReportSuiteTemplate;
	}
	public static String getHtmlReportExecutionTemplate() {
		return htmlReportExecutionTemplate;
	}
	public static String getHtmlReportApiCallActionTemplate() {
		return htmlReportApiCallActionTemplate;
	}
	public static String getHtmlReportAssertPropertyActionTemplate() {
		return htmlReportAssertPropertyActionTemplate;
	}
	public static String getHtmlReportAssertOccurrenceActionTemplate() {
		return htmlReportAssertOccurrenceActionTemplate;
	}
	public static String getHtmlReportAssertValueActionTemplate() {
		return htmlReportAssertValueActionTemplate;
	}
	public static String getHtmlReportSummaryTemplate() {return htmlReportSummaryTemplate;}
	public static String getHtmlReportSummarySuiteTemplate() {return htmlReportSummarySuiteTemplate;}

	public static String getHtmlFooter() {return htmlReportFooter;}
}