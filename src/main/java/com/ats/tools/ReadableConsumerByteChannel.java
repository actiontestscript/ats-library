/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.tools;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.util.function.IntConsumer;

public class ReadableConsumerByteChannel implements ReadableByteChannel{

	private final ReadableByteChannel rbc;
	private final IntConsumer onRead;
	private final int totalBytes;

	private int totalByteRead;

	private int currentPercent = 0;

	public ReadableConsumerByteChannel(ReadableByteChannel rbc, int totalBytes, IntConsumer onBytesRead) {
		this.rbc = rbc;
		this.totalBytes = totalBytes;
		this.onRead = onBytesRead;
	}

	@Override
	public int read(ByteBuffer dst) throws IOException {
		int nRead = rbc.read(dst);
		notifyBytesRead(nRead);
		return nRead;
	}

	protected void notifyBytesRead(int nRead) {
		if (nRead <= 0) {
			return;
		}
		totalByteRead += nRead;

		if (totalBytes != -1) {
			int percent = (int) (((float) totalByteRead / totalBytes) * 100);
			if (percent % 5 == 0 && currentPercent != percent) {
				currentPercent = percent;
				onRead.accept(currentPercent);
			}
		} else if (totalByteRead % 10000 == 0) {
			onRead.accept(totalByteRead / 10000);
		}
	}

	@Override
	public boolean isOpen() {
		return rbc.isOpen();
	}

	@Override
	public void close() throws IOException {
		rbc.close();
	}
}

