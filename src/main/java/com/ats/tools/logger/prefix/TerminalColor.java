package com.ats.tools.logger.prefix;

import static com.diogonunes.jcolor.Ansi.colorize;
import static com.diogonunes.jcolor.Attribute.BRIGHT_BLUE_TEXT;
import static com.diogonunes.jcolor.Attribute.BRIGHT_RED_TEXT;
import static com.diogonunes.jcolor.Attribute.BRIGHT_WHITE_TEXT;
import static com.diogonunes.jcolor.Attribute.BRIGHT_YELLOW_TEXT;
import static com.diogonunes.jcolor.Attribute.TEXT_COLOR;

import com.ats.tools.logger.ExecutionLogger;
import com.diogonunes.jcolor.Attribute;

public class TerminalColor extends Terminal {

	@Override
	public String getErrorData() {
		return getBracketLog(colorize(ERROR, BRIGHT_RED_TEXT()));
	}

	@Override
	public String getFailData(String actionName, String testName, int line, String info, String details) {
		final StringBuilder sb =
				new StringBuilder(
						getBracketLog(
								colorize(
										FAILED,
										Attribute.TEXT_COLOR(124)
										)
								)
						)
				.append(actionName)
				.append(" (")
				.append(testName)
				.append(":")
				.append(line)
				.append(")")
				.append(ExecutionLogger.RIGHT_ARROW_LOG)
				.append(info)
				.append(ExecutionLogger.RIGHT_ARROW_LOG)
				.append(details);
		return sb.toString();
	}

	@Override
	public String getScriptData(String value) {
		return getBracketLog(colorize(EXEC, Attribute.TEXT_COLOR(150)));
	}

	@Override
	public String getLoggerData() {
		return getBracketLog(colorize(LOGGER, BRIGHT_WHITE_TEXT()));
	}

	@Override
	public String getInfoData() {
		return getBracketLog(colorize(INFO, BRIGHT_BLUE_TEXT()));
	}

	@Override
	public String getWarningData() {
		return getBracketLog(colorize(WARNING, BRIGHT_YELLOW_TEXT()));
	}

	@Override
	public String getActionData() {
		return getBracketLog(colorize(ACTION, Attribute.TEXT_COLOR(140)));
	}

	@Override
	public String getCommentData(String message) {

		final StringBuilder sb =
				new StringBuilder(getBracketLog(colorize(COMMENT, TEXT_COLOR(103))))
				.append(" <![CDATA[\n")
				.append(message)
				.append("\n]]>");

		return sb.toString();
	}

	@Override
	public String getDriverLog() {
		return getBracketLog(colorize(DRIVER_INFO, BRIGHT_BLUE_TEXT()));
	}

	@Override
	public String getDriverWarning() {
		return getBracketLog(colorize(DRIVER_WARNING, TEXT_COLOR(89, 67, 25)));
	}

	@Override
	public String getDriverError() {
		return getBracketLog(colorize(DRIVER_ERROR, BRIGHT_RED_TEXT()));
	}

	@Override
	public String getDriverOutput() {
		return getBracketLog(colorize(DRIVER_OUTPUT, TEXT_COLOR(109)));
	}

	@Override
	public String getSuiteData() {
		return getBracketLog(colorize(SUITE, TEXT_COLOR(81)));
	}
}