package com.ats.tools.logger.prefix;

public interface ILogType {
	String getFailData(String actionName, String testName, int line, String info, String details);
	String getCommentData(String message);
	String getScriptData(String value);
	String getLoggerData();
	String getErrorData();
	String getInfoData();
	String getSuiteData();
	String getTestData(String value);
	String getWarningData();
	String getActionData();
	String getDriverLog();
	String getDriverWarning();
	String getDriverError();
	String getDriverOutput();
}