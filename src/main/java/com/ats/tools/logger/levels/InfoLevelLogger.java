package com.ats.tools.logger.levels;

import java.io.PrintStream;

import com.ats.script.actions.Action;
import com.google.gson.JsonObject;

public class InfoLevelLogger extends ErrorLevelLogger {

	public InfoLevelLogger(PrintStream out, String type, String level) {
		super(out, type, level);
	}

	@Override
	public void info(String message) {
		printInfo( message);
	}
		
	@Override
	public void action(Action action, String testName, int line) {
		printAction(action.getActionLogs(testName, line, new JsonObject()).toString());
	}

	@Override
	public void driverOutput(String value) {
	}
}