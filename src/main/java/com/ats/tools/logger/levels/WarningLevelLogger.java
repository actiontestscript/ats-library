package com.ats.tools.logger.levels;

import java.io.PrintStream;

public class WarningLevelLogger extends InfoLevelLogger {

	public WarningLevelLogger(PrintStream out, String type, String level) {
		super(out, type, level);
	}

	@Override
	public void warning(String message) {
		printWarning(message);
	}

	@Override
	public void driverOutput(String value) {
	}
}