package com.ats.data;

public class Rectangle {

	private double x;
	private double y;
	private double width;
	private double height;

	private double xMax;
	private double yMax;

	public Rectangle(double x, double y, double w, double h) {
		setX(x);
		setY(y);
		setWidth(w);
		setHeight(h);
	}

	private void recalculateX() {
		xMax = x + width;
	}

	private void recalculateY() {
		yMax = y + height;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
		recalculateX();
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
		recalculateY();
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
		recalculateX();
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
		recalculateY();
	}

	public boolean equals(Rectangle rect) {
		if(rect != null) {
			return rect.getX() == x && rect.getY() == y && rect.getWidth() == width && rect.getHeight() == height;
		}else{
			System.out.println("[WARN] : rectangle input element is null");
			return false;
		}
	}

	public boolean contains(double _x, double _y) {
		return contains(_x, _y, 0.0, 0.0);
	}

	public boolean contains(Rectangle rec) {
		return contains(rec.getX(), rec.getY(), rec.getWidth(), rec.getHeight());
	}

    public boolean contains(double _x, double _y, double _w, double _h) {

        if (this.width < 0 || this.height < 0 || _w < 0 || _h < 0) {
            return false;
        }

        if(_x < this.x || _y < this.y || (_x + _w > xMax) || (_y + _h > yMax)) {
        	return false;
        }

        return true;
    }
}