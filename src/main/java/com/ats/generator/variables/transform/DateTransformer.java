/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.generator.variables.transform;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.ats.executor.ActionTestScript;

public class DateTransformer extends Transformer {

	private static final DateTimeFormatter STANDARD_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final String DATE_ORDER_KEY = "ats.date.order";

	public static final String DATE_PARSE_ERROR = "#DateParseError#";

	private static final Pattern PATTERN_DAY_TRANSFORM = Pattern.compile("(-?\\d+)([ymd])");

	private static final String[] AVAILABLE_DATE_ORDER = {"DMY", "MDY", "MYD", "DYM", "YMD", "YDM"}; 

	private int year = 0;
	private int month = 0;
	private int day = 0;

	private String formatter = "";
	private String dateOrder = "";

	private String error = null;

	public DateTransformer() {} // Needed for serialization

	public DateTransformer(String ... data) {
		for(String item : data){
			final Matcher dataMatcher = PATTERN_DAY_TRANSFORM.matcher(item);
			if(dataMatcher.find()){
				if("y".equals(dataMatcher.group(2))){
					setYear(getInt(dataMatcher.group(1)));
				}else if("m".equals(dataMatcher.group(2))){
					setMonth(getInt(dataMatcher.group(1)));
				}else if("d".equals(dataMatcher.group(2))){
					setDay(getInt(dataMatcher.group(1)));
				}
			}else if(StringUtils.isNoneEmpty(item)) {
				setFormatter(item);
			}
		}

		String dto = System.getenv(DATE_ORDER_KEY);
		if(dto == null) {
			dto = System.getProperty(DATE_ORDER_KEY);
		}
		
		if(dto != null && dto.length() == 3) {
			dto = dto.toUpperCase();
			if(Arrays.asList(AVAILABLE_DATE_ORDER).contains(dto)) {
				this.dateOrder = dto;
			}
		}
	}

	public void setDateOrder(String value) {
		dateOrder = value;
	}

	public static String getTodayValue() {
		return LocalDate.now().format(STANDARD_FORMATTER);
	}

	@Override
	public String getJavaCode() {

		final StringJoiner joiner = new StringJoiner(", ");

		if(StringUtils.isNotEmpty(formatter)) {
			joiner.add("\"" + formatter + "\"");
		}

		if(year != 0) {
			joiner.add("\"" + year + "y\"");
		}

		if(month != 0) {
			joiner.add("\"" + month + "m\"");
		}

		if(day != 0) {
			joiner.add("\"" + day + "d\"");
		}

		final StringBuilder sb = new StringBuilder(", ").append(ActionTestScript.JAVA_DATE_FUNCTION_NAME);
		sb.append("(").append(joiner.toString()).append(")");
		return sb.toString();
	}

	@Override
	public String format(String data) {
		if(data.length() > 0) {
			final String[] dateArray = data.replace("-", "/").replace(".", "/").replace("\\", "/").split("/");

			if(dateArray.length == 3) {

				LocalDate localDate = null;

				try {
					if(dateOrder.length() == 3) {
						Map<String, Integer> parser = new HashMap<>();
						for (int i=0; i<3; i++){
							parser.put(dateOrder.substring(i, i+1), Integer.parseInt(dateArray[i]));
						}
						localDate = LocalDate.of(parser.get("Y"), parser.get("M"), parser.get("D"));

					}else {
						localDate = LocalDate.of(Integer.parseInt(dateArray[0]), Integer.parseInt(dateArray[1]), Integer.parseInt(dateArray[2]));
					}

					localDate = localDate.plusYears(year).plusMonths(month).plusDays(day);

					if(StringUtils.isNotEmpty(formatter)) {
						return localDate.format(DateTimeFormatter.ofPattern(formatter));
					}else {
						return localDate.format(STANDARD_FORMATTER);
					}

				} catch (DateTimeException | IllegalArgumentException e) {
					error =  DATE_PARSE_ERROR + " [dto=" + this.dateOrder + "] -> " + e.getMessage();
				}

			}else {
				error = DATE_PARSE_ERROR + " -> cannot split date : " + data;
			}
		}else {
			error = DATE_PARSE_ERROR + " -> date is empty";
		}

		return error;
	}

	public String getError() {
		return error;
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public String getFormatter() {
		return formatter;
	}

	public void setFormatter(String format) {
		this.formatter = format;
	}
}