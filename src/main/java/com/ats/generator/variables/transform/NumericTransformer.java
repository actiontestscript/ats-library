/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.generator.variables.transform;

import com.ats.MathEvaluation;
import com.ats.executor.ActionTestScript;


public class NumericTransformer extends Transformer {

	private int decimal = -1;
	private boolean comma = false;

	//private static final String NUMERIC_FUNCTION = "0123456789.,()abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWYYZ-*/+^!><=$;";
	//private static final Pattern IF_THEN_ELSE_REGEXP = Pattern.compile("if\\(([^\\)]*);([^\\)]*);([^\\)]*)\\)");

	public NumericTransformer() {} // Needed for serialization

	public NumericTransformer(Object dp, Object useComma) {
		this((int)dp, (boolean)useComma);
	}

	public NumericTransformer(int dp) {
		setDecimal(dp);
		setComma(false);
	}

	public NumericTransformer(int dp, boolean useComma) {
		setDecimal(dp);
		setComma(useComma);
	}

	public NumericTransformer(String data) {
		if(data.contains(",")) {
			setComma(true);
			data = data.replace(",", "");
		}
		setDecimal(getInt(data.replace("dp", "").trim()));
	}

	@Override
	public String getJavaCode() {
		final StringBuilder sb =
				new StringBuilder(", ").
				append(ActionTestScript.JAVA_NUMERIC_FUNCTION_NAME)
				.append("(")
				.append(decimal)
				.append(", ")
				.append(comma)
				.append(")");
		return sb.toString();
	}

	@Override
	public String format(String data) {
		MathEvaluation math = new MathEvaluation(comma, decimal);
		return math.eval(data);
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public int getDecimal() {
		return decimal;
	}

	public void setDecimal(int decimal) {
		this.decimal = decimal;
	}

	public boolean getComma() {
		return comma;
	}

	public void setComma(boolean value) {
		this.comma = value;
	}
}