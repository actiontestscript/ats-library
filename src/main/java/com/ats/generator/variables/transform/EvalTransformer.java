/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.generator.variables.transform;

import com.ats.executor.ActionTestScript;
import com.ats.executor.java.JavaCodeEval;
import com.ats.generator.variables.transform.code.JavaScriptCodeEval;
import com.ats.generator.variables.transform.code.PythonCodeEval;


public class EvalTransformer extends Transformer {

	public static final String CODE_ERROR = "#CodeEvalError#";

	private static final String JAVA = "java";
	public static final String JS = "js";
	public static final String PY = "py";
	
	private static final String JAVASCRIPT = "javascript";
	public static final String PYTHON = "python";

	private String language = JAVA;

	public EvalTransformer() {} // Needed for serialization

	public EvalTransformer(String language) {
		if(JAVA.equals(language)  || JS.equals(language)  || PY.equals(language)) {
			this.language = language;
		}else if(JAVASCRIPT.equals(language)) {
			this.language = JS;
		}else if(PYTHON.equals(language)) {
			this.language = PY;
		}
	}

	@Override
	public String getJavaCode() {
		final StringBuilder sb =
				new StringBuilder(", ").
				append(ActionTestScript.JAVA_SCRIPTING_FUNCTION_NAME)
				.append("(\"")
				.append(language)
				.append("\")");
		return sb.toString();
	}

	@Override
	public String format(String data) {

		if(JAVA.equals(language)) {
			final JavaCodeEval code = new JavaCodeEval(data);
			try {
				return code.eval();
			} catch (Exception e) {
				throw new AssertionError(e.getMessage());
			}
		}else if(JAVASCRIPT.equals(language) || JS.equals(language)) {
			return new JavaScriptCodeEval(data).eval();
		}else if(PYTHON.equals(language) || PY.equals(language)) {
			return new PythonCodeEval(data).eval();	
		}

		return "";
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String value) {
		this.language = value;
	}
}