package com.ats.generator.variables;

import com.ats.executor.ActionTestScript;
import com.ats.script.Script;
import com.google.gson.JsonObject;

public class ApiData extends CalculatedProperty {
	
	public ApiData() {}
	
	public ApiData(String key, CalculatedValue calculatedValue) {
		super(key, calculatedValue);
	}

	public ApiData(Script script, String s) {
		super(script, s);
	}

	@Override
	public String getJavaCode() {
		return ActionTestScript.JAVA_PROPERTY_FUNCTION_NAME + "(\"" + getName() + "\", " + getValue().getJavaCode() + ")";
	}
	
	public JsonObject getJson() {
		final JsonObject element = new JsonObject();
		element.addProperty(getName(), getValue().getSafeCalculated());
		return element;
	}
}
