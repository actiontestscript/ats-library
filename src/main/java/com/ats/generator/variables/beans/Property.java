package com.ats.generator.variables.beans;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Property {
	
	private String operator;
	private String name;
	private String expected;
	
	public Property(String name, String operator, String expected) {
		this.name = name;
		this.operator = operator;
		this.expected = expected;
	}

	public String getOperator() {
		return operator;
	}

	public String getName() {
		return name;
	}

	public String getExpected() {
		return expected;
	}

	public String toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return "";
		}
	}
}