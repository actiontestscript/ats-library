/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

package com.ats.generator.variables.parameter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import com.ats.element.FoundElement;
import com.ats.element.ParameterElement;
import com.ats.executor.ActionTestScript;

public class ParameterList {

	private int iterations = 1;
	private List<Parameter> list;
	
	private ParameterElement element;

	public ParameterList() {
		this(0);
	}

	public ParameterList(int iterations) {
		this.list = new ArrayList<>();
		this.iterations = iterations;
	}

	public ParameterList(int iterations, List<Parameter> list) {
		this.list = new ArrayList<>(list);
		this.iterations = iterations;
	}

	public ParameterList(FoundElement elem, List<Parameter> list) {
		this.list = new ArrayList<>(list);
		this.iterations = list.size() + 1;
		this.element = elem.getParameterElement();
	}

	public ParameterList(FoundElement elem, String[] array) {
		ArrayList<Parameter> list = new ArrayList<>(array.length);
		int loop = 0;
		for(String s : array) {
			list.add(new Parameter(loop, s));
			loop++;
		}
		setList(list);
		this.element = elem.getParameterElement();
	}

	public ParameterList(FoundElement elem, String[] cols, List<Parameter> parameterList) {
		this(elem, cols);
		for(Parameter param : parameterList) {
			this.list.add(param);
		}
		this.iterations = list.size() + 1;
	}

	public ParameterList(String data) {
		this.list = List.of(new Parameter(0, data));
	}

	public void addParameter(Parameter param) {
		this.list.add(param);
		this.iterations = list.size() + 1;
	}

	public void updateCalculated(ActionTestScript ts) {
		list.forEach(p -> p.updateCalculated(ts));
	}

	public void appendJavaCode(StringBuilder codeBuilder) {
		final StringJoiner joiner = new StringJoiner(", ");

		for (Parameter param : list){
			joiner.add(param.getJavaCode());
		}
		codeBuilder.append(", ")
		.append(ActionTestScript.JAVA_PARAM_FUNCTION_NAME)
		.append("(")
		.append(joiner.toString())
		.append(")");
	}

	public String getParameterValue(String name, String defaultValue) {
		for(Parameter item : list) {
			if(name.equals(item.getName())){
				return item.getCalculated();
			}
		}
		return defaultValue;
	}

	public String getParameterValue(int index, String defaultValue) {
		if(list.size() > index) {
			return list.get(index).getCalculated();
		}
		return defaultValue;
	}

	public String[] getParameters() {
		final String[] result = new String[list.size()];
		int loop = 0;
		for(Parameter item : list) {
			if(item.getValue() == null) {
				result[loop] = item.getData();
			}else {
				result[loop] = item.getValue().getCalculated();
			}
			loop++;
		}
		return result;
	}

	public int getParametersSize() {
		if(list != null) {
			return list.size();
		}
		return 0;
	}

	public Map<String, String> getMap(){
		return list.stream().collect(Collectors.toMap(e -> e.getName(), e -> e.getCalculated()));
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public int getIterations() {
		return iterations;
	}

	public void setIterations(int value) {
		this.iterations = value;
	}

	public List<Parameter> getList() {
		return list;
	}

	public void setList(ArrayList<Parameter> list) {
		this.list = list;
		this.iterations = list.size() + 1;
	}

	public ParameterElement getElement() {
		return element;
	}

	public void setElement(ParameterElement element) {
		this.element = element;
	}
}