package com.ats.generator.variables;

import com.ats.executor.ActionTestScript;

public class PropertyVariable {

	private ActionTestScript script;
	private String name;
	private String file;
	
	public PropertyVariable(ActionTestScript script, String file, String name) {
		this.script = script;
		this.name = name;
		this.file = file;
	}
	
	@Override
	public String toString() {
		return script.getPropertyVariableValue(file, name);
	}
}