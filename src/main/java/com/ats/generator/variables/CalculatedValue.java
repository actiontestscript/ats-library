/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.generator.variables;

import java.util.ArrayList;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.text.StringEscapeUtils;

import com.ats.AtsSingleton;
import com.ats.crypto.Password;
import com.ats.executor.ActionTestScript;
import com.ats.executor.SendKeyData;
import com.ats.executor.channels.Channel;
import com.ats.generator.variables.transform.DateTransformer;
import com.ats.generator.variables.transform.TimeTransformer;
import com.ats.script.Project;
import com.ats.script.Script;
import com.ats.tools.Utils;

public class CalculatedValue{

	public final static String CRYPTED_DATA_SHOW = "########";

	private static final Pattern TODAY_PATTERN = Pattern.compile("\\$today", Pattern.CASE_INSENSITIVE);
	private static final Pattern NOW_PATTERN = Pattern.compile("\\$now", Pattern.CASE_INSENSITIVE);
	private static final Pattern UUID_PATTERN = Pattern.compile("\\$uuid", Pattern.CASE_INSENSITIVE);
	private static final Pattern ITERATION_PATTERN = Pattern.compile("\\$iteration", Pattern.CASE_INSENSITIVE);
	private static final Pattern ITERATIONS_COUNT_PATTERN = Pattern.compile("\\$iterationsCount", Pattern.CASE_INSENSITIVE);
	private static final Pattern LAST_ACTION_DURATION_PATTERN = Pattern.compile("\\$lastActionDuration", Pattern.CASE_INSENSITIVE);

	public static final Pattern KEY_REGEXP = Pattern.compile("\\$key\\s?\\((\\w+)\\-?([^\\)]*)?\\)");

	public static final Pattern ASSET_PATTERN = Pattern.compile("\\$asset\\s*?\\(([^\\)]*)\\)", Pattern.CASE_INSENSITIVE);
	public static final Pattern IMAGE_PATTERN = Pattern.compile("\\$image\\s*?\\(([^\\)]*)\\)", Pattern.CASE_INSENSITIVE);

	private static final Pattern PASSWORD_PATTERN = Pattern.compile("\\$pass\\s*?\\(([^\\)]*)\\)", Pattern.CASE_INSENSITIVE);
	private static final Pattern PASSWORD_ENV_PATTERN = Pattern.compile("\\$pass\\s*?\\(.*\\$(.*)\\(([^\\)]*)\\).*\\)", Pattern.CASE_INSENSITIVE);

	private static final Pattern ATS_DATA = Pattern.compile("\\$ats\\s*?\\(([^\\)]*)\\)", Pattern.CASE_INSENSITIVE);

	private static final Pattern SYS_PATTERN = Pattern.compile("\\$sys\\s*?\\(([^\\)]*)\\)", Pattern.CASE_INSENSITIVE);
	public static final Pattern PARAMETER_PATTERN = Pattern.compile("\\$param\\s*?\\((\\$?\\w+),?(\\s*?[^\\)]*)?\\)", Pattern.CASE_INSENSITIVE);
	public static final Pattern ENV_PATTERN = Pattern.compile("\\$env\\s*?\\(([\\w.]+),?(\\s*?[^\\)]*)?\\)", Pattern.CASE_INSENSITIVE);

	public static final Pattern PROJECT_VARIABLE_PATTERN = Pattern.compile("\\$prj\\s*?\\(([^\\)]*)\\)", Pattern.CASE_INSENSITIVE);
	public static final Pattern PROJECT_ENV_PATTERN = Pattern.compile("\\$prj\\s*?\\(.*\\$(.*)\\(([^\\)]*)\\).*\\)", Pattern.CASE_INSENSITIVE);

	public static final Pattern PROPERTY_VARIABLE_PATTERN = Pattern.compile("\\$prop\\s*?\\(([^\\)]*) *, *([^\\)]*)\\)", Pattern.CASE_INSENSITIVE);
	public static final Pattern PROPERTY_ENV_PATTERN = Pattern.compile("\\$prop\\s*?\\(([^\\)]*).*,.*\\$(.*)\\(([^\\)]*)\\).*\\)", Pattern.CASE_INSENSITIVE);

	private static final Pattern RND_PATTERN = Pattern.compile("\\$rnd(?:string)?\\s*?\\((\\d+),?(\\w{0,3}?[^\\)]*)?\\)", Pattern.CASE_INSENSITIVE);

	private static final Pattern VARIABLE_PATTERN = Pattern.compile("\\$var\\s*?\\(([^\\)\\.]*)\\)", Pattern.CASE_INSENSITIVE);
	private static final Pattern GLOBAL_VARIABLE_PATTERN = Pattern.compile("\\$var\\s*?\\(([^\\)]*)\\)", Pattern.CASE_INSENSITIVE);

	private static final Pattern invalidUnicodePattern = Pattern.compile("[\\p{C}]");

	//-----------------------------------------------------------------------------------------------------
	// variable and parameter management
	//-----------------------------------------------------------------------------------------------------

	private static final Pattern unnecessaryStartQuotes = Pattern.compile("^\"\", ?");
	private static final Pattern unnecessaryMiddleQuotes = Pattern.compile(" \"\",");
	private static final Pattern unnecessaryEndQuotes = Pattern.compile(", \"\"$");

	//-----------------------------------------------------------------------------------------------------
	// instance data
	//-----------------------------------------------------------------------------------------------------

	private Script script;
	private String data = "";
	private String calculated;
	private String safeCalculated;

	private String rawJavaCode = "";
	private Object[] dataList;

	private boolean crypted = false;

	public CalculatedValue() {}

	public CalculatedValue(String value) {
		setData(value);
	}

	public CalculatedValue(int value) {
		this(String.valueOf(value));
	}

	public CalculatedValue(ActionTestScript actionTestScript, Object[] data) {
		dataList = data;
	}

	public CalculatedValue(Script script) {
		setScript(script);
		setCalculated("");
	}

	public CalculatedValue(Script script, String dataValue) {
		setScript(script);

		if(dataValue.length() > 0){

			final Matcher matcher = invalidUnicodePattern.matcher(dataValue);
			dataValue = Utils.unescapeAts(matcher.replaceAll(""));

			updateData(dataValue);
		}
	}
	
	public void updateData(String value) {
		setData(value);
		setCalculated(initCalculated(value));
	}

	private String initCalculated(String dataValue) {

		rawJavaCode = StringEscapeUtils.escapeJava(dataValue);

		Matcher mv = ATS_DATA.matcher(dataValue);
		while (mv.find()) {
			final String replace = mv.group(0);
			final String value = mv.group(1).trim();

			dataValue = dataValue.replace(replace, script.getSpecialValue(value));
			final StringBuilder sb = new StringBuilder("\",")
					.append(ActionTestScript.JAVA_ATS_FUNCTION_NAME)
					.append("(\"")
					.append(value)
					.append("\"), \"");

			rawJavaCode = rawJavaCode.replace(replace, sb.toString());
		}

		mv = ITERATIONS_COUNT_PATTERN.matcher(dataValue);
		while (mv.find()) {
			rawJavaCode = rawJavaCode.replace(mv.group(0), "\", " + ActionTestScript.JAVA_ITERATION_COUNT_FUNCTION_NAME + "(), \"");
		}

		mv = ITERATION_PATTERN.matcher(dataValue);
		while (mv.find()) {
			rawJavaCode = rawJavaCode.replace(mv.group(0), "\", " + ActionTestScript.JAVA_ITERATION_FUNCTION_NAME + "(), \"");
		}

		mv = PROPERTY_ENV_PATTERN.matcher(dataValue);
		while (mv.find()) {
			final String replace = mv.group(0);
			final String file = mv.group(1);
			final String type = mv.group(2);
			final String name = mv.group(3);

			final StringBuilder sb = new StringBuilder("\", new ")				
					.append(PropertyVariable.class.getSimpleName())
					.append("(this, \"")
					.append(file)
					.append("\", ");

			if("env".equals(type)) {

				dataValue = dataValue.replace(replace, script.getProjectVariableValue(script.getEnvironmentValue(name, "")));
				sb.append(ActionTestScript.JAVA_ENV_FUNCTION_NAME);

			}else if("param".equals(type)) {

				final ParameterValue sp = new ParameterValue(name);
				
				final String paramValue = script.getParameterValue(sp.getValue(), sp.getDefaultValue());
				final String rep = sp.getReplace();
				dataValue = dataValue.replace(rep, paramValue);
				
				sb.append(ActionTestScript.JAVA_PARAM_FUNCTION_NAME);
			}

			sb.append("(\"")
			.append(name)
			.append("\")), \"");

			rawJavaCode = rawJavaCode.replace(replace, sb.toString());
		}
		
		mv = PROPERTY_VARIABLE_PATTERN.matcher(dataValue);
		while (mv.find()) {
			final String g0 = mv.group(0);
			final String g1 = mv.group(1);
			final String g2 = mv.group(2);

			dataValue = dataValue.replace(g0, script.getPropertyVariableValue(g1, g2));
			rawJavaCode = rawJavaCode.replace(g0, "\", new " + PropertyVariable.class.getSimpleName() + "(this, \"" + g1 + "\", \"" + g2 + "\"), \"");
		}

		mv = PROJECT_ENV_PATTERN.matcher(dataValue);
		while (mv.find()) {
			final String g0 = mv.group(0);
			final String type = mv.group(1);
			final String name = mv.group(2);

			if("env".equals(type)) {
				dataValue = dataValue.replace(g0, script.getProjectVariableValue(script.getEnvironmentValue(name, "")));
				rawJavaCode = rawJavaCode.replace(g0, "\", new " + ProjectVariable.class.getSimpleName() + "(this, " + ActionTestScript.JAVA_ENV_FUNCTION_NAME + "(\"" + name + "\")), \"");
			}else if("param".equals(type)) {
				final ParameterValue sp = new ParameterValue(name);

				dataValue = dataValue.replace(sp.getReplace(), script.getParameterValue(sp.getValue(), sp.getDefaultValue()));
				rawJavaCode = rawJavaCode.replace(g0, "\", new " + ProjectVariable.class.getSimpleName() + "(this, " + ActionTestScript.JAVA_PARAM_FUNCTION_NAME + "(\"" + name + "\")), \"");
			}
		}		

		mv = PROJECT_VARIABLE_PATTERN.matcher(dataValue);
		while (mv.find()) {
			final String replace = mv.group(0);
			final String variableName = mv.group(1);

			dataValue = dataValue.replace(replace, script.getProjectVariableValue(variableName));
			rawJavaCode = rawJavaCode.replace(replace, "\", new " + ProjectVariable.class.getSimpleName() + "(this, \"" + variableName + "\"), \"");
		}

		mv = PASSWORD_ENV_PATTERN.matcher(dataValue);
		while (mv.find()) {
			final String replace = mv.group(0);
			final String type = mv.group(1);
			final String name = mv.group(2);

			if("env".equals(type)) {
				dataValue = dataValue.replace(replace, script.getPassword(script.getEnvironmentValue(name, "")));
				rawJavaCode = rawJavaCode.replace(mv.group(0), "\", new " + Password.class.getSimpleName() + "(this, " + ActionTestScript.JAVA_ENV_FUNCTION_NAME + "(\"" + name + "\")), \"");
			}else if("param".equals(type)) {
				final ParameterValue sp = new ParameterValue(name);

				final String rp = sp.getReplace();
				
				if(rp != null) {
										
					dataValue = dataValue.replace(
							rp, 
							script.getParameterValue(
									sp.getValue(), 
									sp.getDefaultValue()));
					
				}
				
				rawJavaCode = rawJavaCode.replace(mv.group(0), "\", new " + Password.class.getSimpleName() + "(this, " + ActionTestScript.JAVA_PARAM_FUNCTION_NAME + "(\"" + name + "\")), \"");
			}
		}
		
		mv = PASSWORD_PATTERN.matcher(dataValue);
		while (mv.find()) {
			crypted = true;
			rawJavaCode = rawJavaCode.replace(mv.group(0), "\", new " + Password.class.getSimpleName() + "(this, \"" + mv.group(1) + "\"), \"");
		}

		mv = PARAMETER_PATTERN.matcher(dataValue);
		while (mv.find()) {
			final ParameterValue sp = new ParameterValue(mv);

			dataValue = dataValue.replace(sp.getReplace(), script.getParameterValue(sp.getValue(), sp.getDefaultValue()));
			rawJavaCode = rawJavaCode.replace(sp.getReplace(), "\", " + ActionTestScript.JAVA_PARAM_FUNCTION_NAME + sp.getCode() + ", \"");
		}

		mv = IMAGE_PATTERN.matcher(dataValue);
		while (mv.find()) {
			rawJavaCode = rawJavaCode.replace(mv.group(0), Project.getAssetsImageJavaCode(mv.group(1)));
		}

		mv = ASSET_PATTERN.matcher(dataValue);
		while (mv.find()) {

			final String assetExpression = mv.group(0);
			final String relativePath = mv.group(1);

			dataValue = dataValue.replace(assetExpression, script.getAssetsFilePath(relativePath));
			rawJavaCode = rawJavaCode.replace(assetExpression, Project.getAssetsJavaCode(relativePath));
		}

		mv = VARIABLE_PATTERN.matcher(dataValue);
		while (mv.find()) {
			final String replace = mv.group(0);
			final String variableName = mv.group(1);

			dataValue = dataValue.replace(replace, script.getVariableValue(variableName));
			rawJavaCode = rawJavaCode.replace(replace, "\", " + variableName + ", \"");
		}

		mv = GLOBAL_VARIABLE_PATTERN.matcher(dataValue);
		while (mv.find()) {
			final String replace = mv.group(0);
			final String variableName = mv.group(1);

			dataValue = dataValue.replace(replace, script.getGlobalVariableValue(variableName));
			rawJavaCode = rawJavaCode.replace(replace, "\", " + ActionTestScript.JAVA_GLOBAL_VAR_FUNCTION_NAME + "(\"" + variableName + "\"), \"");
		}

		mv = SYS_PATTERN.matcher(dataValue);
		while (mv.find()) {
			final String replace = mv.group(0);
			final String value = StringEscapeUtils.escapeJava(mv.group(1).trim());

			dataValue = dataValue.replace(replace, AtsSingleton.getInstance().getSystemValue(value));
			rawJavaCode = rawJavaCode.replace(replace, "\"," + ActionTestScript.JAVA_SYSTEM_FUNCTION_NAME + "(\"" + value + "\"), \"");
		}

		mv = ENV_PATTERN.matcher(dataValue);
		while (mv.find()) {
			final EnvironmentValue sp = new EnvironmentValue(mv);

			dataValue = dataValue.replace(sp.getReplace(), script.getEnvironmentValue(sp.getValue(), sp.getDefaultValue()));
			rawJavaCode = rawJavaCode.replace(sp.getReplace(), "\", " + ActionTestScript.JAVA_ENV_FUNCTION_NAME + sp.getCode() + ", \"");
		}

		mv = TODAY_PATTERN.matcher(dataValue);
		while (mv.find()) {
			final String replace = mv.group(0);

			dataValue = dataValue.replace(replace, DateTransformer.getTodayValue());
			rawJavaCode = rawJavaCode.replace(replace, "\", " + ActionTestScript.JAVA_TODAY_FUNCTION_NAME + "(), \"");
		}

		mv = LAST_ACTION_DURATION_PATTERN.matcher(dataValue);
		while (mv.find()) {
			final String replace = mv.group(0);
			dataValue = dataValue.replace(replace, String.valueOf(script.getLastActionDuration()));
			rawJavaCode = rawJavaCode.replace(replace, "\", " + ActionTestScript.JAVA_LAST_ACTION_DURATION_FUNCTION_NAME + "(), \"");
		}		

		mv = NOW_PATTERN.matcher(dataValue);
		while (mv.find()) {
			final String replace = mv.group(0);

			dataValue = dataValue.replace(replace, TimeTransformer.getNowValue());
			rawJavaCode = rawJavaCode.replace(replace, "\", " + ActionTestScript.JAVA_NOW_FUNCTION_NAME + "(), \"");
		}

		mv = UUID_PATTERN.matcher(dataValue);
		while (mv.find()) {
			final String replace = mv.group(0);

			dataValue = dataValue.replace(replace, UUID.randomUUID().toString());
			rawJavaCode = rawJavaCode.replace(replace, "\", " + ActionTestScript.JAVA_UUID_FUNCTION_NAME + "(), \"");
		}

		mv = RND_PATTERN.matcher(dataValue);
		while (mv.find()) {
			final RandomStringValue rds = new RandomStringValue(mv);

			dataValue = dataValue.replace(rds.getReplace(), rds.exec());
			rawJavaCode = rawJavaCode.replace(rds.getReplace(), "\", " + ActionTestScript.JAVA_RNDSTRING_FUNCTION_NAME + rds.getCode() + ", \"");
		}

		return dataValue;
	}

	public boolean isCrypted() {
		return crypted;
	}

	public void dispose() {
		script = null;
		dataList = null;
	}

	//-----------------------------------------------------------------------------------------------------
	// java code
	//-----------------------------------------------------------------------------------------------------

	public String getJavaCode(){

		String value = "\"" + rawJavaCode + "\"";

		value = unnecessaryStartQuotes.matcher(value).replaceFirst("");
		value = unnecessaryEndQuotes.matcher(value).replaceFirst("");
		value = unnecessaryMiddleQuotes.matcher(value).replaceAll("");

		return ActionTestScript.JAVA_VALUE_FUNCTION_NAME + "(" + value + ")";
	}

	//-----------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------

	public void uncryptData(Script script) {
		if(dataList != null && dataList.length > 0) {
			calculated = "";
			for(Object obj : dataList) {
				if(obj instanceof Password) {
					calculated += ((Password)obj).getValue();
				}else {
					calculated += obj.toString();
				}
			}
		}
	}

	public void uncrypt(Script script) {
		if(calculated != null) {
			final Matcher mv = PASSWORD_PATTERN.matcher(getCalculated());
			while (mv.find()) {
				crypted = true;
				safeCalculated = calculated.replace(mv.group(0), CRYPTED_DATA_SHOW);
				calculated = calculated.replace(mv.group(0), script.getPassword(mv.group(1)));
			}
		}
	}

	public String uncrypt(ActionTestScript script, String value) {
		final Matcher mv = PASSWORD_PATTERN.matcher(value);
		while (mv.find()) {
			crypted = true;
			safeCalculated = value.replace(mv.group(0), CRYPTED_DATA_SHOW);
			value = value.replace(mv.group(0), script.getPassword(mv.group(1)));
		}
		return value;
	}

	private void addTextChain(boolean oneChar, ActionTestScript script, ArrayList<SendKeyData> list, String value) {
		final String unCrypted = uncrypt(script, value);
		if(oneChar) {
			for(int i=0; i<unCrypted.length(); i++) {
				list.add(new SendKeyData(String.valueOf(unCrypted.charAt(i))));
			}
		}else {
			list.add(new SendKeyData(unCrypted));
		}
	}

	public ArrayList<SendKeyData> getCalculatedText(ActionTestScript script, boolean oneChar){

		final ArrayList<SendKeyData> chainKeys = new ArrayList<>();

		final String calc = getCalculated();

		int start = 0;

		final Matcher match = KEY_REGEXP.matcher(calc);
		while(match.find()) {

			int end = match.start();
			if(end > 0) {
				addTextChain(oneChar, script, chainKeys, calc.substring(start, end));
			}

			start = match.end();
			chainKeys.add(new SendKeyData(match.group(1), match.group(2).replace("-", "")));
		}

		if(start == 0) {
			addTextChain(oneChar, script, chainKeys, calc);
		}else if(start != calc.length()){
			addTextChain(oneChar, script, chainKeys, calc.substring(start));
		}

		return chainKeys;
	}

	public String getDataListItem() {
		if(dataList != null && dataList.length > 0) {
			return dataList[0].toString();
		}
		return calculated;
	}

	public String getKeywords() {
		return data;
	}

	public String getSafeCalculated() {
		if(safeCalculated != null) {
			return safeCalculated;
		}else {
			return getCalculated();
		}
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public String getCalculated() {
		if(calculated == null){
			if(dataList != null) {
				final StringBuilder builder = new StringBuilder("");
				for(final Object obj : dataList) {
					if (obj instanceof Variable) {
						builder.append(((Variable) obj).getCalculatedValue());
					}else if(obj != null) {
						builder.append(Channel.checkSystemValue(obj));
					}
				}
				return builder.toString();
			}else {
				return data;
			}
		}

		return calculated;
	}

	public int getCalculatedInteger() {
		return Utils.string2Int(getCalculated());
	}

	public void setCalculated(String value) {
		this.calculated = value;
	}

	public String getData() {
		return data;
	}

	public void setData(String value) {
		if(value == null){
			value = "";
		}
		this.data = value;
	}

	public Script getScript() {
		return script;
	}

	public void setScript(Script script) {
		this.script = script;
	}
}