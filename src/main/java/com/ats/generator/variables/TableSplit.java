package com.ats.generator.variables;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ats.executor.ActionTestScript;
import com.ats.generator.variables.parameter.Parameter;
import com.ats.generator.variables.parameter.ParameterList;
import com.ats.tools.Utils;

public class TableSplit {

	private static final Pattern TR_REGEXP = Pattern.compile("TR(.)", Pattern.CASE_INSENSITIVE);
	private static final Pattern TD_REGEXP = Pattern.compile("TD(.)", Pattern.CASE_INSENSITIVE);
	
	private String rowSeparator;
	private String columnSeparator;
	private CalculatedValue value;

	public TableSplit() {}
	
	public TableSplit(String row, String col, CalculatedValue value) {
		this.rowSeparator = row;
		this.columnSeparator = col;
		this.value = value;
	}
	
	public TableSplit(String value) {
		value = Utils.unescapeAts(value);

		if(value.length() > 0) {
			
			Matcher match = TR_REGEXP.matcher(value);
			if(match.find()) {
				rowSeparator = match.group(1);
			}
			
			match = TD_REGEXP.matcher(value);
			if(match.find()) {
				columnSeparator = match.group(1);
			}
		}
	}

	public TableSplit(String optionValue, ParameterList parameters) {
		this(optionValue);
		if(parameters.getParametersSize() > 0) {
			this.value = parameters.getList().getFirst().getValue();
		}else {
			this.value = new Parameter(0, "").getValue();
		}
	}

	public Object getJavaCode() {
		
		final StringBuilder sb = new StringBuilder(ActionTestScript.JAVA_SPLIT_FUNCTION_NAME).append("(");
				
		if(rowSeparator != null) {
			sb.append("\"").append(rowSeparator).append("\"");
		}else {
			sb.append("null");
		}
		sb.append(", ");
		
		if(columnSeparator != null) {
			sb.append("\"").append(columnSeparator).append("\"");
		}else {
			sb.append("null");
		}
		sb.append(", ").append(value.getJavaCode()).append(")");

		return sb.toString();
	}

	public List<ParameterList> getData() {
		
		final ArrayList<ParameterList> params = new ArrayList<ParameterList>();
		String[] rows = null;
		
		if(rowSeparator != null) {
			rows = value.getCalculated().split("\\" + rowSeparator);
		}else {
			rows = new String[]{value.getCalculated()};
		}
		
		if(columnSeparator != null) {
			for(int i=0; i<rows.length; i++) {
				ParameterList paramRow = new ParameterList();
				String[] cols = rows[i].split("\\" + columnSeparator);
				for(int j=0; j<cols.length; j++) {
					paramRow.addParameter(new Parameter(j, cols[j]));
				}
				params.add(paramRow);
			}
		}else {
			for(int i=0; i<rows.length; i++) {
				params.add(new ParameterList(rows[i]));
			}
		}
						
		return params;
	}
	
	//----------------------------------------------------------------------------------------------------------------------

	public String getRowSeparator() {
		return rowSeparator;
	}

	public void setRowSeparator(String rowSeparator) {
		this.rowSeparator = rowSeparator;
	}

	public String getColumnSeparator() {
		return columnSeparator;
	}

	public void setColumnSeparator(String columnSeparator) {
		this.columnSeparator = columnSeparator;
	}

	public CalculatedValue getValue() {
		return value;
	}

	public void setValue(CalculatedValue value) {
		this.value = value;
	}
}