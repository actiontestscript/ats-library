/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.generator.parsers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;

import com.ats.generator.variables.CalculatedValue;
import com.ats.generator.variables.Variable;
import com.ats.generator.variables.transform.Transformer;
import com.ats.script.AtsScript;
import com.ats.script.ScriptHeader;
import com.ats.tools.Utils;

public class ScriptParser {

	public static final String ATS_SEPARATOR = "->";
	public static final int ATS_SEPARATOR_SIZE = ATS_SEPARATOR.length();
	public static final String ATS_ASSIGN_SEPARATOR = "=>";
	
	private Lexer lexer;

	public ScriptParser(Lexer lexer) {
		this.lexer = lexer;
	}

	public Lexer getLexer() {
		return lexer;
	}

	public boolean isGenerator() {
		return lexer.isGenerator();
	}

	public void addScript(){
		lexer.addScript();
	}

	private String getDataGroup(Matcher m, int index){
		if(m.groupCount() > index -1 && m.group(index) != null){
			return m.group(index).trim();
		}
		return "";
	}

	private String getHeaderData(String data) {
		return data.substring(data.indexOf(ATS_SEPARATOR) + ATS_SEPARATOR_SIZE).trim();
	}

	public void parse(AtsScript script, ScriptHeader header, String data){

		if(ScriptHeader.GROUPS_MATCH.test(data)){

			header.parseGroups(getHeaderData(data));

		}else if(ScriptHeader.DESCRIPTION_MATCH.test(data)){

			header.setDescription(getHeaderData(data));

		} else if(ScriptHeader.MEMO_MATCH.test(data)){

			header.setMemo(getHeaderData(data));

		} else if(ScriptHeader.PRE_PROCESSING_MATCH.test(data)){

			header.setPreprocessing(getHeaderData(data));

		} else if(ScriptHeader.POST_PROCESSING_MATCH.test(data)){

			header.setPostprocessing(getHeaderData(data));

		} else if(ScriptHeader.ID_MATCH.test(data)){

			header.setId(getHeaderData(data));

		}else if(ScriptHeader.EXTERNAL_ID_MATCH.test(data)){

			header.setExternalId(getHeaderData(data));

		}else if(ScriptHeader.UUID_MATCH.test(data)){

			header.setUuid(getHeaderData(data));

		}else if(ScriptHeader.DATE_CREATED_MATCH.test(data)){

			header.setCreatedAt(Utils.parseDateFormat(getHeaderData(data)));

		}else if(ScriptHeader.AUTHOR_MATCH.test(data)){

			header.setAuthor(getHeaderData(data));

		}else if(ScriptHeader.MODIFIED_AT_MATCH.test(data)){

			header.setModifiedAt(Utils.parseDateFormat(getHeaderData(data)));

		}else if(ScriptHeader.MODIFIED_BY_MATCH.test(data)){

			header.setModifiedBy(getHeaderData(data));

		}else if(ScriptHeader.PREREQUISITE_MATCH.test(data)){

			header.setPrerequisite(getHeaderData(data));

		}else if(data.regionMatches(true, 0, Variable.SCRIPT_LABEL, 0, Variable.SCRIPT_LABEL_LENGTH)){

			final ArrayList<String> dataArray = new ArrayList<>(Arrays.asList(getHeaderData(data).split(ScriptParser.ATS_SEPARATOR)));

			if(dataArray.size() > 0){

				final String name = dataArray.remove(0).trim();

				String value = "";
				Transformer transformer = new Transformer();

				if(dataArray.size() > 0) {

					final String nextData = dataArray.remove(0).trim();
					final Matcher m = Transformer.TRANSFORM_PATTERN.matcher(nextData);

					if(m != null && m.find()){

						transformer = Transformer.createTransformer(getDataGroup(m, 1), getDataGroup(m, 2));

						if(dataArray.size() > 0){
							value = dataArray.remove(0).trim();
						}

					}else {
						value = nextData;
					}
				}

				script.addVariable(name, new CalculatedValue(script, value), transformer, data.startsWith(Variable.VAR_FIXED));
			}

		}else if(ScriptHeader.RETURN_MATCH.test(data)){

			final String[] returnsData = getHeaderData(data).split(ATS_SEPARATOR);
			CalculatedValue[] returns = new CalculatedValue[returnsData.length];

			for(int i=0; i < returnsData.length; i++){
				returns[i] = new CalculatedValue(script, returnsData[i].trim());
			}
			script.setReturns(returns);

		}else{

			boolean actionDisabled = false;
			if(data.startsWith("//")){
				data = data.substring(2);
				actionDisabled = true;
			}
			lexer.createAction(script, data, actionDisabled);
		}
	}
}