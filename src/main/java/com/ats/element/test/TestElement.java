/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.element.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import com.ats.AtsSingleton;
import com.ats.data.Rectangle;
import com.ats.element.AtsBaseElement;
import com.ats.element.AtsElement;
import com.ats.element.FoundElement;
import com.ats.element.ParameterElement;
import com.ats.element.SearchedElement;
import com.ats.executor.ActionStatus;
import com.ats.executor.ActionTestScript;
import com.ats.executor.SendKeyData;
import com.ats.executor.TestBound;
import com.ats.executor.channels.Channel;
import com.ats.executor.drivers.engines.IDriverEngine;
import com.ats.generator.objects.MouseDirection;
import com.ats.generator.variables.CalculatedProperty;
import com.ats.generator.variables.CalculatedValue;
import com.ats.generator.variables.parameter.Parameter;
import com.ats.generator.variables.parameter.ParameterList;
import com.ats.recorder.IVisualRecorder;
import com.ats.tools.logger.MessageCode;
import com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class TestElement {

	public final static String CLIENT_WIDTH = "clientWidth";
	public final static String CLIENT_HEIGTH = "clientHeight";

	public final static AtsProperty ATS_OCCURRENCES = new AtsProperty("-ats-occurrences", "-ats-occurences");
	public final static AtsProperty ATS_OCCURRENCES_INDEX = new AtsProperty("-ats-occurrences-index", "-ats-occurences-index");
	public final static AtsProperty ATS_ID = new AtsProperty("-ats-id");
	public final static String ATS_TABLE_DATA = "-ats-table-data";
	public final static String ATS_MAX_TRY = "-ats-max-try";
	public final static String ATS_SEARCH_DURATION = "-ats-search-duration";
	public final static String ATS_SEARCH_TAG = "-ats-search-tag";
	public final static String ATS_SELECTED_TEXT = "-ats-selected-text";
	
	public final static String ATS_COL_NAME = "-ats-col-name";

	private final static String MAT_SELECT = "MAT-SELECT";

	private final static String PRE = "PRE";
	private final static String DATE = "date";
	private final static String DATE_TIME = "datetime-local";
	private final static String TIME = "time";
	private final static String MUI_INPUT = "MuiInputBase";

	public final static String SYSCOMP = "SYSCOMP";

	public final static String ELEMENT_NOT_FOUND = "ELEMENT_NOT_FOUND";
	private final static String NOT_AVAILABLE = "NOT_AVAILABLE";

	protected Channel channel;
	protected ActionTestScript script;
	protected IDriverEngine engine;

	private Predicate<Integer> occurrences;

	private int count = 0;

	private long searchDuration = 0;
	private long totalSearchDuration = 0;

	protected TestElement parent;
	private List<FoundElement> foundElements = new ArrayList<>();

	private int maxTry = 20;
	private int maxTryInteractable = AtsSingleton.getInstance().getMaxTryInteractable();
	private int index = 0;
	private boolean shadowRoot = false;
	private int evalIndex = 0;

	private SearchedElement searchedElement;

	protected IVisualRecorder recorder;

	private boolean sysComp = false;

	public TestElement() {}

	public TestElement(ActionTestScript script, Channel channel) {
		this(script, channel, 1, 0);
		this.foundElements = new ArrayList<>(Arrays.asList(new FoundElement(channel)));
	}

	public TestElement(ActionTestScript script, Channel channel, int count, int index) {
		this.channel = channel;
		this.script = script;
		this.count = count;
		this.index = index;
		this.occurrences = p -> true;
		this.engine = channel.getDriverEngine();
	}

	public TestElement(ActionTestScript script, Channel channel, Predicate<Integer> predicate, int index) { //root
		this.channel = channel;
		this.script = script;
		this.count = 1;
		this.index = index;
		this.occurrences = predicate;
		this.engine = channel.getDriverEngine();
	}

	public TestElement(ActionTestScript script, Channel channel, int maxTry) {
		this.channel = channel;
		this.script = script;
		this.maxTry = maxTry;
	}

	public TestElement(ActionTestScript script, FoundElement element, Channel currentChannel) {
		this(script, currentChannel);
		this.foundElements.add(element);
		this.count = getElementsCount();
	}

	public TestElement(ActionTestScript script, Channel channel, int maxTry, Predicate<Integer> occurrences) {
		this(script, channel, maxTry);
		this.occurrences = occurrences;
	}

	public TestElement(ActionTestScript script, Channel channel, int maxTry, Predicate<Integer> predicate, int index, boolean shadowRoot) {
		this(script, channel, maxTry, predicate);
		this.setIndex(index);
		this.setShadowRoot(shadowRoot);
	}

	public TestElement(ActionTestScript script, Channel channel, int maxTry, Predicate<Integer> predicate, int index) {
		this(script, channel, maxTry, predicate);
		this.setIndex(index);
	}

	public TestElement(ActionTestScript script, Channel channel, SearchedElement searchedElement) {
		this(script, channel, 1, p -> true, searchedElement);
	}

	public TestElement(ActionTestScript script, Channel channel, int maxTry, Predicate<Integer> predicate, SearchedElement searchedElement) {

		this(script, channel, maxTry, predicate, searchedElement.getIndex().getCalculatedInteger(), searchedElement.isShadowRoot());

		if(searchedElement.getParent() != null) {
			this.parent = new TestElement(script, channel, maxTry, predicate, searchedElement.getParent());
		}

		setEngine(channel.getDriverEngine());
		startSearch(searchedElement.isSysComp(), searchedElement);
	}

	public String getNotFoundDescription() {
		final StringBuilder builder = new StringBuilder(ELEMENT_NOT_FOUND).append(" ");
		builder.append(searchedElement.getSelector());
		return builder.toString();
	}

	protected void setEngine(IDriverEngine engine) {
		this.engine = engine;
	}

	protected void reloadFoundElements() {
		this.foundElements = new ArrayList<>(Arrays.asList(new FoundElement(channel)));
	}

	public void dispose() {
		channel = null;
		engine = null;
		recorder = null;
		occurrences = null;

		if (parent != null) {
			parent.dispose();
			parent = null;
		}

		foundElements.forEach(fe -> fe.dispose());
		foundElements = new ArrayList<>();
	}

	public boolean isAngularSelect() {
		return MAT_SELECT.equalsIgnoreCase(getSearchedTag());
	}

	public boolean isPreElement() {
		return PRE.equalsIgnoreCase(getSearchedTag());
	}

	public boolean isInputDate() {
		final String type = getWebElement().getDomAttribute("type");
		return isInput() && DATE.equalsIgnoreCase(type);
	}

	public boolean isInputDateTime() {
		final String type = getWebElement().getDomAttribute("type");
		return isInput() && DATE_TIME.equalsIgnoreCase(type);
	}

	public boolean isInputTime() {
		final String type = getWebElement().getDomAttribute("type");
		return isInput() && TIME.equalsIgnoreCase(type);
	}

	public boolean isMuiDateTimeInput() {
		String attr = getWebElement().getDomAttribute("class");
		if(attr != null && attr.startsWith(MUI_INPUT)){
			attr = getWebElement().getDomProperty("placeholder");
			if(attr != null) {
				if(attr.contains("/") && (attr.contains("DD") || attr.contains("MM") || attr.contains("YY"))) {
					return true;
				}
				if(attr.contains("hh:mm")) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isSysComp() {
		return sysComp;
	}

	protected int getMaxTry() {
		return maxTry;
	}

	protected Channel getChannel() {
		return channel;
	}

	public String getSearchedTag() {
		if(searchedElement != null) {
			return searchedElement.getTag();
		}
		return "";
	}

	protected void startSearch(boolean sys, SearchedElement elem) {

		sysComp = sys;

		if (channel != null) {

			searchedElement = elem;
			searchDuration = System.currentTimeMillis();

			if (parent == null || (parent != null && parent.getCount() > 0)) {
				foundElements = loadElements(searchedElement);
			}

			searchDuration = System.currentTimeMillis() - searchDuration;
			totalSearchDuration = getTotalDuration();
			count = getElementsCount();
		}
	}

	protected List<FoundElement> loadElements(SearchedElement searchedElement) {

		if(searchedElement.isParamElement()) {
			ParameterElement pe = null;
			if(script.getParameterElement()!= null) {
				pe = script.getParameterElement();
			}else {
				pe = searchedElement.getParameterElement();
			}
			
			if(pe == null) {
				pe = new ParameterElement();
			}
			return Arrays.asList(engine.getParamElement(pe));
		}

		Predicate<AtsBaseElement> fullPredicate = searchedElement.getPredicate();

		try {
			List<FoundElement> elementsLst = engine.findElements(
					sysComp,
					this, 
					getSearchedTag(), 
					searchedElement.getAttributes(), 
					searchedElement.getAttributesValues(), 
					fullPredicate, 
					searchedElement.getStartElement(engine));

			if (elementsLst.size() == 1 && searchedElement.getAttributes().length == 0 && index > 0) {
				index = 0;
			}

			return elementsLst;

		} catch (StaleElementReferenceException e) {
			return Collections.<FoundElement>emptyList();
		}
	}

	public int getElementsCount() {
		final int size = foundElements.size();
		if (size > 0) {
			if (index >= 0) {
				int i = index;
				if (i > 0) {
					i--;
				}

				if (size > i) {
					return size;
				}
			} else if (index < 0 && size == 1) {
				return size;
			} else {
				if (size > -index - 1) {
					return size;
				}
			}
		}
		return 0;
	}

	private long getTotalDuration() {
		if (parent != null) {
			return searchDuration + parent.getTotalDuration();
		} else {
			return searchDuration;
		}
	}

	public FoundElement getFoundElement() {
		FoundElement result = null;
		final int size = getElementsCount();
		if (size > 0) {
			int i = 0;
			if (index >= 0) {
				if (index > 0) {
					i = index - 1;
				}
			} else {

				i = size - 1;
				if (index != -1) {
					i += index + 1;
					if (i < 0) {
						i = 0;
					}
				}
			}

			try {
				result = foundElements.get(i);
				evalIndex = i;
			} catch (IndexOutOfBoundsException e) {
			}
		}

		return result;
	}

	public int getEvalIndex() {
		return evalIndex;
	}

	public boolean isPassword() {
		return getFoundElement().isPassword();
	}

	public boolean isNumeric() {
		return getFoundElement().isNumeric();
	}

	public WebElement getWebElement() {
		return getFoundElement().getValue();
	}

	public boolean isBody() {
		return getFoundElement().isBody();
	}

	public boolean isInput() {
		return getFoundElement().isInput();
	}

	public String getWebElementId() {
		return getFoundElement().getId();
	}

	public Rectangle getWebElementRectangle() {
		return getFoundElement().getRectangle();
	}

	public boolean isValidated() {
		final int count = getElementsCount();
		return occurrences.test(count);
	}

	public boolean isIframe() {
		if (getElementsCount() > 0) {
			return getFoundElement().isIframe();
		} else {
			return false;
		}
	}

	//----------------------------------------------------------------------------------------------------------------------
	// Getter and setter for serialization
	//----------------------------------------------------------------------------------------------------------------------

	public boolean isShadowRoot() {
		return shadowRoot;
	}

	public void setShadowRoot(boolean value) {
		this.shadowRoot = value;
	}

	public TestElement getParent() {
		return parent;
	}

	public void setParent(TestElement parent) {
		this.parent = parent;
	}

	public long getSearchDuration() {
		return searchDuration;
	}

	public void setSearchDuration(long searchDuration) {
		this.searchDuration = searchDuration;
	}

	public long getTotalSearchDuration() {
		return totalSearchDuration;
	}

	public void setTotalSearchDuration(long totalSearchDuration) {
		this.totalSearchDuration = totalSearchDuration;
	}

	public List<FoundElement> getFoundElements() {
		return foundElements;
	}

	public void setFoundElements(ArrayList<FoundElement> data) {
		this.foundElements = data;
	}

	public String getSelector() {
		if(searchedElement == null){
			return "root[]";
		}
		return searchedElement.getSelector();
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int value) {
		this.index = value;
	}

	//-------------------------------------------------------------------------------------------------------------------
	// Assertion ...
	//-------------------------------------------------------------------------------------------------------------------

	public void checkOccurrences(ActionTestScript ts, ActionStatus status, String operator, int expected) {

		int error = 0;

		if (isValidated()) {
			status.setNoError();
		} else {

			error = ActionStatus.OCCURRENCES_ERROR;

			final StringBuilder sb = new StringBuilder();
			sb.append("[").
			append(expected).
			append("] expected occurrence(s) but [").
			append(count).
			append("] occurrence(s) found using selector : ").
			append(searchedElement.getSelector());

			status.setError(ActionStatus.OCCURRENCES_ERROR, sb.toString(), count);
		}

		status.endDuration();
		ts.getRecorder().updateScreen(status);
		terminateExecution(status, ts, error, status.getDuration(), String.valueOf(count), operator + " " + expected);
	}

	//-------------------------------------------------------------------------------------------------------------------
	// Text ...
	//-------------------------------------------------------------------------------------------------------------------

	public void clearText(ActionStatus status, MouseDirection md) {
		engine.clearText(status, this, md);
	}

	public String enterText(ActionStatus status, CalculatedValue text, ActionTestScript script, int waitChar) {

		final MouseDirection md = new MouseDirection();
		over(status, md, false, 0, 0);

		return finalizeEnterText(status, text, md, script, waitChar);
	}

	protected String finalizeEnterText(ActionStatus status, CalculatedValue text, MouseDirection md, ActionTestScript script, int waitChar) {
		if (status.isPassed()) {

			//recorder.updateScreen(false);
			if (!text.getCalculated().startsWith("$key")) {
				clearText(status, md);
			}

			final String enteredText = sendText(script, status, text, waitChar);
			if (isPassword() || text.isCrypted()) {
				return CalculatedValue.CRYPTED_DATA_SHOW;
			} else {
				return enteredText;
			}
		}
		return "";
	}

	public String sendText(ActionTestScript script, ActionStatus status, CalculatedValue text, int waitChar) {
		waitTextInteractable(maxTry, status, text.getCalculatedText(script, waitChar > 0), waitChar, script.getTopScript());
		channel.actionTerminated(status);
		return text.getCalculated();
	}

	//-------------------------------------------------------------------------------------------------------------------
	// Wait Animation
	//-------------------------------------------------------------------------------------------------------------------

	private void waitAnimation(int loop) {
		if (loop > 0) {
			final Rectangle rect = engine.getBoundRect(this);
			if (rect != null) {
				channel.sleep(200);
				if (rect.equals(engine.getBoundRect(this))) {
					if (rect.getWidth() > 0 && rect.getHeight() > 0) {
						getFoundElement().updateBounding(rect.getX(), rect.getY(), channel, rect.getWidth(), rect.getHeight());
					} else {
						loop--;
						channel.sendWarningLog(MessageCode.OBJECT_TRY_SEARCH, "Element size is 0, wait before execute action", loop);
						waitAnimation(loop);
					}
				} else {
					loop--;
					channel.sendWarningLog(MessageCode.OBJECT_TRY_SEARCH, "Element is moving, wait before execute action", loop);
					waitAnimation(loop);
				}
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------------------
	// Wait Interactable
	//-------------------------------------------------------------------------------------------------------------------

	private void waitTextInteractable(int loop, ActionStatus status, ArrayList<SendKeyData> text, int waitChar, ActionTestScript topScript) {

		final long start = System.currentTimeMillis();

		String errorMessage = "";

		while (loop > 0) {
			try {
				engine.sendTextData(status, this, text, waitChar, topScript);
				return;
			} catch (ElementNotInteractableException e) {
				loop--;
				errorMessage = e.getMessage();

				channel.sendLog(MessageCode.OBJECT_TRY_SEARCH, "Element is not interactable, wait before try action again", loop);
				channel.sleep(300);
			}
		}

		if(!Strings.isNullOrEmpty(errorMessage)) {
			errorMessage = errorMessage.split("\n")[0];
		}

		channel.sleep(200);
		status.setError(ActionStatus.OBJECT_NOT_INTERACTABLE, errorMessage, start);
	}

	private void waitInteractable(int loop, ActionStatus status) {
		if (getFoundElement().getId() != null) {
			final long start = System.currentTimeMillis();

			String errorMessage = "";

			while (loop > 0) {
				try {
					engine.mouseMoveToElement(getFoundElement());
					return;
				} catch (ElementNotInteractableException e) {
					loop--;
					errorMessage = e.getMessage();

					channel.sendLog(MessageCode.OBJECT_TRY_SEARCH, "Element is not interactable, wait before try action again", loop);
					channel.sleep(300);
				}
			}

			if(!Strings.isNullOrEmpty(errorMessage)) {
				errorMessage = errorMessage.split("\n")[0];
			}

			channel.sleep(200);
			status.setError(ActionStatus.OBJECT_NOT_INTERACTABLE, errorMessage, start);
		}
	}

	private void waitSelectInteractable(int loop, ActionStatus status, CalculatedProperty selectProperty, boolean keepSelect) {
		final long start = System.currentTimeMillis();
		String errorMessage = null;
		while (loop > 0) {
			try {
				engine.selectOptionsItem(status, this, selectProperty, keepSelect);
				return;
			} catch (ElementNotInteractableException e) {
				loop--;
				errorMessage = e.getMessage();

				channel.sendLog(MessageCode.OBJECT_TRY_SEARCH, "Element is not interactable, wait before try action again", loop);
				channel.sleep(300);
			}
		}

		channel.sleep(200);
		status.setError(ActionStatus.OBJECT_NOT_INTERACTABLE, errorMessage, start);
	}

	//-------------------------------------------------------------------------------------------------------------------
	// Select ...
	//-------------------------------------------------------------------------------------------------------------------

	public void select(ActionStatus status, CalculatedProperty selectProperty, boolean keepSelect) {
		if (isValidated()) {
			waitSelectInteractable(maxTry, status, selectProperty, keepSelect);
		}
	}

	//-------------------------------------------------------------------------------------------------------------------
	// Mouse ...
	//-------------------------------------------------------------------------------------------------------------------

	public void engineOver(ActionStatus status, MouseDirection position, boolean desktopDragDrop, int offsetX, int offsetY) {
		engine.mouseMoveToElement(status, getFoundElement(), position, desktopDragDrop, offsetX, offsetY);
	}

	public void over(ActionStatus status, MouseDirection position, boolean desktopDragDrop, int offsetX, int offsetY) {
		waitAnimation(maxTryInteractable);
		waitInteractable(maxTry, status);

		final Rectangle rect = getFoundElement().getRectangle();

		channel.updateVisualAction(false);
		if (status.isPassed()) {
			engineOver(status, position, desktopDragDrop, offsetX, offsetY);

			final Rectangle rect1 = engine.getBoundRect(this);
			if(rect1 != null && !rect1.equals(rect)) {
				getChannel().getSystemDriver().updateVisualElement(this);
			}
		}else {
			getChannel().getSystemDriver().updateVisualElement(this);
		}
	}

	public void click(ActionStatus status, MouseDirection position, Keys key) {
		engine.keyDown(key);
		click(status, position);
		engine.keyUp(key);
	}

	public void click(ActionStatus status, MouseDirection position) {

		int tryLoop = maxTry;
		mouseClick(status, position, 0, 0);

		while (tryLoop > 0 && !status.isPassed()) {
			channel.progressiveWait(tryLoop);
			mouseClick(status, position, 0, 0);
			tryLoop--;
		}
	}

	protected void mouseClick(ActionStatus status, MouseDirection position, int offsetX, int offsetY) {
		engine.mouseClick(status, getFoundElement(), position, offsetX, offsetY);
		channel.actionTerminated(status);
	}

	public void drag(ActionStatus status, MouseDirection md, int offsetX, int offsetY, boolean offset) {
		engine.drag(status, getFoundElement(), md, offsetX, offsetY, offset);
		channel.actionTerminated(status);
	}

	public void drop(ActionStatus status, MouseDirection md, boolean desktopDragDrop) {
		engine.drop(getFoundElement(), md, desktopDragDrop);
		status.setPassed(true);
	}

	public void swipe(ActionStatus status, MouseDirection position, MouseDirection direction) {
		engine.swipe(status, getFoundElement(), position, direction);
	}

	public void swipe(int direction) {
		drag(null, new MouseDirection(), 0, 0, false);
		engine.moveByOffset(0, direction);
	}

	public void mouseWheel(int delta) {
		engine.scroll(getFoundElement(), delta);
		channel.progressiveWait(1);
	}

	public void wheelClick(ActionStatus status, MouseDirection position) {
		engine.middleClick(status, position, this);
	}

	public void doubleClick() {
		engine.doubleClick();
	}

	public void rightClick() {
		engine.rightClick();
	}

	public void tap(int count) {
		engine.tap(count, getFoundElement());
	}

	public void press(int duration, ArrayList<String> paths) {
		engine.press(duration, paths, getFoundElement());
	}

	//-------------------------------------------------------------------------------------------------------------------
	// Attributes
	//-------------------------------------------------------------------------------------------------------------------

	protected CalculatedProperty getAtsProperty(String name) {
		return new CalculatedProperty(name, new CalculatedValue(getAtsAttribute(name)));
	}

	protected String getAtsAttribute(String name) {
		if (ATS_OCCURRENCES.equals(name)) {
			return String.valueOf(count);
		} else if (ATS_OCCURRENCES_INDEX.equals(name)) {
			return String.valueOf(index);
		} else if (ATS_ID.equals(name)) {
			return NOT_AVAILABLE;
		} else if (ATS_TABLE_DATA.equals(name)) {
			return getTableData();
		} else if (ATS_SELECTED_TEXT.equals(name)) {
			return getSelectedText();
		} else if (ATS_MAX_TRY.equals(name)) {
			return String.valueOf(maxTry);
		} else if (ATS_SEARCH_DURATION.equals(name)) {
			return String.valueOf(totalSearchDuration);
		} else if (ATS_SEARCH_TAG.equals(name)) {
			return getSearchedTag();
		} else if (ATS_COL_NAME.equals(name)) {
			final FoundElement fe = getFoundElement();
			if(fe != null && AtsElement.TD.equalsIgnoreCase(fe.getTag())) {
				return engine.getColName(getFoundElement().getValue());
			}
			return NOT_AVAILABLE;
		}
		return null;
	}

	protected String getAtsAttributeNotFound(String name) {
		if (ATS_OCCURRENCES.equals(name)) {
			return "0";
		} else if (ATS_OCCURRENCES_INDEX.equals(name)) {
			return "-1";
		} else if (ATS_ID.equals(name)) {
			return NOT_AVAILABLE;
		} else if (ATS_TABLE_DATA.equals(name)) {
			return "";
		} else if (ATS_SELECTED_TEXT.equals(name)) {
			return "";
		} else if (ATS_MAX_TRY.equals(name)) {
			return String.valueOf(maxTry);
		} else if (ATS_SEARCH_DURATION.equals(name)) {
			return String.valueOf(totalSearchDuration);
		} else if (ATS_SEARCH_TAG.equals(name)) {
			return getSearchedTag();
		}
		return "";
	}

	protected String getTableData() {
		final JsonArray dataArray = new JsonArray();
		getTextData().parallelStream().forEach(l -> addJsonData(dataArray, l.getList()));
		return dataArray.toString();
	}

	protected String getSelectedText() {
		if (AtsElement.SELECT.equalsIgnoreCase(getFoundElement().getTag())) {
			return engine.getSelectedText(this);
		}
		return "";
	}

	private void addJsonData(JsonArray array, List<Parameter> pl) {
		final JsonObject jso = new JsonObject();
		pl.forEach(p -> jso.addProperty(p.getName(), p.getCalculated()));
		array.add(jso);
	}

	public String getAttribute(ActionStatus status, String name) {
		final FoundElement element = getFoundElement();

		if (element == null) {
			return getAtsAttributeNotFound(name);
		} else {
			final String attr = getAtsAttribute(name);
			if (attr != null) {
				return attr;
			} else if (isValidated()) {
				return engine.getAttribute(status, element, name, maxTry);
			}
			return null;
		}
	}

	private CalculatedProperty[] getAllProperties(CalculatedProperty[] props) {
				
		int count = 9;
		
		final CalculatedProperty[] result = new CalculatedProperty[props.length + count];
		
		result[0] = getAtsProperty(ATS_OCCURRENCES.toString());
		result[1] = getAtsProperty(ATS_OCCURRENCES_INDEX.toString());
		result[2] = getAtsProperty(ATS_ID.toString());
		result[3] = getAtsProperty(ATS_TABLE_DATA);
		result[4] = getAtsProperty(ATS_MAX_TRY);
		result[5] = getAtsProperty(ATS_SEARCH_DURATION);
		result[6] = getAtsProperty(ATS_SEARCH_TAG);
		result[7] = getAtsProperty(ATS_SELECTED_TEXT);
		result[8] = getAtsProperty(ATS_COL_NAME);
				
		for (CalculatedProperty prop : props) {
			result[count] = prop;
			count++;
		}
		
		return result;
	}

	public CalculatedProperty[] getCssAttributes() {
		return engine.getCssAttributes(getFoundElement());
	}
	
	public CalculatedProperty[] getHtmlAttributes() {
		return getAllProperties(engine.getHtmlAttributes(getFoundElement()));
	}
	
	public CalculatedProperty[] getAttributes(boolean reload) {
		return getAllProperties(engine.getAttributes(getFoundElement(), reload));
	}
		
	public CalculatedProperty[] getFunctions() {
		return engine.getFunctions(getFoundElement());
	}

	//TODO remove after dependencies synchronized
	public List<ParameterList> getTextData(Boolean reload) {
		return getTextData();
	}

	public List<ParameterList> getTextData() {

		final ArrayList<ParameterList> result = new ArrayList<>();

		if (getFoundElements().size() > 1) {

			if (index == 0) {
				getFoundElements().forEach(e -> addTextParameters(result, e, engine.getTextData(e)));
			} else if (index > 0 && getFoundElements().size() >= index) {
				final FoundElement elem = getFoundElements().get(index - 1);
				addTextParameters(result, elem, engine.getTextData(elem));
			}

		} else {
			if (AtsElement.SELECT.equalsIgnoreCase(getFoundElement().getTag())) {
				final List<String[]> options = engine.loadSelectOptions(this);
				if (options.size() > 0) {
					for (String[] option : options) {
						if (option.length > 0) {
							result.add(new ParameterList(getFoundElement(), option));
						} else {
							result.add(new ParameterList(getFoundElement(), getParameterList(engine.getAttributes(getFoundElement(), true))));
						}
					}
				} else {
					result.add(new ParameterList(getFoundElement(), getParameterList(engine.getAttributes(getFoundElement(), true))));
				}
			} else {

				String data;

				if (getFoundElement().isSAP()) {
					data = engine.getTextData(getFoundElement());
				} else {
					data = getFoundElement().getInnerText();
				}

				if (data != null && data.length() > 0) {
					final String[] lines = data.split("\n");
					if (lines.length > 0) {
						for (String l : lines) {
							final String[] cols = l.split("\t");
							if (cols.length > 0) {
								result.add(new ParameterList(getFoundElement(), cols, getParameterList(engine.getAttributes(getFoundElement(), true))));
							} else {
								result.add(new ParameterList(getFoundElement(), getParameterList(engine.getAttributes(getFoundElement(), true))));
							}
						}
					} else {
						result.add(new ParameterList(getFoundElement(), getParameterList(engine.getAttributes(getFoundElement(), true))));
					}
				} else {
					result.add(new ParameterList(getFoundElement(), getParameterList(engine.getAttributes(getFoundElement(), true))));
				}
			}
		}

		return Collections.unmodifiableList(result);
	}

	private static List<Parameter> getParameterList(CalculatedProperty[] properties) {
		if(properties.length > 0) {
			final Parameter[] params = new Parameter[properties.length];
			int loop = 0;
			for(CalculatedProperty prop : properties) {
				params[loop] = new Parameter(loop, prop);
				loop++;
			}
			return Arrays.asList(params);
		}
		return Arrays.asList(new Parameter(0, ""));
	}

	private void addTextParameters(ArrayList<ParameterList> result, FoundElement elem, String data) {

		final String[] cols = data.split("\t");
		ParameterList paramList;

		if (cols.length > 0) {
			paramList = new ParameterList(elem, cols, getParameterList(engine.getAttributes(elem, true)));
		} else {
			paramList = new ParameterList(elem, getParameterList(engine.getAttributes(elem, true)));
		}

		result.add(paramList);
	}

	//----------------------------------------------------------------------------------------------

	public Object executeScript(ActionStatus status, String script, boolean returnValue) {
		if (isValidated()) {
			return engine.executeJavaScript(status, script, this);
		}
		return null;
	}

	public void terminateExecution(ActionStatus status, ActionTestScript script, int error, long duration) {
		recorder = script.getRecorder();
		recorder.update(error, duration, this);
		channel.actionTerminated(status);
		
		if(count == 1 && searchedElement != null && engine.isAtsLearningEnabled()) {
			script.getTopScript().saveAtsLearning(this, engine, searchedElement);
		}
	}

	public void terminateExecution(ActionStatus status, ActionTestScript script, int error, long duration, String value, String data) {
		recorder = script.getRecorder();
		recorder.update(error, duration, value, data, this);
		channel.actionTerminated(status);
	}

	public void updateScreen() {
		recorder.updateScreen(this);
	}

	public void updateScreen(ActionStatus status, IVisualRecorder rec) {
		rec.updateScreen(status);
	}

	public Double[] getBound() {
		final int numElements = getElementsCount();
		final int i = getEvalIndex();

		Double[] elementBound = new Double[]{0D, 0D, 0D, 0D};
		if(numElements > i ) {

			final TestBound bound = getFoundElements().get(i).getTestBound();

			elementBound[0] = bound.getX();				//x
			elementBound[1] = bound.getY();				//y

			elementBound[2] = bound.getWidth();			//width
			elementBound[3] = bound.getHeight();		//height

			if(isSysComp()) {
				elementBound[0] += 8;			//x
				elementBound[1] += 8;			//y
			}
		}

		return elementBound;
	}
}