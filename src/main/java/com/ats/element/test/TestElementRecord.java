/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.element.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.ats.element.FoundElement;
import com.ats.element.SearchedElement;
import com.ats.executor.ActionStatus;
import com.ats.executor.ActionTestScript;
import com.ats.executor.channels.Channel;
import com.ats.executor.drivers.desktop.DesktopData;
import com.ats.generator.objects.MouseDirection;
import com.ats.generator.variables.CalculatedProperty;
import com.ats.generator.variables.CalculatedValue;

public class TestElementRecord extends TestElement {

	public final static String ATS_DURATION = "capture-duration";
	public final static String ATS_FRAMERATE = "capture-framerate";
	public final static String SHAPES_COUNT = "shapes-count";
	public final static String SHAPES_AVERAGE = "shapes-average";

	public final static String DEVICES_COUNT = "devicesCount";

	public final static String DEVICE = "device";
	public final static String DURATION = "duration";

	public final static String ERROR_MESSAGE = "error-message";

	public TestElementRecord(ActionTestScript script, Channel channel, int maxTry, Predicate<Integer> predicate, SearchedElement searchElement) {
		super(script, channel, maxTry, predicate, searchElement);
	}

	@Override
	protected List<FoundElement> loadElements(SearchedElement searchedElement) {
		if(parent != null) {
			final int[] result = searchedElement.updateRecordSelector();
			return new ArrayList<>(Arrays.asList(new FoundElement(channel, parent, result[0], result[1])));
		}

		return Collections.emptyList();
	}

	private Map<String, String> loadShapes() throws Exception {
		if(getCount() > 0) {

			final List<DesktopData> results = channel.getShapes(
					getFoundElement().getAttribute().get(DURATION),
					getFoundElement().getAttribute().get(DEVICE),
					getFoundElement().getScreenRectangle());

			final Map<String, String> map = results.stream().collect(Collectors.toMap(e -> e.getName(), e -> e.getValue()));
			if(map.get(ERROR_MESSAGE) != null){
				throw new Exception(map.get(ERROR_MESSAGE));
			}

			return map;
		}

		throw new Exception(ELEMENT_NOT_FOUND);
	}

	//-------------------------------------------------------------------------------------------------------------------
	// Mouse actions ...
	//-------------------------------------------------------------------------------------------------------------------

	@Override
	public void over(ActionStatus status, MouseDirection position, boolean desktopDragDrop, int offsetX, int offsetY) {
		// do nothing for the moment
	}

	@Override
	protected void mouseClick(ActionStatus status, MouseDirection position, int offsetX, int offsetY) {
		// do nothing for the moment
	}

	@Override
	public void mouseWheel(int delta) {
		// do nothing for the moment
	}

	//-------------------------------------------------------------------------------------------------------------------
	// Text ...
	//-------------------------------------------------------------------------------------------------------------------

	@Override
	public void clearText(ActionStatus status, MouseDirection md) {
		// do nothing for the moment
	}

	@Override
	public String sendText(ActionTestScript script, ActionStatus status, CalculatedValue text, int waitChar) {
		// do nothing for the moment
		return "";
	}

	@Override
	public String enterText(ActionStatus status, CalculatedValue text, ActionTestScript script, int waitChar) {
		// do nothing for the moment
		return "";
	}

	//-------------------------------------------------------------------------------------------------------------------
	// Drag drop ...
	//-------------------------------------------------------------------------------------------------------------------

	@Override
	public void drag(ActionStatus status, MouseDirection position, int offsetX, int offsetY, boolean offset) {
		// do nothing for the moment
	}

	//-------------------------------------------------------------------------------------------------------------------
	// Scripting ...
	//-------------------------------------------------------------------------------------------------------------------

	@Override
	public Object executeScript(ActionStatus status, String script, boolean returnValue) {
		return null;
	}

	//-------------------------------------------------------------------------------------------------------------------
	// Attributes ...
	//-------------------------------------------------------------------------------------------------------------------

	@Override
	public String getAttribute(ActionStatus status, String name) {
		if(ATS_OCCURRENCES.equals(name)) {
			return String.valueOf(getCount());
		}else if(ATS_OCCURRENCES_INDEX.equals(name)) {
			return String.valueOf(getIndex());
		}else if(SHAPES_COUNT.equals(name) || SHAPES_AVERAGE.equals(name)) {
			try {
				return loadShapes().get(name);
			} catch (Exception e) {
				status.setError(ActionStatus.WEB_DRIVER_ERROR, e.getMessage());
				return e.getMessage();
			}
		}
		return "";
	}
		
	@Override
	public CalculatedProperty[] getHtmlAttributes() {
		return getAttributes(true);
	}

	@Override
	public CalculatedProperty[] getAttributes(boolean reload) {
		try {
			final Map<String, String> map = loadShapes();
			return new CalculatedProperty[] {
					getAtsProperty(ATS_OCCURRENCES.toString()),
					getAtsProperty(ATS_OCCURRENCES_INDEX.toString()),
					new CalculatedProperty(SHAPES_COUNT, new CalculatedValue(map.get(SHAPES_COUNT))),
					new CalculatedProperty(SHAPES_AVERAGE, new CalculatedValue(map.get(SHAPES_AVERAGE)))};
		} catch (Exception e) {
			return new CalculatedProperty[] {
					getAtsProperty(ATS_OCCURRENCES.toString()),
					getAtsProperty(ATS_OCCURRENCES_INDEX.toString())};
		}
	}
}