package com.ats.element.test;

public class AtsProperty {
	private String[] listName = {""};

	public AtsProperty(String ...prop) {
		this.listName = prop;
	}

	@Override
	public boolean equals(Object obj) {
		for(String s : listName) {
			if(s.equals(obj.toString())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return listName[0];
	}
}
