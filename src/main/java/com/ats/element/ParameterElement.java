/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

package com.ats.element;

import org.openqa.selenium.remote.RemoteWebElement;

import com.ats.driver.AtsRemoteWebDriver;
import com.ats.executor.TestBound;

public class ParameterElement {
	private String id;
	private String tag;
	private TestBound bound;
	
	public ParameterElement() {}
	
	public ParameterElement(String id, String tag, TestBound bound) {
		this.id = id;
		this.tag = tag;
		this.bound = bound;
	}
	
	public RemoteWebElement getRemoteWebElement(AtsRemoteWebDriver driver) {
		final RemoteWebElement rwe = new RemoteWebElement();
		rwe.setId(getId());
		rwe.setParent(driver);
		rwe.setFileDetector(driver.getFileDetector());
		return rwe;
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getTag() {
		return tag;
	}
	
	public void setTag(String tag) {
		this.tag = tag;
	}

	public TestBound getBound() {
		return bound;
	}

	public void setBound(TestBound bound) {
		this.bound = bound;
	}
}