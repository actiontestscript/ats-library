/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

package com.ats.element;

import java.util.List;
import java.util.Map;

import com.ats.generator.variables.CalculatedProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AtsSapElement extends AtsBaseElement {

	public static final String X = "X";
	public static final String Y = "Y";
	public static final String SCREEN_X = "ScreenX";
	public static final String SCREEN_Y = "ScreenY";
	public static final String WIDTH = "Width";
	public static final String HEIGHT = "Height";
	public static final String ID = "Id";
	public static final String TAG = "Tag";

	private static final String ROOT = "root";
	private static final String INNER_TEXT = "innerText";
	private static final String TEXT = "Text";
	public static final String ELEMENT_ID = "ElementId";
	private static final String PATH = "Path";
	private static final String ATTRIBUTES = "Attributes";

	private AtsSapElement parent;

	private String innerText;
	private double screenX;
	private double screenY;

	public AtsSapElement(ObjectMapper mapper, JsonNode node) {

		setId(node.findValue(ID).asText());

		setTag(node.findValue(TAG).asText());
		setWidth(node.findValue(WIDTH).asDouble(0.0));
		setHeight(node.findValue(HEIGHT).asDouble(0.0));
		setX(node.findValue(X).asDouble(0.0));
		setY(node.findValue(Y).asDouble(0.0));

		screenX = node.findValue(SCREEN_X).asDouble(0.0);
		screenY = node.findValue(SCREEN_Y).asDouble(0.0);

		final Map<String, String> attributes = mapper.convertValue(node.findValue(ATTRIBUTES), new TypeReference<Map<String, String>>(){});

		attributes.put(TEXT, node.findValue(TEXT).asText());
		attributes.put(ELEMENT_ID, node.findValue(ELEMENT_ID).asText());
		attributes.put(PATH, node.findValue(PATH).asText());

		final JsonNode itNode = node.findValue(INNER_TEXT);
		if(itNode != null) {
			innerText = itNode.asText();
			attributes.put(INNER_TEXT, innerText);
		}

		setAttributes(attributes);
	}

	public AtsSapElement(JsonNode node, ObjectMapper mapper) {
		setId(node.findValue(ID).asText());
		setTag(node.findValue(TAG).asText());
		setWidth(node.findValue(WIDTH).asDouble(0.0));
		setHeight(node.findValue(HEIGHT).asDouble(0.0));
		setX(node.findValue(X).asDouble(0.0));
		setY(node.findValue(Y).asDouble(0.0));
	}

	public AtsSapElement(FoundElement element) {
		setId(element.getId());
		setTag(element.getTag());
		setWidth(element.getWidth());
		setHeight(element.getHeight());
		setX(element.getX());
		setY(element.getY());
		setScreenX(element.getScreenX());
		setScreenY(element.getScreenY());
		setAttributes(element.getAttribute());
	}

	public String getInnerText() {
		return innerText;
	}

	public double getScreenX() {
		return screenX;
	}
	public double getScreenY() {
		return screenY;
	}

	public void setScreenX(double x) { screenX = x;	}
	public void setScreenY( double y) { screenY = y; }

	public CalculatedProperty[] getAttributes() {
		final List<CalculatedProperty> properties = getProperties();
		return properties.toArray(new CalculatedProperty[properties.size()]);
	}

	public boolean isRoot() {
		return ROOT.equals(getTag());
	}

	public FoundElement getFoundElement() {
		return new FoundElement(this);
	}

	public AtsSapElement getParent() {
		return parent;
	}

	public void setParent(AtsSapElement parent) {
		this.parent = parent;
	}
}