/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

package com.ats.element;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.ats.generator.variables.CalculatedProperty;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.NullNode;

public class AtsBaseElement {

	private String id;
	private String tag;
	private Double width = 0D;
	private Double height = 0D;
	private Double x = 0D;
	private Double y = 0D;
	private Map<String, String> attributes = new HashMap<>();

	public AtsBaseElement() {}
	public AtsBaseElement(JsonNode node) {
		jsonDeserialize(node);
	}

	@JsonGetter("id")
	public String getId() {
		return id;
	}

	@JsonSetter("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonGetter("tag")
	public String getTag() {
		return tag;
	}

	@JsonSetter("tag")
	public void setTag(String tag) {
		this.tag = tag;
	}

	@JsonGetter("x")
	public Double getX() {
		return x;
	}

	@JsonSetter("x")
	public void setX(Double x) {
		this.x = x;
	}

	@JsonGetter("y")
	public Double getY() {
		return y;
	}

	@JsonSetter("y")
	public void setY(Double y) {
		this.y = y;
	}

	@JsonGetter("width")
	public Double getWidth() {
		return width;
	}

	@JsonSetter("width")
	public void setWidth(Double width) {
		this.width = width;
	}

	@JsonGetter("height")
	public Double getHeight() {
		return height;
	}

	@JsonSetter("height")
	public void setHeight(Double height) {
		this.height = height;
	}


	public void setAttributes(Map<String, String> value) {
		this.attributes = value;
	}

	protected List<CalculatedProperty> getProperties() {
		return attributes.entrySet().stream().parallel().map(e -> new CalculatedProperty(e.getKey(), e.getValue())).collect(Collectors.toCollection(ArrayList::new));
	}
	
	protected void addAttribute(String name, String value) {
		if(value != null) {
			attributes.put(name, value);
		}
	}

	//----------------------------------------------------------------------------------------
	// Predicate search
	//----------------------------------------------------------------------------------------

	public String getAttribute(String key) {
		return attributes.get(key);
	}

	public Map<String, String> getAttributesMap() {
		return attributes;
	}

	/*private void jsonAttributes(AtsBaseElement result, JsonNode attr){
		JsonNode dataNode = attr.get("data");
		if (dataNode != null) {
			String dataValue = dataNode.asText();
			String[] dataValueArray = dataValue.split("\n", 2);
			if (dataValueArray.length == 2) {
				String dataKey = dataValueArray[0];
				String dataValue2 = dataValueArray[1];
				result.setAttributes(Map.of(dataKey, dataValue2));
			}
		}
	}*/

	public void jsonDeserialize(JsonNode node){
		AtsBaseElement defaultValue = new AtsBaseElement();
		if(node instanceof NullNode || node.get("error") != null) { return; }

		setId(JsonUtils.getJsonValue(node, "id", defaultValue.id, String.class));
		setTag(JsonUtils.getJsonValue(node, "tag", defaultValue.tag, String.class));
		setWidth(JsonUtils.getJsonValue(node, "width", defaultValue.width, Double.class));
		setHeight(JsonUtils.getJsonValue(node, "height", defaultValue.height, Double.class));
		setX(JsonUtils.getJsonValue(node, "x", defaultValue.x, Double.class));
		setY(JsonUtils.getJsonValue(node, "y", defaultValue.y, Double.class));

		final JsonNode attributesNode = node.get("attributes");
		if (attributesNode != null ) {
			attributes = new HashMap<>();
			for(JsonNode attr : attributesNode) {
				JsonNode dataNode = attr.get("data");
				if (dataNode != null) {
					String dataValue = dataNode.asText();
					String[] dataValueArray = dataValue.split("\n", 2);
					if (dataValueArray.length == 2) {
						attributes.put(dataValueArray[0], dataValueArray[1]	);
					}
				}
			}
		}
	}
}

