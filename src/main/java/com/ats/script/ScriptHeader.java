/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.script;

import java.io.File;
import java.nio.file.Path;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.text.StringEscapeUtils;
import org.openqa.selenium.Keys;
import org.testng.annotations.Test;

import com.ats.crypto.Password;
import com.ats.executor.ActionTestScript;
import com.ats.executor.channels.SystemValues;
import com.ats.generator.ATS;
import com.ats.generator.objects.Cartesian;
import com.ats.generator.objects.mouse.Mouse;
import com.ats.generator.variables.ConditionalValue;
import com.ats.generator.variables.ProjectVariable;
import com.ats.generator.variables.PropertyVariable;
import com.ats.script.actions.Action;
import com.ats.script.actions.neoload.ActionNeoload;
import com.ats.script.actions.performance.ActionPerformance;
import com.ats.script.actions.performance.octoperf.ActionOctoperfVirtualUser;
import com.ats.tools.Operators;
import com.ats.tools.Utils;
import com.ats.tools.logger.ExecutionLogger;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;

import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ScriptHeader {

	private static final String NAME = "name";
	private static final String PROJECT_ID = "project-id";
	private static final String PROJECT_DESCRIPTION = "project-description";
	private static final String PROJECT_PATH = "project-path";

	public static final String ID = "id";
	public static final String UUID = "uuid";
	public static final String EXTERNAL_ID = "external";
	public static final String GROUPS = "groups";
	public static final String DESCRIPTION = "description";
	public static final String DATE_CREATED = "created";
	public static final String RETURN = "return";

	public static final String AUTHOR = "author";
	public static final String MODIFIED_AT = "modified-at";
	public static final String MODIFIED_BY = "modified-by";
	public static final String PREREQUISITE = "prerequisite";

	public static final String MEMO = "memo";
	public static final String PRE_PROCESSING = "pre-processing";
	public static final String POST_PROCESSING = "post-processing";

	public static final Predicate<String> GROUPS_MATCH = g -> g.regionMatches(true, 0, GROUPS, 0, GROUPS.length());
	public static final Predicate<String> ID_MATCH = g -> g.regionMatches(true, 0, ID, 0, ID.length());
	public static final Predicate<String> UUID_MATCH = g -> g.regionMatches(true, 0, UUID, 0, UUID.length());
	public static final Predicate<String> AUTHOR_MATCH = g -> g.regionMatches(true, 0, AUTHOR, 0, AUTHOR.length());
	public static final Predicate<String> MODIFIED_AT_MATCH = g -> g.regionMatches(true, 0, MODIFIED_AT, 0, MODIFIED_AT.length());
	public static final Predicate<String> MODIFIED_BY_MATCH = g -> g.regionMatches(true, 0, MODIFIED_BY, 0, MODIFIED_BY.length());
	public static final Predicate<String> PREREQUISITE_MATCH = g -> g.regionMatches(true, 0, PREREQUISITE, 0, PREREQUISITE.length());
	public static final Predicate<String> DESCRIPTION_MATCH = g -> g.regionMatches(true, 0, DESCRIPTION, 0, DESCRIPTION.length());
	public static final Predicate<String> DATE_CREATED_MATCH = g -> g.regionMatches(true, 0, DATE_CREATED, 0, DATE_CREATED.length());
	public static final Predicate<String> RETURN_MATCH = g -> g.regionMatches(true, 0, RETURN, 0, RETURN.length());
	public static final Predicate<String> EXTERNAL_ID_MATCH = g -> g.regionMatches(true, 0, EXTERNAL_ID, 0, EXTERNAL_ID.length());
	public static final Predicate<String> MEMO_MATCH = g -> g.regionMatches(true, 0, MEMO, 0, MEMO.length());
	public static final Predicate<String> PRE_PROCESSING_MATCH = g -> g.regionMatches(true, 0, PRE_PROCESSING, 0, PRE_PROCESSING.length());
	public static final Predicate<String> POST_PROCESSING_MATCH = g -> g.regionMatches(true, 0, POST_PROCESSING, 0, POST_PROCESSING.length());

	private File testReportFolder;
	private File javaDestinationFolder;
	private File javaFile;

	private String path = "";
	private String projectPath = "";
	
	@Expose(serialize = true)
	private String packageName = "";
	
	@Expose(serialize = true)
	private String name = "";
	
	@Expose(serialize = true)
	private String id = "";
	
	@Expose(serialize = true)
	private String uuid = "";
	
	@Expose(serialize = true)
	private String externalId = "";

	@Expose(serialize = true)
	private List<String> groups = List.of();

	@Expose(serialize = true)
	private String description = "";
	
	@Expose(serialize = true)
	private String author = "";
	
	@Expose(serialize = true)
	private String prerequisite = "";
	
	@Expose(serialize = true)
	private String memo = "";
	
	private String preprocessing = "";
	private String postprocessing = "";

	@Expose(serialize = true)
	private Date createdAt = new Date();
	
	@Expose(serialize = true)
	private Date modifiedAt = new Date(0L);
	
	@Expose(serialize = true)
	private String modifiedBy = "";

	@Expose(serialize = true)
	private String projectId = "";
	
	@Expose(serialize = true)
	private String projectDescription = "";
	
	@Expose(serialize = true)
	private String projectUuid = "";
	
	private String logsType = "";
	
	@Expose(serialize = true)
	private String error = null;

	public ScriptHeader(){} // needed for serialization

	public ScriptHeader(String projectUuid, String projectId, String projectDescription, String testName, String id, String author, String description, String prerequisites, String externalId, String memo, String preprocessing, String postprocessing, String groups, String logsType, Date modifiedAt, String modifiedBy){
		this.projectUuid = projectUuid;
		this.projectId = projectId;
		this.projectDescription = projectDescription;
		this.name = testName;
		this.id = id;
		this.author = author;
		this.description = description;
		this.modifiedAt = modifiedAt;
		this.modifiedBy = modifiedBy;
		this.prerequisite = prerequisites;
		this.externalId = externalId;
		this.memo = memo;
		this.preprocessing = preprocessing;
		this.postprocessing = postprocessing;
		this.logsType = logsType;
		this.setGroups(Arrays.asList(groups.split(",")));
	}

	public ScriptHeader(Project prj, File file){

		this.setProjectPath(prj.getFolderPath());
		this.setPath(file.getAbsolutePath());
		this.setName(Utils.removeExtension(file.getName()));

		try {
			final Path relative = prj.getAtsSourceFolder().relativize(file.getParentFile().toPath());
			this.setPackageName(relative.toString().replace(File.separator, "."));
		}catch (IllegalArgumentException e) {}

		this.testReportFolder = prj.getReportFolder().toFile();
		this.javaFile = prj.getJavaFile(getPackagePath() + name + ".java");
		this.javaDestinationFolder = prj.getJavaDestinationFolder().toFile();
	}

	public ScriptHeader(Project prj){
		this.projectPath = prj.getFolderPath();
		this.path = this.projectPath + "/" + "AtsTemp-";
		this.name = "AtsTemp-";
		this.packageName = "";
	}

	public ScriptHeader(String name){
		this.name = name;
		this.packageName = "";
	}

	public File getTestReportFolder(Path path) {
		return testReportFolder;
	}

	public File getReportFolder() {
		return testReportFolder;
	}

	public File getJavaDestinationFolder() {
		return javaDestinationFolder;
	}

	public File getJavaFile() {
		return javaFile;
	}

	public String getPackagePath() {
		String path = packageName.replace(".", File.separator);
		if(path.length() > 0) {
			path += File.separator;
		}
		return path;
	}

	public void parseGroups(String data) {
		this.setGroups(Arrays.stream(data.split(",")).map(String :: trim).filter(s -> (s.length() > 0)).collect(Collectors.toList()));
	}

	public String getDataGroups() {
		if(groups != null){
			return String.join(",", groups);
		}
		return "";
	}

	public String getGroupsList() {
		if(groups != null){
			StringJoiner joiner = new StringJoiner(",");
			for (String group : groups) {
				if(group.length() > 0) {
					joiner.add(group);
				}
			}
			return joiner.toString();
		}
		return "";
	}

	public String getJoinedGroups() {
		if(groups != null){
			StringJoiner joiner = new StringJoiner(", ");
			for (String group : groups) {
				if(group.length() > 0) {
					joiner.add("\"" + group + "\"");
				}
			}
			return joiner.toString();
		}
		return "";
	}

	private String getTestAnnotation() {

		final StringBuilder annotation = new StringBuilder();
		final String groups = getJoinedGroups();

		if(groups.length() > 0){
			annotation.append("groups={")
			.append(groups)
			.append("}");
		}

		if(description != null && description.length() > 0) {
			if(annotation.length() > 0) {
				annotation.append(",");
			}
			annotation.append("description=\"");
			annotation.append(StringEscapeUtils.escapeJava(Utils.leftString(description, 30)));
			annotation.append("\"");
		}

		/*if(id != null && id.length() > 0) {
			if(annotation.length() > 0) {
				annotation.append(",");
			}
			annotation.append("@CustomAttribute(name=\"id\", values=\"");
			annotation.append(id);
			annotation.append("\")");
		}*/

		if(annotation.length() > 0) {
			annotation.insert(0, "(").append(")");
			return annotation.toString();
		}

		return "";
	}

	public String getQualifiedName() {
		if(packageName != null && packageName.length() > 0) {
			return packageName + "." + name;
		}else {
			return name;
		}
	}

	private static final String javaCode = String.join(
			System.getProperty("line.separator")
			, ""
			, "//---------------------------------------------------------------------------------------"
			, "//\t    _  _____ ____     ____                           _             "
			, "//\t   / \\|_   _/ ___|   / ___| ___ _ __   ___ _ __ __ _| |_ ___  _ __ "
			, "//\t  / _ \\ | | \\___ \\  | |  _ / _ \\ '_ \\ / _ \\ '__/ _` | __/ _ \\| '__|"
			, "//\t / ___ \\| |  ___) | | |_| |  __/ | | |  __/ | | (_| | || (_) | |   "
			, "//\t/_/   \\_\\_| |____/   \\____|\\___|_| |_|\\___|_|  \\__,_|\\__\\___/|_|   "
			, "//"
			, "//---------------------------------------------------------------------------------------"
			, "//\t/!\\ Warning /!\\"
			, "//\tThis class was automatically generated by ATS Script Generator (ver. #ATS_VERSION#)"
			, "//\tYou may lose modifications if you edit this file manually !"
			, "//---------------------------------------------------------------------------------------"
			, ""
			, "import " + ScriptHeader.class.getPackageName() + ".*;"
			, "import " + Action.class.getPackageName() + ".*;"
			, "import " + ActionNeoload.class.getPackageName() + ".*;"
			, "import " + ActionPerformance.class.getPackageName() + ".*;"
			, "import " + ActionOctoperfVirtualUser.class.getPackageName() + ".*;"
			, "import " + ActionTestScript.class.getName() + ";"
			, "import " + Cartesian.class.getName() + ";"
			, "import " + Mouse.class.getName() + ";"
			, "import " + ConditionalValue.class.getName() + ";"
			, "import " + ProjectVariable.class.getName() + ";"
			, "import " + Password.class.getName() + ";"
			, "import " + PropertyVariable.class.getName() + ";"
			, "import " + Operators.class.getName() + ";"
			, "import " + Keys.class.getName() + ";"
			, ""
			, "public class #CLASS_NAME# extends " + ActionTestScript.class.getSimpleName() + "{"
			, ""
			, "\t/**"
			, "\t* Test Name : <b>#TEST_NAME#</b>"
			, "\t* Generated at : <b>" + DateFormat.getDateTimeInstance().format(new Date()) + "</b>"
			, "\t* Script created at : #TEST_CREATED_AT#"
			, "\t* #TEST_DESCRIPTION#"
			, "\t* @author\t#TEST_AUTHOR_NAME#"
			, "\t*/"
			, ""
			, "\t// -----------------[ ATS script info ]----------------- //"
			, ""
			, "\tpublic static final java.lang.String ATS_TEST_ID = \"#TEST_ID#\";"
			, "\tpublic static final java.lang.String ATS_TEST_NAME = \"#TEST_NAME#\";"
			, "\tpublic static final java.lang.String ATS_AUTHOR_NAME = \"#TEST_AUTHOR_NAME#\";"
			, "\tpublic static final java.lang.String ATS_DESCRIPTION = \"#TEST_DESCRIPTION#\";"
			, "\tpublic static final java.lang.String ATS_PREREQUISITES = \"#TEST_PREREQUISITES#\";"
			, "\tpublic static final java.lang.String ATS_MEMO = \"#TEST_MEMO#\";"
			, "\tpublic static final java.util.Date TEST_MODIFIED_AT = getIsoDate(\"#TEST_MODIFIED_AT#\");"
			, "\tpublic static final java.lang.String TEST_MODIFIED_BY = \"#TEST_MODIFIED_BY#\";"
			, "\tpublic static final java.lang.String ATS_EXTERNAL_ID = \"#TEST_EXTERNAL_ID#\";"
			, "\tpublic static final java.lang.String ATS_GROUPS = \"#GROUP_DESCRIPTION#\";"
			, "\tpublic static final java.lang.String ATS_PROJECT_ID = \"#PROJECT_ID#\";"
			, "\tpublic static final java.lang.String ATS_PROJECT_DESCRIPTION = \"#PROJECT_DESCRIPTION#\";"
			, "\tpublic static final java.lang.String ATS_PROJECT_UUID = \"#PROJECT_UUID#\";"
			, "\tpublic static final java.lang.String ATS_LOGS_TYPE = \"#PROJECT_LOGS_TYPE#\";"
			, "\tpublic static final java.lang.String ATS_PRE_PROCESSING = \"#TEST_PRE_PROCESSING#\";"
			, "\tpublic static final java.lang.String ATS_POST_PROCESSING = \"#TEST_POST_PROCESSING#\";"
			, ""
			, "\t// -----------------[ Constructors ]----------------- //"
			, ""
			, "\tpublic #CLASS_NAME#(){super();}"
			, "\tpublic #CLASS_NAME#(" + ActionTestScript.class.getName() + " sc){super(sc);}"
			, ""
			, "\t// -----------------[ Overrides ]----------------- //"
			, ""
			, "\t@java.lang.Override"
			, "\tprotected " + ScriptHeader.class.getSimpleName() + " getScriptHeader(){return new ScriptHeader(ATS_PROJECT_UUID, ATS_PROJECT_ID, ATS_PROJECT_DESCRIPTION, ATS_TEST_NAME, ATS_TEST_ID, ATS_AUTHOR_NAME, ATS_DESCRIPTION, ATS_PREREQUISITES, ATS_EXTERNAL_ID, ATS_MEMO, ATS_PRE_PROCESSING, ATS_POST_PROCESSING, ATS_GROUPS, ATS_LOGS_TYPE, TEST_MODIFIED_AT, TEST_MODIFIED_BY);}"
			, ""
			, "\t// -----------------[ Test ]----------------- //"
			, ""
			, "\tpublic void " + ActionTestScript.MAIN_TEST_FUNCTION + "(){"
			, "\t\t#SCRIPT_PREFIX##CLASS_NAME#();"
			, "\t}"
			, ""
			, "\t@" + Test.class.getName() + "#TEST_ANNOTATION#"
			, "\tpublic void #SCRIPT_PREFIX##CLASS_NAME#(){"
			);

	public String getJavaCode(Project project) {

		final String code = javaCode.replace("#CLASS_NAME#", name)
				.replace("#TEST_NAME#", getQualifiedName())
				.replace("#TEST_ID#", StringEscapeUtils.escapeJava(id))
				.replace("#TEST_DESCRIPTION#", StringEscapeUtils.escapeJava(description))
				.replace("#TEST_PREREQUISITES#", StringEscapeUtils.escapeJava(prerequisite))
				.replace("#TEST_AUTHOR_NAME#", StringEscapeUtils.escapeJava(author))
				.replace("#TEST_EXTERNAL_ID#", StringEscapeUtils.escapeJava(externalId))
				.replace("#TEST_PRE_PROCESSING#", StringEscapeUtils.escapeJava(preprocessing))
				.replace("#TEST_POST_PROCESSING#", StringEscapeUtils.escapeJava(postprocessing))
				.replace("#TEST_MEMO#", StringEscapeUtils.escapeJava(memo))
				.replace("#TEST_MODIFIED_AT#", Utils.dateFormat(modifiedAt))
				.replace("#TEST_MODIFIED_BY#", StringEscapeUtils.escapeJava(modifiedBy))
				.replace("#TEST_ANNOTATION#", getTestAnnotation())
				.replace("#GROUP_DESCRIPTION#", getDataGroups())
				.replace("#ATS_VERSION#", ATS.getAtsVersion())
				.replace("#TEST_CREATED_AT#", DateFormat.getDateTimeInstance().format(createdAt))
				.replace("#PROJECT_UUID#", StringEscapeUtils.escapeJava(project.getUuid()))
				.replace("#PROJECT_DESCRIPTION#", StringEscapeUtils.escapeJava(project.getDescription()))
				.replace("#SCRIPT_PREFIX#", StringEscapeUtils.escapeJava(project.getScriptPrefix()))
				.replace("#PROJECT_LOGS_TYPE#", StringEscapeUtils.escapeJava(project.getLogsType()))
				.replace("#PROJECT_ID#", StringEscapeUtils.escapeJava(project.getGav()));

		if(!packageName.isEmpty()) {
			return "package " + packageName + ";\r\n" + code;
		}

		return code;
	}

	//@TODA A supprimer quand bascule json
	/*public Object[] getData(int quality, long started) {
		final Object[] result = new Object[9];
		result[0] = getId();
		result[1] = getQualifiedName();
		result[2] = getDescription();
		result[3] = getAuthor();
		result[4] = getGroupsList();
		result[5] = getPrerequisite();
		result[6] = getExternalId();
		result[7] = quality;
		result[8] = started;
		return result;
	}*/

	public Map<String,Object> getJsonData(int quality, long started) {
		Map<String,Object> valueNode = new HashMap<>();
		valueNode.put("id", getId() );
		valueNode.put("fullName", getQualifiedName() );
		valueNode.put("description", getDescription() );
		valueNode.put("author", getAuthor() );
		valueNode.put("modifiedAt", String.valueOf(getModifiedAt().getTime()) );
		valueNode.put("modifiedBy", getModifiedBy() );
		valueNode.put("groups", getGroupsList() );
		valueNode.put("preRequisites", getPrerequisite() );
		valueNode.put("externalId", getExternalId() );
		valueNode.put("memo", getMemo() );
		valueNode.put("videoQuality", quality );
		valueNode.put("started", Long.toString(started));
		return valueNode;
	}

	public void setSystemProperties(String value) {}

	public String getSystemProperties() {
		return String.join(",", SystemValues.VALUES_NAME);
	}

	public String getProjectId() {
		return projectId;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public String getProjectUuid() {
		return projectUuid;
	}

	public String getData(String key) {
		switch (key) {
		case DESCRIPTION:
			return getDescription();
		case NAME:
			return getName();
		case EXTERNAL_ID:
			return getExternalId();
		case PREREQUISITE:
			return getPrerequisite();
		case ID:
			return getId();
		case GROUPS:
			return getGroupsList();
		case AUTHOR:
			return getAuthor();
		case PROJECT_ID:
			return getProjectId();
		case PROJECT_DESCRIPTION:
			return getProjectDescription();
		case PROJECT_PATH:
			return Project.getProjectFolderPath();
		case MODIFIED_BY:
			return getModifiedBy();
		case MEMO:
			return getMemo();
		case POST_PROCESSING:
			return getPostprocessing();
		case PRE_PROCESSING:
			return getPreprocessing();
		}
		return "";
	}

	//-------------------------------------------------------------------------------------------------
	//  send test stats
	//-------------------------------------------------------------------------------------------------

	public void sendPreprocessing(ExecutionLogger logger, String suiteName) {
		sendRestData(logger, preprocessing, suiteName, -1L, -1, false);
	}

	public void sendPostprocessing(ExecutionLogger logger, String suiteName, long duration, int actions, boolean passed) {
		sendRestData(logger, postprocessing, suiteName, duration, actions, passed);
	}

	private void sendRestData(ExecutionLogger logger, String url, String suiteName, long duration, int actions, boolean passed) {

		if(url != null && url.length() > 0) {

			if(!url.toLowerCase().startsWith("http")) {
				url = "http://" + url;
			}
			
			try {
				final okhttp3.Request.Builder reqBuilder = new Request.Builder().url(url);
				final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
				
				final JsonObject jsonObj = (JsonObject)gson.toJsonTree(this);
				jsonObj.addProperty("suite", suiteName);
				
				if(duration != -1L && actions != -1) {
					jsonObj.addProperty("duration", duration);
					jsonObj.addProperty("passed", passed);
					jsonObj.addProperty("actions", actions);
				}
				
				final RequestBody body = RequestBody.Companion.create(jsonObj.toString(), Utils.JSON_MEDIA);

				final OkHttpClient client = new Builder().cache(null)
						.connectTimeout(60, TimeUnit.SECONDS)
						.writeTimeout(60, TimeUnit.SECONDS)
						.readTimeout(60, TimeUnit.SECONDS)
						.build();

				final Request request = reqBuilder
						.addHeader("User-Agent", "Ats-Execution-Agent")
						.addHeader("Content-Type", "application/json; charset=utf-8")
						.post(body)
						.build();

				Response response = client.newCall(request).execute();
				response.close();

			}catch (Exception e) {
				logger.sendWarning("unable to send REST data", e.getMessage());
			}
		}
	}

	//-------------------------------------------------------------------------------------------------
	//  getters and setters for serialization
	//-------------------------------------------------------------------------------------------------

	public List<String> getGroups() {
		return groups;
	}

	public void setGroups(List<String> value) {
		this.groups = value.stream().distinct().collect(Collectors.toList());
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String value) {
		this.description = value;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String value) {
		this.author = value;
	}

	public String getPrerequisite() {
		return prerequisite;
	}

	public void setPrerequisite(String value) {
		this.prerequisite = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String value) {
		this.name = value;
	}

	public String getId() {
		return id;
	}

	public void setId(String value) {
		this.id = value;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String value) {
		this.externalId = value;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String value) {
		this.packageName = value;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getProjectPath() {
		return projectPath;
	}

	public void setProjectPath(String value) {
		this.projectPath = value;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date value) {
		this.createdAt = value;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String value) {
		this.uuid = value;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getLogsType() {
		return logsType;
	}

	public void setLogsType(String logsType) {
		this.logsType = logsType;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getPreprocessing() {
		return preprocessing;
	}

	public void setPreprocessing(String preprocessing) {
		this.preprocessing = preprocessing;
	}

	public String getPostprocessing() {
		return postprocessing;
	}

	public void setPostprocessing(String postprocessing) {
		this.postprocessing = postprocessing;
	}
	
	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Date value) {
		this.modifiedAt = value;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
}