/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.script;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import org.ahocorasick.trie.Trie;

import com.ats.executor.ActionTestScript;
import com.ats.generator.GeneratorReport;
import com.ats.generator.parsers.Lexer;
import com.ats.generator.parsers.ScriptParser;
import com.ats.generator.variables.CalculatedValue;
import com.ats.generator.variables.Variable;
import com.ats.generator.variables.parameter.ParameterList;
import com.ats.script.actions.Action;
import com.ats.script.actions.ActionCallscript;

public class AtsScript extends Script {

	public static final String SCRIPT_EXTENSION_ERROR = "ERR #198 - wrong file extension";
	public static final String SCRIPT_LOADING_ERROR = "ERR #199 - the script cannot be loaded";

	private ScriptParser parser;
	private ScriptHeader header;
	private String javaCode = null;

	private Charset charset;

	private ArrayList<Action> actions = new ArrayList<>();

	private String projectGav = "";

	private int actionsCount = 0;
	private int callscripts = 0;
	private long size;

	public AtsScript() {}

	public AtsScript(Lexer lexer) {
		this.parser = new ScriptParser(lexer);
		this.parser.addScript();
	}

	public AtsScript(String type, Lexer lexer, File file, Project project) {
		this(type, lexer, file, project, DEFAULT_CHARSET);
	}

	public AtsScript(String type, Lexer lexer, File file, Project project, Charset charset){
		this.header =  new ScriptHeader(project, file);
		this.setCharset(charset);
		this.projectGav = project.getGav();

		if(ATS_EXTENSION.equals(type)){

			if(file.getName().endsWith(ATS_FILE_EXTENSION)) {
				this.setParameterList(new ParameterList(1));
				this.setVariables(new ArrayList<>());

				this.parser = new ScriptParser(lexer);
				this.parser.addScript();

				try {
					Stream <String> lines = Files.lines(file.toPath(), this.charset);
					lines
					.map(String::trim)
					.filter(a -> !a.isEmpty())
					.filter(a -> !a.startsWith("["))
					.forEach(a -> parser.parse(this, header, a));
					lines.close();

				} catch (Exception e) {
					System.err.println("ATS load project error (" + SCRIPT_LOADING_ERROR + ") :\n" + file.getAbsolutePath());
					header.setName(file.getName());
					header.setPackageName("...");
					header.setError(SCRIPT_LOADING_ERROR);
				}
			}else {
				header.setName(file.getName());
				header.setPackageName("...");
				header.setError(SCRIPT_EXTENSION_ERROR);
			}

		}else if("java".equals(type)) {
			try {
				this.javaCode = new String(Files.readAllBytes(file.toPath()));
			} catch (IOException ignored) {}
		}
	}

	public AtsScript(Lexer lexer, String content, Project project){
		final ScriptHeader header = new ScriptHeader(project);

		this.header = header;
		this.setCharset(DEFAULT_CHARSET);
		this.projectGav = project.getGav();

		this.setParameterList(new ParameterList(1));
		this.setVariables(new ArrayList<>());

		this.parser = new ScriptParser(lexer);
		this.parser.addScript();

		try {
			parser.parse(this, header, content);
		} catch (Exception ignored) {}
	}

	public AtsScript(ScriptHeader header, File f) {
		this.header = header;
		size = f.length();
		// _nativePath = f.getAbsolutePath();
	}

	public AtsScript(ScriptHeader header) {
		this(header, new File(header.getPath()));
	}

	public AtsScript(Lexer lexer, File file) {
		this(lexer.loadScript(file), file);
	}

	public AtsScript(AtsScript script, File file) {
		this(script.header, file);
		script.getActions().parallelStream().forEach(this::count);
	}

	public AtsScript(Project project, File file) {
		this(new ScriptHeader(project, file), file);
	}

	private void setCharset(Charset value) {
		this.charset = value;
	}

	private void count(Action a) {
		if(a instanceof ActionCallscript) {
			callscripts++;
		}else {
			actionsCount++;
		}
	}

	public void addAction(Action data, boolean disabled){
		data.setDisabled(disabled);
		data.setLine(actions.size());
		actions.add(data);
	}

	public void addAction(Action data, boolean disabled, String atsCode){
		addAction(data, disabled);
		data.setCode(atsCode);
	}

	public boolean isSubscriptCalled(String scriptName) {
		for (Action action : actions) {
			if(action instanceof ActionCallscript) {
				if(((ActionCallscript)action).isSubscriptCalled(scriptName)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean getActionsKeywords(Trie trie) {
		for (Action action : actions) {
			final List<String> actionKeywords = action.getKeywords();
			for(String keywords : actionKeywords) {
				if(trie.containsMatch(keywords)) {
					return true;
				}
			}
		}
		return false;
	}

	public void updateSubscript(ArrayList<String> result, String oldName, String newName) {

		final ArrayList<String[]> replacements = new ArrayList<>();

		for(Action a : actions) {
			if(a instanceof ActionCallscript) {
				final ActionCallscript action = (ActionCallscript)a;
				if(oldName.equals(action.getName().getData())) {
					int start = a.getAtsCode().indexOf(oldName);
					int end = start + oldName.length();
					replacements.add(new String[] {a.getAtsCode(), a.getAtsCode().substring(0, start) + newName + a.getAtsCode().substring(end)});
				}
			}
		}
		saveUpdates(result, replacements);
	}

	public void updateDataFile(ArrayList<String> result, String oldPath, String newPath) {

		final ArrayList<String[]> replacements = new ArrayList<>();

		for(Action a : actions) {
			if(a instanceof ActionCallscript) {
				final ActionCallscript action = (ActionCallscript)a;
				if(action.getParameterFilePath() != null && oldPath.equals(action.getParameterFilePath().getData())) {
					replacements.add(new String[] {a.getAtsCode(), a.getAtsCode().replace(oldPath, newPath)});
				}
			}
		}
		saveUpdates(result, replacements);
	}

	private void saveUpdates(ArrayList<String> result, ArrayList<String[]> replacements) {
		if (!replacements.isEmpty()) {

			final ArrayList<String> lines = new ArrayList<>();
			final Path path = Paths.get(header.getPath());

			try {
				Files.lines(path).forEach(lines::add);
			} catch (IOException ignored) {}

			int i = 0;
			String[] replace = replacements.removeFirst();
			for (String line : lines) {
				if(line.equals(replace[0])) {
					lines.set(i, replace[1]);
					if(!replacements.isEmpty()) {
						replace = replacements.removeFirst();
					}
				}
				i++;
			}

			try {
				Files.write(path, lines, Charset.defaultCharset());
			} catch (IOException ignored) {}

			result.add(header.getQualifiedName());
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	// Java Code Generator
	//---------------------------------------------------------------------------------------------------------------------------------

	public String getJavaCode(Project project){

		if(javaCode != null) {

			return javaCode;

		}else {

			final StringBuilder code = new StringBuilder(header.getJavaCode(project));

			//-------------------------------------------------------------------------------------------------
			// variables
			//-------------------------------------------------------------------------------------------------

			code.append("\r\n\r\n\t\t//   ---< Variables >---   //\r\n");

			final List<Variable> variables = getVariables();
			Collections.sort(variables);

			for(Variable variable : variables){
				code.append("\r\n\t\t")
				.append(variable.getJavaCode())
				.append(";");
			}

			//-------------------------------------------------------------------------------------------------
			// actions
			//-------------------------------------------------------------------------------------------------

			code.append("\r\n\r\n\t\t//   ---< Actions >---   //\r\n");

			for(Action action : actions){
				if(!action.isDisabled() && !action.isScriptComment()){
					code.append("\r\n\t\t").append(action.getJavaCode()).append(");");
				}
			}

			//-------------------------------------------------------------------------------------------------
			// returns
			//-------------------------------------------------------------------------------------------------

			final CalculatedValue[] returnValues = getReturns();
			if(returnValues != null) {

				code.append("\r\n\r\n\t\t//   ---< Return >---   //\r\n\r\n\t\t")
				.append(ActionTestScript.JAVA_RETURNS_FUNCTION_NAME)
				.append("(");

				final ArrayList<String> returnValuesCode = new ArrayList<>();
				for(CalculatedValue ret : returnValues){
					returnValuesCode.add(ret.getJavaCode());
				}

				code.append(String.join(", ", returnValuesCode)).append(");");
			}

			code.append("\r\n\t}\r\n}");

			return code.toString();
		}
	}

	public void generateJavaFile(Project project){
		if(header.getJavaDestinationFolder() != null){
			final File javaFile = header.getJavaFile();
			try {
				javaFile.getParentFile().mkdirs();

				final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter( new FileOutputStream(javaFile, false), charset));
				writer.write(getJavaCode(project));
				writer.close();

			} catch (IOException ignored) {}
		}
	}

	//-------------------------------------------------------------------------------------------------
	//  getters and setters for serialization
	//-------------------------------------------------------------------------------------------------
	
	public int getActionsCount() {
		return actionsCount;
	}

	public void setActionsCount(int value) {
		this.actionsCount = value;
	}

	public int getCallscripts() {
		return callscripts;
	}

	public void setCallscripts(int callscripts) {
		this.callscripts = callscripts;
	}

	public String getAuthor() {
		return header.getAuthor();
	}

	public void setAuthor(String author) {
		header.setAuthor(author);
	}

	public String getDescription() {
		return header.getDescription();
	}

	public void setDescription(String description) {
		header.setDescription(description);
	}

	public String getId() {
		return header.getId();
	}

	public void setId(String id) {
		header.setId(id);
	}

	public List<String> getGroups() {
		return header.getGroups();
	}

	public void setGroups(List<String> groups) {
		header.setGroups(groups);
	}

	public String getPrerequisite() {
		return header.getPrerequisite();
	}

	public void setPrerequisite(String prerequisite) {
		header.setPrerequisite(prerequisite);
	}

	public Date getCreatedAt() {
		return header.getCreatedAt();
	}

	public void setCreatedAt(Date createdAt) {
		header.setCreatedAt(createdAt);
	}

	public Date getModifiedAt() {
		return header.getModifiedAt();
	}

	public void setModifiedAt(Date modifiedAt) {
		header.setModifiedAt(modifiedAt);
	}

	public String getModifiedBy() {
		return header.getModifiedBy();
	}

	public void setModifiedBy(String name) {
		header.setModifiedBy(name);
	}

	public String getName() {
		return header.getName();
	}

	public void setName(String name) {
		header.setName(name);
	}

	public String getPackageName() {
		return header.getPackageName();
	}

	public void setPackageName(String packageName) {
		header.setPackageName(packageName);
	}

	public String getQualifiedName() {
		return header.getQualifiedName();
	}

	public void setQualifiedName(String qualifiedName) {

	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getExternalId() {
		return header.getExternalId();
	}

	public void setExternalId(String id) {
		header.setExternalId(id);
	}

	public String getNativePath() {
		return header.getPath();
	}

	public void setNativePath(String nativePath) {
		header.setPath(nativePath);
	}

	public String getError() {
		return header.getError();
	}

	public void setError(String error) {
		header.setError(error);
	}

	public String getMemo() {
		return header.getMemo();
	}

	public void setMemo(String memo) {
		header.setMemo(memo);
	}

	public String getPreprocessing() {
		return header.getPreprocessing();
	}

	public void setPreprocessing(String preprocessing) {
		header.setPreprocessing(preprocessing);
	}

	public String getPostprocessing() {
		return header.getPostprocessing();
	}

	public void setPostprocessing(String postprocessing) {
		header.setPostprocessing(postprocessing);
	}

	public String getProjectPath() {
		return header.getProjectPath();
	}

	public void setProjectPath(String value) {
		header.setProjectPath(value);
	}

	public ScriptHeader getHeader() {
		return header;
	}

	public ArrayList<Action> getActions() {
		return actions;
	}

	public void setActions(ArrayList<Action> value) {
		this.actions = value;
	}

	public String getProjectGav() {
		return projectGav;
	}

	public void setProjectGav(String value) {} // read only */

	//-------------------------------------------------------------------------------------------------
	//  transient getters and setters
	//-------------------------------------------------------------------------------------------------

	public Lexer getLexer() {
		return parser.getLexer();
	}

	//----------------------------------------------------------------------------------------------------
	// Script content java
	//----------------------------------------------------------------------------------------------------

	public static void main(String[] args) {
		if (args.length == 1) {

			final File scriptFile = new File(args[0]);
			if (scriptFile.exists() && scriptFile.isFile() && scriptFile.getName().toLowerCase().endsWith(ATS_EXTENSION)){

				final Project projectData = Project.getProjectData(scriptFile, null, null);
				final GeneratorReport report = new GeneratorReport();
				final Lexer lexer = new Lexer(projectData, report, AtsScript.DEFAULT_CHARSET);
				lexer.loadScript(scriptFile);
			}
		}
	}
}