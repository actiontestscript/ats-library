/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

package com.ats.script.actions;

import java.util.function.Predicate;

import com.ats.executor.channels.Channel;
import com.ats.generator.objects.BoundData;
import com.ats.script.Script;
import com.ats.script.actions.condition.ExecuteOptions;
import com.ats.tools.logger.ExecutionLogger;
import com.google.gson.JsonObject;

public class ActionWindowResize extends ActionWindow {

	private static final String SCRIPT_RESIZE_LABEL = SCRIPT_LABEL + "resize";
	public static final Predicate<String> PREDICATE = g -> SCRIPT_RESIZE_LABEL.equals(g);

	private static final String X = "x";
	private static final String Y = "y";
	private static final String WIDTH = "width";
	private static final String HEIGHT = "height";

	private BoundData x;
	private BoundData y;
	private BoundData width;
	private BoundData height;

	public ActionWindowResize() {}

	public ActionWindowResize(Script script, ExecuteOptions options, String size) {
		super(script, options);
		String[] sizeData = size.split(",");

		for(String data : sizeData) {
			String[] dataValue = data.split("=");
			if(dataValue.length == 2) {
				switch (dataValue[0].trim().toLowerCase()) {
				case X:
					x = getBoundData(dataValue[1].trim(), false);
					break;
				case Y:
					y = getBoundData(dataValue[1].trim(), false);
					break;
				case WIDTH:
					width = getBoundData(dataValue[1].trim(), true);
					break;
				case HEIGHT:
					height = getBoundData(dataValue[1].trim(), true);
					break;
				}
			}
		}
	}

	private BoundData getBoundData(String data, boolean absolute) {
		try {
			return new BoundData(Integer.parseInt(data), absolute);
		}catch(NumberFormatException ex) {
			return null;
		}
	}

	public ActionWindowResize(Script script, ExecuteOptions options, Integer x, Integer y, Integer width, Integer height) {
		super(script, options);
		setX(getBoundDataValue(x, false));
		setY(getBoundDataValue(y, false));
		setWidth(getBoundDataValue(width, true));
		setHeight(getBoundDataValue(height, true));
	}

	private BoundData getBoundDataValue(Integer value, boolean absolute) {
		if(value != null) {
			return new BoundData(value.intValue(), absolute);
		}
		return null;
	}

	private String getBoundDataJavaCode(BoundData value) {
		if(value != null) {
			return String.valueOf(value.getValue());
		}
		return null;
	}

	@Override
	public StringBuilder getJavaCode() {
		StringBuilder codeBuilder = super.getJavaCode();
		codeBuilder.append(getBoundDataJavaCode(x))
		.append(", ").append(getBoundDataJavaCode(y))
		.append(", ").append(getBoundDataJavaCode(width))
		.append(", ").append(getBoundDataJavaCode(height))
		.append(")");
		return codeBuilder;
	}

	@Override
	public String exec(Channel channel, ExecutionLogger logger) {
		return channel.updateWindowBound(getStatus(), x, y, width, height);
	}

	@Override
	public StringBuilder getActionLogs(String scriptName, int scriptLine, JsonObject data) {

		if(x != null) {
			data.addProperty("x", x.getValue());
		}

		if(y != null) {
			data.addProperty("y", y.getValue());
		}

		if(width != null) {
			data.addProperty("width", width.getValue());
		}

		if(height != null) {
			data.addProperty("height", height.getValue());
		}

		data.addProperty("duration", status.getDuration());
		return super.getActionLogs(scriptName, scriptLine, data);
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public BoundData getX() {
		return x;
	}

	public void setX(BoundData x) {
		this.x = x;
	}

	public BoundData getY() {
		return y;
	}

	public void setY(BoundData y) {
		this.y = y;
	}

	public BoundData getWidth() {
		return width;
	}

	public void setWidth(BoundData width) {
		this.width = width;
	}

	public BoundData getHeight() {
		return height;
	}

	public void setHeight(BoundData height) {
		this.height = height;
	}

	public static StringBuilder getAtsCodeStr(String subFolder) {
		return new StringBuilder().append(SCRIPT_LABEL).append(ExecutionLogger.RIGHT_ARROW_LOG);
	}
}