/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.script.actions.performance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.text.StringEscapeUtils;

import com.ats.executor.ActionTestScript;
import com.ats.script.Script;
import com.ats.script.actions.condition.ExecuteOptions;

public class ActionPerformanceStart extends ActionPerformance {

	public static final String SCRIPT_LABEL = ActionPerformance.SCRIPT_PERFORMANCE_LABEL + "-start";
	public static final Predicate<String> PREDICATE = g -> SCRIPT_LABEL.equals(g);

	private long sendBandWidth = 0L;
	private long receiveBandWidth = 0L;
	private int latency = 0;
	private int trafficIdle = 0;

	private List<String> whiteList = null;
	private String filters = "";

	public ActionPerformanceStart() {}

	public ActionPerformanceStart(Script script, ExecuteOptions options, ArrayList<String> dataArray) {
		super(script, options);
		setWhiteList(dataArray.stream().map(String :: trim).collect(Collectors.toList()));
		options.updateBandWidth(this);
	}

	public ActionPerformanceStart(Script script, ExecuteOptions options, int iddle, int ltcy, long sbw, long rbw, String[] whiteList) {
		super(script, options);
		setTrafficIdle(iddle);
		setSendBandWidth(sbw);
		setReceiveBandWidth(rbw);
		setWhiteList(Arrays.asList(whiteList));
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	// Code Generator
	//---------------------------------------------------------------------------------------------------------------------------------

	@Override
	public StringBuilder getJavaCode() {
		final ArrayList<String> list = new ArrayList<>();
		for(String s : whiteList) {
			list.add("\"" + StringEscapeUtils.escapeJava(s) + "\"");
		}
		return super.getJavaCode().append(trafficIdle).append(", ").append(latency).append(", ").append(sendBandWidth).append("L, ").append(receiveBandWidth).append("L, new String[]{").append(String.join(", ", list)).append("})");
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	// Execution
	//---------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void execute(ActionTestScript ts, String testName, int testLine, int tryNum) {
		super.execute(ts, testName, testLine, tryNum);
		getCurrentChannel().startHarServer(status, whiteList, trafficIdle, latency, sendBandWidth, receiveBandWidth);
		status.endDuration();
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public int getLatency() {
		return latency;
	}

	public void setLatency(int latency) {
		this.latency = latency;
	}

	public int getTrafficIdle() {
		return trafficIdle;
	}

	public void setTrafficIdle(int value) {
		this.trafficIdle = value;
	}

	public long getSendBandWidth() {
		return sendBandWidth;
	}

	public void setSendBandWidth(long value) {
		this.sendBandWidth = value;
	}

	public long getReceiveBandWidth() {
		return receiveBandWidth;
	}

	public void setReceiveBandWidth(long value) {
		this.receiveBandWidth = value;
	}

	public List<String> getWhiteList() {
		return whiteList;
	}

	public void setWhiteList(List<String> data) {
		this.whiteList = data;
	}

	public String getFilters() {
		return filters;
	}

	public void setFilters(String filters) {
		this.filters = filters;
	}
}