/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.script.actions;

import java.util.ArrayList;
import java.util.function.Predicate;

import com.ats.AtsSingleton;
import com.ats.executor.ActionTestScript;
import com.ats.script.Script;
import com.ats.script.actions.condition.ExecuteOptions;
import com.ats.script.AtsScript;

public class ActionButton extends ActionCondition implements IActionStoppable{

	public static final String SCRIPT_LABEL = "button";
	public static final Predicate<String> PREDICATE = g -> SCRIPT_LABEL.equals(g);

	private String buttonType = "home";

	public ActionButton() { }

	public ActionButton(Script script, ExecuteOptions options, String buttonType) {
		super(script, options);
		setButtonType(buttonType);
	}

	public ActionButton(AtsScript script, ArrayList<String> dataArray) {
		String[] data = dataArray.get(0).split("=");
		if(data.length == 2) {
			setButtonType(data[1].replace("]", "").trim());
		}
	}

	@Override
	public void execute(ActionTestScript ts, String testName, int testLine, int tryNum) {
		super.execute(ts, testName, testLine, tryNum);

		if (AtsSingleton.getInstance().getCurrentChannel() != null) {
			AtsSingleton.getInstance().getCurrentChannel().buttonClick(status, getButtonType());
		}

		status.endAction();
		ts.getRecorder().update(status.getCode(), status.getDuration(), getButtonType());
	}

	@Override
	public StringBuilder getJavaCode() {
		return super.getJavaCode().append("\"").append(buttonType).append("\")");
	}

	@Override
	public boolean isStop() {
		return true;
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public String getButtonType() {
		return buttonType;
	}

	public void setButtonType(String value) {
		this.buttonType = value;
	}

	public static StringBuilder getAtsCodeStr(String subFolder) {
		return new StringBuilder().append("callscript -> ").append(subFolder).append(".Button");
		// return new StringBuilder().append(SCRIPT_LABEL).append(ExecutionLogger.RIGHT_ARROW_LOG);
	}
}
