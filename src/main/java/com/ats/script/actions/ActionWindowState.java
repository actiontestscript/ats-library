/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.script.actions;

import java.util.function.Predicate;

import com.ats.executor.channels.Channel;
import com.ats.script.Script;
import com.ats.script.actions.condition.ExecuteOptions;
import com.ats.tools.logger.ExecutionLogger;
import com.google.gson.JsonObject;

public class ActionWindowState extends ActionWindow {

	public static final String MAXIMIZE = "maximize";
	public static final String REDUCE = "reduce";
	public static final String RESTORE = "restore";
	public static final String CLOSE = "close";

	public static final String SCRIPT_STATE_LABEL = SCRIPT_LABEL + "state";
	public static final String SCRIPT_CLOSE_LABEL = SCRIPT_LABEL + CLOSE;
	
	public static final Predicate<String> PREDICATE_STATE = g -> SCRIPT_STATE_LABEL.equals(g);
	public static final Predicate<String> PREDICATE_CLOSE = g -> SCRIPT_CLOSE_LABEL.equals(g);

	private String state = CLOSE;

	public ActionWindowState() {}

	public ActionWindowState(Script script, ExecuteOptions options, String state) {
		super(script, options);
		setState(state);
	}

	@Override
	public StringBuilder getJavaCode() {
		StringBuilder codeBuilder = super.getJavaCode();
		codeBuilder.append("\"").append(state).append("\")");
		return codeBuilder;
	}

	@Override
	public String exec(Channel channel, ExecutionLogger logger) {
		if(CLOSE.equals(state)) {
			channel.closeWindow(status);
		}else {
			channel.windowState(status, state);
			if(!REDUCE.equals(state)) {
				channel.getDriverEngine().updateDimensions();
				getCurrentChannel().sleep(80);
				getCurrentChannel().getSystemDriver().updateVisualImage(getCurrentChannel().getDimension(),false);
			}
		}

		return state;
	}

	@Override
	public StringBuilder getActionLogs(String scriptName, int scriptLine, JsonObject data) {
		data.addProperty("state", state);
		return super.getActionLogs(scriptName, scriptLine, data);
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public String getState() {
		return state;
	}

	public void setState(String state) {
		if(state != null) {
			state = state.toLowerCase();
			if(MAXIMIZE.equals(state) || REDUCE.equals(state) || CLOSE.equals(state)) {
				this.state = state;
				return;
			}
		}
		this.state = RESTORE;
	}

	public static StringBuilder getAtsCodeStr(String subFolder) {
		return new StringBuilder().append("callscript -> ").append(subFolder).append(".WindowsState");
		// return new StringBuilder().append(SCRIPT_LABEL).append(ExecutionLogger.RIGHT_ARROW_LOG);
	}
}