/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.script.actions;

import java.util.ArrayList;
import java.util.function.Predicate;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ats.executor.ActionTestScript;
import com.ats.executor.channels.Channel;
import com.ats.generator.variables.CalculatedValue;
import com.ats.recorder.IVisualRecorder;
import com.ats.recorder.VisualAction;
import com.ats.script.Script;
import com.ats.script.ScriptHeader;
import com.ats.script.AtsScript;
import com.google.gson.JsonObject;

public class ActionComment extends Action {

	public static final String SCRIPT_LABEL = "comment";
	public static final Predicate<String> PREDICATE = g -> g.startsWith(SCRIPT_LABEL);

	public static final String STEP_TYPE = "step";
	public static final String LOG_TYPE = "log";
	private static final String SCRIPT_TYPE = "script";
	private static final String SUMMARY_TYPE = "summary";

	private CalculatedValue comment;
	private String type = SCRIPT_TYPE;

	public ActionComment() {}

	public ActionComment(AtsScript script, String type, ArrayList<String> dataArray) {
		super(script);

		if(!SCRIPT_LABEL.equals(type)) {
			int minusPos = type.indexOf("-");
			if(minusPos > -1) {
				setType(type.substring(minusPos+1));
			}else if(dataArray.size() > 0){
				setType(dataArray.remove(0).trim());
			}
		}

		if(dataArray.size() > 0){
			if(dataArray.size() > 1){
				setType(dataArray.remove(0).trim());
			}
			setComment(new CalculatedValue(script, dataArray.remove(0).trim()));
		}else {
			setComment(new CalculatedValue(script));
		}
	}

	public ActionComment(AtsScript script, String data) {
		super(script);
		setType(SCRIPT_TYPE);
		setComment(new CalculatedValue(script, data));
	}

	public ActionComment(Script script, String type, CalculatedValue value) {
		super(script);
		setType(type);
		setComment(value);
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	// Code Generator
	//---------------------------------------------------------------------------------------------------------------------------------

	@Override
	public StringBuilder getActionLogs(String scriptName, int scriptLine, JsonObject data) {
		data.addProperty("type", type);
		data.addProperty("value", comment.getCalculated().replaceAll("##\\[.*\\]", ""));//remove AZD tags
		return super.getActionLogs(scriptName, scriptLine, data);
	}

	@Override
	public StringBuilder getJavaCode() {
		StringBuilder codeBuilder = super.getJavaCode();
		codeBuilder.append("\"").append(getType()).append("\", ").append(comment.getJavaCode()).append(")");
		return codeBuilder;
	}

	@Override
	public boolean isScriptComment() {
		return SCRIPT_TYPE.equals(type);
	}

	@Override
	public ArrayList<String> getKeywords() {
		ArrayList<String> keywords = super.getKeywords();
		keywords.add(comment.getKeywords());
		return keywords;
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------------------------------------------

	@Override
	public Element getXmlElement(Document document, ScriptHeader header, int index, String errorText) {
		Element e = super.getXmlElement(document, header, index, errorText);
		e.appendChild(document.createElement("value")).setTextContent(getType());
		e.appendChild(document.createElement("data")).setTextContent(comment.getCalculated());
		return e;
	}

	@Override
	public VisualAction getVisualAction() {
		VisualAction va = super.getVisualAction();
		va.setValue(getType());
		va.setData(comment.getCalculated());
		return va;
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void execute(ActionTestScript ts, String testName, int testLine, int tryNum) {

		super.execute(ts, testName, testLine, tryNum);
		status.setNoError();
		status.endDuration();

		if (STEP_TYPE.equals(type)) {
			getCurrentChannel().addStepComment(this, ts.getRecorder(), type, comment.getCalculated());
		} else {
			if(LOG_TYPE.equals(type)) {
				ts.getTopScript().getLogger().sendComment(comment.getCalculated().replaceAll("<br>", "\n"));
			}else if(SUMMARY_TYPE.equals(type)) {
				ts.addSummary(this);
			}
		}
	}

	@Override
	public void initAction(Channel channel, String testNameAndLine, IVisualRecorder recorder, String testName, int testLine) {
		if (STEP_TYPE.equals(type)) {
			super.initAction(channel, testNameAndLine, recorder, testName, testLine);
		}
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public CalculatedValue getComment() {
		return comment;
	}

	public void setComment(CalculatedValue comment) {
		this.comment = comment;
	}

	public String getType() {
		return type;
	}

	public void setType(String value) {
		if(LOG_TYPE.equals(value) || STEP_TYPE.equals(value) || SUMMARY_TYPE.equals(value)){
			this.type = value;
		}else {
			this.type = SCRIPT_TYPE;
		}
	}
}