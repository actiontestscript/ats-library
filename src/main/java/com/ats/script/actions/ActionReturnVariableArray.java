package com.ats.script.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import com.ats.generator.variables.Variable;
import com.ats.script.Script;
import com.ats.script.actions.condition.ExecuteOptions;

public abstract class ActionReturnVariableArray extends ActionCondition {

	private ArrayList<Variable> variables;

	public ActionReturnVariableArray() { }

	public ActionReturnVariableArray(Script script, ExecuteOptions options) {
		super(script, options);
	}

	public ActionReturnVariableArray(Script script, ExecuteOptions options, ArrayList<Variable> variables) {
		super(script, options);
		setVariables(variables);
	}

	public ActionReturnVariableArray(Script script, ExecuteOptions options, Variable variable) {
		super(script, options);
		setVariables(new ArrayList<>(Collections.singletonList(variable)));
	}

	public ActionReturnVariableArray(Script script, ExecuteOptions options, Variable[] variables) {
		super(script, options);
		setVariables(new ArrayList<>(Arrays.asList(variables)));
	}

	@Override
	public ArrayList<String> getKeywords() {
		final ArrayList<String> keywords = super.getKeywords();
		if(variables != null) {
			for (Variable vr : variables) {
				keywords.addAll(vr.getKeywords());
			}
		}
		return keywords;
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public ArrayList<Variable> getVariables() {
		return variables;
	}

	public void setVariables(ArrayList<Variable> variables) {
		this.variables = variables;
	}

}
