/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.script.actions;

import com.ats.executor.ActionTestScript;
import com.ats.script.Script;
import com.ats.script.actions.condition.ExecuteOptions;
import com.google.gson.JsonObject;

public class ActionChannelExist extends ActionChannel{

	public ActionChannelExist() {}

	public ActionChannelExist(Script script, ExecuteOptions options, String name) {
		super(script, options, name);
	}

	protected boolean isEmptyChannel = true;

	@Override
	public void execute(ActionTestScript ts, String testName, int testLine, int tryNum) {
		super.execute(ts, testName, testLine, tryNum);
		ts.getRecorder().update(getName());
	}

	@Override
	protected JsonObject getActionLogsData() {
		final JsonObject logs = super.getActionLogsData();
		if(isEmptyChannel) {
			logs.addProperty("warning", "channel does not exist");
		}
		return logs;
	}

	public static StringBuilder getAtsCodeStr(String subFolder) {
		return new StringBuilder().append("callscript -> ").append(subFolder).append(".ChannelExist");
		// return new StringBuilder().append(SCRIPT_LABEL).append(ExecutionLogger.RIGHT_ARROW_LOG);
	}
}