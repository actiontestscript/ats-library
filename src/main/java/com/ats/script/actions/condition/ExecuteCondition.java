package com.ats.script.actions.condition;

import com.ats.executor.ActionTestScript;
import com.ats.generator.variables.CalculatedValue;
import com.ats.generator.variables.transform.code.JavaScriptCodeEval;
import com.ats.script.Script;
import com.google.gson.JsonObject;

public class ExecuteCondition implements IExecuteCondition {

	private CalculatedValue code;
	
	public ExecuteCondition() {}
	
	public ExecuteCondition(Script script, String code) {
		this.code = new CalculatedValue(script, code);
	}
	
	public ExecuteCondition(CalculatedValue code) {
		this.code = code;
	}

	public void updateLegacy() {
		
		final String codeData = code.getData();

		String separator = "==";
		String[] data = codeData.split(separator);
		
		if(data.length != 2) {
			separator = "!=";
			data = codeData.split(separator);
		}
		
		if(data.length == 2) {
			String varName = data[0].trim();
			if(code.getScript().getVariable(varName) != null) {
				code.updateData("'$var(" + varName + ")' " + separator + " '" + data[1].trim() + "'");
			}
		}
	}

	public StringBuilder getJavaCode() {
		StringBuilder sb = 
				new StringBuilder(ActionTestScript.JAVA_CONDITION_FUNCTION)
				.append("(")
				.append(code.getJavaCode())
				.append("), ");
		
		return sb;
	}

	@Override
	public JsonObject getLog(JsonObject jsonObject) {

		return jsonObject;
	}

	//-------------------------------------------------------------------------------------------------------------
	// Getters and setters
	//-------------------------------------------------------------------------------------------------------------
	
	@Override
	public boolean isPassed() {
		final String eval = new JavaScriptCodeEval(code.getCalculated()).eval();
		return "true".equals(eval);
	}

	@Override
	public void setPassed(boolean value) {
	}
	
	public void setCode(CalculatedValue code) {
		this.code = code;
	}
	
	public CalculatedValue getCode() {
		return code;
	}
}