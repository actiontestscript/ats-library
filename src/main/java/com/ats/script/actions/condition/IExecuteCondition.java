package com.ats.script.actions.condition;

import com.google.gson.JsonObject;

public interface IExecuteCondition {
	void updateLegacy();
	StringBuilder getJavaCode();
	JsonObject getLog(JsonObject jsonObject);
	
	boolean isPassed();
	void setPassed(boolean value);
}
