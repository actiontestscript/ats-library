package com.ats.script.actions.condition;

import com.ats.executor.ActionTestScript;
import com.google.gson.JsonObject;

public class ExecuteConditionNone implements IExecuteCondition {

	public ExecuteConditionNone() {}

	@Override
	public void updateLegacy() {}

	@Override
	public StringBuilder getJavaCode() {
		return new StringBuilder(ActionTestScript.JAVA_CONDITION_FUNCTION).append("(), ");
	}
	
	@Override
	public JsonObject getLog(JsonObject jsonObject) {
		return jsonObject;
	}

	@Override
	public boolean isPassed() {
		return true;
	}

	@Override
	public void setPassed(boolean value) {}
}