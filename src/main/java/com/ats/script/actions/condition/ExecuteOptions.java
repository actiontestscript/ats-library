package com.ats.script.actions.condition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import com.ats.executor.channels.Channel;
import com.ats.generator.objects.Cartesian;
import com.ats.generator.objects.MouseDirection;
import com.ats.generator.objects.MouseDirectionData;
import com.ats.generator.objects.TryAndDelay;
import com.ats.generator.variables.CalculatedValue;
import com.ats.script.AtsScript;
import com.ats.script.Script;
import com.ats.script.actions.ActionCallscript;
import com.ats.script.actions.ActionChannelClose;
import com.ats.script.actions.ActionChannelStart;
import com.ats.script.actions.ActionExecute;
import com.ats.script.actions.ActionMouseKey;
import com.ats.script.actions.neoload.ActionNeoload;
import com.ats.script.actions.performance.ActionPerformance;
import com.ats.script.actions.performance.ActionPerformanceStart;
import com.ats.tools.Utils;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

public class ExecuteOptions {

	private static final String TRY = "try";
	private static final String DELAY = "delay";
	private static final String WAITCHAR = "waitchar";
	private static final String CACHE = "cache";
	private static final String PORT = "port";
	private static final String SCRIPT = "script";

	public static final String ATTACH = "attach";
	public static final String USE_COOKIE = "use-cookie";

	public static final String UPLOAD = "upload";
	public static final String DOWNLOAD = "download";
	public static final String IDLE = "idle";
	public static final String LATENCY = "latency";

	private static final String APPEND_OPTION = "append";
	private static final String REFRESH_AFTER_SWITCH = "refresh";
	private static final String KEEP_SELECT = "keep-select";

	public static final String RANDOM_LABEL = "rnd";
	private static final String RANDOM2_LABEL = "random";
	public static final String SUITE_LABEL = "suite";

	private static final Pattern IF_PATTERN = Pattern.compile("if\\((.*)\\)");

	private static final String REPLACE = "[ =\\(\\)]";

	private List<String> options = Collections.emptyList();
	private IExecuteCondition condition = new ExecuteConditionNone();

	public ExecuteOptions() {}
		
	public ExecuteOptions(CalculatedValue calc) {
		condition = new ExecuteCondition(calc);
	}

	public ExecuteOptions(Script script, String[] opt) {
		setOptions(script, Arrays.asList(opt));
	}

	private void setOptions(Script script, List<String> value) {
		final List<String> list = new ArrayList<String>();
		for(String o : value) {
			final Matcher ifMatch = IF_PATTERN.matcher(o);
			if(ifMatch.find()) {
				condition = new ExecuteCondition(script, ifMatch.group(1));
			}else {
				list.add(o);
			}
		}
		this.options = ImmutableList.copyOf(list);
	}

	public String init(Script script, String actionType, Pattern pattern) {
		final Matcher matcher = pattern.matcher(actionType);
		if (matcher.find()) {
			actionType = matcher.group(1).trim().toLowerCase();

			final String data = matcher.group(2).trim();
			setOptions(script, Arrays.stream(data.split(",")).map(String :: trim).collect(Collectors.toUnmodifiableList()));
		}

		return actionType;
	}
	
	public ExecuteOptions add(String[] value) {
		options = Stream.concat(options.stream(), Arrays.asList(value).stream()).collect(Collectors.toUnmodifiableList());
		return this;
	}

	public int getPort() {

		for(String o : options) {
			if(o.startsWith(PORT)) {
				return Utils.string2Int(replaceData(4, o));
			}
		}

		for(String o : options) {
			if(StringUtils.isNumeric(o)) {
				return Utils.string2Int(o, -1);
			}
		}

		return -1;
	}

	public boolean hasCache() {
		return options.contains(CACHE);
	}

	public boolean hasKeepSelect() {
		return options.contains(KEEP_SELECT);
	}

	public boolean hasRefresh() {
		return options.contains(REFRESH_AFTER_SWITCH);
	}

	public boolean hasScript() {
		return options.contains(SCRIPT);
	}

	public boolean hasAppend() {
		return options.contains(APPEND_OPTION);
	}

	public boolean hasNoStopLabel() {
		return options.contains(ActionChannelClose.NO_STOP_LABEL);
	}

	public int getWaitChar() {
		for(String o : options) {
			if(o.startsWith(WAITCHAR)) {
				return Utils.string2Int(replaceData(8, o));
			}
		}
		return -1;
	}

	public void updateMouseDirections(MouseDirection direction, Script script) {

		for(String o : options) {
			if(Cartesian.RIGHT.isPresent(o)) {

				direction.setHorizontalPos(
						new MouseDirectionData(
								Cartesian.RIGHT.toString(), 
								new CalculatedValue(
										script, 
										replaceData(5, o))));

			}else if(Cartesian.LEFT.isPresent(o)) {

				direction.setHorizontalPos(
						new MouseDirectionData(
								Cartesian.LEFT.toString(), 
								new CalculatedValue(
										script, 
										replaceData(4, o))));

			}else if(Cartesian.CENTER.isPresent(o)) {

				direction.setHorizontalPos(
						new MouseDirectionData(
								Cartesian.CENTER.toString(), 
								new CalculatedValue(
										script, 
										replaceData(6, o))));

			}else if(Cartesian.TOP.isPresent(o)) {

				direction.setVerticalPos(
						new MouseDirectionData(
								Cartesian.TOP.toString(), 
								new CalculatedValue(
										script, 
										replaceData(3, o))));

			}else if(Cartesian.BOTTOM.isPresent(o)) {

				direction.setVerticalPos(
						new MouseDirectionData(
								Cartesian.BOTTOM.toString(), 
								new CalculatedValue(
										script, 
										replaceData(6, o))));


			}else if(Cartesian.MIDDLE.isPresent(o)) {

				direction.setVerticalPos(
						new MouseDirectionData(
								Cartesian.MIDDLE.toString(), 
								new CalculatedValue(
										script, 
										replaceData(6, o))));

			}
		}
	}

	private String replaceData(int l, String s) {
		return s.substring(l).replaceAll(REPLACE,"");
	}

	public void updateMousKeys(ActionMouseKey action) {
		for(String o : options) {
			action.setKey(o);
		}
	}

	public IExecuteCondition getCondition(Script script) {
		return condition;
	}

	public TryAndDelay getTryAndDelay() {

		int tryExec = 0;
		int delayExec = 0;
		for(String o : options) {
			if(o.startsWith(TRY)) {
				tryExec = Utils.string2Int(replaceData(3, o));
			}else if(o.startsWith(DELAY)) {
				delayExec = Utils.string2Int(replaceData(5, o));
			}
		}

		return new TryAndDelay(tryExec, delayExec);
	}

	public int getStopPolicy() {

		for(String o : options) {
			if(ActionExecute.NO_FAIL_LABEL.equalsIgnoreCase(o)) {
				return ActionExecute.TEST_CONTINUE_PASS;
			}else if(ActionExecute.TEST_FAIL_LABEL.equalsIgnoreCase(o)) {
				return ActionExecute.TEST_CONTINUE_FAIL;
			}
		}

		return ActionExecute.TEST_STOP_FAIL;
	}

	public void updateChannelStart(ActionChannelStart action, List<String> dataArray, Script script) {
		checkOption(action, dataArray, script);
		checkOption(action, options, script);
	}
	
	private void checkOption(ActionChannelStart action, List<String> data, Script script) {
		for(String o : data) {
			o = o.trim();
			if(ActionNeoload.SCRIPT_NEOLOAD_LABEL.equalsIgnoreCase(o)) {
				action.setPerformance(ActionChannelStart.NEOLOAD);
			}else if(ATTACH.equalsIgnoreCase(o)) {
				action.setAttach(true);
			}else if(USE_COOKIE.equalsIgnoreCase(o)) {
				action.setUseCookie(true);
			}else if(ActionPerformance.SCRIPT_PERFORMANCE_LABEL.equalsIgnoreCase(o)) {
				action.setPerformance(ActionChannelStart.PERF);
			}else if(Channel.BASIC_AUTHENTICATION.equalsIgnoreCase(o) || Channel.BEARER_AUTHENTICATION.equalsIgnoreCase(o)){
				action.setAuthentication(o);
			}else if(!Strings.isNullOrEmpty(action.getAuthentication()) && Strings.isNullOrEmpty(action.getAuthenticationValue())) {
				action.setAuthenticationValue(o);
			}else {
				action.getArguments().add(new CalculatedValue(script, o));
			}
		}
	}

	public void updateBandWidth(ActionPerformanceStart action) {
		for (String o : options) {
			if(o.startsWith(UPLOAD)) {
				action.setSendBandWidth(Utils.string2Long(replaceData(6, o)));
			}else if(o.startsWith(DOWNLOAD)) {
				action.setReceiveBandWidth(Utils.string2Long(replaceData(8, o)));
			}else if(o.startsWith(LATENCY)) {
				action.setLatency(Utils.string2Int(replaceData(7, o)));
			}else if(o.startsWith(IDLE)) {
				action.setTrafficIdle(Utils.string2Int(replaceData(4, o)));
			}
		}
	}

	public void updateCallScript(ActionCallscript action, List<String> dataArray, AtsScript script) {

		if (dataArray != null) {
			dataArray.addAll(options);
		}else {
			dataArray = options;
		}

		for (String o : dataArray) {
			o = o.trim();
			if (RANDOM_LABEL.equalsIgnoreCase(o) || RANDOM2_LABEL.equalsIgnoreCase(o)) {
				action.setRandom(true);
			} else if (SUITE_LABEL.equalsIgnoreCase(o)) {
				action.setSuite(true);
			} else if(o.startsWith(ActionCallscript.RANGE)) {
				action.setRange(replaceData(5, o));
			}else if(o.startsWith(ActionCallscript.LOOP)) {
				action.setLoop(Utils.string2Int(replaceData(4, o)));
			}else if(o.startsWith(ActionCallscript.TABLE)) {
				action.updateTableSplit(replaceData(5, o));
			}
		}
		
		action.callscriptLegacy();
	}

	public void checkLegacy(AtsScript script) {
		if(options != null && options.size() == 1 &&  condition instanceof ExecuteConditionNone) {
			final String data = options.get(0);
			
			String[] arr = data.split("<>");
			String operator = "!=";
			if(arr.length != 2) {
				operator = "==";
				arr = data.split("=");
			}
			
			if(arr.length == 2) {
				condition = new ExecuteCondition(script, "'$var(" + arr[0].trim() + ")' " + operator + " '" + arr[1].trim() + "'");
			}
		}
	}
}