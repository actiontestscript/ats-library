/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.script.actions;

import java.util.ArrayList;
import java.util.function.Predicate;

import com.ats.element.SearchedElement;
import com.ats.executor.ActionTestScript;
import com.ats.generator.objects.MouseDirection;
import com.ats.generator.variables.CalculatedProperty;
import com.ats.script.Script;
import com.ats.script.actions.condition.ExecuteOptions;
import com.ats.script.AtsScript;
import com.google.gson.JsonObject;

public class ActionSelect extends ActionExecuteElement {

	public static final String SCRIPT_LABEL = "select";
	public static final Predicate<String> PREDICATE = g -> g.contains(SCRIPT_LABEL);

	public static final String SELECT_TEXT = "text";
	public static final String SELECT_VALUE = "value";
	public static final String SELECT_INDEX = "index";

	private CalculatedProperty selectValue;
	private boolean keepSelect = false;

	public ActionSelect() {}

	public ActionSelect(AtsScript script, ExecuteOptions options, String data, int stopPolicy, ArrayList<String> objectArray) {
		super(script, options, stopPolicy, objectArray);

		setSelectValue(new CalculatedProperty(script, data));
		setKeepSelect(options.hasKeepSelect());
	}

	public ActionSelect(Script script, ExecuteOptions options, int stopPolicy, int maxTry, int delay, SearchedElement element, CalculatedProperty selectValue) {
		super(script, options, stopPolicy, maxTry, delay, element);
		setSelectValue(selectValue);
	}

	public ActionSelect(Script script, ExecuteOptions options, int stopPolicy, int maxTry, int delay, SearchedElement element, CalculatedProperty selectValue, boolean keepSelect) {
		super(script, options, stopPolicy, maxTry, delay, element);
		setSelectValue(selectValue);
		setKeepSelect(keepSelect);
	}

	public ActionSelect(Script script, ExecuteOptions options, int stopPolicy, int maxTry, int delay, SearchedElement element, boolean keepSelect) {
		super(script, options, stopPolicy, maxTry, delay, element);
		setKeepSelect(keepSelect);
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	// Code Generator
	//---------------------------------------------------------------------------------------------------------------------------------

	@Override
	public StringBuilder getJavaCode() {
		StringBuilder codeBuilder =
				super.getJavaCode()
				.append(", ")
				.append(selectValue.getJavaCode());

		if(keepSelect) {
			codeBuilder.append(", true");
		}

		codeBuilder.append(")");
		return codeBuilder;
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void terminateExecution(ActionTestScript ts) {

		super.terminateExecution(ts);

		if(status.isPassed()) {
			getTestElement().over(status, new MouseDirection(), false, 0, 0);

			ts.getRecorder().updateScreen(true);

			ts.getRecorder().updateScreen(status);

			getTestElement().select(status, selectValue, keepSelect);
		}

		status.endAction();
		ts.getRecorder().update(selectValue.getValue().getCalculated(),selectValue.getValue().getCalculated());

		getCurrentChannel().sleep(50);
		ts.getRecorder().updateScreen(false);

		ts.getRecorder().updateScreen(status);

	}

	@Override
	public StringBuilder getActionLogs(String scriptName, int scriptLine, JsonObject data) {
		data.addProperty("select", selectValue.getValue().getCalculated());
		data.addProperty("regexp", selectValue.isRegexp());
		data.addProperty("name", selectValue.getName());
		return super.getActionLogs(scriptName, scriptLine, data);
	}

	@Override
	public ArrayList<String> getKeywords() {
		ArrayList<String> keywords = super.getKeywords();
		keywords.addAll(selectValue.getKeywords());
		return keywords;
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public CalculatedProperty getSelectValue() {
		return selectValue;
	}

	public void setSelectValue(CalculatedProperty value) {
		this.selectValue = value;
	}

	public boolean isKeepSelect() {
		return keepSelect;
	}

	public void setKeepSelect(boolean value) {
		this.keepSelect = value;
	}

	public static StringBuilder getAtsCodeStr(String subFolder) {
		return new StringBuilder().append("callscript -> ").append(subFolder).append(".Select");
		// return new StringBuilder().append(SCRIPT_LABEL).append(ExecutionLogger.RIGHT_ARROW_LOG);
	}
}