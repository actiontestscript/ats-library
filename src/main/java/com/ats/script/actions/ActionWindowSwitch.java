/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.script.actions;

import java.util.function.Predicate;

import com.ats.executor.channels.Channel;
import com.ats.generator.variables.CalculatedValue;
import com.ats.script.Script;
import com.ats.script.actions.condition.ExecuteOptions;
import com.ats.tools.logger.ExecutionLogger;
import com.google.gson.JsonObject;

public class ActionWindowSwitch extends ActionWindow implements IActionStoppable{

	public static final String SCRIPT_SWITCH_LABEL = SCRIPT_LABEL + "switch";
	public static final Predicate<String> PREDICATE = g -> g.startsWith(SCRIPT_SWITCH_LABEL);

	public static final String SWITCH_INDEX = "index";
	public static final String SWITCH_NAME = "name";
	public static final String SWITCH_URL = "url";

	private CalculatedValue value = new CalculatedValue(0);

	private int tries = 0;
	private int delay = 0;
	private boolean refresh = false;
	private String type = SWITCH_INDEX;
	
	private boolean regexp;

	public ActionWindowSwitch() {}

	public ActionWindowSwitch(Script script, ExecuteOptions options, CalculatedValue value, String type, boolean regexp) {
		super(script, options);

		setType(type);
		setValue(value);
		setRegexp(regexp);
		setTries(options.getTryAndDelay().getMaxTry());
		setDelay(options.getTryAndDelay().getDelay());
		setRefresh(options.hasRefresh());
	}

	public ActionWindowSwitch(Script script, ExecuteOptions options, CalculatedValue value, String type, boolean regexp, int tries, int delay, boolean refresh) {
		super(script, options);
		setValue(value);
		setType(type);
		setRegexp(regexp);
		setTries(tries);
		setDelay(delay);
		setRefresh(refresh);
	}

	@Override
	public StringBuilder getJavaCode() {
		return super.getJavaCode()
				.append(value.getJavaCode())
				.append(", ")
				.append("\"")
				.append(type)
				.append("\", ")
				.append(regexp)
				.append(", ")
				.append(tries)
				.append(", ")
				.append(delay)
				.append(", ")
				.append(refresh)
				.append(")");
	}

	@Override
	public String exec(Channel channel, ExecutionLogger logger) {
		channel.switchWindow(status, type, regexp, value, tries, delay, refresh);
		return value.getCalculated();
	}

	@Override
	public StringBuilder getActionLogs(String scriptName, int scriptLine, JsonObject data) {
		data.addProperty(type, value.getCalculated());
		data.addProperty("tries", tries);
		data.addProperty("refresh", refresh);
		return super.getActionLogs(scriptName, scriptLine, data);
	}

	@Override
	public boolean isStop() {
		return true;
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public int getDelay() {
		return delay;
	}

	public void setDelay(int delay) {
		this.delay = delay;
	}

	public boolean isRefresh() {
		return refresh;
	}

	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}

	public CalculatedValue getValue() {
		return value;
	}

	public void setValue(CalculatedValue value) {
		this.value = value;
	}

	public int getTries() {
		return tries;
	}

	public void setTries(int tries) {
		this.tries = tries;
	}

	public static StringBuilder getAtsCodeStr() {
		return new StringBuilder().append(SCRIPT_LABEL).append(ExecutionLogger.RIGHT_ARROW_LOG);
	}

	public String getType() {
		return type;
	}

	public void setType(String value) {
		if(SWITCH_INDEX.equalsIgnoreCase(value) || SWITCH_NAME.equalsIgnoreCase(value) || SWITCH_URL.equalsIgnoreCase(value)) {
			value = value.toLowerCase();
		}else {
			value = SWITCH_INDEX;
		}
		this.type = value;
	}
	
	public boolean isRegexp() {
		return regexp;
	}

	public void setRegexp(boolean value) {
		this.regexp = value;
	}
}