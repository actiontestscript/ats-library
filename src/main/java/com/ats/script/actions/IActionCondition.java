package com.ats.script.actions;

import com.ats.script.actions.condition.ExecuteCondition;

public interface IActionCondition {
	public void setCondition(ExecuteCondition value);
	public ExecuteCondition getCondition();
}