package com.ats.script.actions;

import com.ats.script.Script;
import com.ats.script.actions.condition.ExecuteConditionNone;
import com.ats.script.actions.condition.ExecuteOptions;
import com.ats.script.actions.condition.IExecuteCondition;
import com.google.gson.JsonObject;

public class ActionCondition extends Action {

	private IExecuteCondition condition = new ExecuteConditionNone();
	
	public ActionCondition() {}

	public ActionCondition(Script script, ExecuteOptions options) {
		super(script);
		this.condition = options.getCondition(script);
	}

	@Override
	public StringBuilder getJavaCode() {
		return super.getJavaCode().append(condition.getJavaCode());
	}
	
	public JsonObject getConditionLogs() {
		return condition.getLog(new JsonObject());
	}
	
	public void callscriptLegacy() {
		condition.updateLegacy();
	}
	
	//-------------------------------------------------------------------------------------------------------------
	// Getters and setters
	//-------------------------------------------------------------------------------------------------------------
	
	public IExecuteCondition getCondition() {
		return condition;
	}

	public void setCondition(IExecuteCondition condition) {
		if(condition == null) {
			this.condition = new ExecuteConditionNone();
		}else {
			this.condition = condition;
		}
	}
}