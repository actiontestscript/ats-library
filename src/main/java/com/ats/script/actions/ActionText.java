/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.script.actions;

import java.util.ArrayList;
import java.util.function.Predicate;

import com.ats.element.SearchedElement;
import com.ats.executor.ActionStatus;
import com.ats.executor.ActionTestScript;
import com.ats.executor.SendTextException;
import com.ats.generator.variables.CalculatedValue;
import com.ats.script.AtsScript;
import com.ats.script.Script;
import com.ats.script.actions.condition.ExecuteOptions;
import com.google.gson.JsonObject;

public class ActionText extends ActionExecuteElement {

	public static final String SCRIPT_LABEL = "keyboard";
	public static final Predicate<String> PREDICATE = g -> SCRIPT_LABEL.equals(g);



	private CalculatedValue text;

	private int waitChar = 0;

	public ActionText() {}

	public ActionText(AtsScript script, ExecuteOptions options, int stopPolicy, String text, ArrayList<String> objectArray) {
		super(script, options, stopPolicy, objectArray);
		setText(new CalculatedValue(script, text));
		setWaitChar(options.getWaitChar());
	}

	public ActionText(Script script, ExecuteOptions options, int stopPolicy, int maxTry, int delay, SearchedElement element, CalculatedValue text) {
		super(script, options, stopPolicy, maxTry, delay, element);
		setText(text);
	}

	public ActionText(Script script, ExecuteOptions options, int stopPolicy, int maxTry, int delay, SearchedElement element, CalculatedValue text, int wait) {
		super(script, options, stopPolicy, maxTry, delay, element);
		setText(text);
		setWaitChar(wait);
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	// Code Generator
	//---------------------------------------------------------------------------------------------------------------------------------

	@Override
	public StringBuilder getJavaCode() {
		if(waitChar > 0) {
			return super.getJavaCode().append(", ").append(text.getJavaCode()).append(", ").append(waitChar).append(")");
		}else {
			return super.getJavaCode().append(", ").append(text.getJavaCode()).append(")");
		}
	}

	@Override
	public ArrayList<String> getKeywords() {
		ArrayList<String> keywords = super.getKeywords();
		keywords.add(text.getKeywords());
		return keywords;
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void execute(ActionTestScript ts, String testName, int testLine, int tryNum) {
		int maxTry = 0;
		while(maxTry < 3) {
			try {
				super.execute(ts, testName, testLine, tryNum + maxTry);
				return;
			}catch(SendTextException e) {}
			maxTry++;
		}

		status.setError(ActionStatus.ENTER_TEXT_FAIL, "send keys action fail on the element");
		terminateExecution(ts, ActionStatus.ENTER_TEXT_FAIL);
	}

	@Override
	public void terminateExecution(ActionTestScript ts) {
		super.terminateExecution(ts);
		if(status.isPassed()) {

			String safeDataValue = getTestElement().enterText(status, text, ts, waitChar);
			status.endAction();

			if (text.isCrypted()) {
				safeDataValue = CalculatedValue.CRYPTED_DATA_SHOW;
			}

			ts.getRecorder().updateTextScreen(status, safeDataValue);

		}else {

			String safeDataValue = text.getCalculated();

			if (text.isCrypted()) {
				safeDataValue = CalculatedValue.CRYPTED_DATA_SHOW;
			}

			ts.getRecorder().update(status.getCode(), status.getDuration(), safeDataValue);
		}

		ts.getRecorder().updateScreen(false);
	}

	@Override
	public StringBuilder getActionLogs(String scriptName, int scriptLine, JsonObject data) {
		data.addProperty("text", text.getCalculated().replaceAll("\"", "\\\""));
		return super.getActionLogs(scriptName, scriptLine, data);
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public CalculatedValue getText() {
		return text;
	}

	public void setText(CalculatedValue text) {
		this.text = text;
	}

	public int getWaitChar() {
		return waitChar;
	}

	public void setWaitChar(int value) {
		if(value > 5000) {
			value = 5000;
		}else if(value < 0) {
			return;
		}
		this.waitChar = value;
	}

	public static StringBuilder getAtsCodeStr(String subFolder) {
		return new StringBuilder().append("callscript -> ").append(subFolder).append(".EnterText");
		// return new StringBuilder().append(SCRIPT_LABEL).append(ExecutionLogger.RIGHT_ARROW_LOG);
	}
}