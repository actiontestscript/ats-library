/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.script.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;
import java.util.function.Predicate;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ats.AtsSingleton;
import com.ats.executor.ActionStatus;
import com.ats.executor.ActionTestScript;
import com.ats.executor.channels.Channel;
import com.ats.generator.variables.CalculatedValue;
import com.ats.script.Script;
import com.ats.script.ScriptHeader;
import com.ats.script.actions.condition.ExecuteOptions;
import com.ats.script.actions.neoload.ActionNeoload;
import com.ats.script.actions.performance.ActionPerformance;
import com.google.gson.JsonObject;

public class ActionChannelStart extends ActionChannel {

	private static final String UNKNOWN_ICON = "iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABhWlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw1AUhU9TRdGqgx1EHCJUJ7tUEcdSxSJYKG2FVh1MXvoHTRqSFBdHwbXg4M9i1cHFWVcHV0EQ/AFxdnBSdJES70sKLWK88Hgf591zeO8+QGhUmGp2RQFVs4xUPCZmc6tizyt86McgxhGRmKkn0osZeNbXPXVT3YV5lnffnzWg5E0G+ETiKNMNi3iDeHbT0jnvEwdZSVKIz4mnDLog8SPXZZffOBcdFnhm0Mik5omDxGKxg+UOZiVDJZ4hDimqRvlC1mWF8xZntVJjrXvyFwby2kqa67TGEMcSEkhChIwayqjAQph2jRQTKTqPefhHHX+SXDK5ymDkWEAVKiTHD/4Hv2drFqYjblIgBnS/2PbHBNCzCzTrtv19bNvNE8D/DFxpbX+1Acx9kl5va6EjYGgbuLhua/IecLkDjDzpkiE5kp+WUCgA72f0TTlg+BboW3Pn1jrH6QOQoVkt3wAHh8BkkbLXPd7d2zm3f3ta8/sBzblyy7AF0DoAAAAJcEhZcwAAAdgAAAHYAfpcpnIAAAAHdElNRQfoAQ0QBSpDI1zBAAAAGXRFWHRDb21tZW50AENyZWF0ZWQgd2l0aCBHSU1QV4EOFwAAA4ZJREFUSMeNll1om1UYx3/nvG/evk2afiZphmvmQHBQ6ZXCCoOC82J1ggiCNxM/diNBEEEUmQFlne7KCxW9ceJF7xSsiA2zFGSDBS8m2GlxrjO1cbb7iGsX8tW87zle5G2XNHlj/rfPc/7Px/mf5zkCH4zNLAfRKoEQx9D6CWACacSNcAxhWhvAErAAnAfWsslwqR2PaEt+ZnkSpV4GjgP7dg3SwAvQ6L4OfA98kU2GMx0DHDi7Yqta9QToGSACGI12yzAw+mM40kI387jAHeAdYDabDFdaAhw4u2Irp/oqWp8CBhtPPtwneTxuMDxsMTAU4V7JZOGGy+Ut3ZwBbAJnNHyy6gUxdyxe5k3kAFMjBq9M2uwfgIApiY9aGAGL6buKjy5Vmcu5je6DwClRD/T5bgVez78BRhu9RwKCz57qYzToeldgEI2NErR7AMiXNC/NlVja0sjmSm4Cz2ST4Ywcm1kOehcaafQoaXhuvHeX3NGSS9cVc7/UyN5V9QSCgtces8irFp1EgJMHPy0EJVolPLU0tTOv4MgD3kUJ+HbZ5fVMhTd+2ubjC1XKTt02HjMYs1vEaABPAgmJEMeapNgA6Z3TCC6vVrGAHgF/3lPky3UdSQnDVtuntA+YNr1H1IKohPNXHQ4/ZHFrS/FXSQOCmq5nHQvWozsO5MrsVdMOjprARDuLLeDLlW0Wcg4FR7PlaJSEZw8avDnVg2WA1nBxzaVY0/TLtgEmTCDuNy60hr8r929wKmry1hGbAa/n2X9dPvx5248cIG7SJUZMwQuP2oyEQAPXbrm892OV2xXd8ZwENroJEO0RHIrUqypUNO9erJJpo8892JDeVOwIV8ODMQuBQgi4lld8ta66yWtJIsRiN55Wg0yym4pB0VVnFyVap72R6wtDwJUbNW4WBP9sar77wyEk/5d8HUibINZAzwMv4itnuF52ef6HKsGhCreLJmbnClxgHsjJXGq8BJzz5rkvDoUkXx+3ST9tMz1moDqL5w5wLpsMFyVALvVIBkh587wFSkMiEmCgVzEUkhzeb1DxJ98EUjvb7f47MMxZlDuE1m/v3QlSwJWNGhdWLaJFh/TV+kv3If8AmG27MhPv/96rXecEcLrdyuwLGFj9MQr4rsyUtzLLnZf+6V8ngZPeyO1m6c97Pc909auof1t+C6H1GDANHPX5tiwCaSCXTYaL7Xj+A+GqRtjLA+pTAAAAAElFTkSuQmCC";

	private static final String SCRIPT_START_LABEL = SCRIPT_LABEL + "start";
	public static final Predicate<String> PREDICATE = SCRIPT_START_LABEL::equals;

	public static final int PERF = 1;
	public static final int NEOLOAD = 2;

	private CalculatedValue application;
	private ArrayList<CalculatedValue> arguments = new ArrayList<>();

	private int performance = 0;

	private boolean useCookie = false;

	private boolean attach = false;

	private String authentication = "";
	private String authenticationValue = "";

	public ActionChannelStart() {
		super();
	}

	public ActionChannelStart(Script script, ExecuteOptions options, String name, CalculatedValue value) {
		this(script, options, name, value, Collections.emptyList());
	}

	public ActionChannelStart(Script script, ExecuteOptions options, String name, CalculatedValue value, List<String> dataArray) {
		super(script, options, name);
		setApplication(value);

		options.updateChannelStart(this, dataArray, script);
	}

	public ActionChannelStart(Script script, ExecuteOptions options, boolean attach, boolean useCookie, String name, CalculatedValue value, String[] optionsx) {
		this(script, options.add(optionsx), name, value, Collections.emptyList());
		this.setAttach(attach);
		this.setUseCookie(useCookie);
	}

	public ActionChannelStart(Script script, ExecuteOptions options, boolean attach, boolean useCookie, String name, CalculatedValue value, String[] optionsx, CalculatedValue ...calculatedValues) {
		this(script, options.add(optionsx), name, value, Collections.emptyList());
		this.setArguments(new ArrayList<>(Arrays.asList(calculatedValues)));
		this.setAttach(attach);
		this.setUseCookie(useCookie);
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------------------------------------------

	@Override
	public Element getXmlElement(Document document, ScriptHeader header, int index, String errorText) {
		Element e = super.getXmlElement(document, header, index, errorText);

		final Element dataJson = document.createElement(DATA_JSON);
		e.appendChild(dataJson);

		Element param = document.createElement("parameter");
		param.setAttribute("type", "name");
		param.setAttribute("value", getName());
		dataJson.appendChild(param);

		param = document.createElement("parameter");
		param.setAttribute("type", "appIcon");
		param.setAttribute("value", UNKNOWN_ICON);
		dataJson.appendChild(param);

		param = document.createElement("parameter");
		param.setAttribute("type", "app");
		param.setAttribute("value", application.getCalculated());
		dataJson.appendChild(param);

		Element channel = document.createElement("channel");
		channel.setAttribute("name", getName());

		e.appendChild(channel);

		return e;
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void execute(ActionTestScript ts, String testName, int line, int tryNum) {
		this.line = line;
		if(getCondition().isPassed()) {
			AtsSingleton.getInstance().startChannel(this, ts, testName, line);
		}else {
			setStatus(new ActionStatus(testName, line));
		}
	}

	@Override
	public JsonObject getActionLogsData() {

		final JsonObject data = super.getActionLogsData();

		if(getCondition().isPassed()) {
			final Channel channel = status.getChannel();

			data.addProperty("app", application.getCalculated());
			data.addProperty("appVersion", channel.getApplicationVersion());

			if(!arguments.isEmpty()){
				ArrayList<String> appParameters = new ArrayList<>();
				for(CalculatedValue dataCalculated : arguments) {
					appParameters.add(dataCalculated.getCalculated()) ;
				}
				data.addProperty("appParameters", String.join(", ", appParameters));
			}

			data.addProperty("os", channel.getOs());
		}else {
			data.addProperty("condition", "not_validated");
		}
		return data;
	}

	public JsonObject getActionData() {
		final Channel channel = status.getChannel();
		final JsonObject data = getActionLogsData();
		data.addProperty("appIcon", Base64.getEncoder().encodeToString(channel.getIcon()));

		return data;
	}

	@Override
	public ArrayList<String> getKeywords() {
		ArrayList<String> keywords = super.getKeywords();
		keywords.add(application.getKeywords());
		return keywords;
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	// Code Generator
	//---------------------------------------------------------------------------------------------------------------------------------

	@Override
	public StringBuilder getJavaCode() {

		final StringBuilder codeBuilder =
				super.getJavaCode()
				.append(isAttach()).append(", ")
				.append(isUseCookie()).append(", ")
				.append("\"").append(getName()).append("\", ")
				.append(application.getJavaCode())
				.append(", new java.lang.String[]{");

		final ArrayList<String> options = new ArrayList<>();
		if(performance == PERF) {
			options.add(String.format("\"%s\"", ActionPerformance.SCRIPT_PERFORMANCE_LABEL));
		}else if(performance == NEOLOAD) {
			options.add(String.format("\"%s\"", ActionNeoload.SCRIPT_NEOLOAD_LABEL));
		}

		if (authentication != null && !authentication.isEmpty()) {
			options.add(String.format("\"%s\"", authentication));
			options.add(String.format("\"%s\"", authenticationValue));
		}

		codeBuilder.append(String.join(", ", options)).append("}");

		if (!arguments.isEmpty()) {
			codeBuilder.append(", ");

			final StringJoiner argumentsJoiner = new StringJoiner(", ");
			for(CalculatedValue calc : arguments) {
				argumentsJoiner.add(calc.getJavaCode());
			}
			codeBuilder.append(argumentsJoiner);
		}

		return codeBuilder.append(")");
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public CalculatedValue getApplication() {
		return application;
	}

	public void setApplication(CalculatedValue value) {
		this.application = value;
	}

	public String getAuthentication() {
		return authentication;
	}

	public void setAuthentication(String value) {
		this.authentication = value;
	}

	public String getAuthenticationValue() {
		return authenticationValue;
	}

	public void setAuthenticationValue(String value) {
		this.authenticationValue = value;
	}

	public int getPerformance() {
		return performance;
	}

	public void setPerformance(int value) {
		this.performance = value;
	}

	public ArrayList<CalculatedValue> getArguments() {
		return arguments;
	}

	public void setArguments(ArrayList<CalculatedValue> arguments) {
		this.arguments = arguments;
	}

	public boolean isAttach() {
		return attach;
	}

	public void setAttach(boolean attach) {
		this.attach = attach;
	}

	public boolean isUseCookie() {
		return useCookie;
	}

	public void setUseCookie(boolean value) {
		this.useCookie = value;
	}

	public static StringBuilder getAtsCodeStr(String subFolder) {
		return new StringBuilder().append("callscript -> ").append(subFolder).append(".ChannelStart");
		// return new StringBuilder().append(SCRIPT_LABEL).append(ExecutionLogger.RIGHT_ARROW_LOG);
	}
}