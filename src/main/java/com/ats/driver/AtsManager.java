/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.driver;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ats.executor.ActionStatus;
import com.ats.executor.ActionTestScript;
import com.ats.executor.TestBound;
import com.ats.script.Project;
import com.ats.tools.AtsClassLoader;
import com.ats.tools.Utils;
import com.ats.tools.performance.external.OctoperfApi;
import com.ats.tools.wait.IWaitGuiReady;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.base.Strings;

public class AtsManager {

	public static final String ATS_FOLDER_SIMPLE = "ats";
	public static final String ATS_FOLDER = ".actiontestscript";

	public static final String SCRIPTS_FOLDER = "ats_scripts";

	public static final String SCRIPTS_FOLDER_URL = 
			new StringBuilder(Project.ASSETS_FOLDER)
			.append("/")
			.append(SCRIPTS_FOLDER).toString();

	private static final String DRIVERS_FOLDER = "drivers";
	private static final String ATS_PROPERTIES_FILE = ".atsProperties";

	private static final int SYSTEM_DRIVER_PORT = 9700;

	private static final Double APPLICATION_WIDTH = 1280D;
	private static final Double APPLICATION_HEIGHT = 960D;

	private static final Double APPLICATION_X = 10D;
	private static final Double APPLICATION_Y = 10D;

	private static final int SCRIPT_TIMEOUT = 60;
	private static final int PAGELOAD_TIMEOUT = 120;
	private static final int WATCHDOG_TIMEOUT = 200;
	private static final int WEBSERVICE_TIMEOUT = 30;
	private static final int REGEX_TIMEOUT = 5;

	private static final int MAX_TRY_SEARCH = 15;
	private static final int MAX_TRY_INTERACTABLE = 20;
	private static final int MAX_TRY_PROPERTY = 10;
	private static final int MAX_TRY_WEBSERVICE = 1;
	private static final int MAX_TRY_IMAGE_RECOGNITION = 20;
	private static final int MAX_TRY_SCROLL_SEARCH = 15;

	private static final int SAP_START_TIMEOUT = 180;

	private static final int MAX_TRY_MOBILE = 5;

	private static final int SCROLL_UNIT = 120;
	private static final int MAX_STALE_OR_JAVASCRIPT_ERROR = 10;

	private static final int CAPTURE_PROXY_TRAFFIC_IDLE = 3;

	private static final String ATS_KEY_NAME = "ATS_KEY";

	//-----------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------

	private double applicationWidth = APPLICATION_WIDTH;
	private double applicationHeight = APPLICATION_HEIGHT;

	private double applicationX = APPLICATION_X;
	private double applicationY = APPLICATION_Y;

	private int scriptTimeOut = SCRIPT_TIMEOUT;
	private int pageloadTimeOut = PAGELOAD_TIMEOUT;
	private int watchDogTimeOut = WATCHDOG_TIMEOUT;
	private int regexTimeOut = REGEX_TIMEOUT;

	private int sapStartTimeOut = SAP_START_TIMEOUT;

	private int webServiceTimeOut = WEBSERVICE_TIMEOUT;

	private int maxTrySearch = MAX_TRY_SEARCH;
	private int maxTryInteractable = MAX_TRY_INTERACTABLE;
	private int maxTryProperty = MAX_TRY_PROPERTY;
	private int maxTryWebservice = MAX_TRY_WEBSERVICE;
	private int maxTryMobile = MAX_TRY_MOBILE;
	private int maxTryImageRecognition = MAX_TRY_IMAGE_RECOGNITION;
	private int maxTryScrollSearch = MAX_TRY_SCROLL_SEARCH;

	private int waitEnterText = 0;
	private int waitSearch = 0;
	private int waitMouseMove = 0;
	private int waitSwitchWindow = 0;
	private int waitGotoUrl = 0;

	private AtsProxy proxy = new AtsProxy();
	private AtsProxy neoloadProxy;

	private String neoloadDesignApi;
	
	private String atsPropertiesFilePath = null;

	private OctoperfApi octoperf;
	private ArrayList<String> blackListServers;
	private int trafficIdle = CAPTURE_PROXY_TRAFFIC_IDLE;

	private List<ApplicationProperties> applicationsList = new ArrayList<>();

	private String error;
	private String atsKey;

	public static int getScrollUnit() {
		return SCROLL_UNIT;
	}

	public static int getMaxStaleOrJavaScriptError() {
		return MAX_STALE_OR_JAVASCRIPT_ERROR;
	}

	private static String getElementText(Node node) {
		return getElementText((Element)node);
	}

	private static String getElementText(Element elem) {
		if(elem != null) {
			final String textContent = elem.getTextContent();
			if(textContent != null && textContent.length() > 0) {
				return textContent.replaceAll("\n", "").replaceAll("\r", "").trim();
			}
		}
		return null;
	}

	private static double getElementDouble(Element elem) {
		final String value = elem.getTextContent().replaceAll("\n", "").replaceAll("\r", "").trim();
		try {
			return Double.parseDouble(value);
		}catch(NumberFormatException e){}
		return 0D;
	}

	private static int getElementInt(Element elem) {
		final String value = elem.getTextContent().replaceAll("\n", "").replaceAll("\r", "").trim();
		try {
			return Integer.parseInt(value);
		}catch(NumberFormatException e){}
		return 0;
	}

	public AtsManager(){
		init();
	}

	public static String getAtsHomeFolder() {
		String atsHome = System.getProperty("ats-home");
		if(atsHome == null || atsHome.length() == 0) {
			atsHome = System.getProperty("ats.home");
			if(atsHome == null || atsHome.length() == 0) {
				atsHome = System.getenv("ATS_HOME");
				if(atsHome == null || atsHome.length() == 0) {
					final Path userPath = Paths.get(System.getProperty("user.home"));
					Path atsPath = userPath.resolve(ATS_FOLDER);
					if(!Files.exists(atsPath)) {
						atsPath = userPath.resolve(ATS_FOLDER_SIMPLE);
					}
					atsHome = atsPath.toString();
				}
			}
		}
		return atsHome;
	}

	//-----------------------------------------------------------------------------------------------
	// Instance management
	//-----------------------------------------------------------------------------------------------

	private AtsClassLoader atsClassLoader;

	public void init() {

		final String atsHome = getAtsHomeFolder();

		Path atsFolderPath = null;
		try {
			atsFolderPath = Paths.get(atsHome);
		}catch(Exception e) {}

		if(atsFolderPath != null && atsFolderPath.toFile().exists()) {

			final Path userHomePath = Paths.get(System.getProperty("user.home"));
			Path atsPropertiesFilePath = userHomePath.resolve(ATS_PROPERTIES_FILE);

			if(!Files.exists(atsPropertiesFilePath)) {
				atsPropertiesFilePath = userHomePath.resolve(ATS_FOLDER).resolve(ATS_PROPERTIES_FILE);
				if(!Files.exists(atsPropertiesFilePath)) {
					atsPropertiesFilePath = atsFolderPath.resolve(ATS_PROPERTIES_FILE);
				}
			}

			loadProperties(atsPropertiesFilePath);
			addSystemDriver(System.getProperty("system-driver-url"));

			driverPath = atsFolderPath.resolve(DRIVERS_FOLDER);

		}else {
			driverPath = Paths.get("");
		}

		if(proxy == null) {
			proxy = new AtsProxy(AtsProxy.SYSTEM);
		}

		atsClassLoader = new AtsClassLoader(this);
	}

	public IWaitGuiReady getWaitGuiReady() {
		return atsClassLoader.getWaitGuiReady();
	}

	public Class<ActionTestScript> loadTestScriptClass(ActionStatus status, String name){
		return atsClassLoader.loadTestScriptClass(status, name);
	}

	private Path driverPath;
	private List<URI> systemDriverUris = new ArrayList<>();

	public Path getDriverPath() {
		return driverPath;
	}

	public List<URI> getSystemDriverUris() {
		return systemDriverUris;
	}

	private void addSystemDriver(String value) {
		if(!Strings.isNullOrEmpty(value)) {
			if(!value.startsWith("http")) {
				value = "http://" + value;
			}

			if(value.split(":").length != 3) {
				value += ":" + SYSTEM_DRIVER_PORT;
			}

			try {
				final URI newUri = new URI(value);
				if(!uriExists(newUri)) {
					systemDriverUris.add(newUri);
				}
			} catch (Exception e) {}
		}
	}

	private boolean uriExists(URI u) {
		for(URI uri : systemDriverUris) {
			if(uri.toString().equals(u.toString())) {
				return true;
			}
		}
		return false;
	}

	//-----------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------

	private void loadProperties(Path propertiesPath) {

		final File xmlFile = propertiesPath.toFile();
		if(xmlFile.exists()) {
			atsPropertiesFilePath = xmlFile.getAbsolutePath();
			try {
				final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				try {
					final DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					final Document doc = dBuilder.parse(xmlFile);

					doc.getDocumentElement().normalize();

					final NodeList executeChildren = ((Element) doc.getChildNodes().item(0)).getChildNodes();
					for (int i = 0; i < executeChildren.getLength(); i++) {
						if (executeChildren.item(i) instanceof Element) {

							final Element type = (Element) executeChildren.item(i);
							String url = null;

							switch (type.getNodeName().toLowerCase()) {

							case "atskey":
								atsKey = type.getChildNodes().item(0).getNodeValue();
								break;
							case "systemdriver":

								final NodeList dl = type.getChildNodes();
								if(dl != null) {
									for (int k = 0; k < dl.getLength(); k++) {
										if("driver".equals(dl.item(k).getNodeName())){
											addSystemDriver(getElementText(dl.item(k)));
										}
									}
								}
								break;

							case "atsremoteagents":

								final NodeList urls = type.getElementsByTagName("url");
								for (int j = 0; j < urls.getLength(); j++) {
									addSystemDriver(getElementText(urls.item(j)));
								}

								break;

							case "performance":
								final NodeList perfChildren = type.getChildNodes();
								for (int j = 0; j < perfChildren.getLength(); j++) {
									if (perfChildren.item(j) instanceof Element) {

										final Element perfElement = (Element) perfChildren.item(j);
										switch(perfElement.getNodeName().toLowerCase()) {

										case "idle":
											trafficIdle = getElementInt(perfElement);
											break;
										case "octoperf":

											String host = null;
											String apiKey = null;
											String workspaceName = null;
											String projectName = null;

											NodeList nl = perfElement.getElementsByTagName("host");
											if(nl != null) {
												host = getElementText(nl.item(0));
											}

											nl = perfElement.getElementsByTagName("apiKey");
											if(nl != null) {
												apiKey = getElementText(nl.item(0));
											}

											nl = perfElement.getElementsByTagName("workspaceName");
											if(nl != null) {
												workspaceName = getElementText(nl.item(0));
											}

											nl = perfElement.getElementsByTagName("projectName");
											if(nl != null) {
												projectName = getElementText(nl.item(0));
											}

											if(host != null && apiKey != null) {
												octoperf = new OctoperfApi(host, apiKey, workspaceName, projectName);
											}

											break;

										case "blacklist":
											blackListServers = new ArrayList<>();
											final NodeList blackList = perfElement.getElementsByTagName("url");
											for (int k = 0; k < blackList.getLength(); k++) {
												if (blackList.item(k) instanceof Element) {
													blackListServers.add(getElementText(blackList.item(k)));
												}
											}
											break;
										}
									}
								}
								break;

							case "neoload":
								final NodeList neoloadChildren = type.getChildNodes();
								for (int j = 0; j < neoloadChildren.getLength(); j++) {
									if (neoloadChildren.item(j) instanceof Element) {
										final Element neoloadElement = (Element) neoloadChildren.item(j);

										String host = null;
										String port = null;
										String api = null;
										String apiPort = null;

										if(neoloadElement.getTagName().equals("recorder")) {
											final NodeList recorderChildren = neoloadElement.getChildNodes();
											for (int k = 0; k < recorderChildren.getLength(); k++) {
												if (recorderChildren.item(k) instanceof Element) {
													final Element recorderElement = (Element) recorderChildren.item(k);
													if(recorderElement.getNodeName().equals("host")) {
														host = getElementText(recorderElement);
													}else if(recorderElement.getNodeName().equals("port")){
														port = getElementText(recorderElement);
													}
												}
											}
										}else if(neoloadElement.getTagName().equals("design")) {
											final NodeList designChildren = neoloadElement.getChildNodes();
											for (int k = 0; k < designChildren.getLength(); k++) {
												if (designChildren.item(k) instanceof Element) {
													final Element designElement = (Element) designChildren.item(k);
													if(designElement.getNodeName().equals("api")) {
														api = getElementText(designElement);
													}else if(designElement.getNodeName().equals("port")){
														apiPort = getElementText(designElement);
													}
												}
											}
										}

										if(host != null && port != null) {
											neoloadProxy = new AtsProxy(host, Utils.string2Int(port, 8080));

											if(api != null && apiPort != null) {

												if(!api.startsWith("/")) {
													api = "/" + api;
												}
												if(!api.endsWith("/")) {
													api = api + "/";
												}

												neoloadDesignApi = "http://" + neoloadProxy.getHost() + ":" + apiPort + api;
											}
										}
									}
								}

								break;

							case "proxy":

								final NodeList proxyChildren = type.getChildNodes();

								String proxyType = null;
								String host = null;
								int port = 0;

								for (int j = 0; j < proxyChildren.getLength(); j++) {
									if (proxyChildren.item(j) instanceof Element) {
										final Element proxyElement = (Element) proxyChildren.item(j);
										switch (proxyElement.getNodeName().toLowerCase()) {
										case "type":
											proxyType = getElementText(proxyElement);
											break;
										case "host":
											host = getElementText(proxyElement);
											break;
										case "url":
											url = getElementText(proxyElement);
											break;
										case "port":
											port = getElementInt(proxyElement);
											break;
										}
									}
								}

								if(AtsProxy.AUTO.equals(proxyType)) {
									proxy = new AtsProxy(AtsProxy.AUTO);
								}else if(AtsProxy.DIRECT.equals(proxyType)) {
									proxy = new AtsProxy(AtsProxy.DIRECT);
								}else if(AtsProxy.PAC.equals(proxyType)) {
									proxy = new AtsProxy(AtsProxy.PAC, url);
								}else if(AtsProxy.MANUAL.equals(proxyType) && host != null && port > 0) {
									proxy = new AtsProxy(AtsProxy.MANUAL, host, port);
								}else if(AtsProxy.SYSTEM.equals(proxyType)) {
									proxy = new AtsProxy(AtsProxy.SYSTEM);
								}
								break;

							case "appbounding":
								final NodeList boundingChildren = type.getChildNodes();
								for (int j = 0; j < boundingChildren.getLength(); j++) {
									if (boundingChildren.item(j) instanceof Element) {
										final Element boundingElement = (Element) boundingChildren.item(j);

										switch(boundingElement.getTagName()) {
										case "x":
											applicationX = getElementDouble(boundingElement);
											break;
										case "y":
											applicationY = getElementDouble(boundingElement);
											break;
										case "width":
											applicationWidth = getElementDouble(boundingElement);
											break;
										case "height":
											applicationHeight = getElementDouble(boundingElement);
											break;
										}
									}else {
										final String content = boundingChildren.item(j).getTextContent();
										if("fullscreen".equalsIgnoreCase(content) || "max".equalsIgnoreCase(content) || "maxscreen".equalsIgnoreCase(content) || "maxsize".equalsIgnoreCase(content)) {
											applicationX = 0;
											applicationY = 0;
											applicationWidth = -1;
											applicationHeight = -1;
										}
									}
								}
								break;

							case "wait":
								final NodeList waitChildren = type.getChildNodes();
								for (int j = 0; j < waitChildren.getLength(); j++) {
									if (waitChildren.item(j) instanceof Element) {
										final Element waitElement = (Element) waitChildren.item(j);

										switch(waitElement.getTagName().toLowerCase()) {
										case "entertext":
											waitEnterText = getElementInt(waitElement);
											break;
										case "search":
											waitSearch = getElementInt(waitElement);
											break;
										case "mousemove":
											waitMouseMove = getElementInt(waitElement);
											break;
										case "switchwindow":
											waitSwitchWindow = getElementInt(waitElement);
											break;
										case "gotourl":
											waitGotoUrl = getElementInt(waitElement);
											break;
										}
									}
								}
								break;

							case "maxtry":
								final NodeList maxtryChildren = type.getChildNodes();
								for (int j = 0; j < maxtryChildren.getLength(); j++) {
									if (maxtryChildren.item(j) instanceof Element) {
										final Element maxtryElement = (Element) maxtryChildren.item(j);

										switch(maxtryElement.getTagName().toLowerCase()) {
										case "searchelement":
										case "elementsearch":
											maxTrySearch = getElementInt(maxtryElement);
											break;
										case "interactable":
											maxTryInteractable = getElementInt(maxtryElement);
											break;
										case "getproperty":
											maxTryProperty = getElementInt(maxtryElement);
											break;
										case "webservice":
											maxTryWebservice = getElementInt(maxtryElement);
											break;
										case "searchimage":
										case "imagesearch":
											maxTryImageRecognition = getElementInt(maxtryElement);
											break;
										case "searchscroll":
										case "scrollsearch":
											maxTryScrollSearch = getElementInt(maxtryElement);
											break;
										}
									}
								}
								break;

							case "timeout":
								final NodeList timeoutChildren = type.getChildNodes();
								for (int j = 0; j < timeoutChildren.getLength(); j++) {
									if (timeoutChildren.item(j) instanceof Element) {
										final Element timeoutElement = (Element) timeoutChildren.item(j);

										switch(timeoutElement.getTagName().toLowerCase()) {
										case "script":
											scriptTimeOut = getElementInt(timeoutElement);
											break;
										case "pageload":
											pageloadTimeOut = getElementInt(timeoutElement);
											break;
										case "watchdog":
											watchDogTimeOut = getElementInt(timeoutElement);
											break;
										case "regexp":
										case "regex":
											regexTimeOut = getElementInt(timeoutElement);
											if(regexTimeOut > 5) {
												regexTimeOut = 5;
											}
											break;
										case "webservice":
											webServiceTimeOut = getElementInt(timeoutElement);
											break;
										}
									}
								}
								break;

							case "browsers":
								final NodeList browsers = type.getElementsByTagName("browser");
								for (int j = 0; j < browsers.getLength(); j++) {

									String name = null;
									String waitAction = null;
									String path = null;
									String driver = null;
									String dataDir = null;
									String check = null;
									String lang = null;
									String debugPort = null;

									String[] options = null;
									String[] excludedOptions = null;

									String title = "";

									if (browsers.item(j) instanceof Element) {
										final Element browser = (Element) browsers.item(j);

										final String elementName = browser.getAttribute("name");
										if(Utils.isNotEmpty(elementName)) {
											name = elementName;
										}

										final NodeList browserAttributes = browser.getChildNodes();
										for (int k = 0; k < browserAttributes.getLength(); k++) {
											if (browserAttributes.item(k) instanceof Element) {
												final Element browserElement = (Element) browserAttributes.item(k);
												final String nodeName = browserElement.getNodeName().toLowerCase();

												switch (nodeName) {
												case "name":
													name = getElementText(browserElement);
													break;
												case "waitaction":
													waitAction = getElementText(browserElement);
													break;
												case "title":
													title = getElementText(browserElement);
													break;
												case "driver":
													driver = getElementText(browserElement);
													break;
												case "path":
													path = getElementText(browserElement);
													break;
												case "userdatadir":
												case "userdata":
												case "profile":
												case "profiledir":
													dataDir = getElementText(browserElement);
													break;
												case "waitproperty":
													check = getElementText(browserElement);
													break;
												case "lang":
													lang = getElementText(browserElement);
													break;
												case "debugport":
													debugPort = getElementText(browserElement);
													break;
												case "excludedoptions":
													final NodeList excludedOptionsList = browserElement.getElementsByTagName("option");
													final int excludedOptionsLen = excludedOptionsList.getLength();
													if(excludedOptionsLen > 0) {
														excludedOptions = new String[excludedOptionsLen];
														for( int l = 0; l < excludedOptionsLen; l++ ){
															excludedOptions[l] = excludedOptionsList.item(l).getTextContent().replaceFirst("^--", "");
														}
													}
													break;
												case "options":
													final NodeList optionsList = browserElement.getElementsByTagName("option");
													final int optionsLen = optionsList.getLength();
													if(optionsLen > 0) {
														options = new String[optionsLen];
														for( int l = 0; l < optionsLen; l++ ){
															options[l] = optionsList.item(l).getTextContent();
														}
													}
													break;
												}
											}
										}

										if(name != null) {
											addApplicationProperties(ApplicationProperties.BROWSER_TYPE, name, driver, path, waitAction, check, lang, dataDir, title, options, excludedOptions, debugPort);
										}
									}
								}
								break;

							case "applications":
								final NodeList applications = type.getElementsByTagName("application");
								for (int j = 0; j < applications.getLength(); j++) {

									String name = null;
									String waitAction = null;
									String path = null;
									String[] options = null;

									if (applications.item(j) instanceof Element) {
										final Element application = (Element) applications.item(j);
										final NodeList applicationAttributes = application.getChildNodes();
										for (int k = 0; k < applicationAttributes.getLength(); k++) {
											if (applicationAttributes.item(k) instanceof Element) {
												final Element applicationElement = (Element) applicationAttributes.item(k);

												switch (applicationElement.getNodeName().toLowerCase()) {
												case "name":
													name = getElementText(applicationElement);
													break;
												case "waitaction":
													waitAction = getElementText(applicationElement);
													break;
												case "path":
													path = getElementText(applicationElement);
													if(!path.startsWith("file:///")) {
														path = "file:///" + path;
													}
													break;
												case "options":
													final NodeList optionsList = application.getElementsByTagName("option");
													final int optionsLen = optionsList.getLength();
													if(optionsLen > 0) {
														options = new String[optionsLen];
														for( int l = 0; l < optionsLen; l++ ){
															options[l] = optionsList.item(l).getTextContent();
														}
													}
													break;
												}
											}
										}

										if(name != null && path != null) {
											addApplicationProperties(ApplicationProperties.DESKTOP_TYPE, name, path, waitAction, "", null, null, null, options, null);
										}
									}
								}

								break;

							case "mobiles":
								final NodeList mobiles = type.getElementsByTagName("mobile");
								for (int j = 0; j < mobiles.getLength(); j++) {

									String name = null;
									String waitAction = null;
									String endpoint = null;
									String packageName = null;

									if (mobiles.item(j) instanceof Element) {
										final Element mobile = (Element) mobiles.item(j);
										final NodeList mobileAttributes = mobile.getChildNodes();
										for (int k = 0; k < mobileAttributes.getLength(); k++) {
											if (mobileAttributes.item(k) instanceof Element) {
												final Element mobileElement = (Element) mobileAttributes.item(k);

												switch (mobileElement.getNodeName().toLowerCase()) {
												case "name":
													name = getElementText(mobileElement);
													break;
												case "waitaction":
													waitAction = getElementText(mobileElement);
													break;
												case "endpoint":
													endpoint = getElementText(mobileElement);
													break;
												case "package":
													packageName = getElementText(mobileElement);
													break;
												}
											}
										}

										if(name != null && endpoint != null && packageName != null) {
											addApplicationProperties(ApplicationProperties.MOBILE_TYPE, name, endpoint + "/" + packageName, waitAction, "", null, null, null, null, null);
										}
									}
								}
								break;

							case "apis":
								final NodeList apis = type.getElementsByTagName("api");
								for (int j = 0; j < apis.getLength(); j++) {

									String name = null;
									String waitAction = null;

									if (apis.item(j) instanceof Element) {
										final Element api = (Element) apis.item(j);
										final NodeList apiAttributes = api.getChildNodes();
										for (int k = 0; k < apiAttributes.getLength(); k++) {
											if (apiAttributes.item(k) instanceof Element) {
												final Element apiElement = (Element) apiAttributes.item(k);

												switch (apiElement.getNodeName().toLowerCase()) {
												case "name":
													name = getElementText(apiElement);
													break;
												case "waitaction":
													waitAction = getElementText(apiElement);
													break;
												case "url":
													url = getElementText(apiElement);
													break;
												}
											}
										}

										if(name != null && url != null) {
											addApplicationProperties(ApplicationProperties.API_TYPE, name, url, waitAction, "", null, null, null, null, null);
										}
									}
								}
								break;

							case "sap":

								final NodeList sapChilds = type.getChildNodes();

								for (int j = 0; j < sapChilds.getLength(); j++) {

									final Node sapElement = sapChilds.item(j);
									final String sapElementName = sapElement.getNodeName().toLowerCase();

									if("timeout".equals(sapElementName)) {

										final NodeList toList = sapElement.getChildNodes();
										for (int k = 0; k < toList.getLength(); k++) {

											final Node toElement = toList.item(k);
											final String toElementName = toElement.getNodeName().toLowerCase();

											if("startdriver".equals(toElementName)) {
												sapStartTimeOut = getElementInt((Element) toElement);
											}
										}

									}else if ("sessions".equals(sapElementName)) {

										final NodeList sessionsList = sapElement.getChildNodes();
										for (int k = 0; k < sessionsList.getLength(); k++) {

											String name = null;
											String client = null;
											String connection = null;
											String authentication = null;
											String lang = null;

											final Node sessionElement = sessionsList.item(k);
											final NodeList sessionProps = sessionElement.getChildNodes();

											for (int l = 0; l < sessionProps.getLength(); l++) {
												final Node sessionProp = sessionProps.item(l);

												switch (sessionProp.getNodeName().toLowerCase()) {
												case "name":
													name = getElementText(sessionProp);
													break;
												case "client":
													client = getElementText(sessionProp);
													break;
												case "connection":
													connection = getElementText(sessionProp);
													break;
												case "authentication":
													authentication = getElementText(sessionProp);
													break;
												case "lang":
													lang = getElementText(sessionProp);
													break;
												}
											}

											if(name != null && client != null && connection != null) {
												applicationsList.add(new ApplicationProperties(name, connection, client, authentication, lang));
											}
										}
									}
								}

								break;
							}
						}
					}

				} catch (ParserConfigurationException e) {
					error = e.getMessage();
				} catch (SAXException e) {
					error = e.getMessage();
				}

			} catch (IOException e) {
				error = e.getMessage();
			}
		}
	}

	private void addApplicationProperties(int type, String name, String path, String wait, String check, String lang, String userDataDir, String title, String[] options, String[] excludedOptions) {
		addApplicationProperties(type, name, null, path, wait, check, lang, userDataDir, title, options, excludedOptions, null);
	}

	private void addApplicationProperties(int type, String name, String driver, String path, String wait, String check, String lang, String userDataDir, String title, String[] options, String[] excludedOptions, String debugPort) {
		applicationsList.add(new ApplicationProperties(type, name, driver, path, Utils.string2Int(wait, -1), Utils.string2Int(check, -1), lang, userDataDir, title, options, excludedOptions, Utils.string2Int(debugPort, -1)));
	}

	//------------------------------------------------------------------------------------------------------------------
	// Getters
	//------------------------------------------------------------------------------------------------------------------

	public String getError() {
		return error;
	}

	public ApplicationProperties getApplicationProperties(String name) {

		for (final ApplicationProperties properties : this.applicationsList) {
			if (name.equals(properties.getName())){
				return properties;
			}
		}
		return new ApplicationProperties(name);
	}

	public int getScriptTimeOut() {
		return scriptTimeOut;
	}

	public int getSapStartTimeout() {
		return sapStartTimeOut;
	}

	public int getPageloadTimeOut() {
		return pageloadTimeOut;
	}

	public int getRegexTimeOut() {
		return regexTimeOut;
	}

	public int getWatchDogTimeOut() {
		return watchDogTimeOut;
	}

	public int getWebServiceTimeOut() {
		return webServiceTimeOut;
	}

	public TestBound getApplicationBound() {
		return new TestBound(applicationX, applicationY, applicationWidth, applicationHeight);
	}

	public int getMaxTrySearch() {
		return maxTrySearch;
	}

	public int getMaxTryInteractable() {
		return maxTryInteractable;
	}

	public int getMaxTryImageRecognition() {
		return maxTryImageRecognition;
	}

	public int getMaxTryScrollSearch() {
		return maxTryScrollSearch;
	}

	public int getMaxTryProperty() {
		return maxTryProperty;
	}

	public int getMaxTryMobile() {
		return maxTryMobile;
	}

	public int getMaxTryWebservice() {
		return maxTryWebservice;
	}

	public int getWaitEnterText() {
		return waitEnterText;
	}

	public int getWaitSearch() {
		return waitSearch;
	}

	public int getWaitMouseMove() {
		return waitMouseMove;
	}

	public int getWaitSwitchWindow() {
		return waitSwitchWindow;
	}

	public int getWaitGotoUrl() {
		return waitGotoUrl;
	}

	public String getNeoloadDesignApi() {
		return neoloadDesignApi;
	}

	public AtsProxy getNeoloadProxy() {
		if(neoloadProxy != null) {
			return neoloadProxy;
		}
		return getProxy();
	}

	public AtsProxy getProxy() {
		return proxy;
	}

	public ArrayList<String> getBlackListServers() {
		return blackListServers;
	}

	public OctoperfApi getOctoperf() {
		return octoperf;
	}

	public int getTrafficIdle() {
		return trafficIdle;
	}

	public String getAtsKey() {

		final String key = System.getenv(ATS_KEY_NAME);
		if(key != null && key.length() == 16) {
			return key;
		}

		if(atsKey != null && atsKey.length() == 16) {
			return atsKey;
		}

		return null;
	}

	public String getApplicationsList() {
		final ObjectWriter ow = new ObjectMapper().writer();
		try {
			return ow.writeValueAsString(applicationsList);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public String getAtsPropertiesFilePath() {
		return atsPropertiesFilePath;
	}
}