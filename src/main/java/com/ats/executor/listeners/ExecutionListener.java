/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor.listeners;

import static com.diogonunes.jcolor.Ansi.colorize;
import static com.diogonunes.jcolor.Attribute.BRIGHT_BLUE_TEXT;
import static com.diogonunes.jcolor.Attribute.CYAN_BACK;
import static com.diogonunes.jcolor.Attribute.MAGENTA_BACK;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.testng.IExecutionListener;

import com.ats.AtsSingleton;
import com.ats.generator.ATS;
import com.ats.tools.Utils;
import com.ats.tools.logger.levels.AtsLogger;
import com.ats.tools.report.AtsReport;
import com.ats.tools.report.HtmlCampaignReportGenerator;
import com.ats.tools.report.SuitesReport;
import com.ats.tools.report.SuitesReportItem;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

public class ExecutionListener implements IExecutionListener {

    private final static Gson gson = new Gson();
    private final static String ATS_REPORT_LEVEL_FROM_COMMANDLINE = "ats.report";
    private static SuitesReport suiteReport = null;

    private static SuitesReportItem suiteItem = null;

    private static final List<String> trueList = Arrays.asList(new String[]{"on", "true", "1", "yes", "y"});


	private static void setSuitesFile(File value) {

		final File outputDir = value.getParentFile();
		outputDir.mkdirs();

		AtsSingleton.getInstance().setJsonSuitesFilePath(value.getAbsolutePath());
		AtsSingleton.getInstance().setAtsOutputFolder(outputDir.getAbsolutePath());
		AtsSingleton.getInstance().setProjectPath(outputDir.getParentFile().getAbsolutePath());
	}

	public static void startSuite(SuitesReportItem suite) {

		suiteItem = suite;
		
		try {

			File jsonSuiteFile = AtsSingleton.getInstance().getJsonSuitesFile();
			if (jsonSuiteFile == null) {

				jsonSuiteFile = suiteItem.getSuitesFile();
				setSuitesFile(jsonSuiteFile);
				suiteReport = new SuitesReport(suiteItem);

			} else {

				final JsonReader reader = new JsonReader(new FileReader(jsonSuiteFile));
				suiteReport = gson.fromJson(reader, SuitesReport.class);
				reader.close();

				suiteReport.add(suiteItem);
			}

			final FileWriter writer = new FileWriter(jsonSuiteFile);
			gson.toJson(
					suiteReport,
					writer);

			writer.close();

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void onExecutionStart() {
		IExecutionListener.super.onExecutionStart();

		System.out.println("-------------------------------------------------------");
		System.out.println("   ATS ver. " + ATS.getAtsVersion() + "   ( " + colorize(ATS.WEB_SITE, BRIGHT_BLUE_TEXT()) + " )");
		System.out.println("-------------------------------------------------------");
	}

	@Override
	public void onExecutionFinish() {
		IExecutionListener.super.onExecutionFinish();
		
		if(suiteItem.isNoSuiteLaunch()) {
			AtsLogger.printLog(colorize(" ----------------------------------------------- ", MAGENTA_BACK()));
			AtsLogger.printLog(colorize("         ATS test execution complete             ", MAGENTA_BACK()));
			AtsLogger.printLog(colorize(" ----------------------------------------------- ", MAGENTA_BACK()));
		}else {
			AtsLogger.printLog(colorize(" ----------------------------------------------- ", CYAN_BACK()));
			AtsLogger.printLog(colorize("         ATS suite execution complete            ", CYAN_BACK()));
			AtsLogger.printLog(colorize(" ----------------------------------------------- ", CYAN_BACK()));
		}
		
		final String outputFolder = AtsSingleton.getInstance().getAtsOutputFolder();
		final Path outputFolderPath = Paths.get(outputFolder);

		final String cmdATSReport = System.getProperty(AtsReport.ATS_REPORT);
		int devReport = 0;

		final String atsReportEnv = System.getenv(AtsReport.ATS_REPORT_ENV);
		if(atsReportEnv != null){
			devReport = Utils.string2Int(atsReportEnv,0);
		}

		if (cmdATSReport != null) {
			devReport = Utils.string2Int(cmdATSReport,0);
		}

		final String cmdMGTReport = System.getProperty(AtsReport.MGT_REPORT);
		int mgtReport = 0;
		if (cmdMGTReport != null) {
			mgtReport = Utils.string2Int(cmdMGTReport,0);
		}
		
		final String cmdvalidReport = System.getProperty(AtsReport.VALID_REPORT);
		int validReport = 0;
		
		if (cmdvalidReport != null) {
			validReport = trueList.indexOf(cmdvalidReport.toLowerCase()) > -1 ? 1 : 0;
		}

		boolean reportingAsk = false;

        for (SuitesReportItem item : suiteReport.suites) {
            if (item.isReporting()) {
                reportingAsk = true;
            }
        }

        String atsReportLevelProperty = System.getProperty(ATS_REPORT_LEVEL_FROM_COMMANDLINE);
        if (StringUtils.isNoneEmpty(atsReportLevelProperty) && devReport <= 0) {
            devReport = Integer.parseInt(atsReportLevelProperty);
        }

		if (((devReport + mgtReport + validReport) != 0) || reportingAsk) {

			AtsLogger.printLog("generate ATS report -> " + outputFolder);

			final File jsonSuiteFile = AtsSingleton.getInstance().getJsonSuitesFile();
			if (jsonSuiteFile != null && jsonSuiteFile.exists()) {
				try {
					new HtmlCampaignReportGenerator(outputFolderPath, jsonSuiteFile, devReport, validReport, suiteItem).generateSuitsHtmlReports();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}