/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor.scripts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import org.testng.annotations.Test;

import com.ats.driver.AtsRemoteWebDriver;
import com.ats.element.ParameterElement;
import com.ats.executor.ActionStatus;
import com.ats.script.actions.ActionComment;

public class AtsCallSubscriptJavascript extends AtsCallSubscriptScript {

	private static final String JS_CONSOLE_PREFIX = "ats-js-console: ";
	public static final String JS_LANGUAGE = "js";

	private static final String atsResultPrototype = "function AtsResult(){"
			+ "this.return=null;"
			+ "this.logs=[];"
			+ "this.log=function(s){this.logs.push(s);};"
			+ "this.comments=[];"
			+ "this.comment=function(s){this.comments.push(s);};}"
			+ "const ats_result=new AtsResult();"
			+ "const ats_return=function(data){ats_result.return=data;};"
			+ "const ats_comment=function(data){ats_result.comment(data);};"
			+ "var consoleLogFn=console.log;console.log=function(){consoleLogFn.apply(console,arguments);var args=Array.prototype.slice.call(arguments);for(var i=0;i<args.length;i++){ats_result.log(args[i]);}};\r\n";

	@Test
	public void testMain(){

		List<String> resultArray = new ArrayList<String>();

		ActionStatus status = new ActionStatus();
		final AtsRemoteWebDriver driver = getCurrentChannel().getDriverEngine().getAtsRemoteDriver();

		final StringBuilder jsCode = new StringBuilder(atsResultPrototype);

		ParameterElement elem = getParameterElement();

		jsCode.append("const ats_parameter=function(i){let ").
		append(getParametersCode()).
		append(";if(i < p.length){return p[i];}else{return '';}};\r\n");
		
		if(elem != null) {

			jsCode.
			append("const ats_element=arguments[0], ").append(ATS_ITERATION).append("=").append(getIteration()).
			append(", ").append(ATS_ITERATIONS_COUNT).append("=").append(getIterationsCount()).append(";\r\n").
			append(code).
			append(";\r\nreturn ats_result;");

			try {
				resultArray = getResultData(driver.executeScript(jsCode.toString(), elem.getRemoteWebElement(driver)));
			}catch(Exception e) {
				throw new AtsCallSubscriptException(e.getMessage());
			}

		}else {

			jsCode.
			append("const ").append(ATS_ITERATION).append("=").
			append(getIteration()).
			append(", ").append(ATS_ITERATIONS_COUNT).append("=").
			append(getIterationsCount()).append(";\r\n").
			append(code).
			append(";\r\nreturn ats_result;");

			try {
				resultArray = getResultData(driver.executeScript(jsCode.toString()));
			}catch(Exception e) {
				throw new AtsCallSubscriptException(e.getMessage());
			}
		}

		if(status.getFailMessage() != null) {
			System.out.println(status.getFailMessage());
		}		

		returnValues(resultArray.toArray());
	}

	@SuppressWarnings("unchecked")
	private List<String> getResultData(Object obj){
		if(obj != null && (obj instanceof LinkedHashMap)) {

			LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) obj;

			List<Object> logs = (List<Object>) map.get("logs");
			if(logs != null) {
				logs.forEach(l -> System.out.println(JS_CONSOLE_PREFIX + l.toString()));
			}
			
			List<Object> comments = (List<Object>) map.get("comments");
			if(comments != null) {
				comments.forEach(l -> exec(0,new ActionComment(this, ActionComment.STEP_TYPE, clv(l.toString()))));
			}

			Object ret = map.get("return");
			if(ret != null) {
				if(ret instanceof ArrayList) {
					return (List<String>) ret;
				}else {
					return Arrays.asList(ret.toString());
				}
			}

		}else if(obj instanceof String) {
			return Arrays.asList(obj.toString());
		}else if(obj instanceof ArrayList) {
			return (List<String>) obj;
		}
		return Collections.emptyList();
	}
}