/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor.scripts;

import java.io.File;
import java.util.StringJoiner;

import com.ats.executor.ActionTestScript;
import com.ats.tools.logger.ExecutionLogger;

public class AtsCallSubscriptScript extends ActionTestScript {

	protected static final String ATS_ITERATION = "ats_iteration";
	protected static final String ATS_ITERATIONS_COUNT = "ats_iterations_count";
	
	protected String code;
	
	public AtsCallSubscriptScript() {}

	public AtsCallSubscriptScript(File assetsFolder) {
		super(assetsFolder);
	}

	public AtsCallSubscriptScript(ExecutionLogger logger) {
		super(logger);
	}

	public AtsCallSubscriptScript(ActionTestScript topScript) {
		super(topScript);
	}
	
	public void setScripCode(String value) {
		this.code = value;
	}
	
	protected StringBuilder getParametersCode() {
		StringBuilder sb = new StringBuilder("p=[");
		String[] parameters = getParameters();
		StringJoiner sj = new StringJoiner(",");

		for(int i=0; i<parameters.length; i++) {
			sj.add("\"" + parameters[i].replace("\n", "").replace("\r", "") + "\"");
		}
		
		return sb.append(sj.toString()).append("]");
	}
}
