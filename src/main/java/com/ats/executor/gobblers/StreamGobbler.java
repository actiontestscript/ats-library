package com.ats.executor.gobblers;

import java.io.InputStream;

import com.ats.executor.drivers.DriverInfoLocal;

public class StreamGobbler extends Thread
{
	protected InputStream inputStream;
	protected DriverInfoLocal driverProcess;

	public StreamGobbler(InputStream is, DriverInfoLocal dp)
	{
		this.inputStream = is;
		this.driverProcess = dp;
	}

	protected void terminate() {
		inputStream = null;
		driverProcess = null;
	}
}