package com.ats.executor.channels;

import com.ats.executor.ActionTestScript;
import com.ats.executor.TestBound;
import com.ats.executor.drivers.DriverManager;
import com.ats.script.actions.ActionChannelStart;

public class MobileChannel extends Channel {

	private double scale;
	private double captureScale;

	private boolean tablet;
	private boolean simulator;

	public MobileChannel(ActionChannelStart action, ActionTestScript script, DriverManager driverManager, String testName, int testLine) {
		super(action, script, driverManager, testName, testLine);
	}

	public void setApplicationData(String type, String os, String system, String version, String dVersion, byte[] icon, String udp, String appName, String userName, String machineName, String osBuild, String country) {
		setApplicationData(type, os + ":" + system, version, dVersion, -1, icon, udp);
		systemValues.setApplicationName(appName);
		systemValues.setUserName(userName);
		systemValues.setMachineName(machineName);
		systemValues.setOsVersion(system);
		systemValues.setOsBuild(osBuild);
		systemValues.setOsCountry(country);
	}

	public void setDimensions(TestBound dim1, TestBound dim2, double scale, double captureScale) {
		setDimensions(dim1, dim2);
		setScale(scale);
		setCaptureScale(captureScale);
	}

	@Override
	protected byte[] getScreenShotEngine(TestBound dim) {
		mainScript.sleep(100);
		return engine.getScreenshot(null, dim);
	}

	//----------------------------------------------------------------------------------------------------------------------
	// Getter and setter for serialization
	//----------------------------------------------------------------------------------------------------------------------

	public double getScale() {
		return scale;
	}

	public void setScale(double scale) {
		this.scale = scale;
	}

	public double getCaptureScale() {
		return captureScale;
	}

	public void setCaptureScale(double captureScale) {
		this.captureScale = captureScale;
	}

	public boolean isTablet() {
		return tablet;
	}

	public void setTablet(boolean value) {
		this.tablet = value;
	}

	public boolean isSimulator() {
		return simulator;
	}

	public void setSimulator(boolean value) {
		this.simulator = value;
	}
}