package com.ats.executor.drivers;

import java.net.URI;
import java.net.URISyntaxException;

import com.fasterxml.jackson.databind.JsonNode;

public class RemoteDriverInfo {

	private static final String HOST_KEYWORD = "#HOST#";

	private String id;
	private URI driverUri;
	private String driverName;
	private String driverVersion;
	private String applicationVersion;
	private String applicationPath;

	public RemoteDriverInfo(String id, String host, JsonNode node) {
		String uri = node.get("driverUrl").asText();

		if(uri.endsWith("/")) {
			uri = uri.substring(0, uri.length()-1);
		}
		uri = uri.replace(HOST_KEYWORD, "http://" + host);

		this.id = id;
		this.driverName = node.get("driverName").asText();
		this.driverVersion = node.get("driverVersion").asText();
		this.applicationVersion = node.get("applicationVersion").asText();
		this.applicationPath = node.get("applicationPath").asText();

		if(uri.startsWith("http")) {
			try {
				driverUri = new URI(uri);
			} catch (URISyntaxException e) {}
		}
	}

	public URI getDriverUri() {
		return driverUri;
	}

	public String getDriverName() {
		return driverName;
	}

	public String getDriverVersion() {
		return driverVersion;
	}

	public String getApplicationVersion() {
		return applicationVersion;
	}

	public String getApplicationPath() {
		return applicationPath;
	}

	public String getId() {
		return id;
	}
}