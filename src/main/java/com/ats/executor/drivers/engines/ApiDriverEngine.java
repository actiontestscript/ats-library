/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor.drivers.engines;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.xml.sax.SAXException;

import com.ats.AtsSingleton;
import com.ats.data.Dimension;
import com.ats.data.Point;
import com.ats.data.Rectangle;
import com.ats.driver.ApplicationProperties;
import com.ats.element.AtsBaseElement;
import com.ats.element.DialogBox;
import com.ats.element.FoundElement;
import com.ats.element.test.TestElement;
import com.ats.executor.ActionStatus;
import com.ats.executor.ActionTestScript;
import com.ats.executor.SendKeyData;
import com.ats.executor.TestBound;
import com.ats.executor.channels.Channel;
import com.ats.executor.drivers.IDriverInfo;
import com.ats.executor.drivers.desktop.DesktopWindow;
import com.ats.executor.drivers.desktop.SystemDriver;
import com.ats.executor.drivers.engines.webservices.ApiExecutor;
import com.ats.executor.drivers.engines.webservices.AtsCookieJar;
import com.ats.executor.drivers.engines.webservices.RestApiExecutor;
import com.ats.executor.drivers.engines.webservices.SoapApiExecutor;
import com.ats.generator.objects.MouseDirection;
import com.ats.generator.variables.CalculatedProperty;
import com.ats.graphic.ImageTemplateMatchingSimple;
import com.ats.script.Project;
import com.ats.script.actions.ActionApi;
import com.ats.script.actions.ActionChannelStart;
import com.google.common.io.CharStreams;

import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.Response;

public class ApiDriverEngine extends DriverEngine implements IDriverEngine{

	private static final byte[] SOAP_ICON = Base64.getDecoder().decode("iVBORw0KGgoAAAANSUhEUgAAAC4AAAAYCAYAAACFms+HAAAA8HpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjajVFbbsQgDPznFHsEv2LgOGSTlXqDHn8HME2zUqVawtiDsYchnd9fr/ToJkWSbbl4dSeYVavSEBSatg/PZMMPkzPO+I4nXpcEkGLXmVYJ/ASOmCOvUc+rfjVaATdE23XQWuD7Hd+joZTPRsFAeU6mIy5EI5VgZDN/BiOvJd+edjzpbuVapll8c84Gb0I5e0VchCxDz6MT1TzGQ6OYtICVr1IBJzmVFTKrqk2W2pdpw67DS0IhqyPphx3yITzhK0EBjets/Gr0I+ZvbS6N/rD/PCu9AZrfda5cnwy4AAABg2lDQ1BJQ0MgcHJvZmlsZQAAeJx9kT1Iw0AcxV9TpSKVDu0gopChOtlFRRxLFYtgobQVWnUwufQLmjQkKS6OgmvBwY/FqoOLs64OroIg+AHi7OCk6CIl/i8ptIjx4Lgf7+497t4BQqvGVLMvDqiaZWSSCTFfWBUDr/BDQAhhjEnM1FPZxRw8x9c9fHy9i/Es73N/jiGlaDLAJxLHmW5YxBvEs5uWznmfOMIqkkJ8Tjxp0AWJH7kuu/zGueywwDMjRi4zTxwhFss9LPcwqxgq8QxxVFE1yhfyLiuctzirtQbr3JO/MFjUVrJcpzmKJJaQQhoiZDRQRQ0WYrRqpJjI0H7Cwz/i+NPkkslVBSPHAupQITl+8D/43a1Zmp5yk4IJoP/Ftj/GgcAu0G7a9vexbbdPAP8zcKV1/fUWMPdJerOrRY+A0DZwcd3V5D3gcgcYftIlQ3IkP02hVALez+ibCkD4Fhhcc3vr7OP0AchRV8s3wMEhMFGm7HWPdw/09vbvmU5/P2d2cqJ2uj8yAAANdmlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNC40LjAtRXhpdjIiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iCiAgICB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIgogICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICAgeG1sbnM6R0lNUD0iaHR0cDovL3d3dy5naW1wLm9yZy94bXAvIgogICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iCiAgICB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iCiAgIHhtcE1NOkRvY3VtZW50SUQ9ImdpbXA6ZG9jaWQ6Z2ltcDowNjZiMWUyMy03NTFjLTQwYmUtYWNiMS05YjZjNGJmYzRjODYiCiAgIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6M2Y1ODg4MDAtNzEyMi00NDJhLTkwOGMtYWUwYzM5ZGUzOTk1IgogICB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6YWRhZWI3Y2EtZGIwOC00ZjkxLWEyNTgtNjhiY2NiNDJkM2E5IgogICBkYzpGb3JtYXQ9ImltYWdlL3BuZyIKICAgR0lNUDpBUEk9IjIuMCIKICAgR0lNUDpQbGF0Zm9ybT0iV2luZG93cyIKICAgR0lNUDpUaW1lU3RhbXA9IjE3MDkzOTI4MDYxODkwMzEiCiAgIEdJTVA6VmVyc2lvbj0iMi4xMC4zNCIKICAgdGlmZjpPcmllbnRhdGlvbj0iMSIKICAgeG1wOkNyZWF0b3JUb29sPSJHSU1QIDIuMTAiCiAgIHhtcDpNZXRhZGF0YURhdGU9IjIwMjQ6MDM6MDJUMTY6MjA6MDYrMDE6MDAiCiAgIHhtcDpNb2RpZnlEYXRlPSIyMDI0OjAzOjAyVDE2OjIwOjA2KzAxOjAwIj4KICAgPHhtcE1NOkhpc3Rvcnk+CiAgICA8cmRmOlNlcT4KICAgICA8cmRmOmxpCiAgICAgIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiCiAgICAgIHN0RXZ0OmNoYW5nZWQ9Ii8iCiAgICAgIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6ODhiZDRlOTMtZWIxMy00OTU5LWI5MTctMTVmNTNjZWNmMmFjIgogICAgICBzdEV2dDpzb2Z0d2FyZUFnZW50PSJHaW1wIDIuMTAgKFdpbmRvd3MpIgogICAgICBzdEV2dDp3aGVuPSIyMDI0LTAzLTAyVDE2OjIwOjA2Ii8+CiAgICA8L3JkZjpTZXE+CiAgIDwveG1wTU06SGlzdG9yeT4KICA8L3JkZjpEZXNjcmlwdGlvbj4KIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAKPD94cGFja2V0IGVuZD0idyI/Po6rpcIAAAAGYktHRADDAMAAyFPm9bYAAAAJcEhZcwAACxMAAAsTAQCanBgAAAAHdElNRQfoAwIPFAYX+7RJAAAAGXRFWHRDb21tZW50AENyZWF0ZWQgd2l0aCBHSU1QV4EOFwAABTlJREFUWMPtl1lvG0cWhb9b1QsXkVpIaqMWWrJjY14CxMg8zADzqwf5BQGSByOBJ5PVSCSNLMmmtXERyWZ31Z2HpiRKspRg5klA6qHRXdVdde6pOufelrN/fqE8wmZ4pO1P4H8C/1+By9SdquLxKArc1nDel48JquDFoeLvLKKTSY0qogL4j0BR5Godj6pH5X7fCG53eIGsXGTmyRNsaQYvjrTTI/vtAEkGk4kNWqkQbSxjZ8pYArJxwvj9O7KjNhZ/gwJFCJ5uEM3Poj5g+Po7JEuvRytVoufPENykS/HpmOTkDH94glF/RdS9wKVUYeHlSyhHGAwCRAsNXL1G/8uvUO+Jnm5RfLYF1uQAVQiBeGWJpPmB8bev0cxdg7cR5c1tTCHEe0O2fEy6v4dIPm6iiHhpCVWPIKCgOIqr64xWjxm9+g5R9zDj4fYalCLSTofhv3+CICBqbeDbbcSDXVun9MkzBEjabZLDA2yiSGOB0sYahcYy8qkwfPUNIiAIcXMJChGaZWANQbNBsr+HuQxNJxfxdL/5Ht8fEq0uUX6yTrFRJ1tZJD08vHGug9un1hSKGBH8KMOfn+EJcKcfMMaDBJRbLUSUcafL4NW3GIUM4LjNxXhI+flfiBcXGc1VodslU6HUXMarp/9mh8p2i2h+hkFlFn/Rxeq0BgQuzpDuBeNuj2Kzji3OYOerpEcH1y/eFqcBsn4fj1JcqlP5x98pfLKFxkWMEygVCKplBBgcvCNwN2Wd/OcInAdRgvp8LtxqBTM7i3EOd3BIcn6KSEBhrYn1cqWrS2NwEuCMxVgDQa4WzRxGHxCnF0h/3SeqzhIuLBBUZzHVGUpb6wx+2IHeGfn+CzoYIUYw1ySQZWPUZZggxIZFUoRofQXFkJ2eY4cD/MEHqNUIl+ukP1tU/Q37KWxuo1lKPDeHBDF4ZXzQnpL6R4CLQpAMGXz9ClOrY1cWKS41sHFI+UWL/lcnoLmrRFFMJg6DvQwbAkGNAQzeOZwYKo1FwDPu9fC1ecZpRkk9Ui4g9Rrafs+l63lRimv1CfsGnwzpvfkNvTh/GPjlOXfqkfY7RsfvSd42qP3tc0wIqXO44YigVCZcbZC83c230CgqYJeXkcCg3jM67WCWG1CICbyh0tqEJ5u5YwgYhbi5yuBDG0RRBes9x7/8ihkMCUYpafcM693kPMv9wBUwT1sU60uM3vyMdM8Jq2VA8JmHsaO395a5F88I6gsUP31OstMGByzOMbu9gVFlfNZBTjoUXr4AgaR/Rnp0kqcWUYJiifJ6k3Cxho9jzKU/O8UdnyDdHh6wk+T2uwlIrVBuriDlEtFfPwNvEDGoQn/vLZKN8Tt79Ishpc114maLYrMFmgNykpF1z+m+/hFbgEJtAY9htLtLuN/GiYLCOIyJV2tYWyBcXcR3eyAeH3oC9QiKIrmno38AuCqdr/9FcatJWJ/DhAWy0ZiLwyP8zg4Gg9WU9Ifv6b4/J95cIarMIAayZMSofUK2e4DNMuzKPO6ij0uVtH2C2qncl6Uku/vEtQa2WiI7PcV3zlEyjPe/W6eogNz9kRBksnmeDIdiEIxylemm6xoVUM2/ERRUUVG8gEMIvUckZ0+n2FMVHIqafEVVQVQxoiDwQJmCykfEmRdODhEhUIPVHITIPZFPvjGXxZVMPGEiQGPMdUmmNy3MTpxM0euCagL6/kOSzxY8uCeSs3xleKp3LEhE7gQlPMzYbUNg6v3pZ5lKTjdtW+4Hrqr8kbXvBPN/tmtPfyhYeaw/Eo8UuKCPlXHlv/dtc16vtOQ8AAAAAElFTkSuQmCC");
	private static final byte[] REST_ICON = Base64.getDecoder().decode("iVBORw0KGgoAAAANSUhEUgAAAC4AAAAYCAYAAACFms+HAAAABmJLR0QArgDOAOl0s3seAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH5wUdFA0Thzm1lwAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAARuSURBVFjD7ZfdTxtXEMV/c3dtL2CbrwRs45QgElVNmqZRVeWlf3bfq740alHTJKrShgSIDcZgggGDbbC9M33Y9UcSaCFPjcSVLO/a9+49M+fMmbvy05Mfjc9wOD7TcQ38Gvglh3/RHxoKmAx/EBARROJaFsUMTEfnGQjRvRgiiogDHKqGiBBPQATMQsw0yp+dn0PxFJFLAjeDr+89ZGI8i6CYOcyMVuuE3VqVer2GuCiQbx89xrlUvEs4CKLbO+PZ819xeBTyt8jnF0gkEqiFNI4abFXKZDMZ8rnlaB3hMENY/K08fbFCL+x8BP5c4IIRJAPGUmnUuqiCcx5BEDA7M0t5c4Py5jriCUEqHQMPRzYEJ1EWFxfvUizewkxRNXwvwezsGCKO45MjxsYCQCMWzAMMk+hJWIiId3mpRGoQBKFxdMiffz3D9xLcv/eAbHqKQq5IeXMDzBBABCrbJd6WVvu4wcA5n0J+AXBsVdYob66TSo6TzxWp7m7T6Zyys1MBlIXcEotf3MFQVlZ+IeQME6Xb64ww8V9SkcHecf326Ha77O+/I5uexXk+zkWU2gCnodaLdIYBHp7zcc7F9w5VpX16zHrpbxAHKKoKCqHqgO1ueErPulGpnCfwfyvOYfoFUw/f95iZvgEIzeYxoSnORayIwVR6mtuLXwFCt3tKpVqipz2arRMmJjIUC0vcmClwcPiO6m6ZZrsJ/aCEQRIMw3C8R93VpBItzWayPP7+B5zz8Tyfs06bN6WXmPQwSYI5BMhkJplIZzERzs6alLbXcU549eY5X955yPjYJEEQkM8vkMvNs7bxmuruFuYJWF/XGjEnURBiHynk8lIxM1SNVCIJZqyvvebk+CSyx/jBBtQP9qntbWMisQsYgtBqNXn6/AmZ7DQ3Z+aYv5nH93xuF5fYrVUws/dsYWCX2MBfLt2ARic3Gkes/P4zjcYBIrC8vEzST+HMIQN6odVuUtuvUHu3yf7hbpwoQcxDDRqNA9bfrlJ6u4YgeH4C3/PimgAxiQ2hf22Y2NU6pxuRijgFMTbKq6j0SCSTLC3eBfNjyCFI5CBBKk0QjBOkxgmSE2A+Dx98xzf3HzGZnSaVDMhMTWICnU6Xbq+HxowNQcZZN/cpndONZF4wcxwe1ant1Zi7mWd+boHqTpVm+zD2Acjnb1HIFwcF1um2ebX6knR6ChGP2akcBqgZakp5qxR114+K0L1faFcBrmIcN49QDWm1T6KObI6N8hpJ3+H5Cebm5nhTqtM4OcC5BA5BCDETTBxnnVPqh3v88eI3CvNFsplJxPNotlvs7GxSP9yLgTtElE6nTaNRxwhBegMJXWh2F71ImOmw+UpsTyZx0RlqHsjwrBElSGMGJMqcKIL2lYva0Poie5aR840XBYFiopjJpx2yIrCjbmQQ21S0aziYZ7GdgUQHMfpBRNQP1Cuj55D+J4yfqXFhXuwkV2tA57JhHwQpV1gtH/iXxC4S1UY46sf/7/O4fNKq6zega+CXHP8AKhYWfO2tuKQAAAAASUVORK5CYII=");

	private ApiExecutor executor;

	private PrintStream logStream;
	private AtsCookieJar cookieJar;

	public ApiDriverEngine(
			Channel channel,
			ActionStatus status,
			String path,
			IDriverInfo driverInfo,
			SystemDriver systemDriver,
			ApplicationProperties props, 
			ActionTestScript script,
			boolean enableLearning) {

		super(channel, driverInfo, systemDriver, props, 0, 0, enableLearning);

		getSystemDriver().setEngine(new SystemDriverEngine(channel, new DesktopWindow(), enableLearning));

		try {

			final File logsFolder = new File(Project.TARGET_FOLDER + File.separator + Project.LOGS_FOLDER);
			if(!logsFolder.exists()) {
				logsFolder.mkdirs();
			}

			final Path logFile = logsFolder.toPath().resolve(script.getTestName() + "_ws_" + System.currentTimeMillis() + ".log");
			logStream = new PrintStream(logFile.toFile());
			logStream.println("Start ATS ws channel ...");

		} catch (FileNotFoundException e1) {}

		final int maxTry = AtsSingleton.getInstance().getMaxTryWebservice();
		final int timeout = AtsSingleton.getInstance().getWebServiceTimeOut();

		if(channel.isUseCookie()) {
			cookieJar = new AtsCookieJar();
		}

		final Builder builder = createHttpBuilder(
				timeout,
				cookieJar,
				logStream,
				channel.getTopScriptPackage(),
				getClass().getClassLoader());

		if(channel.getPerformance() == ActionChannelStart.NEOLOAD) {
			channel.setNeoloadDesignApi(AtsSingleton.getInstance().getNeoloadDesignApi());
			builder.proxy(AtsSingleton.getInstance().getNeoloadProxy().getHttpProxy());
		}else {
			builder.proxy(AtsSingleton.getInstance().getProxy().getHttpProxy());
		}

		final OkHttpClient client = builder.build();

		if(applicationPath == null) {
			applicationPath = path;
		}

		final Request request = new Request.Builder().url(applicationPath).get().build();

		String wsContent = null;
		try {

			final Response response = client.newCall(request).execute();
			wsContent = CharStreams.toString(new InputStreamReader(response.body().byteStream(), StandardCharsets.UTF_8)).trim();
			response.close();

			String osName = response.header("server");
			if(osName == null || osName.isBlank()) {
				osName = response.protocol().name();
				if(osName == null || osName.isBlank()) {
					osName = "N/A";
				}
			}

			if(wsContent.endsWith("definitions>")) {
				try {
					executor = new SoapApiExecutor(logStream, cookieJar, client, timeout, maxTry, channel, wsContent, applicationPath);
					channel.setApplicationData(Channel.API, osName, ActionApi.SOAP, ((SoapApiExecutor)executor).getOperations(), SOAP_ICON);
				} catch (SAXException | IOException | ParserConfigurationException e) {
					status.setError(ActionStatus.CHANNEL_START_ERROR, e.getMessage());
				}
			}else {
				channel.setApplicationData(Channel.API, osName, ActionApi.REST, REST_ICON);
				executor = new RestApiExecutor(logStream, cookieJar, client, timeout, maxTry, channel, applicationPath);
			}

		} catch (IOException e) {
			status.setError(ActionStatus.CHANNEL_START_ERROR, "service is not responding -> " + e.getMessage());
			e.printStackTrace(logStream);
		}
	}

	@Override
	public void api(ActionStatus status, ActionApi api) {
		executor.execute(status, api);
	}

	@Override
	public String getSource() {
		return executor.getSource();
	}

	@Override
	public List<FoundElement> findElements(boolean sysComp, TestElement testObject, String tagName, String[] attributes, String[] attributesValues, Predicate<AtsBaseElement> searchPredicate, WebElement startElement) {
		return executor.findElements(channel, testObject, tagName, searchPredicate);
	}

	@Override
	public List<FoundElement> findElements(TestElement parent, ImageTemplateMatchingSimple template) {
		return null;
	}

	@Override
	public String getAttribute(ActionStatus status, FoundElement element, String attributeName, int maxTry) {
		return executor.getElementAttribute(element.getId(), attributeName);
	}

	@Override
	public CalculatedProperty[] getAttributes(FoundElement element, boolean reload) {
		return executor.getElementAttributes(element.getId());
	}

	@Override
	public CalculatedProperty[] getHtmlAttributes(FoundElement element) {
		return getAttributes(element, true);
	}

	@Override
	public void setSysProperty(String propertyName, String propertyValue) {

	}

	@Override
	public void refreshElementMapLocation() {}

	@Override
	public void setWindowToFront() {
	}

	@Override
	public void goToUrl(ActionStatus status, String url) {
	}

	@Override
	public void close() {
		logStream.println("Close ATS WebService channel");
	}

	@Override
	public List<FoundElement> findSelectOptions(TestBound dimension, TestElement element) {
		return Collections.<FoundElement>emptyList();
	}

	@Override
	public void selectOptionsItem(ActionStatus status, TestElement element, CalculatedProperty selectProperty, boolean keepSelect) {
	}

	@Override
	public void loadParents(FoundElement hoverElement) {
	}

	@Override
	public WebElement getRootElement(Channel cnl) {
		return null;
	}

	//------------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void waitAfterAction(ActionStatus status) {}

	@Override
	public void updateDimensions() {}

	@Override
	public FoundElement getElementFromPoint(Boolean syscomp, Double x, Double y) {
		return null;
	}

	@Override
	public FoundElement getElementFromRect(Boolean syscomp, Double x, Double y, Double w, Double h) {
		return null;
	}

	@Override
	public boolean switchWindow(ActionStatus status, String type, String data, boolean regexp, int tries, boolean refresh) {
		return true;
	}

	@Override
	public void closeWindow(ActionStatus status) {
	}

	@Override
	public Object executeScript(ActionStatus status, String script, Object... params) {
		return null;
	}

	@Override
	public void scroll(FoundElement element) {}

	@Override
	public void scroll(int value) {}

	@Override
	public void scroll(FoundElement element, int delta) {}

	@Override
	public void middleClick(ActionStatus status, MouseDirection position, TestElement element) {}

	@Override
	public void mouseMoveToElement(ActionStatus status, FoundElement foundElement, MouseDirection position, boolean desktopDragDrop, int offsetX, int offsetY) {}

	@Override
	public void sendTextData(ActionStatus status, TestElement element, ArrayList<SendKeyData> textActionList, int waitChar, ActionTestScript topScript) {}

	@Override
	public void clearText(ActionStatus status, TestElement testElement, MouseDirection md) {}

	@Override
	public void mouseClick(ActionStatus status, FoundElement element, MouseDirection position, int offsetX, int offsetY) {}

	@Override
	public void drag(ActionStatus status, FoundElement element, MouseDirection position, int offsetX, int offsetY, boolean offset) {}

	@Override
	public void keyDown(Keys key) {}

	@Override
	public void keyUp(Keys key) {}

	@Override
	public void drop(FoundElement element, MouseDirection md, boolean desktopDriver) {}
	
	@Override
	public void swipe(ActionStatus status, FoundElement foundElement, MouseDirection position, MouseDirection direction) {}

	@Override
	public void moveByOffset(int hDirection, int vDirection) {}

	@Override
	public void doubleClick() {}

	@Override
	public void rightClick() {}

	@Override
	public DialogBox switchToAlert() {
		return null;
	}

	@Override
	public boolean switchToDefaultContent(boolean dialog) {return true;}

	@Override
	public void switchToFrameId(String id) {}

	@Override
	public void buttonClick(ActionStatus status, String id) {}

	@Override
	public void tap(int count, FoundElement element) {}

	@Override
	public void press(int duration, ArrayList<String> paths, FoundElement element) {}

	@Override
	public void windowState(ActionStatus status, Channel channel, String state) {}

	@Override
	public Object executeJavaScript(ActionStatus status, String script, TestElement element) {
		return null;
	}

	@Override
	public Object executeJavaScript(ActionStatus status, String script, boolean returnValue) {
		return null;
	}

	@Override
	public String getTitle() {
		return "";
	}

	@Override
	public String getTextData(FoundElement e) {//TODO implement text data
		return "";
	}

	//------------------------------------------------------------------------------------------------------------------------------------
	// init http client
	//------------------------------------------------------------------------------------------------------------------------------------

	private static Builder createHttpBuilder(int timeout, AtsCookieJar cookieJar, PrintStream logStream, String packageName, ClassLoader classLoader){

		final Path certsFolderPath = Paths.get("").toAbsolutePath().resolve(Project.SRC_FOLDER).resolve(Project.ASSETS_FOLDER).resolve(Project.CERTS_FOLDER);

		File certsFolder = certsFolderPath.toFile();
		if(!certsFolder.exists()) {
			final URL certsFolderUrl = classLoader.getResource(Project.ASSETS_FOLDER + "/" + Project.CERTS_FOLDER);
			if(certsFolderUrl != null) {
				certsFolder = new File(certsFolderUrl.getPath());
			}
		}

		File pfxFile = null;

		if(certsFolder.exists()) {
			File[] pfxFiles = certsFolder.listFiles((dir, name) -> name.toLowerCase().endsWith(".pfx"));
			if(pfxFiles.length > 0) {
				pfxFile = pfxFiles[0];
				logStream.println("Using pfx file -> " + pfxFile.getAbsolutePath());
			}
		}

		final Builder builder = new Builder();
		if(cookieJar != null) {
			builder.cookieJar(cookieJar);
		}

		builder.connectTimeout(timeout, TimeUnit.SECONDS)
		.writeTimeout(timeout, TimeUnit.SECONDS)
		.readTimeout(timeout, TimeUnit.SECONDS)
		.cache(null)
		.followRedirects(true)
		.followSslRedirects(true);

		final TrustManager [] trustManager = getTrustManager ();

		try {

			final SSLSocketFactory sslSocketFactory = getSslSocketFactory(pfxFile, trustManager, logStream);

			builder.sslSocketFactory (sslSocketFactory, (X509TrustManager)trustManager [0]);
			builder.hostnameVerifier(new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			});
		} catch (NoSuchAlgorithmException | KeyManagementException | CertificateException | IOException e) {
			e.printStackTrace(logStream);
		}

		return builder;
	}

	private static SSLSocketFactory getSslSocketFactory(File pfxFile, TrustManager [] trustManager, PrintStream errorStream) throws KeyManagementException, NoSuchAlgorithmException, CertificateException, IOException {
		final SSLContext sslContext = SSLContext.getInstance ("SSL");

		KeyManager[] keyManager = null;

		if(pfxFile != null && pfxFile.exists()) {

			final String password = "";
			final KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");

			try {
				final KeyStore keyStore = KeyStore.getInstance("PKCS12");
				final InputStream keyInput = new FileInputStream(pfxFile);

				keyStore.load(keyInput, password.toCharArray());
				keyInput.close();

				keyManagerFactory.init(keyStore, password.toCharArray());

				keyManager = keyManagerFactory.getKeyManagers();

			} catch (KeyStoreException | UnrecoverableKeyException e) {
				e.printStackTrace(errorStream);
			}

		}else {
			errorStream.println("No pfx files found in the project");
		}

		sslContext.init (keyManager, trustManager, new SecureRandom ());
		return sslContext.getSocketFactory();
	}

	private static TrustManager[] getTrustManager () {
		final X509TrustManager manager = new X509TrustManager () {

			@Override
			public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			}

			@Override
			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			}

			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return new java.security.cert.X509Certificate[]{};
			}
		};

		return new TrustManager [] { manager };
	}

	@Override
	protected void setPosition(Point pt) {
	}

	@Override
	protected void setSize(Dimension dim) {
	}

	@Override
	public List<String[]> loadSelectOptions(TestElement element) {
		return Collections.<String[]>emptyList();
	}

	@Override
	public int getNumWindows() {
		return 0;
	}

	@Override
	public String getUrl() {
		return executor.getCurrentUrl();
	}

	@Override
	public Rectangle getBoundRect(TestElement testElement) {
		return null;
	}

	@Override
	public void mouseMoveToElement(FoundElement element) {
	}

	@Override
	public String getCookies() {
		return "";
	}

	@Override
	public void quit() {
	}

	@Override
	public String getHeaders(ActionStatus status) {
		return "";
	}

	@Override
	public String getSelectedText(TestElement e) {
		return ""; //NOT implemented
	}
}