package com.ats.executor.drivers.engines.browsers;

import com.ats.driver.ApplicationProperties;
import com.ats.executor.ActionStatus;
import com.ats.executor.channels.Channel;
import com.ats.executor.drivers.IDriverInfo;
import com.ats.executor.drivers.desktop.SystemDriver;
import com.ats.executor.drivers.engines.WebDriverEngine;

public class ChromiumBasedDriverEngine extends WebDriverEngine {
	public ChromiumBasedDriverEngine(
			Channel channel,
			ActionStatus status,
			String browser,
			IDriverInfo driverInfo,
			SystemDriver systemDriver,
			ApplicationProperties props,
			boolean enableLearning) {

		super(channel, driverInfo, systemDriver, props, enableLearning);
	}
}