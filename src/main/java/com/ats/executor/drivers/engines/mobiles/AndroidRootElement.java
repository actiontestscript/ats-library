package com.ats.executor.drivers.engines.mobiles;

import java.util.ArrayList;

import javax.annotation.Nonnull;

import com.ats.data.Rectangle;
import com.ats.element.AtsMobileElement;
import com.ats.element.FoundElement;
import com.ats.element.MobileTestElement;
import com.ats.element.test.TestElement;
import com.ats.executor.ActionStatus;
import com.ats.executor.SendKeyData;
import com.ats.executor.drivers.engines.MobileDriverEngine;
import com.ats.generator.objects.MouseDirection;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AndroidRootElement extends RootElement {

	final private ObjectMapper objectMapper = new ObjectMapper();

	public AndroidRootElement(MobileDriverEngine driver) {
		super(driver);
	}

	@Override
	public void refresh(@Nonnull JsonNode node) {
		try {
			this.value = objectMapper.treeToValue(node, AtsMobileElement.class);
		} catch (JsonProcessingException ignored) {}
	}

	@Override
	public void tap(ActionStatus status, FoundElement element, MouseDirection position) {
		final Rectangle rect = element.getRectangle();

		double centerY = driver.getOffsetY(rect, position);
		double centerX = driver.getOffsetX(rect, position);
		if (element.getTag().equals("@IMAGE")) {
			centerY += element.getY();
			centerX += element.getX();
		}

		driver.executeRequest(MobileDriverEngine.ELEMENT, element.getId(), MobileDriverEngine.TAP, centerX + "", centerY + "");
	}

	@Override
	public void tap(FoundElement element, int count) {
		driver.executeRequest(MobileDriverEngine.ELEMENT, element.getId(), MobileDriverEngine.TAP, String.valueOf(count));
	}

	@Override
	public void press(FoundElement element, ArrayList<String> paths, int duration) {
		driver.executeRequest(MobileDriverEngine.ELEMENT, element.getId(), MobileDriverEngine.PRESS, String.join(":", paths));
	}

	@Override
	public void swipe(MobileTestElement testElement, int hDirection, int vDirection) {
		driver.executeRequest(MobileDriverEngine.ELEMENT, testElement.getId(), MobileDriverEngine.SWIPE, testElement.getOffsetX() + "", testElement.getOffsetY() + "", hDirection + "", + vDirection + "");
	}

	@Override
	public void textInput(TestElement element, ArrayList<SendKeyData> textActionList) {
		for (SendKeyData sequence : textActionList) {
			driver.executeRequest(MobileDriverEngine.ELEMENT, element.getFoundElement().getId(), MobileDriverEngine.INPUT, sequence.getSequenceMobile());
		}
	}

	@Override
	public void clearInput(TestElement element) {
		driver.executeRequest(MobileDriverEngine.ELEMENT, element.getFoundElement().getId(), MobileDriverEngine.INPUT, SendKeyData.EMPTY_DATA);
	}

	@Override
	public Object scripting(String script, FoundElement element) {
		return driver.executeRequest(MobileDriverEngine.ELEMENT, element.getId(), MobileDriverEngine.SCRIPTING, script);
	}

	@Override
	public Object scripting(String script) {
		return scripting(script, getValue().getFoundElement());
	}

	@Override
	public MobileTestElement getCurrentElement(FoundElement element, MouseDirection position) {
		final Rectangle rect = element.getRectangle();
		return new MobileTestElement(element.getId(), (driver.getOffsetX(rect, position)), (driver.getOffsetY(rect, position)));
	}
}