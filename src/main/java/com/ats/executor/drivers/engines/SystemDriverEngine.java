/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor.drivers.engines;

import java.awt.MouseInfo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.ats.data.Dimension;
import com.ats.data.Point;
import com.ats.data.Rectangle;
import com.ats.driver.ApplicationProperties;
import com.ats.element.AtsBaseElement;
import com.ats.element.DesktopRootElement;
import com.ats.element.DialogBox;
import com.ats.element.FoundElement;
import com.ats.element.test.TestElement;
import com.ats.executor.ActionStatus;
import com.ats.executor.ActionTestScript;
import com.ats.executor.SendKeyData;
import com.ats.executor.TestBound;
import com.ats.executor.channels.Channel;
import com.ats.executor.drivers.IDriverInfo;
import com.ats.executor.drivers.desktop.DesktopResponse;
import com.ats.executor.drivers.desktop.DesktopWindow;
import com.ats.executor.drivers.desktop.SystemDriver;
import com.ats.executor.drivers.engines.desktop.DesktopAlert;
import com.ats.generator.objects.MouseDirection;
import com.ats.generator.variables.CalculatedProperty;
import com.ats.script.actions.ActionApi;
import com.ats.tools.Utils;

public class SystemDriverEngine extends DriverEngine implements IDriverEngine {

	protected final static int DEFAULT_WAIT = 100;

	protected DesktopWindow window;
	private int windowIndex = -1;

	public SystemDriverEngine(Channel channel, boolean enableLearning) {
		super(channel, enableLearning);
	}

	public SystemDriverEngine(Channel channel, DesktopWindow window, boolean enableLearning) {
		super(channel, enableLearning);
		this.window = window;
	}

	public SystemDriverEngine(Channel channel, IDriverInfo driverInfo, SystemDriver systemDriver, ApplicationProperties props, int defaultWait, boolean enableLearning) {
		super(channel, driverInfo, systemDriver, props, DEFAULT_WAIT, 0, enableLearning);
		systemDriver.setEngine(this);
	}

	public SystemDriverEngine(Channel channel, ActionStatus status, String application, IDriverInfo driverInfo, SystemDriver systemDriver, ApplicationProperties props, boolean enableLearning) {
		this(channel, driverInfo, systemDriver, props, DEFAULT_WAIT, enableLearning);
		startApplication(status, application, props);
	}

	protected void startApplication(ActionStatus status, String application, ApplicationProperties props) {
		if(application.startsWith(Channel.DESKTOP)) {

			channel.setApplicationData(Channel.DESKTOP, getSystemDriver().getOsFullName(), "", getSystemDriver().getDriverVersion(), 0L, getSystemDriver().getIcon());

			final FoundElement desktop = getSystemDriver().getRootElement(-1);
			channel.setDimensions(desktop.getTestScreenBound(), desktop.getTestScreenBound());

		} else {
			ArrayList<String> args;
			if(props.getUri() != null) {
				application = props.getUri();
				args = new ArrayList<>(Arrays.asList(application));
				if(props.getOptions() != null) {
					Arrays.asList(props.getOptions()).forEach(o -> args.add(o));
				}
			}else {
				args = new ArrayList<>(Arrays.asList(application));
				channel.getArguments().forEach(c -> args.add(c.getCalculated()));
			}

			getSystemDriver().setExtendedTimeout();

			final DesktopResponse resp = getSystemDriver().startApplication(channel.attachToExistingProcess(), args);
			if(resp.getErrorCode() == 0) {
				window = resp.getWindow();
				if(window != null) {
					channel.setApplicationData(
							Channel.DESKTOP,
							getSystemDriver().getOsName() + " (" + getSystemDriver().getOsVersion() +")", window.getAppName(), window.getAppVersion() + " (" + window.getAppBuildVersion() + ")",
							getSystemDriver().getDriverVersion(),
							window.getPid(),
							window.getHandle(),
							window.getAppIcon());
					windowIndex = 0;
					applicationPath = window.getAppPath();

					getSystemDriver().moveWindow(channel, channel.getDimension().getPoint());
					getSystemDriver().resizeWindow(channel, channel.getDimension().getSize());

					getSystemDriver().setNormalTimeout();

				}else {
					status.setError(ActionStatus.CHANNEL_START_ERROR, "no window found for this application");
				}
			}else {
				status.setError(ActionStatus.CHANNEL_START_ERROR, resp.getErrorMessage());
			}
		}
	}

	public void setWindow(DesktopWindow window) {
		this.window = window;
	}

	//---------------------------------------------------------------------------------------------------------------------
	//
	//---------------------------------------------------------------------------------------------------------------------

	@Override
	public void waitAfterAction(ActionStatus status) {
		actionWait();
	}

	@Override
	public SystemDriver getSystemDriver() {
		return (SystemDriver)driver;
	}

	@Override
	public void loadParents(FoundElement hoverElement){
		hoverElement.setParent(getSystemDriver().getTestElementParent(hoverElement.getId(), channel));
	}

	@Override
	public String getAttribute(ActionStatus status, FoundElement element, String attributeName, int maxTry) {
		return getSystemDriver().getElementAttribute(element.getId(), attributeName);
	}

	@Override
	public CalculatedProperty[] getAttributes(FoundElement element, boolean reload){
		return getAttributes(element.getId());
	}
	
	@Override
	public CalculatedProperty[] getHtmlAttributes(FoundElement element) {
		return getAttributes(element, true);
	}

	@Override
	public void setSysProperty(String propertyName, String propertyValue) {

	}

	public CalculatedProperty[] getAttributes(String elementId){
		return getSystemDriver().getElementAttributes(elementId);
	}

	@Override
	public List<String[]> loadSelectOptions(TestElement element) {
		final ArrayList<String[]> result = new ArrayList<>();
		final List<FoundElement> options = findSelectOptions(channel.getDimension(), element);

		if(options != null && options.size() > 0) {
			options.stream().forEachOrdered(e -> result.add(e.getItemAttribute()));
		}
		return result;
	}

	@Override
	public List<FoundElement> findSelectOptions(TestBound dimension, TestElement element) {
		return getSystemDriver().getListItems(dimension, element.getFoundElement().getId());
	}

	@Override
	public void selectOptionsItem(ActionStatus status, TestElement element, CalculatedProperty selectProperty, boolean keepSelect) {
		getSystemDriver().selectItem(status, element.getFoundElement().getId(), selectProperty.getName(), selectProperty.getValue().getCalculated(), selectProperty.isRegexp());
	}

	@Override
	public String getTextData(FoundElement e) {
		return getSystemDriver().getTextData(e.getId());
	}

	//---------------------------------------------------------------------------------------------------------------------
	//
	//---------------------------------------------------------------------------------------------------------------------

	@Override
	public FoundElement getElementFromPoint(Boolean syscomp, Double x, Double y){
		return getSystemDriver().getElementFromPoint(x, y);
	}

	@Override
	public FoundElement getElementFromRect(Boolean syscomp, Double x, Double y, Double w, Double h){
		return getSystemDriver().getElementFromRect(x, y, w, h);
	}

	@Override
	public List<FoundElement> findElements(boolean sysComp, TestElement testElement, String tag, String[] attributes, String[] attributesValues, Predicate<AtsBaseElement> predicate, WebElement startElement) {
		if(sysComp) {
			if(TestElement.SYSCOMP.equals(tag.toUpperCase())) {
				return new ArrayList<>(List.of(new FoundElement(window)));
			}else {
				return getSystemDriver().findElements(channel, testElement, tag, attributesValues, predicate);
			}
		}else {
			return getSystemDriver().findElements(channel, testElement, tag, attributesValues, predicate);
		}
	}

	@Override
	public void updateDimensions() {
		final DesktopWindow win = getSystemDriver().getWindowByHandle(channel.getHandle(getSystemDriver()));
		if(win != null && win.getWidth() > 0 && win.getHeight() > 0){
			
			final TestBound tb = new TestBound(
					win.getX(),
					win.getY(),
					win.getWidth(),
					win.getHeight());
			
			channel.setDimensions(tb, tb);
		}
	}

	@Override
	public void close() {
		if(getSystemDriver() != null) {
			getSystemDriver().closeWindows(channel.getProcessId(), channel.getHandle());
		}
	}

	@Override
	public boolean switchWindow(ActionStatus status, String type, String data, boolean regexp, int tries, boolean refresh) {

		int index = Utils.string2Int(data);
		
		DesktopResponse resp = getSystemDriver().switchTo(channel.getProcessId(), index, channel.getHandle());
		int maxTry = 1 + tries;
		while(resp.getErrorCode() == ActionStatus.WINDOW_NOT_FOUND && maxTry > 0) {
			channel.sleep(1000);
			resp = getSystemDriver().switchTo(channel.getProcessId(), index, channel.getHandle());
			maxTry--;
		}

		if(resp.getErrorCode() == ActionStatus.WINDOW_NOT_FOUND) {
			status.setError(ActionStatus.WINDOW_NOT_FOUND, "cannot switch to window index '" + index + "'");
		}else {
			channel.updateWinHandle(getSystemDriver(), index);
			windowIndex = index;
			status.setPassed(true);
		}
		return true;
	}

	@Override
	public void setWindowToFront() {
		channel.toFront();
		getSystemDriver().switchTo(channel.getProcessId(), windowIndex, channel.getHandle());
	}

	@Override
	public void closeWindow(ActionStatus status) {
		getSystemDriver().closeWindow(channel);
	}

	@Override
	public void middleClick(ActionStatus status, MouseDirection position, TestElement element) {
		getSystemDriver().mouseMiddleClick();
	}

	@Override
	public void doubleClick() {
		getSystemDriver().doubleClick();
	}

	@Override
	public void rightClick() {
		getSystemDriver().mouseRightClick();
	}

	@Override
	public void mouseMoveToElement(FoundElement element) {
		getSystemDriver().elementFocus(element);
	}

	@Override
	public void mouseMoveToElement(ActionStatus status, FoundElement foundElement, MouseDirection position, boolean desktopDragDrop, int offsetX, int offsetY) {
		final Rectangle rect = foundElement.getRectangle();
		getSystemDriver().mouseMove(
				getOffsetX(rect, position) + foundElement.getScreenX().intValue() - foundElement.getCenterWidth(),
				getOffsetY(rect, position) + foundElement.getScreenY().intValue() - foundElement.getCenterHeight());
	}

	@Override
	public void mouseClick(ActionStatus status, FoundElement element, MouseDirection position, int offsetX, int offsetY) {
		getSystemDriver().mouseClick();
	}

	@Override
	public void drag(ActionStatus status, FoundElement element, MouseDirection md, int offsetX, int offsetY, boolean offset) {
		getSystemDriver().drag();
	}

	@Override
	public void drop(FoundElement element, MouseDirection md, boolean desktopDriver) {
		getSystemDriver().mouseRelease();
	}
	
	@Override
	public void swipe(ActionStatus status, FoundElement element, MouseDirection position, MouseDirection direction) {
		drag(status, element, position, 0, 0, false);
		getSystemDriver().mouseRelease();
	}

	@Override
	public void keyDown(Keys key) {
		getSystemDriver().keyDown(key.getCodePoint());
	}

	@Override
	public void keyUp(Keys key) {
		getSystemDriver().keyUp(key.getCodePoint());
	}

	@Override
	public void moveByOffset(int hDirection, int vDirection) {
		final java.awt.Point pt = MouseInfo.getPointerInfo().getLocation();
		getSystemDriver().mouseMove(pt.x + hDirection, pt.y + vDirection);
	}

	@Override
	protected void setPosition(Point pt) {
		getSystemDriver().moveWindow(channel, pt);
	}

	@Override
	protected void setSize(Dimension size) {
		getSystemDriver().resizeWindow(channel, size);
	}

	@Override
	public void clearText(ActionStatus status, TestElement te, MouseDirection md) {

		final FoundElement element = te.getFoundElement();

		mouseMoveToElement(status, element, md, false, 0, 0);
		mouseClick(status, element, null, 0, 0);

		getSystemDriver().clearText(te.getWebElementId());
	}

	@Override
	public void sendTextData(ActionStatus status, TestElement element, ArrayList<SendKeyData> textActionList, int waitChar, ActionTestScript topScript) {
		for(SendKeyData sequence : textActionList) {
			if(sequence.getDownKey() != null) {
				getSystemDriver().keyDown(sequence.getDownKey().getCodePoint());
				getSystemDriver().sendKeys(sequence.getSequenceDesktop(), element.getWebElementId());
				getSystemDriver().keyUp(sequence.getDownKey().getCodePoint());
			} else {
				getSystemDriver().sendKeys(sequence.getSequenceDesktop(), element.getWebElementId());
			}
		}
	}

	@Override
	public void refreshElementMapLocation() {
		getSystemDriver().refreshElementMapLocation(channel);
	}

	//--------------------------------------------------
	//do nothing with followings methods for the moment ....
	//--------------------------------------------------

	@Override
	public String getTitle() {
		return channel.getApplication();
	}

	@Override
	public Object executeScript(ActionStatus status, String script, Object... params) {
		status.setPassed(true);
		return null;
	}

	@Override
	public void goToUrl(ActionStatus status, String url) {
		getSystemDriver().gotoUrl(status, window.getHandle(), url);
	}

	@Override
	public WebElement getRootElement(Channel cnl) {
		return new DesktopRootElement(getSystemDriver().getRootElement(cnl));
	}

	@Override
	public void scroll(FoundElement element) {}

	@Override
	public void scroll(int delta) {
		getSystemDriver().mouseWheel(delta);
	}

	@Override
	public void scroll(FoundElement element, int delta) {
		getSystemDriver().mouseWheel(delta);
	}

	@Override
	public DialogBox switchToAlert() {
		return new DesktopAlert(this, channel.getDimension());
	}

	@Override
	public boolean switchToDefaultContent(boolean dialog) {return true;}

	@Override
	public void switchToFrameId(String id) {}

	@Override
	public String getSource() {
		getSystemDriver().refreshElementMap(channel);
		return getSystemDriver().getSource();
	}

	@Override
	public String getSelectedText(TestElement e) {
		return ""; //NOT implemented
	}

	@Override
	public void api(ActionStatus status, ActionApi api) {}

	@Override
	public void buttonClick(ActionStatus status, String id) {}

	@Override
	public void tap(int count, FoundElement element) {}

	@Override
	public void press(int duration, ArrayList<String> paths, FoundElement element) {}

	@Override
	public void windowState(ActionStatus status, Channel channel, String state) {
		getSystemDriver().windowState(status, channel, state);
	}

	@Override
	public Object executeJavaScript(ActionStatus status, String script, TestElement element) {
		return getSystemDriver().executeScript(status, script, element.getFoundElement());
	}

	@Override
	public Object executeJavaScript(ActionStatus status, String script, boolean returnValue) {
		status.setPassed(true);
		return null;
	}

	public List<FoundElement> getDialogBox() {
		return getSystemDriver().getDialogBox(channel.getDimension(), null);
	}

	@Override
	public int getNumWindows() {
		return getSystemDriver().getWindowsByPid(channel.getProcessId()).size();
	}

	@Override
	public String getUrl() {
		return applicationPath;
	}

	@Override
	public Rectangle getBoundRect(TestElement testElement) {
		FoundElement elem = testElement.getFoundElement();
		if(elem != null) {
			return new Rectangle(elem.getX(), elem.getY(), elem.getWidth(), elem.getHeight());
		}
		return null;
	}

	@Override
	public String getCookies() {
		return "";
	}

	@Override
	public String getHeaders(ActionStatus status) {
		return "";
	}

	@Override
	public void quit() {
	}
}