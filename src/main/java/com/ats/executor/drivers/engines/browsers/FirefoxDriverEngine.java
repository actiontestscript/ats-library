/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor.drivers.engines.browsers;

import java.io.IOException;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.URI;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import com.ats.AtsSingleton;
import com.ats.driver.ApplicationProperties;
import com.ats.driver.AtsProxy;
import com.ats.element.FoundElement;
import com.ats.element.test.TestElement;
import com.ats.executor.ActionStatus;
import com.ats.executor.SendKeyData;
import com.ats.executor.channels.Channel;
import com.ats.executor.drivers.DriverManager;
import com.ats.executor.drivers.IDriverInfo;
import com.ats.executor.drivers.desktop.SystemDriver;
import com.ats.executor.drivers.engines.WebDriverEngine;
import com.ats.executor.drivers.engines.browsers.capabilities.FirefoxOptions;
import com.ats.generator.objects.MouseDirection;
import com.ats.tools.Utils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FirefoxDriverEngine extends WebDriverEngine {

	private OkHttpClient client;

	private final static int DEFAULT_WAIT = 250;
	private final static int DEFAULT_PROPERTY_WAIT = 250;

	public FirefoxDriverEngine(Channel channel, ActionStatus status, IDriverInfo driverInfo, SystemDriver systemDriver, ApplicationProperties props, AtsProxy proxy, boolean enableLearning) {
		super(channel, driverInfo, systemDriver, props, DEFAULT_WAIT, DEFAULT_PROPERTY_WAIT, enableLearning);

		//---------------------------------------------------------------------------------------------------
		// Add geckodriver options management by version
		//---------------------------------------------------------------------------------------------------

		if("0.17.0".equals(driverInfo.getDriverVersion())) {
			props.addExcludedOptions("moz:debuggerAddress");
		}

		//---------------------------------------------------------------------------------------------------

		final FirefoxOptions options = new FirefoxOptions(
				new BrowserArgumentsParser(
						driverInfo,
						channel.getArguments(),
						props,
						DriverManager.FIREFOX_BROWSER,
						applicationPath,
						systemDriver),
				props,
				systemDriver,
				proxy);

		Builder builder = new Builder();

		final URI proxyUri = AtsSingleton.getInstance().getProxy().getPacUri();
		if(proxyUri != null) {
			final ArrayList<Proxy> ar = new ArrayList<>(ProxySelector.getDefault().select(proxyUri));
			if(ar != null && ar.size() > 0) {
				final Proxy p = ar.get(0);
				builder = builder.proxy(new Proxy(Proxy.Type.HTTP, p.address()));
			}
		}
		
		builder = builder
				.connectTimeout(30, TimeUnit.SECONDS)
				.writeTimeout(30, TimeUnit.SECONDS)
				.readTimeout(30, TimeUnit.SECONDS)
				.cache(null);

		client = builder.build();

		launchDriver(status, options);
	}

	@Override
	protected CharSequence getSequenceData(SendKeyData seq) {
		return seq.getSequenceWeb(false);
	}

	@Override
	protected boolean switchToWindowHandle(String handle) {
		if(super.switchToWindowHandle(handle)) {
			actionWait();
			return true;
		}
		return false;
	}

	@Override
	protected void switchToFrame(WebElement we) {
		super.switchToFrame(we);
		actionWait();
	}

	@Override
	public void middleClick(ActionStatus status, MouseDirection position, TestElement element) {
		middleClickSimulation(status, position, element);
	}

	@Override
	protected void move(FoundElement element, double offsetX, double offsetY) {

		//-----------------------------------------------------------------------------------------------------
		// I don't know why, but we have to do that to make click action reliable with Firefox and geckodriver
		//-----------------------------------------------------------------------------------------------------

		element.getValue().getTagName();
		element.getValue().getRect();

		//--------------------------------------------------------------------------------------------

		final JsonArray actionList = new JsonArray();

		actionList.add(getMoveAction(getElementOrigin(element.getId()), offsetX, offsetY));
		executeRequestActions(getElementAction(actionList));
	}

	@Override
	protected void click(FoundElement element, double offsetX, double offsetY) {

		move(element, offsetX, offsetY);
		channel.sleep(30);

		final JsonObject origin = getElementOrigin(element.getId());

		final JsonArray actionList = new JsonArray();
		actionList.add(getMouseClickAction(origin, "pointerDown"));
		actionList.add(getMouseClickAction(origin, "pointerUp"));

		executeRequestActions(getElementAction(actionList));
	}

	@Override
	public void doubleClick() {
		final JsonArray actionList = new JsonArray();
		actionList.add(getMouseClickAction("pointerDown"));
		actionList.add(getMouseClickAction("pointerUp"));
		actionList.add(getMouseClickAction("pointerDown"));
		actionList.add(getMouseClickAction("pointerUp"));
		executeRequestActions(getElementAction(actionList));
	}

	@Override
	protected void loadUrl(ActionStatus status, String url) {
		final JsonObject parameters = new JsonObject();
		parameters.addProperty("url", url);
		
		channel.sleep(300);
		int maxTry = 5;
		while(maxTry > 0) {
			try {
				executeRequest(parameters, "url");
				return;
			}catch(WebDriverException e) {
				channel.sleep(300);
			}
			maxTry--;
		}
	}
	
	

	private JsonObject getMouseClickAction(JsonObject origin, String type) {
		final JsonObject action = new JsonObject();
		action.addProperty("duration", 20);
		action.addProperty("type", type);
		action.addProperty("button", 0);
		action.add("origin", origin);
		return action;
	}

	private JsonObject getMouseClickAction(String type) {
		final JsonObject action = new JsonObject();
		action.addProperty("duration", 20);
		action.addProperty("type", type);
		action.addProperty("button", 0);
		return action;
	}

	private JsonObject getMoveAction(JsonObject origin, double offsetX, double offsetY) {
		final JsonObject action = new JsonObject();
		action.addProperty("duration", 150);
		action.addProperty("x", (int)offsetX);
		action.addProperty("y", (int)offsetY);
		action.addProperty("type", "pointerMove");
		action.add("origin", origin);
		return action;
	}

	private JsonObject getElementOrigin(String elemId) {
		final JsonObject origin = new JsonObject();
		origin.addProperty("ELEMENT", elemId);
		origin.addProperty(WEB_ELEMENT_REF, elemId);
		return origin;
	}

	private JsonObject getElementAction(JsonArray actionList) {

		final JsonObject parameters = new JsonObject();
		parameters.addProperty("pointerType", "mouse");

		final JsonObject actions = new JsonObject();

		actions.addProperty("id", "default mouse");
		actions.addProperty("type", "pointer");

		actions.add("parameters", parameters);
		actions.add("actions", actionList);

		final JsonArray chainedAction = new JsonArray();
		chainedAction.add(actions);

		final JsonObject postData = new JsonObject();
		postData.add("actions", chainedAction);

		return postData;
	}

	private void executeRequestActions(JsonObject action) {
		executeRequest(action, "actions");
	}

	private void executeRequest(JsonObject action, String type) {

		final Request request = new Request.Builder()
				.url(getDriverInfo().getDriverSessionUrl().append(type).toString())
				.addHeader("Content-Type", "application/json")
				.post(RequestBody.Companion.create(action.toString(), Utils.JSON_MEDIA))
				.build();

		try {
			final Response response = client.newCall(request).execute();
			response.close();
		} catch (IOException e) {
			throw new WebDriverException("Geckodriver hangup issue after mouse move action (try to raise up 'actionWait' value in ATS properties for firefox)");
		}
	}
}
