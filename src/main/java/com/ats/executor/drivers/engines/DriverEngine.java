/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor.drivers.engines;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;

import com.ats.data.Dimension;
import com.ats.data.Point;
import com.ats.data.Rectangle;
import com.ats.driver.ApplicationProperties;
import com.ats.driver.AtsRemoteWebDriver;
import com.ats.element.FoundElement;
import com.ats.element.ParameterElement;
import com.ats.element.test.TestElement;
import com.ats.executor.ActionStatus;
import com.ats.executor.ActionTestScript;
import com.ats.executor.TestBound;
import com.ats.executor.channels.Channel;
import com.ats.executor.drivers.IDriverInfo;
import com.ats.executor.drivers.desktop.SystemDriver;
import com.ats.generator.objects.BoundData;
import com.ats.generator.objects.Cartesian;
import com.ats.generator.objects.MouseDirection;
import com.ats.generator.objects.MouseDirectionData;
import com.ats.generator.variables.CalculatedProperty;
import com.ats.generator.variables.CalculatedValue;
import com.ats.graphic.ImageTemplateMatchingSimple;

public abstract class DriverEngine {

	protected Channel channel;

	protected AtsRemoteWebDriver driver;
	private SystemDriver systemDriver;
	private IDriverInfo driverInfo;

	protected String applicationPath;

	protected int currentWindow = 0;

	protected String mainWindowHandle = null;
	protected Set<String> windowHandles = null;

	protected Set<String> blackListWindowHandles = null;

	private int actionWait = -1;
	private int propertyWait = -1;
	private boolean enableLearning = false;

	public boolean isEnableLearning() {
		return enableLearning;
	}

	public DriverEngine(Channel channel, boolean enableLearning) {
		this.channel = channel;
		this.enableLearning = enableLearning;
	}

	public DriverEngine(Channel channel, IDriverInfo driverInfo, SystemDriver systemDriver, boolean enableLearning) {
		this(channel, enableLearning);
		setDriverInfo(driverInfo);
		setSystemEngine(systemDriver);

		channel.setDriverUrl(driverInfo.getDriverHostAndPort().toString());
	}

	public DriverEngine(Channel channel, SystemDriver systemDriver, IDriverInfo driverInfo, ApplicationProperties props, boolean enableLearning) {
		this(channel, driverInfo, systemDriver, enableLearning);
	}

	public DriverEngine(Channel channel, IDriverInfo driverInfo, SystemDriver systemDriver, ApplicationProperties props, int defaultWait, int defaultCheck, boolean enableLearning){

		this(channel, driverInfo, systemDriver, enableLearning);

		actionWait = props.getWait();
		propertyWait = props.getCheck();
		applicationPath = props.getUri();

		if(actionWait == -1) {
			actionWait = defaultWait;
		}

		if(propertyWait == -1) {
			propertyWait = defaultCheck;
		}
	}
	
	public String getColName(WebElement element) {
		return null;
	}

	public FoundElement getParamElement(ParameterElement elem){
		return new FoundElement(channel, elem.getTag(), elem.getRemoteWebElement(driver));
	}

	public void started(ActionTestScript script, ActionStatus status) {
		//nothing to do by default
	}
	
	public void newWindow(ActionStatus status, WindowType type, CalculatedValue url) {
		//nothing to do by default
	}
	
	public boolean isAtsLearningEnabled() {
		return enableLearning;
	}
	
	public String getParentsDomCode(WebElement element) {
		return null;
	}

	public void updateScreenshot(TestBound dimension, boolean isRef) {
		getSystemDriver().updateVisualImage(dimension, isRef);
	}

	public void createVisualAction(Channel channel, boolean stop, String actionType, int scriptLine, String scriptName, long timeline, boolean sync) {
		getSystemDriver().createVisualAction(channel, stop, actionType, scriptLine, scriptName, timeline, sync);
	}

	public TestElement getTestElementRoot(ActionTestScript script) {
		return new TestElement(script, channel);
	}

	private void setDriverInfo(IDriverInfo value) {
		this.driverInfo = value;
	}

	public IDriverInfo getDriverInfo() {
		return driverInfo;
	}

	private void setSystemEngine(SystemDriver value) {
		this.systemDriver = value;
	}

	public SystemDriver getSystemDriver() {
		return systemDriver;
	}

	public int getCurrentWindow() {
		return currentWindow;
	}
	
	public List<String[]> getWindowsData() {
		return Collections.emptyList();
	}

	public void setDriver(AtsRemoteWebDriver driver) {
		this.driver = driver;
	}
	
	public AtsRemoteWebDriver getAtsRemoteDriver() {
		return driver;
	}

	public String getApplicationPath() {
		return applicationPath;
	}

	public void actionWait() {
		channel.sleep(actionWait);
	}

	public int getActionWait() {
		return actionWait;
	}

	public int getPropertyWait() {
		return propertyWait;
	}

	protected double getCartesianOffset(double value, MouseDirectionData direction, Cartesian cart1, Cartesian cart2, Cartesian cart3) {
		if(direction != null) {
			if(cart1.equals(direction.getName())) {				// <-- left or top
				return direction.getIntValue() + 1;				//
			}else if(cart3.equals(direction.getName())) {		// <-- right or bottom
				return value - direction.getDoubleValue() - 2;		//
			}													//
			return direction.getDoubleValue() + (value/2); 			// <-- middle or center
		}
		return value/2;
	}

	public double getOffsetX(Rectangle rect, MouseDirection position) {
		return getCartesianOffset(rect.getWidth(), position.getHorizontalPos(), Cartesian.LEFT, Cartesian.CENTER, Cartesian.RIGHT);
	}

	public double getOffsetY(Rectangle rect, MouseDirection position) {
		return getCartesianOffset(rect.getHeight(), position.getVerticalPos(), Cartesian.TOP, Cartesian.MIDDLE, Cartesian.BOTTOM);
	}

	public String setWindowBound(BoundData x, BoundData y, BoundData w, BoundData h) {

		int newX = 0;
		int newY = 0;
		int newWidth = 0;
		int newHeight = 0;

		if(w != null || h != null){

			if(w != null) {
				newWidth = w.getValue();
			}else {
				newWidth = channel.getDimension().getWidth().intValue();
			}

			if(h != null) {
				newHeight = h.getValue();
			}else {
				newHeight = channel.getDimension().getHeight().intValue();
			}

			setSize(new Dimension(newWidth, newHeight));
		}

		if(x != null || y != null){

			if(x != null) {
				newX = x.getValue();
			}else {
				newX = channel.getDimension().getX().intValue();
			}

			if(y != null) {
				newY = y.getValue();
			}else {
				newY = channel.getDimension().getY().intValue();
			}

			setPosition(new Point(newX, newY));
		}

		return newX + "," + newY + "," + newWidth + "," + newHeight;
	}

	protected boolean desktopMoveToElement(FoundElement foundElement, MouseDirection position, int offsetX, int offsetY) {
		final Rectangle rect = foundElement.getRectangle();
		getSystemDriver().mouseMove(
				getOffsetX(rect, position) + foundElement.getScreenX().intValue() + offsetX,
				getOffsetY(rect, position) + foundElement.getScreenY().intValue() + offsetY);

		return true;
	}

	abstract protected void setPosition(Point pt);
	abstract protected void setSize(Dimension dim);

	public List<FoundElement> findElements(TestElement parent, ImageTemplateMatchingSimple template) {

		channel.setWindowToFront();
		channel.refreshLocation();
		
		WebElement parentElement = null;
		TestBound tb = null;
		if(parent == null) {
			tb = channel.getSubDimension();
		}else {
			parentElement = parent.getFoundElement().getValue();
			tb = parent.getFoundElement().getTestBound();
		}
		
		final TestBound bound = tb;

		return template.findOccurrences(
				getScreenshot(parentElement, bound))
				.parallelStream().map(r -> new FoundElement(channel, bound, r)).collect(Collectors.toCollection(ArrayList::new));
	}

	public byte[] getScreenshot(WebElement element, TestBound bound) {
		return getSystemDriver().getScreenshotByte(bound.getX(), bound.getY(), bound.getWidth(), bound.getHeight());
	}

	public CalculatedProperty[] getCssAttributes(FoundElement element) {
		return new CalculatedProperty[0];
	}
	
	public CalculatedProperty[] getFunctions(FoundElement element) {
		return new CalculatedProperty[0];
	}
	
	public String getCurrentHandle() {
		return String.valueOf(channel.getHandle());
	}

	public Channel getChannel() {
		return channel;
	}

	public void setMainWindowHandle(String mainWindowHandle) {
		this.mainWindowHandle = mainWindowHandle;
	}
	public String getMainWindowHandle() {
		return mainWindowHandle;
	}
	public void setWindowHandles(Set<String> windowHandles) {
		this.windowHandles = windowHandles;
	}
	public Set<String> getWindowHandles() {
		return windowHandles;
	}

	public void setBlackListWindowHandles(){
		blackListWindowHandles = new HashSet<>(windowHandles);
		blackListWindowHandles.remove(mainWindowHandle);
	}

	public Set<String> getBlackListWindowHandles(){
		return blackListWindowHandles;
	}
}