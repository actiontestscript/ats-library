package com.ats.executor.drivers;

import java.net.URI;

import com.ats.tools.logger.ExecutionLogger;
import com.fasterxml.jackson.databind.JsonNode;

public interface IDriverInfo {
	public String getName();
	public String getId();
	public String getUuid();
	public String getDriverVersion();
	public boolean isHeadless();
	public void setHeadless(boolean headless);
	public void setUdpInfo(String info);
	public StringBuilder getDriverSessionUrl();
	public void setSessionId(String string);
	public String getSessionId();
	public String getScreenshotUrl();
	public URI getDriverServerUri();
	public URI getDriverLoopback();
	public String getApplicationPath();
	public StringBuilder getDriverHostAndPort();
	public boolean isAlive();
	public void close();
	public void quit();
	public JsonNode toJson();
	public void sendLogsInfo(ExecutionLogger logger);
}