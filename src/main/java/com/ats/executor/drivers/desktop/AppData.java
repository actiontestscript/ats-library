package com.ats.executor.drivers.desktop;

import com.ats.element.JsonUtils;
import com.ats.tools.Utils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.NullNode;

public class AppData {

	private String name;
	private String version;
	private String build;
	private String path;
	private byte[] icon;

	public AppData() {}
	public AppData(JsonNode node) { jsonDeserialize(node); }

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getBuild() {
		return build;
	}
	public void setBuild(String build) {
		this.build = build;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public byte[] getIcon() {
		if(icon == null) {
			return Utils.getWhitePixel();
		}
		return icon;
	}
	public void setIcon(byte[] icon) {
		this.icon = icon;
	}

	public void jsonDeserialize(JsonNode node) {
//		AppData result = new AppData();
		AppData defaultValue = new AppData();
		if(node instanceof NullNode || node.get("error") != null) {	return ; }
		setName(JsonUtils.getJsonValue(node, "name", defaultValue.name, String.class));
		setVersion(JsonUtils.getJsonValue(node, "version", defaultValue.version, String.class));
		setBuild(JsonUtils.getJsonValue(node, "build", defaultValue.build, String.class));
		setPath(JsonUtils.getJsonValue(node, "path", defaultValue.path, String.class));
		icon = JsonUtils.getBase64Image(node,"icon");
	}
}