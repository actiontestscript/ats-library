package com.ats.executor;

import java.lang.management.ManagementFactory;
import java.util.TimerTask;

import com.sun.management.OperatingSystemMXBean;

public class ExecutionStatistics  extends TimerTask{

	private OperatingSystemMXBean osBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);
	
	public ExecutionStatistics() {}
	
	@Override
    public void run() {
	    System.out.println("xx:" + (osBean.getCpuLoad() * 100) + ":" + osBean.getCommittedVirtualMemorySize());
    }
}