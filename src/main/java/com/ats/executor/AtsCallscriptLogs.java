package com.ats.executor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class AtsCallscriptLogs{

	private PrintStream systemOutput = null;

	private ByteArrayOutputStream baos = new ByteArrayOutputStream();
	private PrintStream ps = new PrintStream(baos);

	public AtsCallscriptLogs() {
		systemOutput = System.out;
		System.setOut(ps);
	}

	public String terminate() {

		String result = null;
		try {
			baos.flush();
			result = baos.toString();
		} catch (IOException e) {}

		System.setOut(systemOutput);

		if(result != null) {
			String[] logs = result.replace("\r", "").split("\n");
			for(int i=0; i<logs.length; i++) {
				systemOutput.print("ats-log: " + logs[i]);
			}
		}

		return result;
	}
}