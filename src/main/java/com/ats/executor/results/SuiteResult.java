/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor.results;

public class SuiteResult {
	
	public final static String PASS = "PASS";
	public final static String FAIL = "FAIL";
	
	private String name;
	
	private int testsFailed = 0;
	private int testsPassed = 0;
	private int testsFiltered = 0;

	private boolean nativeSuite = true;
	private long duration = 0;
	private long started = 0;
	
	private String status = PASS;

	public SuiteResult(String name, int testsFailed, int testsPassed, int testsFiltered, boolean nativeSuite, long started) {
		this.name = name;
		this.testsFailed = testsFailed;
		this.testsPassed = testsPassed;
		this.testsFiltered = testsFiltered;
		this.nativeSuite = nativeSuite;
		this.started = started;
		this.duration = System.currentTimeMillis() - started;
		
		if(!isPassed()) {
			this.status = FAIL;
		}
	}
		
	public boolean isPassed() {
		return testsFailed == 0;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTestsFailed() {
		return testsFailed;
	}
	public void setTestsFailed(int testsFailed) {
		this.testsFailed = testsFailed;
	}
	public int getTestsPassed() {
		return testsPassed;
	}
	public void setTestsPassed(int testsPassed) {
		this.testsPassed = testsPassed;
	}
	public int getTestsFiltered() {
		return testsFiltered;
	}
	public void setTestsFiltered(int testsFiltered) {
		this.testsFiltered = testsFiltered;
	}

	public boolean isNativeSuite() {
		return nativeSuite;
	}

	public void setNativeSuite(boolean nativeSuite) {
		this.nativeSuite = nativeSuite;
	}
	
	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public long getStarted() {
		return started;
	}

	public void setStarted(long started) {
		this.started = started;
	}
}